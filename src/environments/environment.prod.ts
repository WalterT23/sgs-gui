export const environment = {
  production: true,
  formatFecha : 'yyyy-MM-dd'
};

export const appPreferences = {
  urlBackEnd: 'http://localhost:8280/sgs-rest/',
  //urlBackEnd: 'http://192.172.18.83:8080/sgs-rest/',
  //urlBackEnd: 'http://192.172.18.75:8080/sgs-rest/',
  //urlBackEnd: 'http://192.168.1.131:8080/sgs-rest/',
  cookName: 'sgsinfo',
  cookNamePer: 'sgsInfoUs',
  toastAlertDuration: 5000,
  toastErrorDuration: 2500,
  toastDebugDuration: 1000,
  toastInfoDuration: 2500,
  toastOkDuration: 2000
}
