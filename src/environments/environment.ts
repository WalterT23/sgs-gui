// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  formatFecha : 'yyyy-MM-dd HH:mm'
};

export const appPreferences = {
  //urlBackEnd: 'http://192.168.255.120:8080/sgs-rest/',
  //urlBackEnd: 'http://192.172.18.92:8080/sgs-rest/',
 //urlBackEnd: 'http://localhost:8080/sgs-rest/',
  //urlBackEnd: 'http://192.172.18.75:8080/sgs-rest/',
  urlBackEnd: 'http://localhost:8280/sgs-rest/',
  //urlBackEnd: 'http://192.168.0.22:8380/sgs-rest/',
  cookName: 'sgsinfo',
  cookNamePer: 'sgsInfoUs',
  toastAlertDuration: 5000,
  toastErrorDuration: 2500,
  toastDebugDuration: 1000,
  toastInfoDuration: 2500,
  toastOkDuration: 2000
}