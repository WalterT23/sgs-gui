export interface CajaUserDTO {
    id?: number;
    idPuntoExpedicion: number;
    idUsuario: number;
    inicio: Date;
    fin: Date;
    usuario: String;
    ingresos: number;
    egresos: number;
    saldo: number;
    puntoExp: number;
    
}
export interface AperturaCierreDTO {
    id?: number;
    idPuntoExpedicion: number;
    idUsuarioCaja: number;
    fechaApertura: Date;
    saldoInicial: number;
    saldoCierre: number;
    fechaCierre: Date;
    usuarioCreacion: String;
    usuarioCierre: String;
    ventaTarjeta:number;
    ventaEfectivo:number;
    egresos:number;
    otrosIngresos:number;
    usuario:String;
}

export interface IngresosEgresosCajaDTO {
	id: number;
	monto: number;
	descripcion: String;
	idAperturaCierre: number;
    idTipoMovimiento: number;
    idMovimiento: number;
    fechaHora: Date;
}

export interface EntregaCajaDTO {
	id: number;
	idAperturaCierre: number;
    montoEfectivo: number;
    montoTarjeta: number;
    total: number;
    fechaCierre: Date;
}

export interface ArqueoCajaDTO {
	id: number;
	idAperturaCierre: number;
    saldoAlCierre: number;
    totalEntregado: number;
    diferencia: String;
    faltanteSobrante: number;
}
