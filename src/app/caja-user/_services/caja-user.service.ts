import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CajaUserDTO, AperturaCierreDTO, IngresosEgresosCajaDTO, EntregaCajaDTO } from '../_dto/CajaUserDTO';
import { UsuarioDTO } from '../../gestion-usuarios/_dto/UsuariosDTO';


@Injectable()
export class CajaUserService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }
  getCajaUser(): Observable<CajaUserDTO[]> {
    return this.http.get<CajaUserDTO[]>(appPreferences.urlBackEnd + 'cajaUser/', this.auth.getHeaders());
  }
  getCajeros(): Observable<UsuarioDTO[]> {
    return this.http.get<UsuarioDTO[]>(appPreferences.urlBackEnd + 'cajaUser/Cajeros', this.auth.getHeaders());
  }
  getCajaById(id: number): Observable<CajaUserDTO> {
    return this.http.get<CajaUserDTO>(appPreferences.urlBackEnd + 'cajaUser/' + id, this.auth.getHeaders());
  }

  getCajaUserByIdPtoExp(idPuntoExpedicion: number): Observable<CajaUserDTO> {
    return this.http.get<CajaUserDTO>(appPreferences.urlBackEnd + 'cajaUser/ByIdPtoExp/' + idPuntoExpedicion, this.auth.getHeaders());
  }
  createCajaUSer(datos: CajaUserDTO): Observable<CajaUserDTO> {
    return this.http.post<CajaUserDTO>(appPreferences.urlBackEnd + 'cajaUser', datos, this.auth.getHeaders());
  }
  updateCajaUSer(id: number, data: CajaUserDTO): Observable<CajaUserDTO> {
    return this.http.put<CajaUserDTO>(appPreferences.urlBackEnd + 'cajaUser/' + id, data, this.auth.getHeaders())
  }
  deleteCajaUSer(id: number): Observable<CajaUserDTO> {
    return this.http.delete<CajaUserDTO>(appPreferences.urlBackEnd + 'cajaUser/' + id, this.auth.getHeaders())
  }

  getAperturaCierre(): Observable<AperturaCierreDTO[]> {
    return this.http.get<AperturaCierreDTO[]>(appPreferences.urlBackEnd + 'aperturaCierre', this.auth.getHeaders());
  }

  getAperturaCierreById(id: number): Observable<AperturaCierreDTO> {
    return this.http.get<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/' + id, this.auth.getHeaders());
  }

  createAperturaCierre(datos: AperturaCierreDTO): Observable<AperturaCierreDTO> {
    return this.http.post<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre', datos, this.auth.getHeaders());
  }

  updateAperturaCierre(id: number, data: AperturaCierreDTO): Observable<AperturaCierreDTO> {
    return this.http.put<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/' + id, data, this.auth.getHeaders())
  }

  deleteAperturaCierre(id: number): Observable<AperturaCierreDTO> {
    return this.http.delete<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/' + id, this.auth.getHeaders())
  }

  cerrarAperturaCierre(id: number, data: AperturaCierreDTO): Observable<AperturaCierreDTO> {
    return this.http.put<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/cerrar/' + id, data, this.auth.getHeaders())
  }

  generarAperturaCierre(datos: AperturaCierreDTO): Observable<AperturaCierreDTO> {
    return this.http.post<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/generar', datos, this.auth.getHeaders());
  }

  
  registrarMovimiento(datos: IngresosEgresosCajaDTO): Observable<IngresosEgresosCajaDTO> {
    return this.http.post<IngresosEgresosCajaDTO>(appPreferences.urlBackEnd + 'ingresosEgresosCaja', datos, this.auth.getHeaders());
  }

  getAperturaCierreByIdPtoExp(idPuntoExpedicion: number): Observable<AperturaCierreDTO> {
    return this.http.get<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/ByIdPtoExp/' + idPuntoExpedicion, this.auth.getHeaders());
  }

  createEntregaCaja(datos: EntregaCajaDTO): Observable<EntregaCajaDTO> {
    return this.http.post<EntregaCajaDTO>(appPreferences.urlBackEnd + 'entregaCaja/generarEntrega', datos, this.auth.getHeaders());
  }

  getPdfEntrega(id: number): Observable<any>{
    return this.http.get<any>(appPreferences.urlBackEnd + 'entregaCaja/imprimir/'+ id, this.auth.getHeaders());
  }

  getPdfMovimiento(id: number): Observable<any>{
    return this.http.get<any>(appPreferences.urlBackEnd + 'ingresosEgresosCaja/imprimir/'+ id, this.auth.getHeaders());
  }


}
