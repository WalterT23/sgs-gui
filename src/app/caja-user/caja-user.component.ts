import { saveAs } from 'file-saver';
import { Component, OnInit } from '@angular/core';
import { CajaUserDTO, AperturaCierreDTO, IngresosEgresosCajaDTO, EntregaCajaDTO, ArqueoCajaDTO } from './_dto/CajaUserDTO';
import { CajaUserService } from './_services/caja-user.service';
import { UtilsService } from '../_utils/utils.service';
import { AuthService } from '../_services/auth.service';
import { NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';
import { UsuarioDTO } from '../gestion-usuarios/_dto/UsuariosDTO';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { InfoRefDTO, InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { generarBlobForPDF } from '../_utils/millard-formatter';


declare var $: any;
declare var M: any;

@Component({
  selector: 'app-caja-user',
  templateUrl: './caja-user.component.html',
  styleUrls: ['./caja-user.component.css']
})

export class CajaUserComponent implements OnInit {

  public datosCajaUser: CajaUserDTO;
  public datosMovimiento: IngresosEgresosCajaDTO;
  public listaCajas: CajaUserDTO[];
  public userCajas: CajaUserDTO[];
  public listaCajeros: UsuarioDTO[];
  public apertura : AperturaCierreDTO;
  public updCajas: boolean = false;
  public search: boolean = false;
  public listaTipoMovimientos : InfoRefOpcDTO[];
  public listaMovimientos : InfoRefOpcDTO[];
  public datosEntregaCaja: EntregaCajaDTO;
  public datosArqueo: ArqueoCajaDTO;
 
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  updBanco:boolean=false;
  constructor(
    private cajaSrv: CajaUserService,
    private infrefSrv: InfoRefService,
    public auth: AuthService,
    private util: UtilsService
  ) { 
    this.listaCajeros= [] ;
    this.listaCajas = [] ;
    this.userCajas= [] ;
    this.listaMovimientos = [] ;
    this.listaTipoMovimientos = [] ;
    this.datosCajaUser= {
    idPuntoExpedicion: null,
    idUsuario: null,
    inicio: null,
    fin: null,
    usuario: '',
    ingresos: null,
    egresos: null,
    saldo: null,
    puntoExp: null
    }

    this.datosMovimiento={
      id: null,
      monto: null,
      descripcion:  '',
      idAperturaCierre: null,
      idTipoMovimiento: null,
      idMovimiento: null,
      fechaHora: null
    }

    this.apertura = {
      id: null,
      idPuntoExpedicion: null,
      idUsuarioCaja:  null,
      fechaApertura: null,
      saldoInicial: null,
      saldoCierre: null,
      fechaCierre: null,
      usuarioCreacion: '',
      usuarioCierre:'',
      ventaTarjeta:null,
      ventaEfectivo:null,
      egresos:null,
      otrosIngresos:null,
      usuario:''
    }
    
    this.datosEntregaCaja={
      id: null,
      idAperturaCierre: null,
      montoEfectivo: null,
      montoTarjeta: null,
      total: null,
      fechaCierre: null
    }

    this.datosArqueo={
      id: null,
      idAperturaCierre: null,
      saldoAlCierre: null,
      totalEntregado: null,
      diferencia: '',
      faltanteSobrante: null,
    }
  }

  ngOnInit() {

    $('select').formSelect();
   
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.modal').modal({
        dismissible : false,
        onOpenEnd: function () {
          M.updateTextFields();
          $('select').formSelect();
        },
        onCloseStart: function () {
          $('.modal-close').click();
        }
      });
    }, 150 );

    this.getLstCajas();
    this.getLstCajaUsu() 
  }


  getLstCajas() {
    this.cajaSrv.getCajaUser().subscribe(
      success => {
        this.listaCajas = success;
        console.log(this.listaCajas);
      },
      err => { }
    );
  }

  getInfoCajas(idPuntoExpedicion: number) {

    $('select').formSelect();
    M.updateTextFields()
    this.cajaSrv.getCajaUserByIdPtoExp(idPuntoExpedicion).subscribe(
      success => {
        this.datosCajaUser = success;
      },
      err => { }
    );
    console.log("hola:",this.datosCajaUser);
    this.cajaSrv.getCajeros().subscribe(
      success => {
        console.log("hola:",this.listaCajeros);
        this.listaCajeros= success;
      },
      err => { }
    );
     

  }

  nuevoMovimiento (id: number){
    console.log("este id le envio:",id);
    this.cajaSrv.getCajaById(id).subscribe(
      success => {
        this.datosCajaUser= success;
        console.log("el success retorna:",this.datosCajaUser);
      
          this.cajaSrv.getAperturaCierreByIdPtoExp(this.datosCajaUser.idPuntoExpedicion).subscribe(
            success => {
              this.apertura= success;
              this.datosMovimiento.idAperturaCierre=this.apertura.id;
              console.log("llego aca y:",this.datosMovimiento.idAperturaCierre);
            },
            err => { }
          );
 
      },
      err => { }
    );
    this.getLstTipoMovimiento();
    this.getLstMovimiento();
    this.resetForm();
    $('select').formSelect();

  }

  genApertura(id: number){
    this.cajaSrv.getCajaById(id).subscribe(
      success => {
        console.log(success);
        this.datosCajaUser= success;
        this.apertura.idPuntoExpedicion= this.datosCajaUser.idPuntoExpedicion;
        this.apertura.idUsuarioCaja=this.datosCajaUser.idUsuario;
      },
      err => { }
    );
    $('select').formSelect();
    this.resetForm();
    this.getLstCajeros();
    this.getLstCajas();

  }

  gencierre(idPuntoExpedicion: number){

    this.cajaSrv.getCajaUserByIdPtoExp(idPuntoExpedicion).subscribe(
      success => {
        this.datosCajaUser = success;
      },
      err => { }
    );

    this.cajaSrv.getAperturaCierreByIdPtoExp(idPuntoExpedicion).subscribe(
      success => {
        this.apertura= success
        this.apertura.saldoCierre=this.apertura.saldoInicial+this.apertura.ventaEfectivo+this.apertura.ventaTarjeta+this.apertura.otrosIngresos-this.apertura.egresos;
      },
      err => { }
    );

    $('select').formSelect();
    this.resetForm();
    this.getLstCajeros();
    this.getLstCajas();

  }

  calcMontoCierre (entrega:EntregaCajaDTO, apertura:AperturaCierreDTO){
    console.log("calcuar monto total",entrega,"apertura", this.apertura);
    let total=entrega.montoEfectivo+entrega.montoTarjeta;
    this.datosEntregaCaja.idAperturaCierre=this.apertura.id;
    this.datosEntregaCaja.total=total;
    setTimeout(() => {  
      M.updateTextFields();  
    }, 100);
    M.updateTextFields();
   
  }

  getLstTipoMovimiento() {
    this.listaTipoMovimientos = [];

    this.infrefSrv.getInfoRefOpcByCodRef('TIPO_MOV').subscribe(
      success => {
        this.listaTipoMovimientos = success;
      },
      err => { }
    );
  }

  getLstMovimiento() {

    this.listaMovimientos = [];

    this.infrefSrv.getInfoRefOpcByCodRef('MOVCAJA').subscribe(
      success => {
        this.listaMovimientos = success;
      },
      err => { }
    );
  }

  getInfoCajaUsu(id: number){
    $('select').formSelect();
    M.updateTextFields()

    this.cajaSrv.getCajaById(id).subscribe(
      success => {
        this.datosCajaUser = success;
        setTimeout(() => {
         console.log(this.datosCajaUser)   
        }, 150);
        
      },
      err => { }
    );


    console.log("hola:",this.datosCajaUser);
    this.cajaSrv.getCajeros().subscribe(
      success => {
        console.log("hola:",this.listaCajeros);
        this.listaCajeros= success;
      },
      err => { }
    );
 
  }

  getLstCajaUsu() {
    this.cajaSrv.getCajaUser().subscribe(
      success => {
        let aux = success;
        this.userCajas = aux.filter(row => (row.idUsuario == this.auth._infoUser.id));
        console.log(this.auth._infoUser.id, "holea", this.userCajas);  
      },
      err => { }
    );
  }

  getLstCajeros() {
    this.cajaSrv.getCajeros().subscribe(
      success => {
        this.listaCajeros= success;
      },
      err => { }
    );
  }

  guardar(forma: NgForm) {
    console.log("datos ",this.datosCajaUser)
      if (this.updCajas) {
      let  cajaUser= forma.value;
      this.cajaSrv.updateCajaUSer(this.datosCajaUser.id, cajaUser).subscribe(
        success => {
          M.toast({html: 'Actualizacion realizada correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalCajaUser').modal('close');
          this.getLstCajas();
        },
        err => { 
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         }
      );
    } else {

      
    this.cajaSrv.createCajaUSer(this.datosCajaUser).subscribe(
        success => {
          M.toast({html: 'Cajero asociado correctamente al punto de expedicion',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalCajaUser').modal('close');
          this.getLstCajas();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
      );
    }
  }

  guardarApertura(forma: NgForm) {
    console.log("datos ",this.apertura)
    let campoNull: boolean = false;
    if (!campoNull) {
      
      this.cajaSrv.generarAperturaCierre(this.apertura).subscribe(
        success => {
          M.toast({html:'Apertura generada correctamente.', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalApertura').modal('close');
          this.getLstCajas();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    }
  }

  generarCierre(forma: NgForm) {
    console.log("datos ",this.datosEntregaCaja)
    let campoNull: boolean = false;
    this.cajaSrv.createEntregaCaja(this.datosEntregaCaja).subscribe(
      success => {
        M.toast({html:'Cierre generado correctamente.', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
        $('#modalCierre').modal('close');
        this.getLstCajas();
        this.cajaSrv.getPdfEntrega(success.id).subscribe(
          success => {
            let pdf = generarBlobForPDF(success.bytes);
            saveAs(pdf, success.fileName + ".pdf");
            console.log(success.type);
          },
          error => {
    
          }
        );
         //aca debo imprimir
      
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
    /*
   
    let campoNull: boolean = false;
    if (!campoNull) {
      this.cajaSrv.cerrarAperturaCierre(this.apertura.id,this.apertura).subscribe(
        success => {
          M.toast({html:'Cierre generado correctamente.', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalCierre').modal('close');
          this.getLstCajas();
          
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    }*/
  }

  guardarMovimiento(forma: NgForm) {
    console.log("datos ",this.datosMovimiento)
 
      this.cajaSrv.registrarMovimiento(this.datosMovimiento).subscribe(
        success => {

          M.toast({html:'Movimiento registrado correctamente.', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalIngresosEgresos').modal('close');
          this.getLstCajas();

          this.cajaSrv.getPdfMovimiento(success.id).subscribe(
            success => {
              let pdf = generarBlobForPDF(success.bytes);
              saveAs(pdf, success.fileName + ".pdf");
              console.log(success.type);
            },
            error => {
      
            }
          );
          
        },
        err => {
         
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         }
      );
    
  }

  resetForm() {
    this.updCajas = false;
    this.datosCajaUser= {
      idPuntoExpedicion: null,
      idUsuario: null,
      inicio: null,
      fin: null,
      usuario: '',
      ingresos: null,
      egresos: null,
      saldo: null,
      puntoExp: null,
  
    }


    this.datosMovimiento={
    id: null,
    monto: null,
    descripcion:  '',
    idAperturaCierre: null,
    idTipoMovimiento: null,
    idMovimiento: null,
    fechaHora: null,
  }

  this.apertura = {
    id: null,
    idPuntoExpedicion: null,
    idUsuarioCaja:  null,
    fechaApertura: null,
    saldoInicial: null,
    saldoCierre: null,
    fechaCierre: null,
    usuarioCreacion: '',
    usuarioCierre:'',
    ventaTarjeta:null,
    ventaEfectivo:null,
    egresos:null,
    otrosIngresos:null,
    usuario:''
  }

  this.datosEntregaCaja={
    id: null,
    idAperturaCierre: null,
    montoEfectivo: null,
    montoTarjeta: null,
    total: null,
    fechaCierre: null
  }

  this.datosArqueo={
    id: null,
    idAperturaCierre: null,
    saldoAlCierre: null,
    totalEntregado: null,
    diferencia: '',
    faltanteSobrante: null,
  }
}

changePag(pagNumber: number){
  this.p = pagNumber;
  // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
  setTimeout(() => {
    $('.tooltipped').tooltip();
  }, 100);
}
}
