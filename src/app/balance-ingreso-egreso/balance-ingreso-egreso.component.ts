import { Component, OnInit, ElementRef } from '@angular/core';
import { appPreferences } from '../../environments/environment';
import { formatearMillar, generarBlobForPDF } from '../_utils/millard-formatter';

// Servicios
import { AuthService } from '../_services/auth.service';
import { BalanceService } from './_services/balance.service';
import { saveAs } from 'file-saver';

//DTO
import { BalanceDetDTO, BalanceIngEgrDTO } from './_dto/BalanceIngEgrDTO';
import { FiltrosChips } from '../_utils/show-chips-filter/plusFile/filtros-chips';
import { StockDTO } from '../gestion-stock/_dto/StockDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-balance-ingreso-egreso',
  templateUrl: './balance-ingreso-egreso.component.html',
  styleUrls: ['./balance-ingreso-egreso.component.css']
})
export class BalanceIngresoEgresoComponent implements OnInit {

  nameVarSession: any = "reporteBalance";
  public listaDetBalance: BalanceDetDTO[];
  public balance: BalanceIngEgrDTO;
  public filtro: BalanceIngEgrDTO;
  public listaFiltros: FiltrosChips[];

  public anhos: string[] = ["2023"];
  
  regXpag: number = 10; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación

  constructor(
    private reporSrv: BalanceService,
    public auth: AuthService
  ) {
    this.listaDetBalance = [];
    this.balance = {
      periodo: null,
      totalIngreso: null,
      totalEgreso: null,
      saldo: null,
      idSucursal: null,
      meses: []
    }
    this.filtro= {
      periodo: null,
      totalIngreso: null,
      totalEgreso: null,
      saldo: null,
      idSucursal: null,
      meses: []
    }
    this.listaFiltros = [];

    let filtro = window.sessionStorage.getItem(btoa(this.nameVarSession));
    if (filtro) {
      this.filtro = JSON.parse(filtro);
    } else {
      this.limpiarFiltro();
    }
  }
  
  ngOnInit() {
    $('select').formSelect();
    setTimeout(() => {
      $('select').formSelect();
    }, 50);

    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });
    this.getlstBalances();
  }

  getlstBalances() {
    console.log('dto enviado en la llamada',this.filtro);
    
    this.reporSrv.getBalance(this.filtro).subscribe(
      success => {
        console.log(success);
        this.balance = success;
        this.listaDetBalance = success.meses;
        
        success.meses.forEach(x=>{
          this.balance.totalIngreso  = this.balance.totalIngreso  + x.ingresos;
          this.balance.totalEgreso = this.balance.totalEgreso + x.egresos;
        })

        this.filtro.totalEgreso = this.balance.totalEgreso;
        this.filtro.totalIngreso = this.balance.totalIngreso;
        this.filtro.saldo = success.saldo;
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  mostrarFrmFiltro() {
    swShowFrmFiltro('filtroReporte');
  }

  limpiarFiltro() {
    console.log('limpia el filtro')
    this.filtro = {
      periodo: null,
      totalIngreso: null,
      totalEgreso: null,
      saldo: null,
      idSucursal: null,
      meses: []
    }
  }

  mostrarChips() {
    console.log('format');
    this.listaFiltros.push({
      descripcion: 'Periodo',
      id: 'anho',
      value: this.filtro.periodo,
      bloquearCierre: true
    });
  }

  nombreMes(mes: number){
    if(mes == 1){
      return 'Enero'
    }else if(mes== 2){
      return 'Febrero'
    }else if(mes== 3){
      return 'Marzo'
    }else if(mes == 4){
      return 'Abril'
    }else if (mes == 5){
      return 'Mayo'
    }else if(mes == 6){
      return 'Junio'
    }else if(mes == 7){
      return 'Julio'
    }else if(mes == 8){
      return 'Agosto'
    }else if(mes == 9){
      return 'Setiembre'
    }else if(mes == 10){
      return 'Octubre'
    }else if(mes == 11){
      return 'Noviembre'
    }else if(mes == 12){
      return 'Diciembre'
    }
  }

  loadRegistros() {

    swShowFrmFiltro('filtroReporte', 'none');

    if (!this.filtro.periodo) {
      alert("Debe seleccionar un periodo");
      return false
    }
    console.log('periodo para la consulta', this.filtro);
    this.listaDetBalance = [];
    this.listaFiltros = [];
    this.mostrarChips();

    window.sessionStorage.setItem(btoa(this.nameVarSession), JSON.stringify(this.filtro));

    setTimeout(() => {
      let aux = parseInt(this.filtro.periodo);
      this.filtro.periodo = aux;
      this.getlstBalances();
    }, 50);
    
  }

  getReporte(){
    console.log(this.filtro);
    this.reporSrv.getReporte(this.filtro).subscribe(
      success => {
        console.log(success);
        let excel = generarBlobForPDF(success.archivo,"application/xls");
        saveAs(excel, success.nombre + success.tipo);
        console.log(success.archivo);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  removerFiltro(event:any) {}
}
