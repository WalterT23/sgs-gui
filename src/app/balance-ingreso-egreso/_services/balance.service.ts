import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BalanceIngEgrDTO } from '../_dto/BalanceIngEgrDTO';
import { FileDTO } from '../../reporte-ventas-realizadas/_dto/VentasRealizadasDTO';

@Injectable()
export class BalanceService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getBalance(fechas: BalanceIngEgrDTO): Observable<BalanceIngEgrDTO> {
    return this.http.post<BalanceIngEgrDTO>(appPreferences.urlBackEnd + 'recaudacion/reporteRecaudacion', fechas, this.auth.getHeaders());
  }

  getReporte(datos: BalanceIngEgrDTO): Observable<FileDTO> {
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'recaudacion/reporteRecaudacion/impresion', datos, this.auth.getHeaders());
  }

}
