export interface BalanceIngEgrDTO {
    periodo: any;
    totalIngreso: number;
    totalEgreso: number;
    saldo: number;
    idSucursal?: number;
    meses: BalanceDetDTO[];
}

export interface BalanceDetDTO {
    mes: number;
    ingresos: number;
    egresos: number;
    saldo: number;
}