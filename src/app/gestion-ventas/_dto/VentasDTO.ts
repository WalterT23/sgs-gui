import { MedioPagoDto } from '../../factura-venta/_dto/venta-send-dto';
import { ClienteDTO } from '../../gestion-clientes/_dto/ClienteDTO';
import { TimbradoPtoExpDTO } from '../../gestion-timbrado-punto-exp/_dto/TimbradoPtoExpDTO';
import { ProductoDTO } from '../../gestion-productos/_dto/ProductoDTO';

export interface VentasDTO {
    id?: number;
    idTimbradoPtoExp?: number;
    nroFactura: string;
	fechaCreacion: Date;
	cajero: string
	gravada10: number;
	gravada5: number;
	exenta:number;
	iva10: number;
	iva5: number;
	descuento: number;
	neto: number;
	idCliente: number;
	idAperturaCierre: number;
    idSucursal?: number;
    detalles?: VentaDetallesDTO[];
    formasPago?: MedioPagoDto[];
    cliente?: ClienteDTO;
    timbradoPtoExpedicion?: TimbradoPtoExpDTO;
    empresa?: string;
}

export interface VentaDetallesDTO {
    id: number;
	idCabVenta: number;
	idProducto: number;
	cantidad: number;
	precioUnitario: number;
	precioTotal: number;
	tipoIdentificadorFiscal: string;
	porcentajeDescuento: number;
    montoDescuento: number;
    producto?: ProductoDTO;
}