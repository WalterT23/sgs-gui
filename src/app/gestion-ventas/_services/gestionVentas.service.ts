import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { VentasDTO } from '../_dto/VentasDTO';

@Injectable()
export class GestionVentasService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getListCabeceraVenta(): Observable<VentasDTO[]> {
    return this.http.get<VentasDTO[]>(appPreferences.urlBackEnd + 'cabeceraVenta', this.auth.getHeaders());
  }

  getCabeceraVentaById(id:number): Observable<VentasDTO> {
    return this.http.get<VentasDTO>(appPreferences.urlBackEnd + 'cabeceraVenta/' + id, this.auth.getHeaders());
  }

  getCabeceraVentaByNroFt(nroFactura:string): Observable<any> {
    return this.http.get<any>(appPreferences.urlBackEnd + 'cabeceraVenta/getByFactura/' + nroFactura, this.auth.getHeaders());
  }

}
