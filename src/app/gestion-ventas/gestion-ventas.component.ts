import { Component, OnInit } from '@angular/core';
import { appPreferences } from '../../environments/environment';

// Servicios
import { AuthService } from '../_services/auth.service';
import { GestionVentasService } from './_services/gestionVentas.service';
import { ClientesService } from '../gestion-clientes/_service/clientes.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { NotaCreditoVentaService } from '../nota-credito/_services/nota-credito-venta.service';

// DTO
import { VentasDTO ,VentaDetallesDTO } from './_dto/VentasDTO';
import { ClienteDTO } from '../gestion-clientes/_dto/ClienteDTO';
import { ProductoDTO, ProductoVentaDTO } from '../gestion-productos/_dto/ProductoDTO';
import { GestionClientesComponent } from '../gestion-clientes/gestion-clientes.component';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { NCreditoVentaDTO, NCreditoVentaDetaleDTO} from '../nota-credito/_dto/NCreditoVentaDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-ventas',
  templateUrl: './gestion-ventas.component.html',
  styleUrls: ['./gestion-ventas.component.css']
})
export class GestionVentasComponent implements OnInit {

  public datosCabVenta: VentasDTO;
  public detalleCV: VentaDetallesDTO;
  public listaCV: VentasDTO[];
  public listaDetCV: VentaDetallesDTO[];

  public datosNotaCre:  NCreditoVentaDTO;
  public listaNCventa: NCreditoVentaDTO[];
  public datosNCdetalle: NCreditoVentaDetaleDTO;
  public listaNCVdetalles: NCreditoVentaDetaleDTO[];

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  regXpag2: number = 5; //variable que utiliza la paginación
  p2: number = 1; //variable que utiliza la paginación

  constructor(
    private ventaSrv: GestionVentasService,
    private ncvSrv: NotaCreditoVentaService,
    private cliSrv: ClientesService,
    private prodSrv: ProductosService,
    public auth: AuthService,
    private infoRef: InfoRefService
  ) { 
    this.listaCV = [];
    this.listaDetCV = [];
    this.listaNCventa = [];
    this.listaNCVdetalles = [];
    this.datosCabVenta = {
      id: null,
      idTimbradoPtoExp: null,
      timbradoPtoExpedicion: null,
      nroFactura: null,
      fechaCreacion: null,
      cajero: '',
      gravada10: null,
      gravada5: null,
      exenta: null,
      iva10: null,
      iva5: null,
      descuento: null,
      neto: null,
      idCliente: null,
      cliente: null,
      idAperturaCierre: null,
      idSucursal: null,
      detalles: [],
      formasPago: [],
      empresa: null
    }
    this.detalleCV= {
      id: null,
      idCabVenta: null,
      idProducto: null,
      cantidad: null,
      precioUnitario: null,
      precioTotal: null,
      tipoIdentificadorFiscal: '',
      porcentajeDescuento: null,
      montoDescuento: null,
      producto: null
    }
    this.datosNotaCre= {
      id: null,
      idCabeceraVenta: null,
      idTimbradoPtoExp: null,
      idCliente: null,
      cliente: null,
      nroNotaCredito: '',
      iva5: null,
      iva10: null,
      exentas: null,
      montoTotal: null,
      fechaCreacion: null,
      usuarioCreacion: '',
      idSucursal: null
    }
    this.datosNCdetalle = {
      id: null,
      idNotaCreditoVenta: null,
      idProducto: null,
      cantidad: null,
      iva5: null,
      iva10: null,
      total: null,
      observaciones: ''
    }
  }

  ngOnInit() {

    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.tabs').tabs();
    }, 100);

    $('.modal').modal({
      dismissible : false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });

    this.getlstVentas();
    this.getLstNotaCredito();
  }

  getlstVentas() {
    this.ventaSrv.getListCabeceraVenta().subscribe(
      success => {
        this.listaCV = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  getLstNotaCredito(){
    this.ncvSrv.getListNCventas().subscribe(
      success => {
        this.listaNCventa = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  getInfoVenta(id: number){
    this.ventaSrv.getCabeceraVentaById(id).subscribe(
      success => {
        this.datosCabVenta = success;
        this.listaDetCV = success.detalles;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  getInfoNCventa(id: number){
    this.ncvSrv.getNCventaById(id).subscribe(
      success => {
        this.datosNotaCre = success;
        this.listaNCVdetalles = success.detalles;
        console.log('infoventa',this.listaNCVdetalles);
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  

  nombreTributo(codigo: string){
    if(codigo == 'TTEXC'){
      return ('Exento')
    }else if (codigo == 'TTI10'){
      return ('IVA 10')
    } else if (codigo == 'TTIV5'){
      return ('IVA 5')
    }else {
      return ('')
    }
  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

  changePag2(pagNumber2: number){
    this.p2 = pagNumber2;
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

}
