export interface GestionPagosDTO {
    facturas:PagosPendientesDTO[];
    pago:PagoProveedoresDTO;
}




export interface PagoProveedoresDTO {
    id?: number;
    idProveedor:number;
    proveedor: String;
    fecha:Date;
    idOp?:number;
    detalles:PagoProvdetalleDTO[];
    totalPagado:number;
    idSucursal?:number;
}

export interface PagoProvdetalleDTO {
    id?: number;
    idPago?: number;
    idCabecera: number;
    monto: number;
    idMedio?: number;
    idFuente?: number;
    idCuota?:number;
    nroCheque?:String;
    fechaEmision?:Date;
    fechaPago?:Date;
    abreviaturaMedio?:string;
}


export interface PagosPendientesDTO
{
    id?: number;
    idCuotas: number;
    idCompra: number;
    idCuotaDetalle: number;
    idProveedor: number;
    montoaPagar:number;
    saldo:number;
    nroFactura:String;
    tipoFactura:String;
    fechaVencimiento: Date;
    nro?:String;
    internalSelected?:boolean;
}