import { Injectable } from '@angular/core';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import { AuthService } from '../../_services/auth.service';
import { GestionPagosDTO } from '../_dto/GestionPagosDTO';
import { Observable } from 'rxjs';
import { appPreferences } from '../../../environments/environment';

@Injectable()
export class GestionPagosService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  realizarPago(pago: GestionPagosDTO): Observable<any>  {
    return this.http.post<any>(appPreferences.urlBackEnd + 'pagoProveedores/pagarFactura', pago, this.auth.getHeaders());
  }
}
