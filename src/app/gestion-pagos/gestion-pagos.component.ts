import { Component, OnInit } from '@angular/core';
import { PagosPendientesDTO, GestionPagosDTO,PagoProveedoresDTO, PagoProvdetalleDTO} from './_dto/GestionPagosDTO';
import { PagoProveedoresService } from '../pago-proveedores/_services/pago-proveedores.service';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { BancosService } from '../bancos/services/bancos.service';
import { CuentaBancoService } from '../cuenta-banco/_services/cuenta-banco.service';
import { UtilsService } from '../_utils/utils.service';
import { AuthService } from '../_services/auth.service';
import { ProveedoresDTO } from '../gestion-proveedores/_dto/ProveedoresDTO';
import { formatearMillar } from '../_utils/millard-formatter';
import { ActivatedRoute } from '../../../node_modules/@angular/router';
import { appPreferences } from '../../environments/environment';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { ChequesDTO } from '../cheques/_dto/ChequesDTO';
import { ChequesService } from '../cheques/_services/cheques.service';
import { CuentaBancoDTO } from '../cuenta-banco/_dto/CuentaBancoDTO';
import { GestionPagosService } from './_services/gestion-pagos.service';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-pagos',
  templateUrl: './gestion-pagos.component.html',
  styleUrls: ['./gestion-pagos.component.css']
})
export class GestionPagosComponent implements OnInit {
 
  public pago : GestionPagosDTO;
  public datosPago : PagoProveedoresDTO;
  //public detallepago:PagoProvdetalleDTO;
  public listaPago: PagoProveedoresDTO[];
  public listacuotasprov: PagosPendientesDTO[];
  public listapagar:PagosPendientesDTO[];
  public listaProv : ProveedoresDTO[];
  public  listaDP: PagoProvdetalleDTO[];
  public listaMedios:InfoRefOpcDTO[];
  public medio :InfoRefOpcDTO;
  public listaCheques: ChequesDTO[];
  public listaCuentaBanco: CuentaBancoDTO[];

  public detallespago:PagoProvdetalleDTO[];
  
  aux: number;
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  creg:number;
  facturasAux: any[] = [];
  //tmpList: any[] = [];
  constructor(
    private pSrv: PagoProveedoresService,
    private pagSrv: GestionPagosService,
    private cheqSrv: ChequesService,
    private provSrv: ProveedoresService,
    private infrefSrv: InfoRefService,
    private bancoSrv: BancosService,
    private ctaBcoSrv: CuentaBancoService,
    private util: UtilsService,
    public auth: AuthService,
    private route: ActivatedRoute,

  ) {


    this.listaProv = [];
    this.listaDP = [];
    this.listapagar= [];
    this.listacuotasprov= [];
    this.pago={
      facturas:[],
      pago:null,
    }
    this.datosPago = {
      id:null,
      idProveedor:null,
      proveedor:'',
      fecha:null,
      idOp:null,
      idSucursal:null,
      totalPagado:null,
      detalles:[],

    }

    this.detallespago = []
   }

  ngOnInit() {
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 250);

    $('#modalCompraPago').modal({
      dismissible : false,
      ready: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });
    this.getPenProv();
   
    this.cargarDetallePago()
  }

  cargarDetallePago() {
    this.detallespago.push(
      {
        id:null,
        idPago:null,
        idCabecera: null,
        monto: null,
        idMedio:null,
        idFuente: null,
        idCuota:null,
        abreviaturaMedio: null
      }
    )
  }

  eliminarDetallePago(index:number){
    console.log('eliminarDetallePago index ' + index)
  }

  formatNumber(nro: number): String {
    return formatearMillar(nro);
  }


  getPenProv(): void {
    const idp = +this.route.snapshot.paramMap.get('idProveedor');
    console.log ("aca")
    this.pSrv.getListPendientesProveedor(idp).subscribe(
      success => {
        this.listacuotasprov = success;   
        console.log(success);
      },
      err => { }
    );
    this.datosPago.idProveedor=idp;
  }  

  goBack(){
    history.back()
  }

  pagar() {
    let tmpList:PagosPendientesDTO []=[];
    let total:number=0;
    let proveedor:number=0;
    this.datosPago.totalPagado=0;
    for (let i = 0; i < this.listacuotasprov.length; i++) {
      if (this.listacuotasprov[i].internalSelected) {
       console.log ("esto",this.listacuotasprov[i]);
        // esta factura está marcada!!
        //poner en una lista auxiliar para terminar el for
        tmpList.push(this.listacuotasprov[i]);
        
      }
    
    }
    setTimeout(() => {
      if (tmpList.length === 0) { // no se seleccionó ninguna factura!! }
        M.toast({html: 'Debe seleccionar por lo menos una cuota.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       // history.back()
       //$('#modalCompraPago').modal('close');
      }
      else{
        $('#modalCompraPago').modal('open');

      }
    }, 250);

    console.log(tmpList)
    //let auxiliar:PagoProvdetalleDTO --listaDP
    for (let y= 0; y< tmpList.length; y++ ){
      total=total+tmpList[y].montoaPagar;
      proveedor=tmpList[y].idProveedor;
      this.listapagar.push(tmpList[y]);
      //this.listapagar.push(tmpList[y])
      //console.log(total)
    }
    console.log(total)
    this.datosPago.totalPagado=total;
    console.log(this.datosPago.totalPagado)
    //this.datosPago.idProveedor=proveedor;
    this.getLstMedios();
    this.getLstCheques();
    this.getLstCuentaBanco();


  }

  confirmarPago()
  { 
    
    this.aux= 0; 
    
    for (let i = 0; i < this.detallespago.length; i++) {
        if(this.detallespago[i].monto!=null  ){
          
              this.aux=this.aux+ this.detallespago[i].monto;
              //console.log("llega aca",this.aux)
        }else{
               M.toast({html: 'Debe ingresar todos los datos del medio de pago.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
              // history.back()
              break; 
        }
        if(this.detallespago[i].abreviaturaMedio=="CQUE"){
            if(this.detallespago[i].idFuente ==null || this.detallespago[i].nroCheque==='' 
            || this.detallespago[i].fechaEmision==null)
              M.toast({html: 'Debe ingresar todos los datos del medio de pago.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
              // history.back()
              break; 
        }else if(this.detallespago[i].abreviaturaMedio=="CTA_CRIA"){
              if(this.detallespago[i].idFuente ==null) 
              M.toast({html: 'Debe ingresar todos los datos del medio de pago.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
              // history.back()
              break; 

        }
        console.log("mira",this.aux)

    }

    if(this.datosPago.totalPagado>this.aux){
      M.toast({html: 'El monto es menor al total a pagar',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      //history.back()
    }else if(this.datosPago.totalPagado<this.aux){
      M.toast({html: 'El monto es mayor al total a pagar',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      //history.back()
    }else{
      this.datosPago.detalles=this.detallespago;
      this.pago.pago=this.datosPago;
      this.pago.facturas=this.listapagar;
      console.log("este es",this.listapagar)
      setTimeout(() => {
        this.pagSrv.realizarPago(this.pago).subscribe(
          success => {
  
            M.toast({html: 'Pago realizado correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
                this.detallespago = [];
                $('#modalCompraPago').modal('close');
                history.back();
          },
          err => { 
            
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          
  
          }
        );
      }, 300);
      
    }
       

   
    
    
          
  }

  getLstMedios() {

    this.listaMedios = [];
   
    this.infrefSrv.getInfoRefOpcByCodRef('MEDPAGO').subscribe(
      success => {
        //filtro solo por medio de pago efectivo
        success = success.filter(x => x.id == 108)
        this.listaMedios = success;
        console.log(success)
      },
      err => { }
    );
  }

  getLstCheques() {
    this.cheqSrv.getCheques().subscribe(
      success => {
        this.listaCheques = success;
        console.log(this.listaCheques);
      },
      err => { }
    );
  }

  getLstCuentaBanco() {
    this.ctaBcoSrv.getCuentaBanco().subscribe(
      success => {
        this.listaCuentaBanco = success;
        
      },
      err => { }
    );
  }


  cancelarPago(){

    for (let i= 0; i< this.listapagar.length; i++ ){
     
      this.listapagar[i].internalSelected=false;
     
    }

    setTimeout(() => {
      this.detallespago = [];
      this.cargarDetallePago();
      $('#modalCompraPago').modal('close');
    }, 250);


  }
  

 changePag(pagNumber: number){
  this.p = pagNumber;
  // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
  setTimeout(() => {
    $('.tooltipped').tooltip();
  }, 100);
}

}
