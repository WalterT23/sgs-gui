import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BancosDTO } from '../_dto/BancosDTO';


@Injectable()
export class BancosService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }
  getBancos(): Observable<BancosDTO[]> {
    return this.http.get<BancosDTO[]>(appPreferences.urlBackEnd + 'bancos/', this.auth.getHeaders());
  }
  getBancoById(id: number): Observable<BancosDTO> {
    return this.http.get<BancosDTO>(appPreferences.urlBackEnd + 'bancos/' + id, this.auth.getHeaders());
  }
  createBanco(datos: BancosDTO): Observable<BancosDTO> {
    return this.http.post<BancosDTO>(appPreferences.urlBackEnd + 'bancos', datos, this.auth.getHeaders());
  }
  updateBanco(id: number, data: BancosDTO): Observable<BancosDTO> {
    return this.http.put<BancosDTO>(appPreferences.urlBackEnd + 'bancos/' + id, data, this.auth.getHeaders())
  }
  deleteBanco(id: number): Observable<BancosDTO> {
    return this.http.delete<BancosDTO>(appPreferences.urlBackEnd + 'bancos/' + id, this.auth.getHeaders())
  }


}
