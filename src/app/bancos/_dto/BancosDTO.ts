export interface BancosDTO {
    id?: number;
    abreviatura: string;
    nombre: string;
    idEstado: number;
}