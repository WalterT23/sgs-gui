import { Component, OnInit,} from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';


// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { BancosService} from './services/bancos.service'



//DTO
import {BancosDTO} from './_dto/BancosDTO'

import { appPreferences } from '../../environments/environment';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-bancos',
  templateUrl: './bancos.component.html',
  styleUrls: ['./bancos.component.css']
})
export class BancosComponent implements OnInit {
  public datosBanco: BancosDTO;
  public listaBancos: BancosDTO[];
  public updBanco: boolean = false;
  public search: boolean = false;
  public listaEstados: InfoRefOpcDTO[];

  constructor(
    private bancoSrv: BancosService,
    private infrefSrv: InfoRefService,
    private util: UtilsService,
    public auth: AuthService
   
    ) 
    { 
      this.listaBancos = [];
      this.listaEstados = [];
      this.datosBanco = {
  
        nombre: '',
        abreviatura: '',
        idEstado: null
  
      }

    }

  ngOnInit() {
    $('select').formSelect();
    
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);

    $('.modal').modal({
      dismissible : false,
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: function () {
        $('#btnCancelModal').click();
      }
    });

    this.getLstBancos();
  }

  getLstBancos() {
    this.bancoSrv.getBancos().subscribe(
      success => {
        this.listaBancos = success;
      },
      err => { }
    );
  }

  getInfoBanco(id: number) {

    $('select').formSelect();
    this.updBanco = true;
    this.bancoSrv.getBancoById(id).subscribe(
      success => {
        this.datosBanco = success;
      },
      err => {

      }
    );
    this.bancoSrv.getBancoById(id).subscribe(
      success => {
        this.datosBanco = success;
      },
      err => { }
    );
    this.infrefSrv.getInfoRefOpcByCodRef('Estado').subscribe(
      success => {
        this.listaEstados = success;
      },
      err => { }
    )

  }

  guardar(forma: NgForm) {
    if (forma.value.nombre == '' || forma.value.abreviatura == '')  {
      console.log(forma.value);
      M.toast({html: 'Faltan completar datos del banco.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
    } else if (this.updBanco) {
      let banco = forma.value;
      this.bancoSrv.updateBanco(this.datosBanco.id, banco).subscribe(
        success => {
          M.toast({html: 'Datos del banco actualizados correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalBancos').modal('close');
          this.getLstBancos();
        },
        err => { 
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         }
      );
    } else {
      let banco = forma.value;
      this.bancoSrv.createBanco(banco).subscribe(
        success => {
          M.toast({html: 'Banco creado correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalBancos').modal('close');
          this.getLstBancos();
        },
        err => { 
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         }
      );
    }
  } 

  EliminarBanco(id: number) {
    var seguro= confirm ("Esta seguro que quiere eliminar el Banco ?");
    if (seguro) {
      this.bancoSrv.deleteBanco(id).subscribe(
      success => {

        M.toast({html: 'Banco eliminado correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
        $('#modalBancos').modal('close');
        this.getLstBancos();
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
    );

  }
  }
  resetForm() {

    this.updBanco = false;
    this.datosBanco = {
      nombre: '',
      abreviatura: '',
      idEstado: null
    }

  }

}
