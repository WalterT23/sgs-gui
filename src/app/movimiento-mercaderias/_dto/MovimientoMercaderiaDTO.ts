export interface MovimientoMercaderiaDTO {
    id?: number;
    idTipoMovimiento: number;
    fechaMovimiento?: Date;
    tipoOperacion?: number;
    usuarioRegistro?: string;
    idSucursal?: number;
    idOperacion?: number;
    detalles?: MovMercDetDTO[];
}

export interface MovMercDetDTO {
    id?: number;
    idMovimiento?: number;
    idProducto?: number;
    producto: string;
    codProducto: string;
    cantidad: number;
}