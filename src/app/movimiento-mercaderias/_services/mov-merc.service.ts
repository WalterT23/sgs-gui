import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MovimientoMercaderiaDTO, MovMercDetDTO } from '../_dto/MovimientoMercaderiaDTO';

@Injectable()
export class MovMercService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }
    
  getMovMerc(): Observable<MovimientoMercaderiaDTO[]> {
    return this.http.get<MovimientoMercaderiaDTO[]>(appPreferences.urlBackEnd + 'movimientoMercaderia', this.auth.getHeaders());
  }

  getMovMercById(id: number): Observable<MovimientoMercaderiaDTO> {
    return this.http.get<MovimientoMercaderiaDTO>(appPreferences.urlBackEnd + 'movimientoMercaderia/' + id, this.auth.getHeaders());
  }

  createAjuste(datos: MovimientoMercaderiaDTO): Observable<MovimientoMercaderiaDTO> {
    return this.http.post<MovimientoMercaderiaDTO>(appPreferences.urlBackEnd + 'movimientoMercaderia', datos, this.auth.getHeaders());
  }

  getMovMercDet(idMov: number): Observable<MovMercDetDTO[]> {
    return this.http.get<MovMercDetDTO[]>(appPreferences.urlBackEnd + 'movMercDetalle/byIdMov/' + idMov, this.auth.getHeaders());
  }
}
