import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { MovMercService } from './_services/mov-merc.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { GestionVentasService } from '../gestion-ventas/_services/gestionVentas.service';

//DTO
import { appPreferences } from '../../environments/environment';
import { MovimientoMercaderiaDTO, MovMercDetDTO } from './_dto/MovimientoMercaderiaDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { VentasDTO } from '../gestion-ventas/_dto/VentasDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-movimiento-mercaderias',
  templateUrl: './movimiento-mercaderias.component.html',
  styleUrls: ['./movimiento-mercaderias.component.css']
})

export class MovimientoMercaderiasComponent implements OnInit {

  public datosMovMerc: MovimientoMercaderiaDTO;
  public listaMovMerc: MovimientoMercaderiaDTO[];
  public datosMovMercDet: MovMercDetDTO;
  public listaMovMercDet: MovMercDetDTO[];
  public tiposOperaciones: InfoRefOpcDTO[] = [];
  public tiposMovimientos:  InfoRefOpcDTO[] = [];
  public datosOpeVenta:  VentasDTO;
  public datosOpeCompra: VentasDTO; //se debe reemplazar por el dto de cabecera compra
  public datosOpeNCventa: VentasDTO; //se debe reemplazar por el dto de nota de credito de venta
  public datosOpeNCcompra: VentasDTO; //se debe reemplazar por el dto de nota de credito de compra
  public tipoOpe: string = '';

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación

  constructor(
    private movSrv: MovMercService,
    private infrefSrv: InfoRefService,
    private util: UtilsService,
    public auth: AuthService
  ) { 
    this.listaMovMerc= [];
    this.listaMovMercDet=[];
    this.datosMovMerc = {
      id: null,
      idTipoMovimiento: null,
      fechaMovimiento: null,
      tipoOperacion: null,
      usuarioRegistro: '',
      idSucursal: null,
      idOperacion: null,
      detalles: []
    }
    this.datosMovMercDet= {
      id: null,
      idMovimiento: null,
      idProducto: null,
      codProducto: '',
      producto: null,
      cantidad: null,
    }
  }

  ngOnInit() {
    $('select').formSelect();
    $('.tooltipped').tooltip();
    $('.modal').modal({
      dismissible : false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    setTimeout(() => {
      this.getLstMovimientos();
    }, 50);

    this.getTipoMovimiento();
    this.getTipoOperacion();
  }

  getLstMovimientos() {
    this.movSrv.getMovMerc().subscribe(
      success => {
        this.listaMovMerc = success;
        console.log ('lista moviemientos',this.listaMovMerc);
      },
      err => { }
    );
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 50);
    console.log("listado",this.listaMovMerc);
  }

  getTipoOperacion() {
    this.infrefSrv.getInfoRefOpcByCodRef('MOV_MERC').subscribe(
      success => {
        this.tiposOperaciones = success;
        console.log('tipos operaciones',success);
      },
      err => { }
    )
  }

  getTipoMovimiento() {
    this.infrefSrv.getInfoRefOpcByCodRef('TIPO_MOV_M').subscribe(
      success => {
        this.tiposMovimientos = success;
        console.log('tipos movimientos',success);
      },
      err => { }
    )
  }

  nombreOpe(idOpe: number){
    let auxLst = this.tiposOperaciones.filter(row => (row.id == idOpe));
    return (auxLst.length > 0) ? auxLst[0].descripcion : '--';
  }

  nombreMov(idMov: number){
    let auxLst = this.tiposMovimientos.filter(row => (row.id == idMov));
    return (auxLst.length > 0) ? auxLst[0].descripcion : '--';
  }
  
  getInfoMov(item: MovimientoMercaderiaDTO){
    this.tipoOpe = this.nombreMov(item.idTipoMovimiento);
    $('select').formSelect();
    this.movSrv.getMovMercDet(item.id).subscribe(
      success => {
        this.listaMovMercDet = success;
        setTimeout(() => {
          M.updateTextFields();
        }, 100);
      },
      err => {

      }
    );
    console.log('detalles del movimiento seleccionado',this.listaMovMercDet, this.tipoOpe);
  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

}
