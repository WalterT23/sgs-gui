
import { Component, OnInit } from '@angular/core';
import { ProveedoresDTO } from '../gestion-proveedores/_dto/ProveedoresDTO';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { UtilsService } from '../_utils/utils.service';
import { AuthService } from '../_services/auth.service';
import { EstadoCuenta, TransaccionesCompra, Meses } from './_dto/EstadoCuentaDTO';
import { ActivatedRoute } from '../../../node_modules/@angular/router';
import { ReporteEstadoCuentaService } from './_services/reporte-estado-cuenta.service';
import { appPreferences } from '../../environments/environment';
import { generarBlobForPDF } from '../_utils/millard-formatter';
import { saveAs } from 'file-saver';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-reporte-estado-cuenta',
  templateUrl: './reporte-estado-cuenta.component.html',
  styleUrls: ['./reporte-estado-cuenta.component.css']
})
export class ReporteEstadoCuentaComponent implements OnInit {
  
  public datosProveedor: ProveedoresDTO;
  public listaProv: ProveedoresDTO[];
  public datosCuenta: EstadoCuenta;
  public transacciones: TransaccionesCompra[];
  public listaTransacciones:TransaccionesCompra[];
  public listaMeses: Meses[];
  private meses: Meses[]=new Array();
  
  saldoaux: number;
  mesAux:any;
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  creg:number;
  facturasAux: any[] = [];
 
  constructor(
    private route: ActivatedRoute,
    private rpeSrv: ReporteEstadoCuentaService,
    private provSrv: ProveedoresService,
    private infrefSrv: InfoRefService,
    private util: UtilsService,
    public auth: AuthService
  ) {
       this.datosCuenta={
      proveedor: '',
      fechaDesde: null,
      fechaHasta:  null,
      mes: null,
      saldo: null,
      saldoAnterior: null,
      idSucursal: null,
      idProveedor: null,
      transacciones: []
    }
    this.transacciones=[{
      fecha: null,
      mes: null,
      comprobante:'',
      transaccion: '',
      debito:  null,
      credito: null,
      saldo:  null,
      idSucursal: null,
      idProveedor:  null,
    }]
  
    this.datosProveedor = {
      ruc: null,
      dv: null,
      razonSocial: '',
      direccion: '',
      fechaRegistro: null,
      fechaModificacion: null,
      idEstado: null,
      notas: null,
      nombreFantasia: '',
      contactos: []
    };
    this.listaProv = [];
    this.mesAux = null;
    }
    ngOnInit() {
      setTimeout(() => {
        $('.tooltipped').tooltip();
      }, 250);
  
      $('#modal').modal({
        dismissible : false,
        ready: function () {
          M.updateTextFields();
          $('select').formSelect();
        },
        complete: function () {
          $('#btnCancelModal').click();
        }
      });
      this.getEstadoCuenta();
  
    }
    getEstadoCuenta(): void {
      const idp = +this.route.snapshot.paramMap.get('idProveedor');
     
      console.log ("mes",this.mesAux)
      console.log ("acdsa",idp)
      let filtroTmp:EstadoCuenta = {
        idProveedor: idp,
        fechaDesde: null,
        fechaHasta: null,
        mes: this.datosCuenta.mes,
        proveedor: null
      };
      console.log ("aca filtro",filtroTmp)
      this.rpeSrv.getEstadoCuentaProveedor(filtroTmp).subscribe(
        success => {
          this.datosCuenta = success;
          this.transacciones = success.transacciones;
          this.saldoaux=success.saldoAnterior;
          let saldo: number=0;
          let tmpList : TransaccionesCompra[]=[];
          if(this.transacciones){
            for (let i = 0; i < this.transacciones.length; i++) {
              if(i<1){
                saldo= this.saldoaux +this.transacciones[i].credito - this.transacciones[i].debito;
              }else {
                saldo=saldo+this.transacciones[i].credito - this.transacciones[i].debito;
                this.transacciones[i].saldo=saldo;
              }
              tmpList.push(this.transacciones[i]);
              this.listaTransacciones=tmpList;
              }
          }else{
            this.listaTransacciones=[];
          }
          
          
        },
        err => { }
      );
      
    } 
  
    getLstProveedores() {
      this.listaProv = [];
  
      this.provSrv.getProveedores().subscribe(
        success => {
          this.listaProv = success;
          console.log(this.listaProv);
  
          setTimeout(() => {
            $('select').formSelect();
          }, 80);
  
        },
        err => {
          $('select').formSelect();
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         }
      );
    }

    getReporte(){
      this.datosCuenta.transacciones=this.listaTransacciones;
      this.rpeSrv.getReporte(this.datosCuenta).subscribe(
        success => {
          let excel = generarBlobForPDF(success.archivo,"application/xls");
          saveAs(excel, success.nombre + success.tipo);
          console.log(success.archivo);
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      );
    }
  
    goBack(){
      history.back();
      this.getLstProveedores();
    }


    changePag(pagNumber: number){
      this.p = pagNumber;
      // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
      setTimeout(() => {
        $('.tooltipped').tooltip();
      }, 100);
    } 
}