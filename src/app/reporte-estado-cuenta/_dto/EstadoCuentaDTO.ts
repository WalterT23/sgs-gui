export interface EstadoCuenta {
    proveedor: string;
    fechaDesde: any;
    fechaHasta: any;
    mes:any;
    saldo?: number;
    saldoAnterior?: number;
    idSucursal?: number;
    idProveedor?: number;
    transacciones?: TransaccionesCompra[];
}

export interface TransaccionesCompra {
    fecha: any;
    mes:any;
    comprobante: string;
    transaccion: string;
    debito: number;
    credito:number;
    saldo?: number;
    idSucursal?: number;
    idProveedor?: number;
    
}
export interface Meses {
    mes:string;
    valor: number;
    
}

export interface FileDTO {
    nombre: string;
    tipoExtension: string;
    archivo: any;
}






