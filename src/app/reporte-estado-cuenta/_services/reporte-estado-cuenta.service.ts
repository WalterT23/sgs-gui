import { Injectable } from '@angular/core';
import { EstadoCuenta } from '../_dto/EstadoCuentaDTO';
import { appPreferences } from '../../../environments/environment';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import { AuthService } from '../../_services/auth.service';
import { FileDTO } from '../../reporte-ventas-realizadas/_dto/VentasRealizadasDTO';
import { Observable } from 'rxjs';

@Injectable()
export class ReporteEstadoCuentaService {

  constructor(
    private http: HttpClient,
    private auth: AuthService

  ) { }

  getEstadoCuentaProveedor(filtros: EstadoCuenta): Observable<EstadoCuenta> {
    return this.http.post<EstadoCuenta>(appPreferences.urlBackEnd + 'proveedores/estadoCuenta', filtros, this.auth.getHeaders());
  }

  getReporte(filtros: EstadoCuenta): Observable<FileDTO> {
    let tmp = Object.assign({},filtros)
    if(filtros.fechaDesde && filtros.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'proveedores/estadoCuenta/impresion', filtros, this.auth.getHeaders());
  }
  
}
