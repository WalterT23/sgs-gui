import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { ProveedoresService } from './_service/proveedores.service';


// DTO
import { ProveedoresDTO, ProveedoresContactoDTO } from './_dto/ProveedoresDTO';
import { appPreferences } from '../../environments/environment';
import { materialize } from 'rxjs/operators';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { InfoRefService } from '../info-ref/_services/info-ref.service';

import { BusquedaGral } from '../pipes/busqueda.pipe';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-proveedores',
  templateUrl: './gestion-proveedores.component.html',
  styleUrls: ['./gestion-proveedores.component.css']
})
export class GestionProveedoresComponent implements OnInit {

  @Input() ordenCompra: boolean = false;

  public datosProveedor: ProveedoresDTO;
  public listaContactos: ProveedoresContactoDTO[];
  public listaProveedores: ProveedoresDTO[];
  public datosContacto: ProveedoresContactoDTO;
  public listaEstados: InfoRefOpcDTO[];
  public updPro: boolean = false;
  public updProCont: boolean = false;

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  search:any;
  searchText = "";

  constructor(

    private proSrv: ProveedoresService,
    private infrefSrv: InfoRefService,
    private util: UtilsService,
    public auth: AuthService

  ) {
    this.datosProveedor = {
      ruc: null,
      dv: null,
      razonSocial: '',
      direccion: '',
      fechaRegistro: null,
      fechaModificacion: null,
      idEstado: null,
      notas: null,
      nombreFantasia: '',
      contactos: []
    };
    this.datosContacto = {
      nombre: '',
      telefono: '',
      correo: '',
      idProveedor: null
    };
    this.listaContactos = [];
    this.listaProveedores = [];
    this.listaEstados = [];



  }

  ngOnInit() {
    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();  
    }, 50);

    if (!this.ordenCompra) {

      setTimeout(() => {
        $('.modal').modal({
          dismissible : false,
          onOpenEnd: function (modal, trigger) {
            M.updateTextFields();
            $('select').formSelect();

            console.log(modal, trigger);
          },
          onCloseStart: function () {
            $('#btnCancelModalProv').click();
          }
        });
      }, 60);

      this.getLstProveedores();
      
    }

    
  }

  getLstProveedores() {
    this.proSrv.getProveedores().subscribe(
      success => {
        this.listaProveedores = success;
      },
      err => {
        M.toast({html:"No se pudieron obtener los datos de los proveedores.",displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  getInfoProveedores(id: number) {

    $('select').formSelect();
    this.updPro = true;
    this.proSrv.getProveedorById(id).subscribe(
      success => {
        this.datosProveedor = success[0];
        console.log(this.datosProveedor);
      },
      err => {
        M.toast({html: 'No se pudo obtener los datos del proveedor.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
    );

  }

  getInfoContactosProveedor(id: number) {

    $('select').formSelect();
    this.proSrv.getContactosByProveedor(id).subscribe(
      success => {
        this.listaContactos = success;
      },
      err => {
        M.toast({html: 'No se pudieron obtener los contactos del proveedor.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
    );

    this.getInfoProveedores(id);
    this.infrefSrv.getInfoRefOpcByCodRef('Estado').subscribe(
      success => {
        this.listaEstados = success;
      },
      err => { }
    );

  }

  getInfoContactoById(contactos: ProveedoresContactoDTO) {
    this.updProCont = true;
    this.proSrv.getContactosById(contactos.id).subscribe(
      success => {
        this.datosContacto = success;
      },
      err => {
        M.toast({html: 'No se pudieron obtener los datos del contacto.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  getLstEstados() {

    this.listaEstados = [];

    this.infrefSrv.getInfoRefOpcByCodRef('Estado').subscribe(
      success => {
        this.listaEstados = success;
      },
      err => { }
    );
  }

  nuevoContacto() {

    this.proSrv.createProveedorContacto(this.datosContacto).subscribe(
      success => {
        M.toast({html: 'Contacto agregado exitosamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
     },
      err => {
        
        M.toast({html: 'No se pudo registrar el contacto.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
     }
    );
  }

  EliminarContacto(contacto: ProveedoresContactoDTO) {
    console.log(contacto);
    this.proSrv.deleteProveedorContacto(contacto.id).subscribe(
      success => {
        M.toast({html: 'Contacto eliminado correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
     
      //$('#modalAlertDelete').modal('close');
        this.proSrv.getContactosByProveedor(contacto.idProveedor).subscribe(
          success => {
            this.listaContactos = success;
          }
        )
      },
      err => {
        M.toast({html: 'No se puedo eliminar el Contacto.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );

  }

  resetForm() {

    this.updPro = false;
    this.datosProveedor = {
      ruc: null,
      dv: null,
      razonSocial: '',
      direccion: '',
      fechaRegistro: null,
      fechaModificacion: null,
      idEstado: null,
      notas: null,
      nombreFantasia: '',
      contactos: []
    }

    //$('#modalProveedor').modal('close');

  }

  resetFormContacto() {

    this.updProCont = false;
    this.datosContacto = {
      nombre: '',
      telefono: '',
      correo: '',
      idProveedor: null
    }
  }

  guardar(forma: NgForm) {
    if( forma.value.ruc == null || forma.value.dv == null|| forma.value.razonSocial == null|| forma.value.nombreFantasia == null)  {
      console.log(forma.value);
      M.toast({html: 'Faltan completar datos del Proveedor',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
    }else if(this.updPro) {
      let proveedor = forma.value;
      console.log("esto:",proveedor);
      this.proSrv.updateProveedor(this.datosProveedor.id, proveedor).subscribe(
        success => {
          M.toast({html: 'Datos del proveedor actualizados correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalProveedor').modal('close');
          this.getLstProveedores();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         }
      );
    } else {

      let proveedor = forma.value;
      this.proSrv.createProveedor(proveedor).subscribe(
        success => {
          M.toast({html: 'Proveedor creado correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalProveedor').modal('close');
          this.getLstProveedores();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         }
      );

    }
  }

  guardarContacto(formaContacto: NgForm) {
    if( formaContacto.value.nombre == '' || formaContacto.value.telefono == '')  {
      console.log(formaContacto.value);
      M.toast({html: 'Faltan completar datos del Contacto',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
    
    }else if (this.updProCont) {
      let contacto = formaContacto.value;
      console.log(contacto);
      this.proSrv.updateProveedorContacto(this.datosContacto.id, contacto).subscribe(
        success => {
          M.toast({html: "Datos del contacto actualizados correctamente.",displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
         $('#modalContacto').modal('close');

          this.proSrv.getContactosByProveedor(this.datosContacto.idProveedor).subscribe(
            success => {
              this.listaContactos = success;
            }
          )
        },
        err => {
          M.toast({html: "No se pudieron actualizar los datos del contacto.",displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    } else {

      this.datosContacto = {
        nombre: formaContacto.value.nombre,
        telefono: formaContacto.value.telefono,
        correo: formaContacto.value.correo,
        idProveedor: this.datosProveedor.id
      };

      console.log(this.datosContacto);

      this.proSrv.createProveedorContacto(this.datosContacto).subscribe(
        success => {
          M.toast({html: "Contacto creado correctamente.",displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalContacto').modal('close');
        },
        err => {
          M.toast({html: "No se pudo crear el contacto.",displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );

    }
  }
  
  changePag(pagNumber: number){
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

}
