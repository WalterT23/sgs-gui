export interface ProveedoresDTO {
    id?: number;
    ruc: number;
    dv?: number;
    razonSocial: string;
    direccion: string;
    fechaRegistro?: Date;
    fechaModificacion?: Date;
    idEstado: number;
    notas: string;
    nombreFantasia: string;
    contactos?: ProveedoresContactoDTO [];
}


export interface ProveedoresContactoDTO {
    id?: number;
    nombre: string;
    telefono: string;
    correo: string;
    idProveedor: number;
}
