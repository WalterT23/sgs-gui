import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ProveedoresDTO } from '../_dto/ProveedoresDTO';
import { ProveedoresContactoDTO } from '../_dto/ProveedoresDTO';

@Injectable()
export class ProveedoresService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }
  getProveedores(): Observable<ProveedoresDTO[]> {
    return this.http.get<ProveedoresDTO[]>(appPreferences.urlBackEnd + 'proveedores', this.auth.getHeaders());
  }

  getProveedorById(id: number): Observable<ProveedoresDTO> {
    return this.http.get<ProveedoresDTO>(appPreferences.urlBackEnd + 'proveedores/' + id, this.auth.getHeaders());
  }

  getContactosByProveedor(idProveedor: number): Observable<ProveedoresContactoDTO[]> {
    return this.http.get<ProveedoresContactoDTO[]>(appPreferences.urlBackEnd + 'proveedoresContacto/byProveedor/' + idProveedor, this.auth.getHeaders());
  }

  getContactosById(id: number): Observable<ProveedoresContactoDTO> {
    return this.http.get<ProveedoresContactoDTO>(appPreferences.urlBackEnd + 'proveedoresContacto/' + id, this.auth.getHeaders());
  }

  getProveedorByRuc(ruc: string): Observable<ProveedoresDTO[]>{
    return this.http.get<ProveedoresDTO[]>(appPreferences.urlBackEnd + 'proveedores/ByRuc/' + ruc, this.auth.getHeaders());
  }

  
  createProveedor(datos: ProveedoresDTO): Observable<ProveedoresDTO> {
    return this.http.post<ProveedoresDTO>(appPreferences.urlBackEnd + 'proveedores', datos, this.auth.getHeaders());
  }

  createProveedorContacto(datos: ProveedoresContactoDTO): Observable<ProveedoresContactoDTO> {
    return this.http.post<ProveedoresContactoDTO>(appPreferences.urlBackEnd + 'proveedoresContacto', datos, this.auth.getHeaders());
  }

  updateProveedor(id: number, data: ProveedoresDTO): Observable<ProveedoresDTO> {
    return this.http.put<ProveedoresDTO>(appPreferences.urlBackEnd + 'proveedores/' + id, data, this.auth.getHeaders())
  }

  updateProveedorContacto(id: number, datos: ProveedoresContactoDTO): Observable<ProveedoresContactoDTO> {
    return this.http.put<ProveedoresContactoDTO>(appPreferences.urlBackEnd + 'proveedoresContacto/' + id, datos, this.auth.getHeaders());
  }

  deleteProveedor(id: number): Observable<ProveedoresDTO> {
    return this.http.delete<ProveedoresDTO>(appPreferences.urlBackEnd + 'proveedores/' + id, this.auth.getHeaders())
  }

  deleteProveedorContacto(id: number): Observable<ProveedoresContactoDTO> {
    return this.http.delete<ProveedoresContactoDTO>(appPreferences.urlBackEnd + 'proveedoresContacto/' + id, this.auth.getHeaders());
  }

  likeProveedor(cod: string): Observable<ProveedoresDTO[]> {
    return this.http.get<ProveedoresDTO[]>( `${appPreferences.urlBackEnd}proveedores/byRuc/${cod}`, this.auth.getHeaders())
  }

}
