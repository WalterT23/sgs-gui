import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FiltrosChips } from '../_utils/show-chips-filter/plusFile/filtros-chips';
import { appPreferences } from '../../environments/environment';
import { Chart } from 'chart.js'
import { DatePipe } from '@angular/common';
import { bar_graphic } from '../_utils/chartjs-options';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';
import { ProveedoresDTO } from '../gestion-proveedores/_dto/ProveedoresDTO';
import { ComprasRealizadas, ComprasForGraphic } from './_dto/ComprasRealizadasDTO';

import { ReporteComprasRealizadasService } from './_services/reporte-compras-realizadas.service';
import { generarBlobForPDF } from '../_utils/millard-formatter';
import { saveAs } from 'file-saver';
import { FacturaCompraCompositeDto } from '../factura-compra/_dto/factura-compra-composite-dto';

declare var $: any;
declare var M: any;


@Component({
  selector: 'app-reporte-compras-realizadas',
  templateUrl: './reporte-compras-realizadas.component.html',
  styleUrls: ['./reporte-compras-realizadas.component.css']
})
export class ReporteComprasRealizadasComponent implements OnInit {
  nameVarSession: any= "reporteCompras";
  public filtro: ComprasRealizadas;
  public comprasRealizadas:ComprasRealizadas;
  public listaCC:FacturaCompraCompositeDto[];
  public listaFiltros: FiltrosChips[];
  public listaProv : ProveedoresDTO[];
  public datoProveedor: ProveedoresDTO;

  chart01: Chart; 
  @ViewChild('grafCtas') grafCtas: ElementRef;
  public colorOperacion = "#43a047";
  public datosGrap: ComprasForGraphic[] ;



  search:any;
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  regXpag2: number = 5; //variable que utiliza la paginación
  p2: number = 1; //variable que utiliza la paginación

  constructor(
    private repoSrv  : ReporteComprasRealizadasService,
    private provSrv : ProveedoresService,
  ) { 
    this.comprasRealizadas=  {
      fechaDesde: null,
      fechaHasta: null,
      total: null,
      idSucursal: null,
      idProveedor: null,
      proveedor:'',
      detalles: [],
      totalXfecha: []
    };

    this.datoProveedor={
      ruc: null,
      dv: null,
      razonSocial: '',
      direccion: '',
      fechaRegistro: null,
      fechaModificacion: null,
      idEstado: null,
      notas: null,
      nombreFantasia: '',
      contactos: []
    }
    


    let filtro = window.sessionStorage.getItem(btoa(this.nameVarSession));
    if (filtro) {
      this.filtro = JSON.parse(filtro);
      let tmpD = this.filtro.fechaDesde.split('-');
      let tmpH = this.filtro.fechaHasta.split('-');

      setTimeout(() => {
        let fechaD: any = document.getElementById('fechaDesde');
        let fechaH: any = document.getElementById('fechaHasta');
        fechaD.value = `${tmpD[2]}/${tmpD[1]}/${tmpD[0]}`;
        fechaH.value = `${tmpH[2]}/${tmpH[1]}/${tmpH[0]}`;
        this.loadRegistros();
      }, 100);
    } else {
      this.limpiarFiltro();
    }


  }

  ngOnInit() {

    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.tabs').tabs();
    }, 50);

    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });

    this.getLstCompras();
    this.getLstProveedores();
   
  }

  mostrarFrmFiltro() {
    swShowFrmFiltro('filtroCompras');
    this.filtro.fechaDesde= null;
    this.filtro.fechaHasta= null;
    this.filtro.idProveedor= null;
  }

  limpiarFiltro() {
    console.log('entra aca')
    this.filtro = {
      fechaDesde: null,
      fechaHasta: null,
      idProveedor: null
    }
  }

  mostrarChips() {
    let fechaD = changeFormatDate(this.filtro.fechaDesde);
    let fechaH = changeFormatDate(this.filtro.fechaHasta);
    
    let tmp =this.listaProv.filter(row=>row.id==this.filtro.idProveedor);
  
     if(tmp.length==1){
      this.listaFiltros.push({
        descripcion: 'Proveedor',
        id: 'prv',
        value:  tmp[0].razonSocial,
        bloquearCierre: true
      });
     }
    
    setTimeout(() => {
      let prov=this.datoProveedor.razonSocial;
      this.listaFiltros.push({
        descripcion: 'Fechas',
        id: 'fdfh',
        value:  fechaD + ' al ' + fechaH,
        bloquearCierre: true
      });

      
    }, 200 );  

  }

  loadRegistros() {

    swShowFrmFiltro('filtroCompras', 'none');

    if (!this.filtro.fechaDesde || !this.filtro.fechaHasta) {
      alert("Los campos fecha desde y fecha hasta son obligatorios.");
      return false
    }

    this.filtro.fechaDesde = ajusteZH(this.filtro.fechaDesde);
    this.filtro.fechaHasta = ajusteZH(this.filtro.fechaHasta);
    // this.filtro.idProveedor=this.filtro.idProveedor;
    console.log('fecha para la consulta', this.filtro);
    this.listaCC = [];
    this.listaFiltros = [];
    this.mostrarChips();

    window.sessionStorage.setItem(btoa(this.nameVarSession), JSON.stringify(this.filtro));

    this.getLstCompras();

  }

  getLstCompras() {
    console.log ("llega aca:")
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 200);
    console.log("este es el filtro:",this.filtro)
    this.repoSrv.getcomprasRealizadas(this.filtro).subscribe(
      success => {
        this.comprasRealizadas = success;
        this.listaCC = success.detalles;
        console.log ("trae:",success)
        setTimeout(() => {
          this.graphicCreate();
        }, 50);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );

    
  }

  getLstProveedores() {

    this.listaProv = [];

    this.provSrv.getProveedores().subscribe(
      success => {
        this.listaProv = success;
        console.log(this.listaProv);

        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      },
      err => {
        $('select').formSelect();
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  graphicCreate(){
    if (this.chart01 != null) { this.chart01.destroy() }

    this.datosGrap = this.comprasRealizadas.totalXfecha;

    let dias: any[] = []; let montos: number[]=[];
    let aux = new DatePipe("es_PY");

    this.datosGrap.forEach(element => {
      dias.push(aux.transform(element.dia, "dd/MM/yyyy"));
      montos.push(element.totalXdia);
    });

    let ctx01 = this.grafCtas.nativeElement.getContext('2d')
    this.chart01 = new Chart(ctx01,
      {
        type: 'bar',


        
        data: { 
                labels: dias,
                datasets: [
                  { label:"Total Compras",
                    backgroundColor: "#43a04778",
                    hoverBackgroundColor: "#f2ba1a",
                    data: montos,
                    
                  }
                ] 
              },
        //options: bar_graphic
      }
    )
  }


  getReporte(){
    
    this.repoSrv.getReporte(this.comprasRealizadas).subscribe(
      success => {
        let excel = generarBlobForPDF(success.archivo,"application/xls");
        saveAs(excel, success.nombre + success.tipo);
        console.log(success.archivo);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }


  changePag(pagNumber: number){
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

  removerFiltro(event:any) {

  }
}
