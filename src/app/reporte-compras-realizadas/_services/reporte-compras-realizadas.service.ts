import { Injectable } from '@angular/core';
import { ComprasRealizadas } from '../_dto/ComprasRealizadasDTO';
import { appPreferences } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { AuthService } from '../../_services/auth.service';
import { HttpClient } from '@angular/common/http';
import { FileDTO } from '../../reporte-ventas-realizadas/_dto/VentasRealizadasDTO';

@Injectable()
export class ReporteComprasRealizadasService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getcomprasRealizadas(filtros: ComprasRealizadas): Observable<ComprasRealizadas> {
    return this.http.post<ComprasRealizadas>(appPreferences.urlBackEnd + 'cabeceraCompra/reporteCompras', filtros, this.auth.getHeaders());
  }

  getReporte(filtros: ComprasRealizadas): Observable<FileDTO> {
    let tmp = Object.assign({},filtros)
    if(filtros.fechaDesde && filtros.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'cabeceraCompra/reporteCompras/impresion', filtros, this.auth.getHeaders());
  }
}
