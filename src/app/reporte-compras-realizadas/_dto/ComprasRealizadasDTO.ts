import { FacturaCompraCompositeDto } from "src/app/factura-compra/_dto/factura-compra-composite-dto";
import { FacturaCompraDTO } from "../../factura-compra/_dto/FacturaCompraDTO";


export interface ComprasRealizadas {
    fechaDesde: any;
    fechaHasta: any;
    total?: number;
    idSucursal?: number;
    idProveedor?: number;
    proveedor?:string;
    detalles?: FacturaCompraCompositeDto[];
    totalXfecha?: ComprasForGraphic[];
}

export interface ComprasForGraphic {
    dia: Date;
    totalXdia: number;
}