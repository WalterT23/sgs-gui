import { formatearMillar } from './millard-formatter';
import { Chart } from 'chart.js';
import 'chartjs-plugin-labels';

declare var $: any;

export const bar_graphic = {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: false,
                callback: function (value, index, values) {
                    return 'Gs. ' + formatearMillar(value);
                }
            }
        }],
        xAxes: [{
            beginAtZero: false,
            barValueSpacing: 20,
            barThickness : 75,
            barPercentage: 1
        }]
    },
    tooltips: {
        mode: 'index',
        axis: 'y',
        callbacks: {
            label: function (tooltipItems, data) {
                return 'Gs. ' + formatearMillar(tooltipItems.yLabel);
            }
        }
    },
    layout: {
        padding: {
            left: 20,
            right: 20,
            top: 50,
            bottom: 0
        }
    }
}

export const pie_graphic = {
    labels: {
        callbacks: {
            label: function (tooltipItems, data) {
                return 'Gs. ' + formatearMillar(tooltipItems.yLabel);
            }
        }
    },
    tooltips: {
        mode: 'index',
        axis: 'y',
        callbacks: {}
    },
    legend: {
        position: 'left',
    },
    layout: {
        padding: {
            left: 20,
            right: 20,
            top: 50,
            bottom: 0
        }
    },
    plugins: {
        labels: {
          fontColor: '#fff',
          textShadow: true,
          shadowBlur: 15,
          shadowColor: 'rgba(0,0,0,1)',
          shadowOffsetX: 0,
          shadowOffsetY: 0
        }
    }
}