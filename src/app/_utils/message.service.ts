import {Injectable} from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { SweetAlertIcon } from 'sweetalert2';
import Swal from 'sweetalert2';
/**
   * Ejemplo 1
     openVerificarIncautacion(){
    let id = "verificarIncautacion";
    let texto = "Estas seguro?";
    this.messageService
      .confirm2(texto, "Que desea verificar la incautación?", id, "Si", "No")
      .subscribe(async (result) => {
        if (result[0] == true && result[1] == id) {
          this.verificarIncautacion();
          this.cantidadTotal = this.listado[0].cantidad;
        }
      });
    }

    Ejemplo 2
     anularEscaner(item:any) {
    let id = 'anularEscaner';
    this.messageService.input2("Desea anular el escaner?",'Ingrese motivo', id,"Si", "No").subscribe(
      async result=> {
        if(result[0] == true && result[1] == id) {
          this.anularEscanerContenedor(item, result[2]);
        }
      },
    );
    }

    Ejemplo 3
     let id = 'anularEscaner';
    this.messageService.alert2('Eliminado exitosamente', this.tuvalor, "success", "Cerrar",id).subscribe(
      async result=>{
        if (result!=null && result[0] == true && result[1] == id) {
          this.listaDescripcionMercaderia();
        }
      }
   */

@Injectable()
export class MessageService {

  confirmResult = new BehaviorSubject<[boolean, string]>([false, '']);
  confirmResult2 = new BehaviorSubject<[boolean, string, string]>([false, '', '']);
  onDestroy$: Subject<void> = new Subject();
  resp$ = this.confirmResult.asObservable();
  resp2$ = this.confirmResult2.asObservable();
  idActCall:string = "";
  tipoIcon = [
    "<p style='text-align: center;' id='alertaGenericaInfoIcon'><i class='large material-icons modal-alerta-generico-info'>check</i></p>",
    "<p style='text-align: center;' id='alertaGenericaWarnIcon'><i class='large material-icons color-dna'>info_outline</i></p>",
    "<p style='text-align: center;' id='alertaGenericaErroIcon'><i class='large material-icons color-dna'>close</i></p>"
  ]

  constructor() {}

  public alert2(title:string, mensajeDetallado:string, iconParam:SweetAlertIcon, closeBtnText:string='Cerrar',id:string='xd') {
    this.resetRespVal();
    this.confirmResult.next([false,id]);
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn waves-light',
        cancelButton: 'btn waves-light',
        denyButton: 'btn waves-light'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: title,
      confirmButtonText: closeBtnText,
      html:mensajeDetallado,
      icon: iconParam
    }).then((result) => {
      if (result.isConfirmed) {
        this.confirmResult.next([true,id]);
        return;
      } else if (result.isDenied) {
        this.confirmResult.next([false,id]);
        return;
      }
    })
    return this.confirmResult;
  }

  /**
   *
   * @param title
   * @param texto
   * @param id
   * @param confirmText
   * @param cancelText
   * @param iconParam  1 info, 2 alerta, 3 error
   * @returns
   */
  public confirm2(title:string, texto:string, id:string, confirmText:string='Confirmar', cancelText:string='Cancelar', iconParam:SweetAlertIcon = 'warning'):BehaviorSubject<[boolean,string]>{
    this.resetRespVal();
    this.confirmResult.next([false,id]);
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn waves-light',
        cancelButton: 'btn waves-light',
        denyButton: 'btn waves-light'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: title,
      text: texto,
      icon: iconParam,
      showDenyButton: true,
      confirmButtonText: confirmText,
      denyButtonText: cancelText
    }).then((result) => {
      if (result.isConfirmed) {
        this.confirmResult.next([true,id]);
        return;
      } else if (result.isDenied) {
        this.confirmResult.next([false,id]);
        return;
      }
    })
    return this.confirmResult;

  }

  public input2(title:string, texto:string, id:string, confirmText:string='Confirmar', cancelText:string='Cancelar', iconParam:SweetAlertIcon = 'info'):BehaviorSubject<[boolean,string, string]>{
    this.resetRespVal2();

    this.confirmResult2.next([false,id,'']);
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn waves-light',
        cancelButton: 'btn waves-light',
        denyButton: 'btn waves-light'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: title,
      text: texto,
      icon: iconParam,
      input:'textarea',
      inputAttributes: {
        autocapitalize: 'off',
        oninput: `value = value.toString().normalize("NFD").replace(/[\\u0300-\\u036f]/g, "").replace(/\\s+/g, ' ').toUpperCase()`,
        maxlength: '150'
      },
      showDenyButton: true,
      confirmButtonText: confirmText,
      denyButtonText: cancelText,
      inputValidator: (value) => {
        if (!value || value.trim() == "") {
          return 'Este campo es obligatorio'
        } else {
          if (value.length > 150) {
            return 'La longitud del texto excede el permitido maximo 150'
          }
        }
      }
    }).then((result) => {
      if (result.isConfirmed) {
        this.confirmResult2.next([true,id, result.value]);
        return;
      } else if (result.isDenied) {
        this.confirmResult2.next([false,id, null]);
        return;
      }
    })
    return this.confirmResult2;

  }



  resetRespVal() {
    this.confirmResult = new BehaviorSubject<[boolean,string]>([false,'']);
    this.resp$ = this.confirmResult.asObservable();
  }

  resetRespVal2() {
    this.confirmResult2 = new BehaviorSubject<[boolean,string,string]>([false,'','']);
    this.resp2$ = this.confirmResult2.asObservable();
  }

}
