import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FiltrosChips } from './plusFile/filtros-chips';

@Component({
  selector: 'app-show-chips-filter',
  template: `
    <div *ngFor="let filtro of lstFiltrosChip" class="chip">
        {{ filtro.descripcion + (filtro.value ? (': ' + filtro.value) : '') }} <i *ngIf="!filtro?.bloquearCierre" (click)="removerFiltro(filtro.id)" class="icon-close" style="cursor: pointer;"></i>
    </div>
  `
})
export class ShowChipsFilterComponent implements OnInit {

  @Input('lstFiltros') lstFiltrosChip: FiltrosChips[] = [];
  @Output('removeFilter') actionRemoveFilter = new EventEmitter<string>();

  constructor() { }
  
  ngOnInit() {
  }

  removerFiltro(id: string) {

    this.actionRemoveFilter.emit(id);

  }

}
