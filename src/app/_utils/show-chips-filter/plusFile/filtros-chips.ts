export interface FiltrosChips {
    id?: string;
    descripcion: string;
    value?: string;
    bloquearCierre: boolean;
}
