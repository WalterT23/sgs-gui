//quitar todos los espacios y pone en mayuscula
export function quitarEspaciosyMinusculas(valor:any): any {
  if (valor) {
    valor = quitarAcentos(valor.replace(/\s+/g, "").toUpperCase());
  }
  return valor;
}

//quitar los saltos de linea, tabuladores y espacios de mas
export function quitarSaltosyTabuladores(valor:any, mayuscula:boolean=false): any {
  if (valor) {
    if (mayuscula) {
      valor = quitarAcentos(valor.toString().replace(/\s+/g, " ").toUpperCase());
    } else {
      valor = quitarAcentos(valor.toString().replace(/\s+/g, " "));
    }
  }
  return valor;
}

//quitar los caracteres especiales
export function quitarCaracteresEspeciales(valor:any, mayuscula:boolean=false,exepciones:string=null): any {
  if (valor) {
    if (mayuscula) {
      if (exepciones) {
        let reg = `[^\\w\\s\\${exepciones}]`;
        var regex = new RegExp(reg,'gi');
        valor = valor.toString().replace(regex, '').toUpperCase();
      } else {
        valor = valor.toString().replace(/[^\w\s]/gi, '').toUpperCase();
      }
    } else {
      if (exepciones) {
        let reg = `[^\\w\\s\\${exepciones}]`;
        var regex = new RegExp(reg,'gi');
        valor = valor.toString().replace(regex, '');
      } else {
        valor = valor.toString().replace(/[^\w\s]/gi, '');
      }

    }

  }
  return valor;
}

function concatRegexp(reg, exp) {
  let flags = reg.flags + exp.flags;
  flags = Array.from(new Set(flags.split(''))).join();
  return new RegExp(reg.source + exp.source, flags);
}

//quitar tildes
export function quitarAcentos(valor:any): any {
  if (valor) {
    valor = valor.toString().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }
  return valor;
}

//quitar todos los valores que no sean numericos incluyendo . y ,
export function soloNumeros(valor:any, convertir:boolean=false): any {
  if (valor) {
    if (convertir) {
      let xu = quitarCaracteresEspeciales(valor.toString().replace(/[a-z]*[A-Z]*/g,""));
      if (xu) {
        valor = parseInt(xu)
      } else {
        valor = xu;
      }
    } else {
      valor = quitarCaracteresEspeciales(valor.toString().replace(/[a-z]*[A-Z]*/g,""));
    }

  }
  return valor
}


export function setSessionStorage(clave: string, valor:any) {
  if (clave == null || clave.trim() == '') return;
  if (valor == null) return;
  let auxbtoa = btoa(clave);
  let auxobjectbtoa = btoa(JSON.stringify(valor));
  window.sessionStorage.removeItem(auxbtoa);
  window.sessionStorage.setItem(auxbtoa,auxobjectbtoa);
}

export function getSessionStorage(clave: string) {
  if (clave == null || clave.trim() == '') return;
  let auxbtoa = btoa(clave);
  let auxobjectbtoa = window.sessionStorage.getItem(auxbtoa);
  if (auxobjectbtoa == null) return;
  return JSON.parse(atob(auxobjectbtoa));
}

export function deleteSessionStorage(clave: string) {
  if (clave == null || clave.trim() == '') return;
  let auxbtoa = btoa(clave);
  window.sessionStorage.removeItem(auxbtoa);
}

//DEVUELVE TRUE SI DATO CORRECTO CASO CONTRARIO FALSE
export function checkAtributosForm(dato:any, idElemento:any, clave?:any):boolean {
  if (dato) {
    if (clave) {
      if (!dato.value || dato.value.toString().trim() == '' || dato.value.value == clave) {
        addClassVibracion(idElemento)
      }
      if (dato.value!= null && dato.value != clave && dato.value.toString().trim() != '') return true;
      else return false;
    } else {
      if (!dato.value || dato.value.toString().trim() == '') {
        addClassVibracion(idElemento)
        dato.setValue(clave);
      }
      if (dato.value != null && dato.value.toString().trim() != '') return true;
      else return false;
    }


  } else {
    return false;
  }

}

export function addClassVibracion(id:any) {
  if (id) {
    let elem = document.getElementById(id)
    if (elem) {
      elem.classList.add('alerta-vibracion');
      setTimeout(() => {
        elem.classList.remove('alerta-vibracion');
      }, 500);
    }
  }
}

/**
 * CLAVE DESDE SETEAMOS 00:00:00
 * CLAVE HASTA SETEAMOS 23:59:59
 */
export function traerFechaString(valor:string, clave:string): string {
  if (!valor || valor.trim() == "") return valor;
  if (!clave || clave.trim() == "") return valor;
  if (clave == "DESDE") {
    return `${valor} 00:00:00`;
  } else if (clave == "HASTA") {
    return `${valor} 23:59:59`;
  }
}
