/**
 * Dado una entrada Number 10000, retorna string "10.000"
 * recibe solo nros enteros
 * @param n numero a formatear (Number)
 * @returns string
 */
export function formatearMillar(n: number): string {
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

export function formatFecha(fecha: string): string {

    let nf = fecha.split('/');
    return nf[2] + "-" + nf[1] + "-" + nf[0];

}

export function newFormatFecha (fecha: Date): string {
    
    let month = (parseInt(fecha.getMonth().toString()) + 1).toString();

    let newF = fecha.getDate()+'-'+ ((month.length == 1) ? ('0'+month) : month) +'-'+fecha.getFullYear();

    return newF;
}

/**
 * 
 * Genera un objeto blob desde la secuencia de
 * string pasada como parámetro
 * 
 * @param data 
 * @param filename
 */
export function generarBlobForPDF(data: string, mimeType: string = 'application/pdf') {

    let x: string = atob(data);
    let buffer = new ArrayBuffer(x.length);
    let view = new Uint8Array(buffer);

    for (var n = 0; n < x.length; n++) {
        view[n] = x.charCodeAt(n);
    }

    return new Blob([buffer], { type: mimeType });
}


