import { Component, OnInit } from '@angular/core';


// servicios
import { AuthService } from '../_services/auth.service';

declare var $: any;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  elementoActivo: any;
  constructor(public auth: AuthService) { }

  ngOnInit() {
    setTimeout(() => {

      $('select').formSelect();
      
      $('.sidenav').sidenav({
        draggable: true
      });

      $('.dropdown-trigger').dropdown({
        coverTrigger: false
      });

      $('.dropdown-trigger1').dropdown({
        hover: true
      });


      $(".collapsible").collapsible();



    }, 100);
    
        
  }

  logout() {
    this.auth.logout();
  }

  guardarSucursalSeleccionada() {
    window.sessionStorage.setItem('sucursalSeleccionada', this.auth._sucursal);
  }

  efectAction(id:any) {
    if (id) {
      let elem = document.getElementById(id);
      if (elem) {
        if (this.elementoActivo) {
          this.removeCssActivo();
        }
        this.elementoActivo = elem;
        this.elementoActivo.classList.add('tabactivo');
      }
    }
}

removeCssActivo() {
  if (this.elementoActivo) {
    this.elementoActivo.classList.remove('tabactivo');
  }
}

}
