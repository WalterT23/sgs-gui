import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';


import { SucursalesDTO } from '../_dto/SucursalesDTO';
import { Observable } from 'rxjs';

@Injectable()
export class SucursalesService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getSucursales(): Observable<SucursalesDTO[]> {
    return this.http.get<SucursalesDTO[]>(appPreferences.urlBackEnd + 'sucursal', this.auth.getHeaders());
  }

  getSucursalById(id: number): Observable<SucursalesDTO> {
    return this.http.get<SucursalesDTO>(appPreferences.urlBackEnd + 'sucursal/' + id, this.auth.getHeaders());
  }

  sucDispByUsuario(idUsuario: number): Observable<SucursalesDTO[]> {
    return this.http.get<SucursalesDTO[]>(appPreferences.urlBackEnd + 'sucursal/dispByUsuario/' + idUsuario, this.auth.getHeaders());
  }

  createSucursal(datos: SucursalesDTO): Observable<SucursalesDTO> {
    return this.http.post<SucursalesDTO>(appPreferences.urlBackEnd + 'sucursal', datos, this.auth.getHeaders());
  }

  updateSucursal(id: number, data: SucursalesDTO): Observable<SucursalesDTO> {
    return this.http.put<SucursalesDTO>(appPreferences.urlBackEnd + 'sucursal/' + id, data, this.auth.getHeaders())
  }

  deleteSucursal(id: number): Observable<SucursalesDTO> {
    return this.http.delete<SucursalesDTO>(appPreferences.urlBackEnd + 'sucursal/' + id, this.auth.getHeaders())
  }
}