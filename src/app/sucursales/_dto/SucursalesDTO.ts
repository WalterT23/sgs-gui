export interface SucursalesDTO {
    id?: number;
    codigoSucursal: string;
    descripcion: string;
    direccion: string;
    telefono: string;
    idEstado: number;
    estado: string;
}