import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';

// Servicios
import { SucursalesService } from './_services/sucursales.service';
import { UtilsService } from '../_utils/utils.service';
import { AuthService } from '../_services/auth.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';

// DTO
import { SucursalesDTO } from './_dto/SucursalesDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-sucursales',
  templateUrl: './sucursales.component.html',
  styleUrls: ['./sucursales.component.css']
})
export class SucursalesComponent implements OnInit {

  public datosSucursal: SucursalesDTO;
  public listaSucursales: SucursalesDTO[];
  public updSuc: boolean = false; // bandera para indicar si se hace una modificacion o se crea un nuevo registro
  public listaEstados: InfoRefOpcDTO[];

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  
  constructor(
    private sucSrv: SucursalesService,
    private infrefSrv: InfoRefService,
    public auth: AuthService,
    private util: UtilsService
  ) {

    this.listaSucursales = [] ;
    this.listaEstados = [];
    this.datosSucursal= {
      codigoSucursal: ' ',
      descripcion: ' ',
      direccion: ' ',
      telefono: ' ',
      idEstado: null,
      estado: ''
    }

   }

  ngOnInit() {

    $('.modal').modal({
      dismissible : false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('.modal-close').click();
      }
    });

    this.getLstSucursales();
    
    setTimeout(() => {
      $('.tooltipped').tooltip();  
    }, 50);
    
  }

  getLstSucursales() {
    this.sucSrv.getSucursales().subscribe(
      success => {
        this.listaSucursales = success;
      },
      err => { }
    );
  }

  getInfoSuc(id: number) {
    $('select').formSelect();
    M.updateTextFields()
    this.updSuc = true;
    
    this.sucSrv.getSucursalById(id).subscribe(
      success => {
        this.datosSucursal = success;
      },
      err => { }
    );

    this.infrefSrv.getInfoRefOpcByCodRef('Estado').subscribe(
      success => {
        this.listaEstados = success;
      },
      err => { }
    )
  }

  guardar(forma: NgForm) {
    if (forma.value.descripcion.trim() == '' || forma.value.direccion.trim() == '' || forma.value.telefono.trim() == '')  {
      console.log(forma.value);
      M.toast({html: 'Faltan completar datos de la sucursal.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
    } else if (this.updSuc) {
      if (forma.value.codigoSucursal.trim() == '') {
        M.toast({html: 'El codigo de la sucursal es obligatorio.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      } else {
        let sucursal = forma.value;
        this.sucSrv.updateSucursal(this.datosSucursal.id, sucursal).subscribe(
          success => {
            M.toast({html: 'Datos de la sucursal actualizados correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
            $('#modalSucursal').modal('close');
            this.getLstSucursales();
          },
          err => { 
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
        );
      }
    } else {
      let sucursal = forma.value;
      this.sucSrv.createSucursal(sucursal).subscribe(
        success => {
          M.toast({html: 'Sucursal creada correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalSucursal').modal('close');
          this.getLstSucursales();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    }
  }

  resetForm() {
    this.updSuc = false;
    this.datosSucursal= {
      codigoSucursal: ' ',
      descripcion: ' ',
      direccion: ' ',
      telefono: ' ',
      idEstado: null,
      estado: ' '
    }
  }
}
