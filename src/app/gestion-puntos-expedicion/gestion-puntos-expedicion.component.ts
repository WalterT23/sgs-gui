import { Component, OnInit, Input } from '@angular/core';
import { appPreferences } from '../../environments/environment';
import { NgForm } from '@angular/forms';

// Servicios
import { PtoExpedicionService } from './_services/pto-expedicion.service';
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';

// DTO
import { PuntoExpDTO } from './_dto/PuntoExpDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-puntos-expedicion',
  templateUrl: './gestion-puntos-expedicion.component.html',
  styleUrls: ['./gestion-puntos-expedicion.component.css']
})
export class GestionPuntosExpedicionComponent implements OnInit {

  @Input() mantenimiento: boolean = false;

  public datosPtoExp: PuntoExpDTO;//datos de un punto de expedicion
  public listaPtosExp: PuntoExpDTO[];//lista de todos los puntos de expedicion del sistema para la sucursal conectada
  public listaEstados: InfoRefOpcDTO[];

  constructor(
    private ptoExpSrv: PtoExpedicionService,
    private infrefSrv: InfoRefService,
    public auth: AuthService,
    private util: UtilsService
  ) { 
    this.listaPtosExp = [];
    this.listaEstados = [];
    this.datosPtoExp = {
      id: null,
      idEstado: null,
      puntoExp: null,
      fechaCreacion: null,
      usuarioCreacion: ''
    }
  }

  ngOnInit() {
    if (!this.mantenimiento) {

      setTimeout(() => {
        $('.modal').modal({
          dismissible : false,
          onOpenEnd: function (modal, trigger) {
            M.updateTextFields();
            $('select').formSelect();
          },
          onCloseEnd: function () {
            $('#cancelModalPtoExp').click();
          }
        });
      }, 60);
    };
    this.getEstados();
  }

  getEstados(){
    this.infrefSrv.getInfoRefOpcByCodRef('ESTAPTO').subscribe(
      success => {
        this.listaEstados = success;
      },
      err => { }
    )
  }

  getDatosPtoExp(id:number) {
    this.ptoExpSrv.getPtosExpById(id).subscribe(
      success => {
        this.datosPtoExp = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  guardar(forma: NgForm){

    let ptoExp = forma.value;
    let faltaCompletar: boolean = false;
    console.log(ptoExp);
    if (!faltaCompletar) {
      this.ptoExpSrv.createPtoExp(ptoExp).subscribe(
        success => {
          M.toast({html:'Pto de Expedición agregado correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor'});
          $('#modalPtoExp').modal('close');
          this.resetForm();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    } else {
      M.toast({html:'Porfavor complete todos los campos.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor'});
    }
    
  }

  resetForm() {
    this.listaPtosExp = [];
    this.datosPtoExp = {
      id: null,
      idEstado: null,
      puntoExp: null,
      fechaCreacion: null,
      usuarioCreacion: ''
    }
  }

}
