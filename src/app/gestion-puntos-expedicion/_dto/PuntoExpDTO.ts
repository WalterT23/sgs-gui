export interface PuntoExpDTO {
    id?: number;
    puntoExp: number;
    idEstado: number;
    usuarioCreacion: string;
    fechaCreacion: Date;
}