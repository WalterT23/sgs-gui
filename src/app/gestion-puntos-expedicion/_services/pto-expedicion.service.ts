import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { PuntoExpDTO } from '../_dto/PuntoExpDTO';
@Injectable()
export class PtoExpedicionService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getPtosExp(): Observable<PuntoExpDTO[]> {
    return this.http.get<PuntoExpDTO[]>(appPreferences.urlBackEnd + 'puntoExpedicion', this.auth.getHeaders());
  }

  getPtosExpById(id: number): Observable<PuntoExpDTO> {
    return this.http.get<PuntoExpDTO>(appPreferences.urlBackEnd + 'puntoExpedicion/' + id, this.auth.getHeaders());
  }

  createPtoExp(datos: PuntoExpDTO): Observable<PuntoExpDTO> {
    return this.http.post<PuntoExpDTO>(appPreferences.urlBackEnd + 'puntoExpedicion', datos, this.auth.getHeaders());
  }

  updatePtoExp(id: number, data: PuntoExpDTO): Observable<PuntoExpDTO> {
    return this.http.put<PuntoExpDTO>(appPreferences.urlBackEnd + 'puntoExpedicion/' + id, data, this.auth.getHeaders())
  }

  inactivarPtoExp(id: number): Observable<PuntoExpDTO> {
    return this.http.put<PuntoExpDTO>(appPreferences.urlBackEnd + 'puntoExpedicion/inactivarPto/' + id, this.auth.getHeaders())
  }

  activarPtoExp(id: number): Observable<PuntoExpDTO> {
    return this.http.put<PuntoExpDTO>(appPreferences.urlBackEnd + 'puntoExpedicion/activarPto/' + id, this.auth.getHeaders())
  }

}
