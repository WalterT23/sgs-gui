import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';


@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    // if ((this.auth._auth) && (this.auth._sucursal)) {

    //   if ((route.data.id != null) && !(this.auth.tienePermiso(route.data.id))) {
    //     this.router.navigate(['/dashboard']);
    //     return false;
    //   }

    //   return true;

    // } else if ((this.auth._auth) && !(this.auth._sucursal)) {
    //   this.router.navigate(['/dashboard']);
    //     return true;

    // } else {
    //   this.router.navigate(['/login']);
    //   return false;
    // }

    if (this.auth._auth) {

      if (this.auth._sucursal != '-1') {

        if ((route.data.id != null) && !(this.auth.tienePermiso(route.data.id))) {
          this.router.navigate(['/dashboard']);
          return false;
        }

        return true;
      } else {

        if(route.routeConfig.path === 'dashboard')
          return true;

        console.log("Redireccion al dashboard por no seleccionar una sucursal.")
        alert("Por favor seleccione una sucursal")
        this.router.navigate(['/dashboard']);
        return false;

      }



    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

}
