import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

import { appPreferences } from '../../environments/environment';

// DTO
import { UsuarioDTO, ResponseUserDTO } from '../login/_dto/UsuarioDTO';

@Injectable()
export class AuthService {
 
  public _auth: boolean = false;
  public _sucursal: string = '-1';
  public _infoUser: ResponseUserDTO;

  constructor(
    private http: HttpClient,
    public router: Router,
    private cookie: CookieService
  ) {

    if (this.cookie.check(appPreferences.cookName) && this.cookie.check(appPreferences.cookNamePer)) {
      this._auth = true;
      this._infoUser = JSON.parse(this.cookie.get(appPreferences.cookNamePer));
      console.log('lo que guarda en el _infoUser', this._infoUser);
      if (this._infoUser.sucursales.length != 0) {
        this._sucursal = (this._infoUser.sucursales.length == 1) ? this._infoUser.sucursales[0].idSucursal.toString() : window.sessionStorage.getItem('sucursalSeleccionada');       
      }
    } else {
      this.resetUser();
    }

  }
  get isAuth(): boolean {
    return this._auth;
  }
  
  resetUser(){
    this._infoUser = {
      id: null,
      nombre: '',
      apellido: '',
      usuario: '',
      pass: '',
      idEstado: null,
      sucursales: [],
      funcionalidades: [],
      roles: null,
      accesstoken: ''
    }
  }

  getHeaders() {
    if (this._auth) {
      return {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('user-principal', this._infoUser.usuario)
          .set('user-sucursal', this._sucursal.toString())
          .set('access-token', this._infoUser.accesstoken)
      };
    } else {
      return {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
        //.set('access-token', this._token)
      };
    }

  }

  get listaSucursales(): any[] {
    let sucursales = this._infoUser.sucursales;
    let sucursalesVigentes = sucursales.filter(row => (row.vigente == true));
    //con el sort() se ordena el listado de sucursales de manera ascendente
    this._infoUser.sucursales = sucursalesVigentes.sort(function(a, b) {if (a.codSucursal > b.codSucursal) { return 1; } if (a.codSucursal < b.codSucursal) { return -1; } return 0});
    return this._infoUser && sucursalesVigentes ? this._infoUser.sucursales : []; 

  }

  tienePermiso(func: string): boolean {

    if (String(func)) {

      let funcs: string[] = this._infoUser.funcionalidades;

      for (let i = 0; i < funcs.length; i++) {

        if (funcs[i] == func) {
          return true;
        }

      }

      return false;
    } else {

      return true;
    }

  }


  getCredencial(info: UsuarioDTO): Observable<ResponseUserDTO> {
    return this.http.post<ResponseUserDTO>(appPreferences.urlBackEnd + 'usuarios/validar', info, this.getHeaders());
  }

  logout() {
    this.cookie.delete(appPreferences.cookName);
    this.cookie.delete(appPreferences.cookNamePer);
    window.sessionStorage.clear();
    this._auth = false;
    this.router.navigate(['/login']);
    this.resetUser();
  }

}
