import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { FacturaCompraDTO, FCdetalleDTO } from '../factura-compra/_dto/FacturaCompraDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { FacturasProveedorService } from '../factura-compra/_services/facturas-proveedor.service';
import { UtilsService } from '../_utils/utils.service';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { appPreferences } from '../../environments/environment';
import { GestionComprasService } from './_services/gestion-compras.service';

import { BusquedaGral } from '../pipes/busqueda.pipe';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-compras',
  templateUrl: './gestion-compras.component.html',
  styleUrls: ['./gestion-compras.component.css']
})
export class GestionComprasComponent implements OnInit {

  public datosFacturaCompra: FacturaCompraDTO;
  public listaFacturasCompra: FacturaCompraDTO[];
  public listaDetFC: FCdetalleDTO[];
  proveedorDato: any;
  listaTipoCompra: InfoRefOpcDTO[] = [];


  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  search:any;
  updOC:boolean= false;
  detOc:any;
  creg:number = 0;
  searchText = "";
  constructor(

    private gfcServ: GestionComprasService,
    //private location: Location,
    private util: UtilsService,
    public auth: AuthService,
    private proveedorSrv: ProveedoresService,
    private infoRef: InfoRefService,
    private facturaCompraSrv: FacturasProveedorService
  ) { 

      
    this.listaFacturasCompra = [];
    this.proveedorDato = {
      nombre: null,
      ruc: null
    }

    this.datosFacturaCompra = {
      nroOrdenCompra: null,
      tipoDocumento: null,
      detalles: [
        {
          codProducto: null,
          producto: null,
          cantidad: 0,
          precioUnitario: null,
          precioTotal: null,
          idProducto: null,
          idCabeceraCompra: null,
          impuestoFiscal: null
        }
      ],
      detallesAAgregar: [],
      idEstado: null,
      estado:'',
      fechaEstado: null,
      gravada10: null,
      gravada5: null,
      idSucursal: null,
      motivo: null,
      nroFactura: null,
      ocAsociada: null,
      timbrado: null,
      tipoFactura: null,
      total: null,
      usuarioCreacion: null,
      cuotas:  null,
      pagos:  null,
      exenta: null,
      iva10: null,
      iva5: null,
      }
    this.getLstFacturasCompra();

  }

  getLstFacturasCompra(){
    this.gfcServ.getFacturaCompra().subscribe(
      success => {
        this.listaFacturasCompra = success;
        console.log("aqui", this.listaFacturasCompra);
        
       },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  getInfoCompra(id: number){
    this.gfcServ.getFacturaCompraById(id).subscribe(
      success => {
        this.datosFacturaCompra = success;
        this.listaDetFC=success.detalles;
        this.getProveedorById(success.idProveedor);
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );

  }

  borrarFactura(id){
    this.gfcServ.deleteFacturaCompra(id).subscribe(
      success => {
        M.toast({ html: 'La Factura ha sido Eliminada', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        this.getLstFacturasCompra();
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  ngOnInit() {

    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
    $('.modal').modal({
      dismissible : false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });

    this.getLstFacturasCompra();
  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

  chargeLstNewOrder() {
    
  }

  resetForm() {
    
  }

  calcMontoTotal(detalle:any, posicion:any) {
    
  }

  getProveedorById(id: number) {

    this.proveedorSrv.getProveedorById(id).subscribe(
      success => {
        this.proveedorDato.ruc = success[0].ruc;
        this.proveedorDato.nombre = success[0].razonSocial;
      }, err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );

  }
}
