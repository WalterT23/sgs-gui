import { TestBed, inject } from '@angular/core/testing';

import { GestionComprasService } from './gestion-compras.service';

describe('GestionComprasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GestionComprasService]
    });
  });

  it('should be created', inject([GestionComprasService], (service: GestionComprasService) => {
    expect(service).toBeTruthy();
  }));
});
