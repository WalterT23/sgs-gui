import { Injectable } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { FacturaCompraDTO } from '../../factura-compra/_dto/FacturaCompraDTO';
import { appPreferences } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class GestionComprasService {

  constructor(

    private http: HttpClient,
    private auth: AuthService
  
  ) { }

  getFacturaCompra(): Observable<FacturaCompraDTO[]> {
    return this.http.get<FacturaCompraDTO[]>(appPreferences.urlBackEnd + 'cabeceraCompra', this.auth.getHeaders());
  }

  getFacturaCompraById(id: number): Observable<FacturaCompraDTO> {
    return this.http.get<FacturaCompraDTO>(appPreferences.urlBackEnd + 'cabeceraCompra/' + id, this.auth.getHeaders());
  }

  deleteFacturaCompra(id: number): Observable<FacturaCompraDTO> {
    return this.http.delete<FacturaCompraDTO>(appPreferences.urlBackEnd + 'cabeceraCompra/' + id, this.auth.getHeaders())
  }

  



}
