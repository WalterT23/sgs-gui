import { Component, OnInit, ViewChild } from '@angular/core';
import { appPreferences } from '../../environments/environment';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { MantenimientoService } from './_services/mantenimiento.service';
import { FamiliaService } from '../mantenimiento-familia/_service/familia.service';
import { MarcaService } from '../mantenimiento-marca/_service/marca.service';
import { UnidadMedidaService } from '../mantenimiento-unidad-medida/_service/unidad-medida.service';
import { TimbradosService } from '../gestion-timbrados/_services/timbrados.service';
import { PtoExpedicionService } from '../gestion-puntos-expedicion/_services/pto-expedicion.service';
import { TimbradoPtoExpService } from '../gestion-timbrado-punto-exp/_service/timbrado-pto-exp.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';

//DTO
import { FamiliaDTO } from '../mantenimiento-familia/_dto/FamiliaDTO';
import { MarcaDTO } from '../mantenimiento-marca/_dto/MarcaDTO';
import { UnidadMedDTO } from '../mantenimiento-unidad-medida/_dto/UnidadMedDTO';
import { TimbradoDTO } from '../gestion-timbrados/_dto/TimbradoDTO';
import { PuntoExpDTO } from '../gestion-puntos-expedicion/_dto/PuntoExpDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { TimbradoPtoExpDTO } from '../gestion-timbrado-punto-exp/_dto/TimbradoPtoExpDTO';
import { NgSelectConfig } from '@ng-select/ng-select';
import { FormControl, FormGroup } from '@angular/forms';
import { MessageService } from '../_utils/message.service';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-mantenimiento',
  templateUrl: './mantenimiento.component.html',
  styleUrls: ['./mantenimiento.component.css']
})
export class MantenimientoComponent implements OnInit {

  public listaFlias: FamiliaDTO[];
  public datosFlia: FamiliaDTO;
  public listaMarcas: MarcaDTO[];
  public datosMarca: MarcaDTO;
  listaUniMeds: UnidadMedDTO[];
  public datosUniMed: UnidadMedDTO;
  public listaTimbrados: any[];
  public datosTimbrado: TimbradoDTO;
  public listaPtosExp: PuntoExpDTO[];
  public datosPtoExp: PuntoExpDTO;
  public listaEstados: InfoRefOpcDTO[];
  public datosTimbradoPtoExp: TimbradoPtoExpDTO;
  public listaTimbradosPtoExp: TimbradoPtoExpDTO[];
  
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; p1: number = 1;//variable que utiliza la paginación
  idTPE: number = null;
  newMenu: string = '';
  elementoActivo: any;
  form: any;
  sortProperty: string = '';
  sortOrder = 2;
  buscarTexto = "";

  constructor(
    private fliaSrv: FamiliaService,
    private marcaSrv: MarcaService,
    private uniMedSrv: UnidadMedidaService,
    private timbSrv: TimbradosService,
    private infrefSrv: InfoRefService,
    private ptoExpSrv: PtoExpedicionService,
    private timbPtoExpSrv: TimbradoPtoExpService,
    public auth: AuthService,
    public config: NgSelectConfig,
    private messageService    : MessageService
  ) {
    this.listaUniMeds = [];
    this.listaMarcas = [];
    this.listaFlias = [];
    this.listaTimbrados = [];
    this.listaPtosExp = [];
    this.listaEstados = [];
    this.listaTimbradosPtoExp = [];

    this.datosUniMed = {
      id: null,
      descripcion: ''
    }
    this.datosMarca = {
      id: null,
      descripcion: ''
    }
    this.datosFlia = {
      id: null,
      abreviatura: '',
      nombre: '',
      idPadre: null
    }
    this.datosTimbrado = {
      id: null,
      numero: null,
      inicioVigencia: null,
      finVigencia: null,
      vigente: null,
      usuarioCreacion: '',
      fechaCreacion: null,
      idSucursal: null
    }
    this.datosPtoExp = {
      id: null,
      idEstado: null,
      puntoExp: null,
      fechaCreacion: null,
      usuarioCreacion: ''
    }
    this.datosTimbradoPtoExp = {
      id: null,
      idTimbrado: null,
      idPtoExpedicion: null,
      numeroDesde: '',
      numeroHasta: '',
      ultimoNumero: '',
      idEstado: null,
      usuarioModificacion: '',
      fechaModificacion: null,
      tipoComprobante: '',
    }
    this.loadSelectConfig();
   }

  ngOnInit() {

    M.updateTextFields();
    $('select').formSelect();
    $('.tooltipped').tooltip({delay: 50});
    $('.modalCont').hide();
    $('.sidenav').sidenav();

    this.cargaInicial();
  }

  cargaInicial() {
    this.getLstUniMed();
    this.getLstMarcas();
    this.getLstAgrupaciones();
    this.getLstTimbrado();
    this.getLstPtosExp();
    this.getLstTimbradoPtoExp();
    this.getEstados('ESTAPTO');  
    this.efectAction('idPunto');
    this.newMenu = 'PUNTO';
    this.initForm();
  }

  private initForm() {
    this.form = new FormGroup({
      medida: new FormControl(null),
      medidaDescripcion: new FormControl(null),
      marca: new FormControl(null),
      marcaDescripcion: new FormControl(null),
      agrupacion: new FormControl(null),
      agrupacionDescripcion: new FormControl(null),
      agrupacionPadre: new FormControl(null)
    });
    this.loadControladoresForm();
  }

  private loadControladoresForm() {
    this.getMedida.valueChanges.subscribe(x=> {
      this.datosUniMed.id = x
      this.getdatosUniMed();
    });
    this.getMedidaDescripcion.valueChanges.subscribe(x=> {
      this.datosUniMed.descripcion = x;
    });
    this.getMarca.valueChanges.subscribe(x=>{
      this.datosMarca.id = x;
      this.getdatosMarcas();
    });
    this.getMarcaDescripcion.valueChanges.subscribe(x=>{
      this.datosMarca.descripcion = x;
    });
    this.getAgrupacion.valueChanges.subscribe(x=> {
      this.datosFlia.id = x;
      this.getdatosAgrupaciones();
    });
    this.getAgrupacionDescripcion.valueChanges.subscribe(x=>{
      this.datosFlia.nombre = x;
    });
    this.getAgrupacionPadre.valueChanges.subscribe(x=>{
      this.datosFlia.idPadre = x;
    })
  }

  get getMedida(): any {
    return this.form.get('medida');
  }
  get getMedidaDescripcion(): any {
    return this.form.get('medidaDescripcion');
  }
  get getMarca(): any {
    return this.form.get('marca');
  }
  get getMarcaDescripcion(): any {
    return this.form.get('marcaDescripcion');
  }
  get getAgrupacion(): any {
    return this.form.get('agrupacion');
  }
  get getAgrupacionDescripcion(): any {
    return this.form.get('agrupacionDescripcion');
  }
  get getAgrupacionPadre(): any {
    return this.form.get('agrupacionPadre');
  }

  loadSelectConfig() {
    this.config.notFoundText = "No se encontraron registros";
    this.config.appendTo = 'body';
    this.config.bindValue = 'value';
  }

  getEstados(tipoEstado: string){
    this.infrefSrv.getInfoRefOpcByCodRef(tipoEstado).subscribe(
      success => {
        this.listaEstados = success;

      },
      err => { }
    )
  }
  nombreEstado(idEstado: number) {
    let auxLst = this.listaEstados.filter(row => (row.id == idEstado));
    return (auxLst.length === 1) ? auxLst[0].descripcion : 'Verificar Estado';
  }
  abrevEstado(idEstado: number) {
    let auxLst = this.listaEstados.filter(row => (row.id == idEstado));
    return (auxLst.length === 1) ? auxLst[0].abreviatura : 'Error';
  }

  // ****************************************************
  // Funciones para Unidades de Medida

  getLstUniMed () {
    this.uniMedSrv.getUniMed().subscribe(
      success => {
        this.listaUniMeds = success;
      },
      err => { }
    )
  }
  gestionUniMed(){ 
    if (this.auth.tienePermiso('munimed')) {
      $('.modalCont').hide();
      $('#uniMedModal').show();
      M.updateTextFields();
      this.getLstUniMed();  
    }
  }
  getdatosUniMed(){
    if (this.datosUniMed.id != null) {
      this.uniMedSrv.getUniMedbyId(this.datosUniMed.id).subscribe(
        success => {
          //this.datosUniMed.descripcion = success.descripcion;
          this.getMedidaDescripcion.setValue(success.descripcion);
        },
        err => { }
      )
    }
    setTimeout(() => {
      M.updateTextFields();
    }, 150);
    
  }
  guardarUniMed(){
    this.uniMedSrv.updateUniMed(this.datosUniMed.id, this.datosUniMed).subscribe(
      success => {
 
        //M.toast({html:'Actualizado con exito!', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
        let id = 'idUnidadMedidaUpdate';
        this.messageService.alert2('Actualizado con exito!', `Valor actual: ${this.getMedidaDescripcion.value}`, "success", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstUniMed();
            }
          })
      },
      err => {
        let msg = err.error;
        //M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        let id = 'idUnidadMedidaUpdate';
        this.messageService.alert2('Error!', msg.errorMessage, "error", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstUniMed();
            }
          })
      }
    )
  } 
  newUniMed(){
    $('#modalUniMed').modal({
      dismissible: false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: () => this.getLstUniMed() 
    });
  }

  resetForm() {
    this.form.reset()
  }

  // ****************************************************
  // Funciones para Marcas

  getLstMarcas () {
    this.marcaSrv.getMarcas().subscribe(
      success => {
        this.listaMarcas = success;
        console.log("consulto lista de marcas", this.listaMarcas);
      },
      err => { }
    )
  }
  gestionMarcas(){ 
    if (this.auth.tienePermiso('mmarca')) {
      $('.modalCont').hide();
      $('#marcaModal').show();
      M.updateTextFields();
      this.getLstMarcas();    
    }
  }
  getdatosMarcas(){
    if (this.datosMarca.id != null) {
      this.marcaSrv.getMarcasById(this.datosMarca.id).subscribe(
        success => {
          //this.datosMarca.descripcion = success.descripcion;
          this.getMarcaDescripcion.setValue(success.descripcion);
        },
        err => { }
      )
    }
    setTimeout(() => {
      M.updateTextFields();
    }, 150);
    
  }
  guardarMarca(){
    this.marcaSrv.updateMarca(this.datosMarca.id, this.datosMarca).subscribe(
      success => {
        //M.toast({html:'Actualizado con exito!', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
        //this.getLstMarcas();
        let id = 'idMarcaUpdate';
        this.messageService.alert2('Actualizado con exito!', `Valor actual: ${this.getMarcaDescripcion.value}`, "success", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstMarcas();
            }
          })
      },
      err => {
        let msg = err.error;
        //M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        let id = 'idMarcaUpdate';
        this.messageService.alert2('Error!', msg.errorMessage, "error", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstMarcas();
            }
          })
      }
    )
  } 
  newMarca(){
    console.log("nueva marca");
    $('#modalMarca').modal({
      dismissible: false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: () => this.getLstMarcas() 
    });
  }
  resetFormMarcas() {
    this.datosMarca = {
      id: null,
      descripcion: ''
    }
  }

  // ****************************************************
  // Funciones para Agrupaciones

  getLstAgrupaciones () {
    this.fliaSrv.getFamilias().subscribe(
      success => {
        this.listaFlias = success;
        //console.log("consulto lista de agrupaciones", this.listaFlias);
      },
      err => { }
    )
  }
  gestionAgrupaciones(){ 
    if (this.auth.tienePermiso('mgroups')) {
      $('.modalCont').hide();
      $('#agrupacionModal').show();
      M.updateTextFields();
      this.getLstAgrupaciones();    
    }
  }
  getdatosAgrupaciones(){
    if (this.datosFlia.id != null) {
      this.fliaSrv.getFamiliasById(this.datosFlia.id).subscribe(
        success => {
          this.datosFlia = success;
          // if (this.datosFlia.idPadre != null) {
          //   let aux = this.listaFlias.filter(nombre => nombre.id === this.datosFlia.idPadre);
          //   this.datosFlia.nombrePadre = aux[0].nombre;
          // }
        },
        err => { }
      )
    }
    setTimeout(() => {
      M.updateTextFields();
    }, 150);
    
  }
  guardarAgrupacion(){
    this.fliaSrv.updateFamilia(this.datosFlia.id, this.datosFlia).subscribe(
      success => {
       /* M.toast({html:'Actualizado con exito!', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
        this.getLstAgrupaciones();*/
        let id = 'idAgrupacion';
        this.messageService.alert2('Actualizado con exito!', '', "success", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstAgrupaciones();
            }
          })
      },
      err => {
        let msg = err.error;
        //M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        let id = 'idAgrupacion';
        this.messageService.alert2('Error!', msg.errorMessage, "error", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstAgrupaciones();
            }
          })
      }
    )
  } 
  newGroup(){
    console.log("nueva agrupacion");
    $('#modalFamilia').modal({
      dismissible: false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: () => this.getLstAgrupaciones() 
    });
  }
  resetFormGroups() {
    this.datosFlia = {
      id: null,
      abreviatura: '',
      nombre: '',
      idPadre: null
    }
  }
  
  // ****************************************************
  // Funciones para Timbrados

  getLstTimbrado() {
    this.timbSrv.getTimbrados().subscribe(
      success => {
        this.listaTimbrados = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }
  gestionTimbrados(){ 
    if (this.auth.tienePermiso('mtimb')) {
      $('.modalCont').hide();
      $('#timbradoModal').show();
      M.updateTextFields();
      setTimeout(() => {
        $('.tooltipped').tooltip();
      }, 50);
      this.getLstTimbrado();    
    }
  }
  cerrarVigTimbrado(id: number){
    this.timbSrv.vencerTimbrado(id).subscribe(
      success => {
        ///M.toast({html:'Vigencia cerrada con exito', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'})
        //this.getLstTimbrado();
        let id = 'idvigTimbrado';
        this.messageService.alert2('Vigencia cerrada con exito', '', "success", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstTimbrado();
            }
          })
      },
      err => {
        let msg = err.error;
        //M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        let id = 'idvigTimbrado';
        this.messageService.alert2('Error!', msg.errorMessage, "error", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstTimbrado();
            }
          })
       }
    )
  }
  newTimb(){
    console.log("nuevo timbrado");
    $('#modalTimbrado').modal({
      dismissible: false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: () => this.getLstTimbrado() 
    });
  }
  resetFormTimb(){
    this.datosTimbrado = {
      id: null,
      numero: null,
      inicioVigencia: null,
      finVigencia: null,
      vigente: null,
      usuarioCreacion: '',
      fechaCreacion: null,
      idSucursal: null
    }
  }

  // ****************************************************
  // Funciones para Ptos de Expedicion

  getLstPtosExp() {
    this.ptoExpSrv.getPtosExp().subscribe(
      success => {
        this.listaPtosExp = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }
  gestionPtosExp(){ 
    if (this.auth.tienePermiso('mptoexp')) {
      $('.modalCont').hide();
      $('#ptoExpModal').show();
      M.updateTextFields();
      setTimeout(() => {
        $('.tooltipped').tooltip();
      }, 100);
      this.getLstPtosExp();  
      this.getEstados('ESTAPTO');  
    }
  }
  inactivarPtoExp(id: number){
    this.ptoExpSrv.inactivarPtoExp(id).subscribe(
      success => {
        //M.toast({html:'Punto de expedición inactivado con exito', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'})
        //this.getLstPtosExp();
        let id = 'idInActivarPto';
        this.messageService.alert2('Punto de expedición inactivado con exito', '', "success", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstPtosExp();
            }
          })
      },
      err => {
        let msg = err.error;
        //M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        let id = 'idInActivarPto';
        this.messageService.alert2('Error!', msg.errorMessage, "error", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstPtosExp();
            }
          })
       }
    )
  }
  activarPtoExp(id: number){
    this.ptoExpSrv.activarPtoExp(id).subscribe(
      success => {
        /*M.toast({html:'Punto de expedición activado con exito', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'})
        this.getLstPtosExp();*/
        let id = 'idActivarPto';
        this.messageService.alert2('Punto de expedición activado con exito', '', "success", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstPtosExp();
            }
          })
      },
      err => {
        let msg = err.error;
        //M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        let id = 'idActivarPto';
        this.messageService.alert2('Error!', msg.errorMessage, "error", "Cerrar",id).subscribe(
          async result=>{
            if (result!=null && result[0] == true && result[1] == id) {
              this.getLstPtosExp();
            }
          })
       }
    )
  }

  newPtoExp(){
    console.log("nuevo pto exp.");
    $('#modalPtoExp').modal({
      dismissible: false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: () => this.getLstPtosExp(),
      onCloseEnd: () =>  $('.tooltipped').tooltip()
    });
  }
  resetFormPtoExp(){
    this.datosPtoExp = {
      id: null,
      idEstado: null,
      puntoExp: null,
      fechaCreacion: null,
      usuarioCreacion: ''
    }
  }

  // ****************************************************
  // Funciones para Aso Timbrados - Ptos de Expedicion

  gestionTimbradosPtoExp(){ 
    if (this.auth.tienePermiso('mtpe')) {
      $('.modalCont').hide();
      $('#timbradoPtoExpModal').show();
      M.updateTextFields();
      setTimeout(() => {
        $('.tooltipped').tooltip();
      }, 50);
      this.getEstados('ESTADO');  
      this.getLstTimbradoPtoExp(); 
      this.getLstTimbrado();
      this.getLstPtosExp(); 
    }
  }

  efectAction(id:any) {
    this.buscarTexto = "";
      if (id) {
        let elem = document.getElementById(id);
        if (elem) {
          if (this.elementoActivo) {
            this.removeCssActivo();
          }
          this.elementoActivo = elem;
          this.elementoActivo.classList.add('tabactivo');
        }
      }
  }

  removeCssActivo() {
    if (this.elementoActivo) {
      this.elementoActivo.classList.remove('tabactivo');
    }
  }

  getLstTimbradoPtoExp(){
    this.timbPtoExpSrv.getTimbradosPtoExp().subscribe(
      success => {
        this.listaTimbradosPtoExp = success;
        console.log('lista TPE',this.listaTimbradosPtoExp);
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  nroTimbrado(idTimbrado: number){
    let aux = this.listaTimbrados.filter(row => (row.id == idTimbrado));
    if (aux.length > 0) {
      return aux[0].numero;
    }
    
  }
  nroPtoExp(idPtoExp){
    let aux = this.listaPtosExp.filter(row => (row.id == idPtoExp));
    if (aux.length > 0) {
      return aux[0].puntoExp;
    }
  }
  estado(idEstado: number){
    let aux= this.listaEstados.filter(row => (row.id == idEstado));
    if (aux.length > 0) {
      return aux[0].descripcion;
    }
  }

  newTimbPtoExp(){
    console.log("nuevo timbrado pto exp");
    $('#modalTimbradoPtoExp').modal({
      dismissible: false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: () => this.getLstTimbradoPtoExp() 
    });
  }
  editTPE(idTPE: number){
    console.log("editar timbrado pto exp");
    this.idTPE= idTPE;
    $('#modalTimbradoPtoExp').modal({
      dismissible: false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: () => this.getLstTimbradoPtoExp() 
    });
  }
  resetFormTimbPtoExp(){
    this.datosTimbradoPtoExp = {
      id: null,
      idTimbrado: null,
      idPtoExpedicion: null,
      numeroDesde: '',
      numeroHasta: '',
      ultimoNumero: '',
      idEstado: null,
      usuarioModificacion: '',
      fechaModificacion: null,
      tipoComprobante: '',
    }
  }
  placeHolderTxt(value:any): string {
    if (!value) return 'Seleccione un valor';
    if (value == -1) return 'Campo requerido';
    return '';
  }

  sortBy(property: string) {
    if (property === this.sortProperty) {
      switch (this.sortOrder) {
          case 0:
            //ASCENDENTE
            this.sortOrder = 1;

            break;
          case 1:
            //DESCENDENTE
            this.sortOrder = 2;
            break;
          case 2:
            //NO ORDEN
            this.sortOrder = 0;
            break;
      }
    } else {
      this.sortOrder = 1;
    }
    this.sortProperty = property;
    this.sortList(property);
}

sortList(property: string) {
  switch (this.sortOrder) {
    case 1:
      this.listaTimbrados.sort((a,b) =>{
        if(!a.property || a.property < b.property){
         return -1
        }else if (!b.property || a.property > b.property){
          return 1
        }
        return 0
      })
      break;
    case 2:
      this.listaTimbrados.sort((b,a) =>{
        if(!a.property || a.property < b.property){
         return -1
        }else if (!b.property || a.property > b.property){
          return 1
        }
        return 0
      })
      break;
  }

}

  sortIcon(property: string) {
      if (property === this.sortProperty) {
          return this.sortOrder === 0 ? '⇅' : this.sortOrder === 1? '⇈' : '⇊';
      }
      return '⇅';
  }

}
