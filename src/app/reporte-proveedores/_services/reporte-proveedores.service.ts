import { Injectable } from '@angular/core';
import { EstadoCuenta } from '../../reporte-estado-cuenta/_dto/EstadoCuentaDTO';
import { appPreferences } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { AuthService } from '../../_services/auth.service';
import { HttpClient } from '@angular/common/http';
import { CuotasPenDTO } from '../../pago-proveedores/_dto/PagoProveedoresDTO';


@Injectable()
export class ReporteProveedoresService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }


  getEstadoCuentaProveedor(): Observable<CuotasPenDTO[]> {
    return this.http.get<CuotasPenDTO[]>(appPreferences.urlBackEnd + 'proveedores/consulta', this.auth.getHeaders());
  }

  

}
