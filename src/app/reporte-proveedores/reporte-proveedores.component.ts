import { Component, OnInit } from '@angular/core';
import { ProveedoresDTO } from '../gestion-proveedores/_dto/ProveedoresDTO';
import { EstadoCuenta, TransaccionesCompra } from '../reporte-estado-cuenta/_dto/EstadoCuentaDTO';
import { ActivatedRoute } from '@angular/router';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { UtilsService } from '../_utils/utils.service';
import { AuthService } from '../_services/auth.service';
import { ReporteProveedoresService } from './_services/reporte-proveedores.service';
import { CuotasPenDTO } from '../pago-proveedores/_dto/PagoProveedoresDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-reporte-proveedores',
  templateUrl: './reporte-proveedores.component.html',
  styleUrls: ['./reporte-proveedores.component.css']
})
export class ReporteProveedoresComponent implements OnInit {

  public datosProveedor: ProveedoresDTO;
  public listaProv: CuotasPenDTO[];
  public datosSaldoProveedor: CuotasPenDTO;
  public transacciones: TransaccionesCompra[];
  public listaTransacciones:TransaccionesCompra[];


 
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  creg:number;
  facturasAux: any[] = [];


  constructor(
    private route: ActivatedRoute,
    private rpRSrv: ReporteProveedoresService,
  

    private infrefSrv: InfoRefService,
    private util: UtilsService,
    public auth: AuthService
  ) { 

    this.datosSaldoProveedor={
      idProveedor: null,
      proveedor: '',
      idSucursal: null,
      montoTotal: null,
      cuotaspendientes: null,
    }
  }

  ngOnInit() {

    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 250);

    $('#modal').modal({
      dismissible : false,
      ready: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });
    this.getProveedoresSaldo();
  }

  getProveedoresSaldo(){
    
    this.rpRSrv.getEstadoCuentaProveedor().subscribe(
      
        success => {
          
          this.listaProv = success;
        },
        err => { }
      );
    
  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  } 

}
