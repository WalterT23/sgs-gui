import { TestBed, inject } from '@angular/core/testing';

import { CajasService } from './cajas.service';

describe('CajasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CajasService]
    });
  });

  it('should be created', inject([CajasService], (service: CajasService) => {
    expect(service).toBeTruthy();
  }));
});
