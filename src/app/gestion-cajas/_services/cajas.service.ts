import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AperturaCierreDTO, PuntoExpedicionDTO, DetalleCierreDTO, IngresosEgresosCajaDTO, ArqueoCajaDTO } from '../_dto/CajasDTO';

@Injectable()
export class CajasService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getAperturaCierre(): Observable<AperturaCierreDTO[]> {
    return this.http.get<AperturaCierreDTO[]>(appPreferences.urlBackEnd + 'aperturaCierre', this.auth.getHeaders());
  }

  getAperturaCierreById(id: number): Observable<AperturaCierreDTO> {
    return this.http.get<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/' + id, this.auth.getHeaders());
  }

  createAperturaCierre(datos: AperturaCierreDTO): Observable<AperturaCierreDTO> {
    return this.http.post<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre', datos, this.auth.getHeaders());
  }

  updateAperturaCierre(id: number, data: AperturaCierreDTO): Observable<AperturaCierreDTO> {
    return this.http.put<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/' + id, data, this.auth.getHeaders())
  }

  deleteAperturaCierre(id: number): Observable<AperturaCierreDTO> {
    return this.http.delete<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/' + id, this.auth.getHeaders())
  }

  cerrarAperturaCierre(id: number, data: AperturaCierreDTO): Observable<AperturaCierreDTO> {
    return this.http.put<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/cerrar/' + id, data, this.auth.getHeaders())
  }

  generarAperturaCierre(datos: AperturaCierreDTO): Observable<AperturaCierreDTO> {
    return this.http.post<AperturaCierreDTO>(appPreferences.urlBackEnd + 'aperturaCierre/generar', datos, this.auth.getHeaders());
  }

}
