export interface AperturaCierreDTO {
    id?: number;
    idPuntoExpedicion: number;
    cajero: string;
    fechaApertura: Date;
    saldoInicial: number;
    saldoCierre: number;
    fechaCierre: Date;
    usuarioCreacion: string;
    usuarioCierre: string;
}

export interface PuntoExpedicionDTO {
    id: number;
	puntoExp: number;
	idEstado: number;
	usuarioCreacion: string;
	fechaCreacion: Date;
}

export interface DetalleCierreDTO {
	id: number;
	idAperturaCierre: number;
	montoEfectivo: number;
	montoTarjetas: number;
	montoTotal: number;
	usuarioCreacion: string;
    fechaCreacion: Date;
}

export interface IngresosEgresosCajaDTO {
	id: number;
	monto: number;
	descripcion: string;
	idAperturaCierre: number;
    tipo: string;
}

export interface ArqueoCajaDTO {
	id: number;
	idAperturaCierre: number;
	saldoAlCierre: number;
	totalEntregado: number;
	diferencias: string;
	faltanteSobrante: number;
}

export interface ArqueoCajaDTO {
	id: number;
	idAperturaCierre: number;
	saldoAlCierre: number;
	totalEntregado: number;
	diferencias: string;
	faltanteSobrante: number;
}