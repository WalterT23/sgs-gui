import { Injectable } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { HttpClient } from '@angular/common/http';
import { CuotasaVencer } from '../_dto/CuotasPendientesDTO';
import { appPreferences } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { FileDTO } from '../../reporte-ventas-realizadas/_dto/VentasRealizadasDTO';


@Injectable()
export class ReporteCuotasProximasService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getCuotasPendientes(fechas: CuotasaVencer): Observable<CuotasaVencer> {
    return this.http.post<CuotasaVencer>(appPreferences.urlBackEnd + 'cuotas/cuotasaVencer', fechas, this.auth.getHeaders());
  }

  getReporte(filtros: CuotasaVencer): Observable<FileDTO> {
    let tmp = Object.assign({},filtros)
    if(filtros.fechaDesde && filtros.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'cuotas/cuotasaVencer/impresion', filtros, this.auth.getHeaders());
  }

}
