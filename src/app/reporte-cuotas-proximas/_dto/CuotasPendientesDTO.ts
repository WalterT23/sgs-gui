import { CuotasPenDTO } from "../../pago-proveedores/_dto/PagoProveedoresDTO";


export interface CuotasaVencer {
    fechaDesde: any;
    fechaHasta: any;
    total?: number;
    idSucursal?: number;
    idProveedor?: number;
    detalles?: CuotasPenDTO[];
    totalXfecha?: CuotasForGraphic[];
}

export interface CuotasForGraphic {
    dia: Date;
    totalXdia: number;
}