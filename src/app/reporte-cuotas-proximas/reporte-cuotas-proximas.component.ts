import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CuotasPenDTO } from '../pago-proveedores/_dto/PagoProveedoresDTO';
import { FiltrosChips } from '../_utils/show-chips-filter/plusFile/filtros-chips';
import { appPreferences } from '../../environments/environment';
import { CuotasaVencer, CuotasForGraphic } from './_dto/CuotasPendientesDTO';
import { ReporteCuotasProximasService } from './_services/reporte-cuotas-proximas.service';
import { Chart, registerables} from 'chart.js'
import { DatePipe } from '@angular/common';
import { bar_graphic } from '../_utils/chartjs-options';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';
import { ProveedoresDTO } from '../gestion-proveedores/_dto/ProveedoresDTO';
import { generarBlobForPDF } from '../_utils/millard-formatter';
import { saveAs } from 'file-saver';

declare var $: any;
declare var M: any;


@Component({
  selector: 'app-reporte-cuotas-proximas',
  templateUrl: './reporte-cuotas-proximas.component.html',
  styleUrls: ['./reporte-cuotas-proximas.component.css']
})
export class ReporteCuotasProximasComponent implements OnInit {
  nameVarSession: any= "reporteCuotasaVencer";
  public filtro: CuotasaVencer;
  public cuotasPendientes:CuotasaVencer;
  public listaCV: CuotasPenDTO[];
  public listaFiltros: FiltrosChips[];
  public listaProv : ProveedoresDTO[];
  public datoProveedor: ProveedoresDTO;
  
  chart01: Chart; 
  
  @ViewChild('grafCtas') grafCtas: ElementRef;
  public colorOperacion = "#43a047";
  public datosGrap: CuotasForGraphic[] ;

  search:any;
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  regXpag2: number = 5; //variable que utiliza la paginación
  p2: number = 1; //variable que utiliza la paginación

  constructor(
    private repoSrv  : ReporteCuotasProximasService,
    private provSrv : ProveedoresService,
  ) { 
    Chart.register(...registerables);
    this.cuotasPendientes=  {
      fechaDesde: null,
      fechaHasta: null,
      total: null,
      idSucursal: null,
      idProveedor: null,
      detalles: [],
      totalXfecha: []
    };
    
    this.datoProveedor={
      ruc: null,
      dv: null,
      razonSocial: '',
      direccion: '',
      fechaRegistro: null,
      fechaModificacion: null,
      idEstado: null,
      notas: null,
      nombreFantasia: '',
      contactos: []
    }

   
    let filtro = window.sessionStorage.getItem(btoa(this.nameVarSession));
    if (filtro) {
      this.filtro = JSON.parse(filtro);
      let tmpD = this.filtro.fechaDesde.split('-');
      let tmpH = this.filtro.fechaHasta.split('-');

      setTimeout(() => {
        let fechaD: any = document.getElementById('fechaDesde');
        let fechaH: any = document.getElementById('fechaHasta');
        fechaD.value = `${tmpD[2]}/${tmpD[1]}/${tmpD[0]}`;
        fechaH.value = `${tmpH[2]}/${tmpH[1]}/${tmpH[0]}`;
        this.loadRegistros();
      }, 100);
    } else {
      this.limpiarFiltro();
    }
    
  }

  ngOnInit() {
    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.tabs').tabs();
    }, 50);

    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });

    this.getLstPagosPen();
    this.getLstProveedores();
   
  }

  mostrarFrmFiltro() {
    swShowFrmFiltro('filtroCuotasaVencer');
    this.filtro.fechaDesde= null;
    this.filtro.fechaHasta= null;
    this.filtro.idProveedor= null;
  }

  limpiarFiltro() {
    console.log('entra aca')
    this.filtro = {
      fechaDesde: null,
      fechaHasta: null,
      idProveedor: null,
    }
  }

  mostrarChips() {
    let fechaD = changeFormatDate(this.filtro.fechaDesde);
    let fechaH = changeFormatDate(this.filtro.fechaHasta);
    console.log('format', fechaD, fechaH);


    this.provSrv.getProveedorById(this.filtro.idProveedor).subscribe(
      success => {
        
        this.datoProveedor.razonSocial = success[0].razonSocial;
      
      }, err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
      
    );

    setTimeout(() => {
      let prov=this.datoProveedor.razonSocial;
      this.listaFiltros.push({
        descripcion: 'Fechas',
        id: 'fdfh',
        value:  fechaD + ' al ' + fechaH,
        bloquearCierre: true
      });
      if(prov!=null){
      this.listaFiltros.push({
        descripcion: 'Proveedor',
        id: 'prv',
        value:  prov,
        bloquearCierre: true
      });}
    }, 200 );  

    
   
  }

  loadRegistros() {

    swShowFrmFiltro('filtroCuotasaVencer', 'none');

    if (!this.filtro.fechaDesde || !this.filtro.fechaHasta) {
      alert("Los campos fecha desde y fecha hasta son obligatorios.");
      return false
    }

    this.filtro.fechaDesde = ajusteZH(this.filtro.fechaDesde);
    this.filtro.fechaHasta = ajusteZH(this.filtro.fechaHasta);
    console.log('fecha para la consulta', this.filtro);
    this.listaCV = [];
    this.listaFiltros = [];
    this.mostrarChips();

    window.sessionStorage.setItem(btoa(this.nameVarSession), JSON.stringify(this.filtro));

    this.getLstPagosPen();

  }

  getLstPagosPen() {
    console.log ("llega aca:")
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 200);
    
    this.repoSrv.getCuotasPendientes(this.filtro).subscribe(
      success => {
        this.cuotasPendientes = success;
        this.listaCV = success.detalles;
        console.log ("trae:",success)
        setTimeout(() => {
          this.graphicCreate();
        }, 50);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );

    
  }

  getLstProveedores() {

    this.listaProv = [];

    this.provSrv.getProveedores().subscribe(
      success => {
        this.listaProv = success;
        console.log(this.listaProv);

        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      },
      err => {
        $('select').formSelect();
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  graphicCreate(){
    if (this.chart01 != null) { this.chart01.destroy() }

    this.datosGrap = this.cuotasPendientes.totalXfecha;

    let dias: any[] = []; let montos: number[]=[];
    let aux = new DatePipe("es_PY");

    this.datosGrap.forEach(element => {
      dias.push(aux.transform(element.dia, "dd/MM/yyyy"));
      montos.push(element.totalXdia);
    });

    let ctx01 = this.grafCtas.nativeElement.getContext('2d')
    this.chart01 = new Chart(ctx01,
      {
        type: 'line',
        data: { 
                labels: dias,
                datasets: [
                  { label:"Monto Cuotas A pagar",
                    backgroundColor: "#43a04778",
                    hoverBackgroundColor: "#f2ba1a",
                    fill:false,
                    data: montos,
                    
                  }
                ] 
              },
        //options: bar_graphic
      }
    )
  }


  changePag(pagNumber: number){
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

  getReporte(){
    
    this.repoSrv.getReporte(this.cuotasPendientes).subscribe(
      success => {
        let excel = generarBlobForPDF(success.archivo,"application/xls");
        saveAs(excel, success.nombre + success.tipo);
        console.log(success.archivo);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  removerFiltro(event:any){

  }
}
