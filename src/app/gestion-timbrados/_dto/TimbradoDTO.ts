export interface TimbradoDTO {
    id?: number;
    numero: number;
    inicioVigencia: Date;
    finVigencia: Date;
    vigente: Boolean;
    usuarioCreacion?: string;
    fechaCreacion?: Date;
    idSucursal?: number;
}