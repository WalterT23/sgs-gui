import { Component, OnInit, Input } from '@angular/core';
import { appPreferences } from '../../environments/environment';
import { NgForm } from '@angular/forms';

// Servicios
import { TimbradosService } from './_services/timbrados.service';
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';

// DTO
import { TimbradoDTO } from './_dto/TimbradoDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-timbrados',
  templateUrl: './gestion-timbrados.component.html',
  styleUrls: ['./gestion-timbrados.component.css']
})
export class GestionTimbradosComponent implements OnInit {

  @Input() mantenimiento: boolean = false;
  
  public datosTimbrado: TimbradoDTO;//datos de un timbrado
  public listaTimbrados: TimbradoDTO[];//lista de todos los timbrados del sistema para la sucursal conectada

  constructor(
    private timbSrv: TimbradosService,
    public auth: AuthService,
    private util: UtilsService
  ) { 

    this.listaTimbrados = [];
    
    this.datosTimbrado = {
      id: null,
      numero: null,
      inicioVigencia: null,
      finVigencia: null,
      vigente: null,
      usuarioCreacion: '',
      fechaCreacion: null,
      idSucursal: null
    }
  }

  ngOnInit() {

    if (!this.mantenimiento) {

      setTimeout(() => {
        $('.modal').modal({
          dismissible : false,
          onOpenEnd: function (modal, trigger) {
            M.updateTextFields();
            $('select').formSelect();

            console.log(modal, trigger);
          },
          onCloseEnd: function () {
            $('#cancelModalTimb').click();
          }
        });
      }, 60);
    };
  }

  getDatosTimbrado(id:number) {
    this.timbSrv.getTimbradoById(id).subscribe(
      success => {
        this.datosTimbrado = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  guardar(forma: NgForm){

    let timbrado = forma.value;
    let faltaCompletar: boolean = false;
    console.log(timbrado);
    if (!faltaCompletar) {
      this.timbSrv.createTimbrado(timbrado).subscribe(
        success => {
          M.toast({html:'Timbrado agregado correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor'});
          $('#modalTimbrado').modal('close');
          this.resetForm();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    } else {
      M.toast({html:'Porfavor complete todos los campos.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor'});
    }
    
  }

  resetForm() {
    this.listaTimbrados = [];
    this.datosTimbrado = {
      id: null,
      numero: null,
      inicioVigencia: null,
      finVigencia: null,
      vigente: null,
      usuarioCreacion: '',
      fechaCreacion: null,
      idSucursal: null
    }
  }

}
