import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { TimbradoDTO } from '../_dto/TimbradoDTO';

@Injectable()
export class TimbradosService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }


  getTimbrados(): Observable<TimbradoDTO[]> {
    return this.http.get<TimbradoDTO[]>(appPreferences.urlBackEnd + 'timbrado', this.auth.getHeaders());
  }

  getTimbradoById(id: number): Observable<TimbradoDTO> {
    return this.http.get<TimbradoDTO>(appPreferences.urlBackEnd + 'timbrado/' + id, this.auth.getHeaders());
  }

  createTimbrado(datos: TimbradoDTO): Observable<TimbradoDTO> {
    return this.http.post<TimbradoDTO>(appPreferences.urlBackEnd + 'timbrado', datos, this.auth.getHeaders());
  }

  updateTimbrado(id: number, data: TimbradoDTO): Observable<TimbradoDTO> {
    return this.http.put<TimbradoDTO>(appPreferences.urlBackEnd + 'timbrado/' + id, data, this.auth.getHeaders())
  }

  vencerTimbrado(id: number): Observable<TimbradoDTO> {
    return this.http.put<TimbradoDTO>(appPreferences.urlBackEnd + 'timbrado/cerrarVigencia/' + id, this.auth.getHeaders())
  }


}
