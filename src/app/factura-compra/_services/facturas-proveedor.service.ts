import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { FacturaCompraDTO } from '../_dto/FacturaCompraDTO';
import { FacturaCompraCompositeDto } from '../_dto/factura-compra-composite-dto';

@Injectable()
export class FacturasProveedorService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getFacturasByIdProveedor(idProveedor: number): Observable<FacturaCompraDTO[]> {
    return this.http.get<FacturaCompraDTO[]>(appPreferences.urlBackEnd + 'cabeceraCompra/ByIdProveedor/' + idProveedor, this.auth.getHeaders());
  }
  createFacturaCompra(datos: FacturaCompraCompositeDto): Observable<FacturaCompraDTO> {
    return this.http.post<FacturaCompraDTO>(appPreferences.urlBackEnd + 'cabeceraCompra', datos, this.auth.getHeaders());
  }

  updateFacturaCompra(id: number, data: FacturaCompraDTO): Observable<FacturaCompraDTO> {
    return this.http.put<FacturaCompraDTO>(appPreferences.urlBackEnd + 'cabeceraCompra/' + id, data, this.auth.getHeaders())
  }

  deleteFacturaCompra(id: number): Observable<FacturaCompraDTO> {
    return this.http.delete<FacturaCompraDTO>(appPreferences.urlBackEnd + 'cabeceraCompra/' + id, this.auth.getHeaders())
  }

  getFacturasById(id: number): Observable<FacturaCompraDTO> {
    return this.http.get<FacturaCompraDTO>(appPreferences.urlBackEnd + 'cabeceraCompra/' + id, this.auth.getHeaders());
  }
}
