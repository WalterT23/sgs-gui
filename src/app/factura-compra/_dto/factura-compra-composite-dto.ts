export interface FacturaCompraCompositeDto extends CabeceraCompraDto {
    detalles: DetalleCompraDTO[];	
	detallesAEliminar?: DetalleCompraDTO[];
	detallesAAgregar: DetalleCompraDTO[];
	cuotas: CuotasCompositeDTO;
	pagos: PagoProveedoresCompositeDTO;
	estado: string;	
	proveedor?: string;
}

interface PagoProveedoresCompositeDTO extends PagoProveedoresDTO {
    detalles: PagoProvDetalleDTO[];
	detallesAEliminar?: PagoProvDetalleDTO[];
	detallesAAgregar?: PagoProvDetalleDTO[];
	proveedor?: string;
	nroOrdenPago?: string;
}

interface PagoProveedoresDTO {
    id?: number;
	idProveedor?: number;
	fecha?: Date;
	idOp?: number;
}

interface PagoProvDetalleDTO {
    id?: number;
	idPago?: number;
	idCabecera?: number;
	monto?: number;
	idMedio: number;
	idFuente: number;
	nroCheque: string;
	fechaEmision?: Date;
	fechaPago?: Date;
	idCuota?: number;
}

interface CuotasCompositeDTO extends CuotasDTO {
    detalles: CuotaDetalleDTO[];
	proveedor?: string;
	nroFactura?: string;
}

interface CuotasDTO {
    id?: number;
	idCabeceraCompra?: number;
	idProveedor?: number;
	idSucursal?: number;
	montoEntregado: number;
	cantidadCuotas: number;
}

interface CuotaDetalleDTO {
    id?: number;
	idCuotas?: number;
	nroCuota: number;
	monto: number;
	fechaVencimiento: any;
	idEstado?: number;
}

export interface DetalleCompraDTO {
    id?: number;
	idCabeceraCompra: number;
	idProducto: number;
	cantidad: number;
	precioUnitario: number;
	precioTotal: number;
	impuestoFiscal: number;
	codProducto: string;
	producto: string;
}

interface CabeceraCompraDto {
    id?: number;
	timbrado: number;
	nroFactura: string;
	tipoFactura: number;
	ocAsociada: Boolean;
	idOrden: number;
	motivo: string;
	idProveedor: number;
	gravada10: number;
	gravada5: number;
	exenta: number;
	iva10: number;
	iva5: number;
	total: number;
	idEstado: number;
	saldo?: number;
	usuarioCreacion: string;
	fecha: any;
	fechaCreacion?: Date;
	idSucursal: number;
	tipoDocumento: number;
	totalIva5?:number;
	totalIva10?:number;
}
