export interface FacturaCompraDTO {
    id?: number;
    nroFactura: string;
    timbrado:number;
    tipoFactura:number;
    ocAsociada: Boolean;
    idOrden?:number;
    nroOrdenCompra: string;
    motivo: string;
    fecha?: Date;
    tipoDocumento: number;
    idProveedor?: number;
    idEstado: number;
    estado:'';
    fechaEstado: Date;
    saldo?: number;
    total: number;
    gravada10: number;
    gravada5: number;
    exenta: number;
    iva5: number;
    iva10: number;
    fechaCreacion?: Date;
    usuarioCreacion: string;
    idSucursal:number;
    detalles: any[];
    detallesAEliminar?: FCdetalleDTO[];
    detallesAAgregar: FCdetalleDTO[];
    cuotas?: CuotasDTO;
    pagos?: PagoProveedoresDTO;
}

export interface FCdetalleDTO {
    id?: number;
    idCabeceraCompra: number;
    idProducto: number;
    codProducto?: string;
    producto?: string;
    cantidad: number;
    precioUnitario: number;
    precioTotal?: number;
    impuestoFiscal: number;
}

export interface CuotasDTO {
    id?: number;
    idProveedor: number;
    proveedor?:String;
    idCabeceraCompra?: number;
    nroFactura:String;
    idSucursal?: number;
    montoTotal: number;
    cuotaspendientes: number;
    detalles:CuotaDetalleDTO[];
}

export interface CuotaDetalleDTO {
    id?: number;
    idCuotas?: number;
    nroCuota:number;
    monto:number;
    fechaVencimiento: Date;
    idEstado: number;
    nro?:String;
    idProveedor?: number;
    proveedor?:String;
    nroFactura?:String;
    idCabeceraCompra?: number;
    internalSelected?:boolean;
}


export interface PagoProveedoresDTO {
    id?: number;
    idProveedor:number;
    proveedor: String;
    fecha:Date;
    idOp?:number;
    detalles:PagoProvdetalleDTO[];
}

export interface PagoProvdetalleDTO {
    id?: number;
    idPago?: number;
    idCabecera: number;
    monto: number;
    idMedio?: number;
    idFuente?: number;
    idCuota?:number;
    nroCheque?:String;
    fechaEmision?:Date;
    fechaPago?:Date;
}