import { Component, OnInit, ViewChild, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';
//import { Location } from '@angular/common';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { FacturasProveedorService } from './_services/facturas-proveedor.service';

// DTO
import { FacturaCompraDTO, FCdetalleDTO } from './_dto/FacturaCompraDTO';
import { OrdenesDeCompraService } from '../gestion-ordenes-de-compra/_service/ordenes-de-compra.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { FacturaCompraCompositeDto, DetalleCompraDTO } from './_dto/factura-compra-composite-dto';
import { ChequesService } from '../cheques/_services/cheques.service';
import { BancosService } from '../bancos/services/bancos.service';
import { CuentaBancoService } from '../cuenta-banco/_services/cuenta-banco.service';
import { ChequesDTO } from '../cheques/_dto/ChequesDTO';
import { CuentaBancoDTO } from '../cuenta-banco/_dto/CuentaBancoDTO';
import { TotalFactCompra } from '../pipes/sumas.pipe';
import { ProveedoresDTO } from '../gestion-proveedores/_dto/ProveedoresDTO';
import { DatePipe } from '../../../node_modules/@angular/common';

import { BusquedaGral } from '../pipes/busqueda.pipe';
import { ProductoDTO } from '../gestion-productos/_dto/ProductoDTO';
import { OrdenCompraDTO } from '../gestion-ordenes-de-compra/_dto/OrdenCompraDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-factura-compra',
  templateUrl: './new-factura-compra.component.html',
  styleUrls: ['./factura-compra.component.css']
})
export class FacturaCompraComponent implements OnInit, AfterViewInit {

  public datosFacturaCompra: FacturaCompraCompositeDto;
  public listaFacturasCompra: FacturaCompraDTO[];
  public listaOC: OrdenCompraDTO[];
  public listaProd: ProductoDTO[];
  public restar: boolean = false;
  public indexDetOC: number = 0;
  proveedorDato: any;
  listaTipoCompra: InfoRefOpcDTO[] = [];
  lstCuotas: { monto: number, fechaVencimiento: string, nroCuota: number }[] = [];
  nroOrdenCompra: string;
  listaProv:ProveedoresDTO[] = [];
  listaMedioPagos: InfoRefOpcDTO[] = [];
  listaFuentes: InfoRefOpcDTO[] = [];
  listaCheques: ChequesDTO[] = [];
  listaCuentaBanco: CuentaBancoDTO[];
  periodicidadItem: string;
  tipoFactura: InfoRefOpcDTO;
  bandera: boolean = false;
  proveedorDescripcion: string;
  search:any;
  ocAsociada:any;
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  searchText: string = "";
  constructor(
    private ftProvSrv: FacturasProveedorService,
    //private location: Location,
    private OCSrv: OrdenesDeCompraService,
    private productoSrv: ProductosService,
    private util: UtilsService,
    public auth: AuthService,
    private proveedorSrv: ProveedoresService,
    private infoRef: InfoRefService,
    private facturaCompraSrv: FacturasProveedorService,
    private cheqSrv: ChequesService,
    private bancoSrv: BancosService,
    private ctaBcoSrv: CuentaBancoService
  ) {

    this.listaFacturasCompra = [];
    this.listaProv = [];
    this.listaOC = [];
    this.proveedorDato = {
      nombre: null,
      ruc: null
    }

    this.datosFacturaCompra = {
      tipoDocumento: null,
      detalles: [
        {
          codProducto: null,
          producto: null,
          cantidad: 0,
          precioUnitario: null,
          precioTotal: null,
          idProducto: null,
          idCabeceraCompra: null,
          impuestoFiscal: null
        }
      ],
      detallesAAgregar: [],
      idEstado: null,
      estado: '',
      exenta: null,
      gravada10: null,
      gravada5: null,
      idSucursal: null,
      motivo: null,
      nroFactura: null,
      ocAsociada: false,
      timbrado: null,
      tipoFactura: null,
      total: null,
      usuarioCreacion: null,
      cuotas: {
        montoEntregado: null,
        cantidadCuotas: null,
        detalles: []
      },
      pagos: {        
        detalles: [{
          idMedio: null,
          idFuente: null,
          nroCheque: null
        }]
      },
      iva10: null,
      iva5: null,
      fecha: null,
      idProveedor: null,
      idOrden: null
    }

    this.getLstTipoVenta();
    this.getMediosPagos();
    this.getLstProveedores();
  }

  actualizarCampos() {
    setTimeout(() => {
      M.updateTextFields();
    }, 10);
  }

  ngAfterViewInit(): void {

    this.actualizarCampos()

  }

  getLstCheques() {
    this.cheqSrv.getCheques().subscribe(
      success => {
        this.listaCheques = success;
        console.log(this.listaCheques);
      },
      err => { }
    );
  }

  getLstCuentaBanco() {
    this.ctaBcoSrv.getCuentaBanco().subscribe(
      success => {
        this.listaCuentaBanco = success;
      },
      err => { }
    );
  }

  getLstProductos() {
    this.productoSrv.getProductos().subscribe(
      success => {
        this.listaProd = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  getLstTipoVenta() {

    this.infoRef.getInfoRefOpcByCodRef('tipo_fc').subscribe(
      success => {

        this.listaTipoCompra = success;
        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      }
    );

  }

  getMediosPagos() {

    this.infoRef.getInfoRefOpcByCodRef('MEDPAGO').subscribe(
      success => {

        this.listaMedioPagos = success.filter(x=> x.id == 108);
        console.log(success);
        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      }
    );

  }

  cargarFuentes(n: any) {

    this.listaCuentaBanco = [];
    this.listaCheques = [];

    switch (n) {
      case 'CTA_CRIA':

        this.getLstCuentaBanco();

        break;
      case 'CQUE':

        this.getLstCheques();

        break;
    }

  }

  buscarPorNroOc() {

    this.OCSrv.getOrdenCompraByNroOc(this.nroOrdenCompra).subscribe(

      success => {
        console.log(success);
        if (success.estado == 'CONFIRMADA'){
          this.datosFacturaCompra.idProveedor = success.idProveedor;
          this.datosFacturaCompra.idSucursal = success.idSucursal;
          this.datosFacturaCompra.totalIva5 = success.totalIva5;
          this.datosFacturaCompra.totalIva10 = success.totalIva10;
          this.datosFacturaCompra.total = success.totalIva10 + success.totalIva5;
          this.datosFacturaCompra.detalles = [];
          this.datosFacturaCompra.idOrden = success.id;
          success.detalles.forEach(det => {
            let detalle: DetalleCompraDTO = {
              cantidad: det.cantidad,
              idCabeceraCompra: null,
              idProducto: det.idProducto,
              impuestoFiscal: det.idTipoTributo,
              precioUnitario: det.ultCostoCompra,
              producto: det.producto,
              codProducto: det.codProducto,
              precioTotal: det.costoTotal
            }
            //det.precioUnitario = det.ultCostoCompra;
            //det.impuestoFiscal = det.idTipoTributo;
            this.datosFacturaCompra.detalles.push(detalle);
          });

          this.getProveedorById(success.idProveedor);
        }
        else{
          let msg;
          if (success.estado == 'EN CURSO'){
            msg = 'Orden de Compra pendiente de confirmacion';
            M.toast({ html: msg, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          }else if (success.estado == 'ANULADA'){
            msg = 'Orden de Compra anulada';
            M.toast({ html: msg, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          }
        }
        

      }, err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }

    );

  }

  getProveedorById(id: number) {

    this.proveedorSrv.getProveedorById(id).subscribe(
      success => {

        this.datosFacturaCompra.idProveedor = success[0].id;
        this.proveedorDato.nombre = success[0].razonSocial;
        this.proveedorDato.ruc = success[0].ruc

      }, err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );

  }

  getTipoFact(tipoFactura:number){
    console.log("holas")
    this.infoRef.getInfoRefOpcById(tipoFactura).subscribe(
      success => {
        this.tipoFactura = success;
        console.log (this.tipoFactura)
      }, err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
   
    setTimeout(() => {
      if (this.tipoFactura.abreviatura=='FCTO') {
        this.bandera = true;
      }
      else{
        this.bandera = false;
      }
      console.log(this.tipoFactura.descripcion ,this.bandera)
    },200);
  }

  searchProveedor(idProveedor: any, modal: boolean = false) {
    this.proveedorDescripcion = '';

    if (idProveedor) {
      this.proveedorSrv.likeProveedor(idProveedor).subscribe(
        success => {
          console.log(success);
          if (success.length === 1 && !modal) {
            let tmpProv = success[0];
            $('#txtProveedor').val(tmpProv.ruc);
            this.proveedorDescripcion = `${tmpProv.razonSocial} - ${tmpProv.nombreFantasia}`;
            this.datosFacturaCompra.idProveedor = tmpProv.id;
          } else if (success.length > 0 && !modal) {
            this.listaProv = success;
            $('#txtProveedor').val('')
            $('#busquedaProveedor').modal('open')
          } else if (modal) {
            this.listaProv = success;
          }

        },err => { 
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    }

  }

  abrirModalBuscarProveedor() {
    $('#busquedaProveedor').modal('open');
    setTimeout(() => {
      $('#textBuscarProveedor').focus();
    }, 20);
  }

  seleccionarProveedor(proveedor: ProveedoresDTO) {

    $('#txtProveedor').val(proveedor.ruc);
    this.datosFacturaCompra.idProveedor = proveedor.id;
    this.proveedorDato = {
      nombre: proveedor.razonSocial,
      ruc: proveedor.ruc
    }
    this.proveedorDescripcion = `${proveedor.razonSocial} - ${proveedor.nombreFantasia}`;
    $('#busquedaProveedor').modal('close');
    $('#textBuscarProveedor').val('');

    setTimeout(() => {
      M.updateTextFields();
    }, 20);

  }

  cambiarEstadoOCasociada(n: boolean) {

    this.datosFacturaCompra.ocAsociada = n;
    if (n) {
      this.datosFacturaCompra.idProveedor = null;
      this.proveedorDato = {
        nombre: null,
        ruc: null
      }
    }

  }

  buscarProducto(pos: number) {

    this.productoSrv.getProductoByCod(this.datosFacturaCompra.detalles[pos].codProducto).subscribe(
      success => {
        console.log(success);
        this.datosFacturaCompra.detalles[pos].producto = success.nombre;
        this.datosFacturaCompra.detalles[pos].idProducto= success.id;
        this.datosFacturaCompra.detalles[pos].impuestoFiscal = success.idTipoTributo;
        this.datosFacturaCompra.detalles[pos].precioUnitario = success.ultCostoCompra;
      }, err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }

    );

  }

  agregarRegistro() {

    let tmp: DetalleCompraDTO = {
      cantidad: 0,
      idCabeceraCompra: null,
      idProducto: null,
      impuestoFiscal: null,
      precioUnitario: null,
      producto: null,
      codProducto: null,
      precioTotal: null
    }

    this.datosFacturaCompra.detalles.push(tmp);

  }

  cerrarModalPago(){

    this.datosFacturaCompra.pagos={        
      detalles: [{
        idMedio: null,
        idFuente: null,
        nroCheque: null
      }]
    }

    this.datosFacturaCompra.cuotas={
      montoEntregado: null,
      cantidadCuotas: null,
      detalles: []
    }

    this.periodicidadItem= null,

    $('#modalCompraPago').modal('close');

  }

  removeRegistro(pos: number) {
    this.restar = true;
    this.datosFacturaCompra.detalles.splice(pos, 1);
    this.calcMontoTotal(this.datosFacturaCompra.detalles[pos])
  }

  ngOnInit() {
    $('.modal').modal();
  }

  volver() {
    history.back();
  }

  gestionCompraCredito() {
    $('#modalCompraPago').modal('open');
    setTimeout(() => {
      $('select').formSelect();
      this.actualizarCampos();
    }, 80);

  }

  calcularMontoCuota() {

    if (this.periodicidadItem && this.datosFacturaCompra.cuotas.cantidadCuotas) {

      this.datosFacturaCompra.cuotas.detalles = [];

      let tmptotal = new TotalFactCompra();

      let totalFactura = tmptotal.transform(this.datosFacturaCompra.detalles);
      let entrega = this.datosFacturaCompra.cuotas.montoEntregado;

      let saldo = totalFactura - entrega;

      let cantCuotas = saldo / this.datosFacturaCompra.cuotas.cantidadCuotas;
      let mesActual = new Date().getMonth() + 1;
      console.log("mes",mesActual)
      let anhoActual = new Date().getFullYear();
      let pDate = new DatePipe('es_PY');
      let hoy = new Date();

      for (let pos = 0; pos < this.datosFacturaCompra.cuotas.cantidadCuotas; pos++) {

        let dias = hoy.getDate() + parseInt(this.periodicidadItem);
        hoy.setDate(dias);
        console.log(hoy);
        console.log(pDate.transform(hoy, 'dd/MM/yyyy'));
        this.datosFacturaCompra.cuotas.detalles.push({
          nroCuota: pos + 1,
          monto: cantCuotas,
          fechaVencimiento: pDate.transform(hoy, 'dd/MM/yyyy')
        })
        mesActual++;
      }

    }


  }

  registrarCompra(sw: boolean = false) {

    // Si el tipo de factura seleccionada es Credito (id = 67);
    if ((this.bandera) && !sw) {
      this.gestionCompraCredito();
      return false;
    }

    if (this.datosFacturaCompra.fecha == null || this.datosFacturaCompra.idProveedor == null ||
      this.datosFacturaCompra.nroFactura == null || this.datosFacturaCompra.tipoFactura == null || this.datosFacturaCompra.timbrado ==null) {

      console.log(this.datosFacturaCompra);
      M.toast({ html: 'Favor completar todos los datos', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });


    } else {
      console.log(this.datosFacturaCompra.fecha);
      this.datosFacturaCompra.fecha = ajusteZH(this.datosFacturaCompra.fecha)

      for (let p = 0; p < this.datosFacturaCompra.cuotas.detalles.length; p++) {
        const element = this.datosFacturaCompra.cuotas.detalles[p].fechaVencimiento;
        console.log(element);
        let nf = element.split('/');  
        console.log(`${nf[2]}-${nf[1]}-${nf[0]}`);
        this.datosFacturaCompra.cuotas.detalles[p].fechaVencimiento = ajusteZH(`${nf[2]}-${nf[1]}-${nf[0]}`)
      }
      console.log(this.datosFacturaCompra);
      this.facturaCompraSrv.createFacturaCompra(this.datosFacturaCompra).subscribe(
        success => {
          M.toast({ html: 'Factura Registrada .', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          history.back()
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          setTimeout(() => {
            this.cerrarModalPago()
          }, 150); 
        }
      );
      console.log(this.datosFacturaCompra)



    }


  }

  calcMontoTotal(producto: DetalleCompraDTO, i?: number) {
    if (!this.restar) {
      let total = producto.precioUnitario * producto.cantidad;
      this.datosFacturaCompra.detalles[i].precioTotal = total;
    }
    const totalIva5 = this.datosFacturaCompra.detalles.filter(p => p.impuestoFiscal === 15).reduce((totalIva5, current) => totalIva5 + current.precioTotal, 0);
    const totalIva10 = this.datosFacturaCompra.detalles.filter(p => p.impuestoFiscal === 16).reduce((totalIva10, current) => totalIva10 + current.precioTotal, 0);
    this.datosFacturaCompra.total = totalIva5 + totalIva10;
    this.restar = false;
  }

  getLstProveedores() {
    this.proveedorSrv.getProveedores().subscribe(
      success => {
        this.listaProv = success;
        console.log(success);
      },
      err => {
        M.toast({html:"No se pudieron obtener los datos de los proveedores.",displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

  abrirModalBuscarProducto(index) {
    this.indexDetOC = index;
    $('#busquedaProducto').modal({
      dismissible : false,
    });
    $('#busquedaProducto').modal('open');
    this.getLstProductos();
  }

  seleccionarProducto(producto: ProductoDTO) {
    this.productoSrv.getProductoByCod(producto.cod).subscribe(
      success => {
        this.datosFacturaCompra.detalles[this.indexDetOC].codProducto = success.cod;
        this.datosFacturaCompra.detalles[this.indexDetOC].producto = success.nombre;
        this.datosFacturaCompra.detalles[this.indexDetOC].idProducto= success.id;
        this.datosFacturaCompra.detalles[this.indexDetOC].impuestoFiscal = success.idTipoTributo;
        this.datosFacturaCompra.detalles[this.indexDetOC].precioUnitario = success.ultCostoCompra;
      },
      err => {
        let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
    $('#busquedaProducto').modal('close');
  }
  
  getLstOrdenesDeCompras() {
    this.OCSrv.getOrdenesDeCompras().subscribe(
      success => {
        this.listaOC = success;
       },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  abrirModalBuscarOc() {
    $('#busquedaOC').modal({
      dismissible : false,
    });
    $('#busquedaOC').modal('open');
    this.getLstOrdenesDeCompras();
  }

  seleccionarOC(oc: OrdenCompraDTO) {
    this.OCSrv.getOrdenCompraByNroOc(oc.nroOrdenCompra).subscribe(
      success => {
        console.log(success);
        if (success.estado == 'CONFIRMADA'){
          this.nroOrdenCompra = success.nroOrdenCompra;
          this.datosFacturaCompra.idProveedor = success.idProveedor;
          this.datosFacturaCompra.idSucursal = success.idSucursal;
          this.datosFacturaCompra.totalIva5 = success.totalIva5;
          this.datosFacturaCompra.totalIva10 = success.totalIva10;
          this.datosFacturaCompra.total = success.totalIva10 + success.totalIva5;
          this.datosFacturaCompra.detalles = [];
          this.datosFacturaCompra.idOrden = success.id;
          success.detalles.forEach(det => {
            let detalle: DetalleCompraDTO = {
              cantidad: det.cantidad,
              idCabeceraCompra: null,
              idProducto: det.idProducto,
              impuestoFiscal: det.idTipoTributo,
              precioUnitario: det.ultCostoCompra,
              producto: det.producto,
              codProducto: det.codProducto,
              precioTotal: det.costoTotal
            }
            //det.precioUnitario = det.ultCostoCompra;
            //det.impuestoFiscal = det.idTipoTributo;
            this.datosFacturaCompra.detalles.push(detalle);
          });

          this.getProveedorById(success.idProveedor);
        }
        else{
          let msg;
          if (success.estado == 'EN CURSO'){
            msg = 'Orden de Compra pendiente de confirmacion';
            M.toast({ html: msg, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          }else if (success.estado == 'ANULADA'){
            msg = 'Orden de Compra anulada';
            M.toast({ html: msg, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          }
        }
        

      }, err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }

    );
    $('#busquedaOC').modal('close');
  }

}
