import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'busqueda', pure: false })
export class BusquedaGral implements PipeTransform{
    transform(value: any, args?: any): any{
        if(!value) return null;
        if(!args) return value;
        console.log(value);
        console.log(args);
        args = args.toLowerCase();

        return value.filter((item: any)=>{
            return JSON.stringify(item).toLowerCase().includes(args);
        })
    }
}