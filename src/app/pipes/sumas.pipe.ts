import { Pipe, PipeTransform } from '@angular/core';
import { OCdetalleDTO } from '../gestion-ordenes-de-compra/_dto/OrdenCompraDTO';
import { DetalleVentaDTO } from '../factura-venta/_dto/FacturaVentaDTO';
import { FacturaCompraDTO, FCdetalleDTO } from '../factura-compra/_dto/FacturaCompraDTO';
import { CrearVentaDto } from '../factura-venta/_dto/venta-send-dto';
import { DetalleCompraDTO } from '../factura-compra/_dto/factura-compra-composite-dto';

@Pipe({ name: 'sumasFacturaCompra', pure: false })
export class SumasFactCompra implements PipeTransform {

  transform(value: DetalleCompraDTO[], args?: number): number {

    let suma: number = 0;
    let sumaT: number = 0;

    for (let index = 0; index < value.length; index++) {
      const element = value[index];
      if (args == element.impuestoFiscal) {
        suma += element.precioTotal;
      }
    }
    return suma;
  }
}

@Pipe({ name: 'sumas', pure: false })
export class SumasPipe implements PipeTransform {

  transform(value: OCdetalleDTO[], args?: string): number {

    let suma: number = 0;
    let sumaT: number = 0;

    for (let index = 0; index < value.length; index++) {

      const element = value[index];
      let aux: number = 0;

      aux = element.ultCostoCompra * element.cantidad;

      if (args == element.tributo) {
        suma += aux;
      }
      sumaT += aux;
    }
    if (args == "total") {
      return sumaT;
    }else {
      return suma;
    }
  }
}

@Pipe({ name: 'totalFactVenta', pure: false })
export class TotalFactVenta implements PipeTransform {

  transform(value: DetalleVentaDTO[], iva: string = null): number {

    if(iva) {
      value = value.filter(row => row.producto.tipoTributo == iva);
    }

    let total: number = 0;
    for (let pos = 0; pos < value.length; pos++) {
      const element = value[pos];
      if(element.precioTotal) {
        total += element.precioTotal;
      }      
    }
    
    return total;
  }
}

@Pipe({ name: 'totalFacCompra', pure: false })
export class TotalFactCompra implements PipeTransform {

  transform(value: any[], iva: string = null): number {

    let total: number = 0;
    for (let pos = 0; pos < value.length; pos++) {
      const element = value[pos];
      if(element.cantidad && element.precioUnitario)
        total += element.cantidad * element.precioUnitario;
    }
    
    return total;
  }
}

@Pipe({ name: 'totalNCventa', pure: false })
export class TotalNCventa implements PipeTransform {

  transform(value: any[], iva: string = null): number {

    let total: number = 0;
    for (let pos = 0; pos < value.length; pos++) {
      const element = value[pos];
      if(element.cantidad && element.precioUni)
        total += element.cantidad * element.precioUni;
    }
    return total;
  }
}

@Pipe({ name: 'totalNCcompra', pure: false })
export class TotalNCcompra implements PipeTransform {

  transform(value: any[], iva: string = null): number {

    let total: number = 0;
    for (let pos = 0; pos < value.length; pos++) {
      const element = value[pos];
      if(element.cantidad && element.costoUnitario)
        total += element.cantidad * element.costoUnitario;
    }
    return total;
  }
}


@Pipe({ name: 'pDetalleVentaPagos', pure: false })
export class PDetalleVentaPagos implements PipeTransform {

  transform(value: CrearVentaDto): number {

    let total: number = 0;
    for (let pos = 0; pos < value.formasPago.length; pos++) {
      const element = value.formasPago[pos];
      
      if (element.montoPago)
        total += element.montoPago;
    }
    
    return total;
  }
}

@Pipe({name: 'iFormatNumber'})
export class iFormatNumber implements PipeTransform {
  transform(value: number): string {
    return (value != undefined) ?  value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
  }
}
