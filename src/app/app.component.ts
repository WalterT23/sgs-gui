import { Component } from '@angular/core';
import { AuthService } from './_services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'SGS';

  constructor(
    public auth: AuthService
  ) {};

  ocultarFiltro() {

    let frmFiltro: any = document.getElementsByClassName('contenedorFiltro')[0];
    let bloqueoPantalla: any = document.getElementsByClassName('filtro-overlay')[0];

    frmFiltro.setAttribute('style', 'display: none');
    bloqueoPantalla.setAttribute('style', 'display: none');
  }

}

