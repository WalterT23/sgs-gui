export interface PagoProveedoresDTO {
    id?: number;
    idProveedor:number;
    proveedor: String;
    fecha:Date;
    idOp?:number;
    idSucursal?:number;
    totalPagado:number;
    detalles:PagoProvdetalleDTO[];
}

export interface PagoProvdetalleDTO {
    id?: number;
    idPago?: number;
    idCabecera: number;
    nroFactura?:string;
    monto: number;
    idMedio?: number;
    medio?: string;
    abreviaturaMedio?:string;
    idFuente?: number;
    idCuota?:number;
    cuota?:number;
    nroCheque?:string;
    fechaEmision?:string;
    fechaPago?:string;
}

export interface CuotasPenDTO {
    id?: number;
    idProveedor: number;
    proveedor: string;
    nombreFantasia?: string;
    ruc?: string;
    idSucursal: number
    montoTotal: number;
    cuotaspendientes: number;
    fechaVencimiento?:any;
    montoaPagar?:number;
    nro?:number;
    nroFactura?:any;
}


export interface CuotasDTO {
    id?: number;
    idProveedor: number;
    proveedor:String;
    idCabeceraCompra: number;
    nroFactura:String;
    idSucursal: number;
    montoTotal: number;
    cuotaspendientes: number;
    detalles:CuotaDetalleDTO[];


}

export interface CuotaDetalleDTO {
    id?: number;
    idCuotas: number;
    nroCuota:number;
    monto:number;
    fechaVencimiento: Date;
    idEstado: number;
    nro?:String;
    idProveedor?: number;
    proveedor?:String;
    nroFactura?:String;
    idCabeceraCompra?: number;
    internalSelected?:boolean;

}