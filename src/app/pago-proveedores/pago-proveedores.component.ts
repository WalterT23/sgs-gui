import { Component, OnInit, ViewChild } from '@angular/core';


import {PagoProveedoresDTO,PagoProvdetalleDTO, CuotasPenDTO, CuotasDTO, CuotaDetalleDTO} from './_dto/PagoProveedoresDTO';
import { ProveedoresDTO} from'../gestion-proveedores/_dto/ProveedoresDTO';
import{FacturaCompraDTO} from '../factura-compra/_dto/FacturaCompraDTO';
import { PagoProveedoresService } from './_services/pago-proveedores.service';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';
import { UtilsService } from '../_utils/utils.service';
import { AuthService } from '../_services/auth.service';
import { formatearMillar, generarBlobForPDF } from '../_utils/millard-formatter';
import { appPreferences } from '../../environments/environment';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { InfoRefDTO, InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { BancosService } from '../bancos/services/bancos.service';
import { BancosDTO } from '../bancos/_dto/BancosDTO';
import { CuentaBancoDTO } from '../cuenta-banco/_dto/CuentaBancoDTO';
import { CuentaBancoService } from '../cuenta-banco/_services/cuenta-banco.service';
import { saveAs } from 'file-saver';


declare var $: any;
declare var M: any;

@Component({
  selector: 'app-pago-proveedores',
  templateUrl: './pago-proveedores.component.html',
  styleUrls: ['./pago-proveedores.component.css']
})
export class PagoProveedoresComponent implements OnInit {

  public datosPago : PagoProveedoresDTO;
  public listaPago: PagoProveedoresDTO[];
  public detallepago: PagoProvdetalleDTO;
  public listaDP: PagoProvdetalleDTO[];
  public listaProv: ProveedoresDTO[];
  public listaCompras : FacturaCompraDTO[];
  public lstcuotaspendientes: CuotasPenDTO[];
  public listacuotasprov: CuotaDetalleDTO[];
  public listaMedios: InfoRefOpcDTO[];
  public listaFuentes: InfoRefOpcDTO[];
  public listaBancos: BancosDTO[];
  public listaCuentaBanco: CuentaBancoDTO[];
  public fuente:number;
  public medio:number;
  public listapagar:CuotasDTO[];
  updCli:any;
  search:any;

  public modoTrabajo:String;

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  creg:number;
  facturasAux: any[] = [];
  //tmpList: any[] = [];


  constructor(


    private pSrv: PagoProveedoresService,
    private provSrv: ProveedoresService,
    private infrefSrv: InfoRefService,
    private bancoSrv: BancosService,
    private prodSrv: ProductosService,
    private ctaBcoSrv: CuentaBancoService,
    private util: UtilsService,
    public auth: AuthService
  ) { 
    this.listaPago = [];
    this.listaProv = [];
    this.listaDP = [];
    this.listapagar= [];
    this.listaCompras = [];
    this.lstcuotaspendientes= [];
    this.listacuotasprov= [];
    this.modoTrabajo='PAGADOS';
    this.datosPago = {
      id:null,
      idProveedor:null,
      proveedor:'',
      fecha:null,
      idOp:null,
      idSucursal:null,
      totalPagado:null,
      detalles:[],
    }
    this.detallepago={
      id:null,
      idPago:null,
      idCabecera: null,
      monto: null,
      idMedio:null,
      idFuente: null,
      idCuota:null
  
    }


  }

  ngOnInit() {


    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 250);

    $('#modal').modal({
      dismissible : false,
      ready: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });
    this.getLstPagosPen();
    this. getLstPagos();
  }

  formatNumber(nro: number): String {
    return formatearMillar(nro);
  }

  getLstPagos() {
    this.pSrv.getPagoProveedores().subscribe(
      success => {
        this.listaPago = success;
        console.log("listaPago", this.listaPago);
        
       },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }


  getLstPagosPen() {

    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 200);
    
    this.pSrv.getListCuotasPendientes().subscribe(
      success => {
        this.lstcuotaspendientes = success;
        console.log("aqui", this.listaPago);
        
       },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );

    
  }



  getLstProveedores() {

    this.listaProv = [];

    this.provSrv.getProveedores().subscribe(
      success => {
        this.listaProv = success;
        console.log(this.listaProv);

        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      },
      err => {
        $('select').formSelect();
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  addPago(idProveedor:number){
    
    this.datosPago.idProveedor=idProveedor;
    console.log ("idproveedor",  this.datosPago.idProveedor)

    setTimeout(() => {
     // this.getLstCuotasProveedor(idProveedor);
      //this.getLstProveedores();
      this.resetForm();
      this.getLstMedios();
    }, 80);

    //this.getLstFuentes();
    $('select').formSelect();

    $('#modalPago').modal({
      dismissible : false,
      ready: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    
    $('#modalGestionPago').modal({
      dismissible : false,
      ready: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });
  }
   
  getLstMedios() {

    this.listaMedios = [];

    this.infrefSrv.getInfoRefOpcByCodRef('MEDPAGO').subscribe(
      success => {
        this.listaMedios = success;
      },
      err => { }
    );


  }

  getLstBancos() {

    this.listaBancos = [];

    this.bancoSrv.getBancos().subscribe(
      success => {
        this.listaBancos = success;
        console.log(this.listaBancos);

        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      },
      err => {
        $('select').formSelect();
        M.toast("No se pudieron obtener los datos del Banco.", appPreferences.toastErrorDuration, 'toastErrorColor');
      }
    );
  }


  getLstCtaBanco() {

    this.listaCuentaBanco = [];

    /*if (this.datosCheque.idBanco != null) {
      this.ctaBcoSrv.getCuentaBancoByIdBanco(this.datosCheque.idBanco).subscribe (
        success => {
          this.listaCuentaBanco = success;
          console.log("id banco",this.datosCheque.idBanco,"cuentas por banco",this.listaCuentaBanco);
        },
        err => { }
      )
    }*/
  }

  switchModoChanged() {

    let showAll = (<HTMLInputElement>document.getElementById('switchModo')).checked
    console.log('Mostrar todos los documentos: ' + showAll)

    showAll ? this.modoTrabajo = 'PENDIENTES'  : this.modoTrabajo = 'PAGADOS'
   // this.listar()
    this.getLstPagos();
  }

  descargar(id:number){
    console.log(id);
    this.pSrv.getPdfOrdenPago(id).subscribe(
      success => {
        let pdf = generarBlobForPDF(success.bytes);
        saveAs(pdf, success.fileName + ".pdf");
        console.log(success.type);
      },
      error => {

      }
    );
  }



  changePag(pagNumber: number){
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

  getInfoPago(id:number){

    $('#verPago').modal({
      dismissible : false,
      ready: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    this.pSrv.getpagoProveedoresById(id).subscribe(
      success => {
        this.datosPago = success;
        this.listaDP=success.detalles;
        console.log("pago", this.datosPago,this.listaDP);
        
       },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );

    
  }

  resetForm(){
    this.listaPago = [];
    this.listaProv = [];
    this.listaDP = [];
    this.listaCompras = [];
    this.lstcuotaspendientes= [];
    this.modoTrabajo='PAGADOS';
    this.datosPago = {
      id:null,
      idProveedor:null,
      proveedor:'',
      fecha:null,
      idOp:null,
      idSucursal:null,
      totalPagado:null,

      detalles:[],
    }
    this.detallepago={
      id:null,
      idPago:null,
      idCabecera: null,
      monto: null,
      idMedio:null,
      idFuente: null,
      medio: null
    } 
  }

  volver(){

    setTimeout(() => {
      this.getLstPagos();
    }, 200);
    
  
  }

  pagar() {}

  guardar() {}
}


