import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PagoProveedoresDTO, PagoProvdetalleDTO,CuotasPenDTO, CuotasDTO, CuotaDetalleDTO } from '../_dto/PagoProveedoresDTO';
import { PagosPendientesDTO } from '../../gestion-pagos/_dto/GestionPagosDTO';



@Injectable()
export class PagoProveedoresService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getPagoProveedores(): Observable<PagoProveedoresDTO[]> {
    return this.http.get<PagoProveedoresDTO[]>(appPreferences.urlBackEnd + 'pagoProveedores', this.auth.getHeaders());
  }

  getOCconfirmadas(): Observable<PagoProveedoresDTO[]> {
    return this.http.get<PagoProveedoresDTO[]>(appPreferences.urlBackEnd + 'pagoProveedores/confirmadas', this.auth.getHeaders());
  }

  getpagoProveedoresById(id: number): Observable<PagoProveedoresDTO> {
    return this.http.get<PagoProveedoresDTO>(appPreferences.urlBackEnd + 'pagoProveedores/' + id, this.auth.getHeaders());
  }

  createpagoProveedores(datosPago: PagoProveedoresDTO): Observable<any>  {
    return this.http.post<any>(appPreferences.urlBackEnd + 'pagoProveedores', datosPago, this.auth.getHeaders());
  }

  updatepagoProveedores(id:number, datosPago: PagoProveedoresDTO): Observable<any> {
    
    return this.http.put<any>(appPreferences.urlBackEnd + 'pagoProveedores/'+ id, datosPago, this.auth.getHeaders())
  }

  updatePagoProvDetalle(dataDetOC: PagoProvdetalleDTO[]): Observable<any> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'pagoProveedores/', dataDetOC, this.auth.getHeaders())
  }

  deletepagoProveedores(id: number): Observable<PagoProveedoresDTO> {
    return this.http.delete<PagoProveedoresDTO>(appPreferences.urlBackEnd + 'pagoProveedores/' + id, this.auth.getHeaders())
  }

  getListCuotasPendientes(): Observable<CuotasPenDTO[]> {
    return this.http.get<CuotasPenDTO[]>(appPreferences.urlBackEnd + 'cuotas/pendientes', this.auth.getHeaders());
  }

  getListPendientesProveedor(idProveedor: number): Observable<PagosPendientesDTO[]> {
    return this.http.get<PagosPendientesDTO[]>(appPreferences.urlBackEnd + 'cuotas/pendientes/' + idProveedor, this.auth.getHeaders());
  }

  getPdfOrdenPago(id: number): Observable<any>{
    return this.http.get<any>(appPreferences.urlBackEnd + 'cabeceraOrdenPago/imprimir/'+ id, this.auth.getHeaders());
  }

  
}
