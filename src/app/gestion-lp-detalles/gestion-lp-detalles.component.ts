import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { LpdetallesService } from './_service/lpdetalles.service';
import { ListaPreciosService } from '../gestion-lista-precios/_service/lista-precios.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';

//DTO
import { appPreferences } from '../../environments/environment';
import { LPdetalleDTO, TipoRecargaDTO } from './_dto/LPdetallesDTO';
import { ListaPreciosDTO } from '../gestion-lista-precios/_dto/ListaPreciosDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-lp-detalles',
  templateUrl: './gestion-lp-detalles.component.html',
  styleUrls: ['./gestion-lp-detalles.component.css']
})
export class GestionLpDetallesComponent implements OnInit {

  public datosLPdetalle: LPdetalleDTO;
  public listLPdetalle: LPdetalleDTO[];
  public datosListaPrecio: ListaPreciosDTO;
  public listaTipoRecarga: TipoRecargaDTO[];
  public onlyView: boolean = false;
  public nroLista: number = null;
  public costoInicial: number = 0;

  regXpag: number = 15; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  updLP:boolean = false;
  constructor(
    public auth: AuthService,
    private lpDetallesSrv: LpdetallesService,
    private listaPrecioSrv: ListaPreciosService,
    private infrefSrv: InfoRefService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.listaTipoRecarga = [];
    this.listLPdetalle = [];
    this.datosLPdetalle = {
      id: null,
      idListaPrecios: null,
      idProducto: null,
      costoBase: null,
      idTipoRecarga: null,
      montoRecarga: null,
      precioVenta: null,
      stock: null
    }
    this.datosListaPrecio = {
      id: null,
      idTipoLista: null,
      descripcion: '',
      inicioVigencia: null,
      finVigencia: null,
      fechaUltimaActualizacion: null,
      usuarioUltActualizacion: '',
      idSucursal: null,
      tipoListaDTO: null,
      detalles: null,
      costoBase: '',
      calcPrecioVenta: null,
      listaBase: ''
    };
  }

  ngOnInit(): void {
    this.getDatos();
    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });
  }

  getDatos(): void {
    const idLP = +this.route.snapshot.paramMap.get('id');
    this.lpDetallesSrv.getLPdetalleByIdLP(idLP).subscribe(
      success => {
        this.listLPdetalle = success;
      },
      err => { }
    );
    this.listaPrecioSrv.getListaPrecioById(idLP).subscribe(
      success => {
        this.datosListaPrecio = success;
      },
      err => { }
    );
    this.nroLista = idLP;
  }
  goBack(): void {
    this.location.back();
  }

  getDatosLPdetalles(id: number) {
    this.lpDetallesSrv.getLPdetalleById(id).subscribe(

    )
  }

  getLstTipoRecarga() {
    this.infrefSrv.getInfoRefOpcByCodRef('TIP_COSTO_VENTA').subscribe(
      success => {
        this.listaTipoRecarga = success;
      },
      err => { }
    )
  }

  getInfoLPD(id: number) {
    this.lpDetallesSrv.getLPdetalleById(id).subscribe(
      success => {
        this.datosLPdetalle = success;
        this.costoInicial = success.costoBase;
        console.log('datos LPdetalle', this.datosLPdetalle, this.costoInicial);
      },
      err => { }
    );
    this.getLstTipoRecarga();
  }

  calcularPrecio(cod?: number) {
    var monto: number = 0;

    if (cod == 1 || cod == 26) {
      monto = this.datosLPdetalle.costoBase + this.datosLPdetalle.montoRecarga;
    } else if (cod == 2 || cod == 25) {
      monto = ((this.datosLPdetalle.costoBase * this.datosLPdetalle.montoRecarga) / 100) + this.datosLPdetalle.costoBase;
    }
    this.datosLPdetalle.precioVenta = monto;
  }
  cambioTR() {
    setTimeout(() => {
      M.updateTextFields();
      this.datosLPdetalle.montoRecarga = null;
    }, 50);
  }

  guardar() {
    console.log("datos al apretar el btn guardar", this.datosLPdetalle);
    var diferencia: number = 0; var accion: Boolean = true;
    if (this.datosLPdetalle.costoBase == null || this.datosLPdetalle.costoBase == 0) {
      M.toast({ html: 'El costo base del producto no puede ser nulo', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      accion = false;
    }

    if (accion) {
      diferencia = this.calcularDiferencia();
      if (diferencia > 50) {
        var seguro = confirm("El nuevo costo base tiene una diferencia mayor al 50% del costo base anterior esta correcto el nuevo valor?");
        if (seguro) {
          this.actualizarProducto();
        }
      } else {
        this.actualizarProducto();
      }
    }
  }

  actualizarProducto() {
    this.lpDetallesSrv.updateLPdetalle(this.datosLPdetalle.id, this.datosLPdetalle).subscribe(
      success => {
        M.toast({ html: 'Producto actualizado correctamente', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
        this.getDatos();
        $('#modalLPdetalle').modal('close');
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  calcularDiferencia() {
    var diferencia: number = 0;

    if (this.costoInicial > this.datosLPdetalle.costoBase || this.costoInicial == this.datosLPdetalle.costoBase) {
      diferencia = this.costoInicial - this.datosLPdetalle.costoBase;
    } else {
      diferencia = this.datosLPdetalle.costoBase - this.costoInicial;
    }

    diferencia = (diferencia / this.costoInicial) * 100;
    return diferencia;
  }

  resetForm() {
    this.datosLPdetalle = {
      id: null,
      idListaPrecios: null,
      idProducto: null,
      costoBase: null,
      idTipoRecarga: null,
      montoRecarga: null,
      precioVenta: null,
      stock: null
    }
  }

  changePag(pagNumber: number) {
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }
}
