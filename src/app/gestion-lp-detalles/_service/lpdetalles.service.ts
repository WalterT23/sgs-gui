import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LPdetalleDTO } from '../_dto/LPdetallesDTO';

@Injectable()
export class LpdetallesService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }
  
  getLPdetalles(): Observable<LPdetalleDTO[]> {
    return this.http.get<LPdetalleDTO[]>(appPreferences.urlBackEnd + 'listaPreciosDetalle', this.auth.getHeaders());
  }

  getLPdetalleById(id: number): Observable<LPdetalleDTO> {
    return this.http.get<LPdetalleDTO>(appPreferences.urlBackEnd + 'listaPreciosDetalle/' + id, this.auth.getHeaders());
  }

  getLPdetalleByIdLP(idLP: number): Observable<LPdetalleDTO[]> {
    return this.http.get<LPdetalleDTO[]>(appPreferences.urlBackEnd + 'listaPreciosDetalle/byLP/' + idLP, this.auth.getHeaders());
  }

  createListaPrecio (data: LPdetalleDTO): Observable<LPdetalleDTO> {
    return this.http.post<LPdetalleDTO>(appPreferences.urlBackEnd + 'listaPreciosDetalle', data, this.auth.getHeaders())
  }

  updateLPdetalle(id: number, data: LPdetalleDTO): Observable<LPdetalleDTO> {
    return this.http.put<LPdetalleDTO>(appPreferences.urlBackEnd + 'listaPreciosDetalle/' + id, data, this.auth.getHeaders())
  }

  deleteLPdetalle(id: number): Observable<LPdetalleDTO> {
    return this.http.delete<LPdetalleDTO>(appPreferences.urlBackEnd + 'listaPreciosDetalle/' + id, this.auth.getHeaders())
  }
}
