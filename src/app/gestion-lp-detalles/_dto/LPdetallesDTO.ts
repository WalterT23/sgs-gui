import { StockDTO } from "../../gestion-stock/_dto/StockDTO";


export interface LPdetalleDTO {
    id?: number;
    idListaPrecios: number;
    idProducto: number;
    costoBase: number;
    idTipoRecarga: number;
    montoRecarga: number;
    precioVenta: number;
    stock: StockDTO;
}

export interface TipoRecargaDTO {
    id?: number;
    idInfoRef?: number;
    descripcion?: string;
}
