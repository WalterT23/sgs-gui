import { Component, OnInit } from '@angular/core';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { CuentaBancoDTO } from './_dto/CuentaBancoDTO';
import { CuentaBancoService } from './_services/cuenta-banco.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';
import { BancosDTO } from '../bancos/_dto/BancosDTO';
import { BancosService } from '../bancos/services/bancos.service';
import { ChequesService } from '../cheques/_services/cheques.service';
import { ChequesDTO } from '../cheques/_dto/ChequesDTO';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-cuenta-banco',
  templateUrl: './cuenta-banco.component.html',
  styleUrls: ['./cuenta-banco.component.css']
})
export class CuentaBancoComponent implements OnInit {


  public datosCuentaBanco: CuentaBancoDTO;
  public listaCuentaBanco: CuentaBancoDTO[];
  public updCtaBco: boolean = false; // bandera para indicar si se hace una modificacion o se crea un nuevo registro
  public listaEstados: InfoRefOpcDTO[];
  public listaBancos: BancosDTO[];
  public listaTipoCuentas : InfoRefOpcDTO[];
  public listaCheques : ChequesDTO[];
  search:any;

  constructor(
    private ctaBcoSrv: CuentaBancoService,
    private infrefSrv: InfoRefService,
    private bancoSrv : BancosService,
    private cqueServ : ChequesService,
    public auth: AuthService,
    private util: UtilsService

  ) { 

    this.listaCuentaBanco = [] ;
    this.listaEstados = [];
    this.listaTipoCuentas = [];
    this.listaBancos= [];
    this.listaCheques= [];

    this.datosCuentaBanco= {
      nombreCuenta: '',
      titular: '',
      idTipoCuenta: null,
      idBanco: null,
      nroCuenta: null,
      idEstado: null,
      estado: '',
      tipoCuenta: '',
      banco: '',

    }

  }

  ngOnInit() {
    $('select').formSelect();
   
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);

    $('.modal').modal({
      dismissible : false,
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: function () {
        $('.modal-close').click();
      }
    });

    this.getLstCuentaBanco();

    
  }
  addNewCuenta(){
    this.resetForm();
    this.getLstBancos();
    this.getLstEstados();
    this.getLstTipoCta();
    $('select').formSelect();

  }

  getLstCuentaBanco() {
    this.ctaBcoSrv.getCuentaBanco().subscribe(
      success => {
        this.listaCuentaBanco = success;
        
      },
      err => { }
    );
  }

  getInfoCtaBanco(id: number) {
    $('select').formSelect();
    M.updateTextFields()
    this.updCtaBco = true;
    console.log(this.datosCuentaBanco);
    this.ctaBcoSrv.getCuentaBancoById(id).subscribe(
      success => {
        this.datosCuentaBanco = success;
      },
      err => { }
    );

    
    this.bancoSrv.getBancos().subscribe(
      success => {
        this.listaBancos = success;
        console.log(this.listaBancos);
      },
      err => { }
    );
    
    this.infrefSrv.getInfoRefOpcByCodRef('tipo_Cta').subscribe(
      success => {
        this.listaTipoCuentas = success;
      },
      err => { }
    );
    
    this.infrefSrv.getInfoRefOpcByCodRef('estcta').subscribe(
      success => {
        this.listaEstados = success;
        console.log(this.listaEstados);
      },
      err => { }
    );

  }

  getLstBancos() {

    this.listaBancos = [];

    this.bancoSrv.getBancos().subscribe(
      success => {
        this.listaBancos = success;
        console.log(this.listaBancos);

        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      },
      err => {
        $('select').formSelect();
        M.toast("No se pudieron obtener los datos del Banco.", appPreferences.toastErrorDuration, 'toastErrorColor');
      }
    );
  }


  getLstEstados() {

    this.listaEstados = [];

    this.infrefSrv.getInfoRefOpcByCodRef('ESTCTA').subscribe(
      success => {
        this.listaEstados = success;
      },
      err => { }
    );
  }

  getLstTipoCta() {

    this.listaTipoCuentas = [];

    this.infrefSrv.getInfoRefOpcByCodRef('tipo_Cta').subscribe(
      success => {
        this.listaTipoCuentas = success;
      },
      err => { }
    );
  }


  getInfoChequesCta (id: number){
    this.getLstCheques(id);
    this.getInfoCtaBanco(id);
    console.log(this.datosCuentaBanco);
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('select').formSelect();
    }, 100);

    this.cqueServ.getChequesByIdCuenta(id).subscribe(
      success => {
        this.listaCheques = success;
        console.log('sucDispByUsuario',this.listaCheques);
      },
      err => {
      }
    )
    
  }

  getLstCheques(id:number) { 
  this.listaCheques = [];
  console.log("id banco",this.datosCuentaBanco.id);

  if (this.datosCuentaBanco.id != null) {
    this.cqueServ.getChequesByIdCuenta(this.datosCuentaBanco.id).subscribe (
      success => {
        this.listaCheques = success;
        console.log("id banco",this.datosCuentaBanco.id,"cheques cuentas",this.listaCheques);
      },
      err => { 
        let msg = err.error;
         M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         
      }
    )
  }
  }


  guardar(forma: NgForm) {
    if ( forma.value.idBanco == null || forma.value.nombreCuenta == ''|| forma.value.idTipoCuenta == null|| forma.value.nroCuenta == null|| forma.value.titular == '' || forma.value.idEstado == null)  {
      console.log(forma.value);
      M.toast({html: 'Faltan completar datos de la Cuenta.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
    } else if (this.updCtaBco) {
      let cuentaBanco = forma.value;
      this.ctaBcoSrv.updateCuentaBanco(this.datosCuentaBanco.id, cuentaBanco).subscribe(
        success => {
          M.toast({html: 'Datos de la Cuenta actualizados correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalCuentaBanco').modal('close');
          this.getLstCuentaBanco();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
      );
    } else {
      let cuentaBanco = forma.value;
      this.ctaBcoSrv.createCuentaBanco(cuentaBanco).subscribe(
        success => {
          M.toast({html: 'Cuenta creada correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalCuentaBanco').modal('close');
          this.getLstCuentaBanco();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
      );
    }
  }

  resetForm() {
    this.updCtaBco = false;
    this.datosCuentaBanco= {
      nombreCuenta: '',
      titular: '',
      idTipoCuenta: null,
      idBanco: null,
      nroCuenta: null,
      idEstado: null,
      estado: '',
      tipoCuenta: '',
      banco: '',
    }
  }

}
