import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../_services/auth.service';
import { CuentaBancoDTO } from '../_dto/CuentaBancoDTO';
import { appPreferences } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class CuentaBancoService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getCuentaBanco(): Observable<CuentaBancoDTO[]> {
    return this.http.get<CuentaBancoDTO[]>(appPreferences.urlBackEnd + 'cuentaBanco', this.auth.getHeaders());
  }

  getCuentaBancoByIdBanco(idBanco: number): Observable<CuentaBancoDTO[]> {
    return this.http.get<CuentaBancoDTO[]>(appPreferences.urlBackEnd + 'cuentaBanco/ByIdBanco/' + idBanco, this.auth.getHeaders());
  }

  getCuentaBancoById(id: number): Observable<CuentaBancoDTO> {
    return this.http.get<CuentaBancoDTO>(appPreferences.urlBackEnd + 'cuentaBanco/' + id, this.auth.getHeaders());
  }


  createCuentaBanco(datos: CuentaBancoDTO): Observable<CuentaBancoDTO> {
    return this.http.post<CuentaBancoDTO>(appPreferences.urlBackEnd + 'cuentaBanco', datos, this.auth.getHeaders());
  }

  updateCuentaBanco(id: number, data: CuentaBancoDTO): Observable<CuentaBancoDTO> {
    return this.http.put<CuentaBancoDTO>(appPreferences.urlBackEnd + 'cuentaBanco/' + id, data, this.auth.getHeaders())
  }

  deleteCuentaBanco(id: number): Observable<CuentaBancoDTO> {
    return this.http.delete<CuentaBancoDTO>(appPreferences.urlBackEnd + 'cuentaBanco/' + id, this.auth.getHeaders())
  }

}
