export interface CuentaBancoDTO {
    id?: number;
    nombreCuenta: string;
    titular: string;
    idTipoCuenta: number;
    idBanco: number;
    nroCuenta: number;
    idEstado: number;
    estado: string;
    tipoCuenta: string;
    banco: string;


}