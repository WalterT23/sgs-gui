import { Component, OnInit } from '@angular/core';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { ListaPreciosService } from './_service/lista-precios.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { DescuentosServiceService } from '../gestion-descuentos/_services/descuentos-service.service'
//DTO
import { appPreferences, environment } from '../../environments/environment';
import { ListaPreciosDTO, LPdescuentosDTO } from './_dto/ListaPreciosDTO';
import { ProductoDTO } from '../gestion-productos/_dto/ProductoDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { DatePipe } from '@angular/common';
import { DescuentoDTO } from '../gestion-descuentos/_dto/DescuentosDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-lista-precios',
  templateUrl: './gestion-lista-precios.component.html',
  styleUrls: ['./gestion-lista-precios.component.css']
})
export class GestionListaPreciosComponent implements OnInit {

  public datosLP: ListaPreciosDTO;
  public listasPrecio: ListaPreciosDTO[];
  public lstProductos: ProductoDTO[];
  public listaTiposLP: InfoRefOpcDTO[];
  public datosLPdscto: LPdescuentosDTO;
  public listaLPdscto: LPdescuentosDTO[];
  public listaDsctos: DescuentoDTO[];
  public updLP: boolean = false;
  public onlyView: boolean = false;
  
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación

  constructor(
    private listaPrecioSrv: ListaPreciosService,
    private productosSrv: ProductosService,
    private infrefSrv: InfoRefService,
    public auth: AuthService,
    private dsctoSrv: DescuentosServiceService
  ) {
    this.lstProductos = [];
    this.listaTiposLP = [];
    this.listasPrecio =[];
    this.listaLPdscto = [];
    this.listaDsctos = [];
    this.datosLP = {
      id: null,
      idTipoLista: null,
      descripcion: '',
      inicioVigencia: null,
      finVigencia: null,
      fechaUltimaActualizacion: null,
      usuarioUltActualizacion: '',
      idSucursal: null,
      tipoListaDTO: null,
      detalles: null,
      costoBase: '',
      calcPrecioVenta: false,
      listaBase: '',
      lpIncompleta: null
    }
    this.datosLPdscto = {
      id: null,
      idListaPrecio: null,
      idDescuento: null,
      inicioVigencia: null,
      finVigencia: null
    }
  }

  ngOnInit() {
    $('select').formSelect();
    
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
    
    $('.modal').modal({
      dismissible : false,
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    this.getLstListaPrecio();
  }

  getTiposLP(){
    this.infrefSrv.getInfoRefOpcByCodRef('TIPOS_LP').subscribe(
      success => {
        this.listaTiposLP = success;
      },
      err => { }
    )
  }

  getLstListaPrecio() {
    this.listaPrecioSrv.getListaPrecio().subscribe(
      success => {
        this.listasPrecio = success;
        console.log ('lista precios',this.listasPrecio);
        
      },
      err => { }
    );
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 50);
    console.log("listado",this.listasPrecio);
  }

  getLstDscto(){
    this.dsctoSrv.getDescuentos().subscribe(
      success => {
        this.listaDsctos = success;
      },
      err => { }
    )
  }

  getInfoLP(id: number){
    this.listaPrecioSrv.getListaPrecioById(id).subscribe(
      success => {

        let dp = new DatePipe('es-PY');
        success.finVigencia = dp.transform(success.finVigencia)
        success.fechaUltimaActualizacion = dp.transform(success.fechaUltimaActualizacion)

        this.datosLP = success;
      },
      err => { }
    );
    this.getTiposLP();
  }

  getLstProductos(){
    this.productosSrv.getProductos().subscribe(
      success => {
        this.lstProductos = success;
        console.log ('lista productos',this.lstProductos);
      },
      err => { }
    );
  }

  addNewLP() {
    this.resetForm();
    this.getLstProductos();
    this.getTiposLP();
    this.datosLP.calcPrecioVenta = false;
    $('select').formSelect();
    setTimeout(() => {
      console.log(this.datosLP);
    }, 50);
  }

  bandera(){
    if (this.datosLP.calcPrecioVenta) {
      this.datosLP.calcPrecioVenta = false;
    }else {
      this.datosLP.calcPrecioVenta = true;
    }
    setTimeout(() => {
      console.log(this.datosLP);
    }, 50);
  }

  guardar(){
    console.log("datos al apretar el btn guardar", this.datosLP);

    if (this.datosLP.idTipoLista == null) {
      M.toast({html:'El tipo de lista no puede ser vacio', displayLength: appPreferences.toastErrorDuration,  classes: 'toastErrorColor'});
    }
    
    if (this.datosLP.inicioVigencia != null){
      this.datosLP.inicioVigencia = ajusteZH(this.datosLP.inicioVigencia);
    }
    if (this.datosLP.finVigencia != null){
      this.datosLP.finVigencia = ajusteZH(this.datosLP.finVigencia); 
    }

    if (this.datosLP.calcPrecioVenta == null){
      this.datosLP.calcPrecioVenta= false;
    }
    console.log("despues de transformar la fecha", this.datosLP);

    if (this.updLP) {
      this.datosLP.fechaUltimaActualizacion = null;
      this.listaPrecioSrv.updateListaPrecio(this.datosLP.id, this.datosLP).subscribe(
        success => {
          M.toast({html:'Datos del producto actualizados correctamente.', displayLength: appPreferences.toastOkDuration,  classes: 'toastOkColor'});
          $('#modalListaPrecio').modal('close');
          this.getLstListaPrecio();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    } else {
      console.log("datosLP a enviar", this.datosLP);
      this.listaPrecioSrv.createListaPrecio(this.datosLP).subscribe(
        success => {
          M.toast({html:'Datos de la LP actualizados correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor'});
          $('#modalListaPrecio').modal('close');
          this.getLstListaPrecio();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    }
  }

  getLstLPdscto(idLP: number){
    this.listaPrecioSrv.getAsoDsctoLpByIdLP(idLP).subscribe(
      success => {
        this.listaLPdscto = success;
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    )
  }

  getDsctoLP(id:number){
    this.getLstDscto();
    this.getLstLPdscto(id);
    this.datosLP.id = id;
  }

  nombreDscto(idDescuento: number){
    let auxLst = this.listaDsctos.filter(row => (row.id == idDescuento));
    return (auxLst.length === 1) ? auxLst[0].descripcion : 'Descuento';
  }
  
  nuevaAso(id:number){
    $('.modal').modal({
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      }
    });
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 50);
    this.datosLPdscto.idListaPrecio = id;
  }

  guardarAsociacion() {
    console.log(this.datosLPdscto);
    let campoNull: boolean = false;
    if (this.datosLPdscto.idDescuento == null || this.datosLPdscto.idListaPrecio == null ) {
      M.toast({ html: 'Debe seleccionar un descuento.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      campoNull = true;
    }
    if (this.datosLPdscto.inicioVigencia != null){
      this.datosLPdscto.inicioVigencia = ajusteZH(this.datosLPdscto.inicioVigencia);
    }
    if (this.datosLPdscto.finVigencia != null){
      this.datosLPdscto.finVigencia = ajusteZH(this.datosLPdscto.finVigencia); 
    }
    if (!campoNull) {
      this.listaPrecioSrv.createNewAso(this.datosLPdscto).subscribe(
        success => {
          M.toast({ html: 'Descuento asociado correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          $('#modalNuevaAsociacion').modal('close');
          this.getLstLPdscto(this.datosLPdscto.idListaPrecio);
          this.resetFormAso();
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          console.log(msg.errorMessage)
        }
      );
    }
  }

  vencerAsoLP(aso: LPdescuentosDTO) {
    var seguro = confirm("Esta seguro que desea cerrar la vigencia de la asociación?");
    console.log(aso);
    if (seguro) {
      this.listaPrecioSrv.vencerVigenciaAso(aso.id).subscribe(
        success => {
          M.toast({ html: 'Realizado con exito', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          this.getLstLPdscto(aso.idListaPrecio);
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      ) 
    }
  }

  resetForm(){
    this.lstProductos = [];
    this.updLP = false;
    this.datosLP = {
      id: null,
      idTipoLista: null,
      descripcion: '',
      inicioVigencia: null,
      finVigencia: null,
      fechaUltimaActualizacion: null,
      usuarioUltActualizacion: '',
      idSucursal: null,
      tipoListaDTO: null,
      detalles: null,
      costoBase: '',
      calcPrecioVenta: false,
      listaBase: '',
      lpIncompleta: null
    }
  }

  resetFormAso(){
    this.datosLPdscto = {
      id: null,
      idListaPrecio: null,
      idDescuento: null,
      inicioVigencia: null,
      finVigencia: null,
      vigente: null
    }
  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }
}
