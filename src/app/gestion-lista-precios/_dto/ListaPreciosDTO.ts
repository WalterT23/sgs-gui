import { InfoRefOpcDTO } from '../../info-ref/_dto/InfoRefDTO';
import { LPdetalleDTO } from '../../gestion-lp-detalles/_dto/LPdetallesDTO';


export interface ListaPreciosDTO {
    id?: number;
	idTipoLista: number;
	descripcion: string;
	inicioVigencia: any;
	finVigencia: any;
	fechaUltimaActualizacion: any;
	usuarioUltActualizacion: string;
	idSucursal: number;
	tipoListaDTO: InfoRefOpcDTO;
	detalles: LPdetalleDTO[];
	costoBase?: string;
	calcPrecioVenta?: Boolean;
	listaBase: string; 
	lpIncompleta?: Boolean;
}

export interface LPdescuentosDTO {
	id?: number;
	idDescuento: number;
	idListaPrecio: number;
	inicioVigencia: any;
	finVigencia: any;
	vigente?: Boolean;
}