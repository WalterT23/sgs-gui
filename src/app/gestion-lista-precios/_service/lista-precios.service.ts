import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ListaPreciosDTO, LPdescuentosDTO } from '../_dto/ListaPreciosDTO';

@Injectable()
export class ListaPreciosService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getListaPrecio(): Observable<ListaPreciosDTO[]> {
    return this.http.get<ListaPreciosDTO[]>(appPreferences.urlBackEnd + 'listaPrecios', this.auth.getHeaders());
  }

  getListaPrecioVigente(): Observable<ListaPreciosDTO[]> {
    return this.http.get<ListaPreciosDTO[]>(appPreferences.urlBackEnd + 'listaPrecios/vigentes', this.auth.getHeaders());
  }

  getListaPrecioById(id: number): Observable<ListaPreciosDTO> {
    return this.http.get<ListaPreciosDTO>(appPreferences.urlBackEnd + 'listaPrecios/' + id, this.auth.getHeaders());
  }

  createListaPrecio (data: ListaPreciosDTO): Observable<ListaPreciosDTO> {
    return this.http.post<ListaPreciosDTO>(appPreferences.urlBackEnd + 'listaPrecios', data, this.auth.getHeaders())
  }

  updateListaPrecio(id: number, data: ListaPreciosDTO): Observable<ListaPreciosDTO> {
    return this.http.put<ListaPreciosDTO>(appPreferences.urlBackEnd + 'listaPrecios/' + id, data, this.auth.getHeaders())
  }

  deleteListaPrecio(id: number): Observable<ListaPreciosDTO> {
    return this.http.delete<ListaPreciosDTO>(appPreferences.urlBackEnd + 'listaPrecios/' + id, this.auth.getHeaders())
  }

  getAsoDsctoLpByIdLP(id:number): Observable<LPdescuentosDTO[]> {
    return this.http.get<LPdescuentosDTO[]>(appPreferences.urlBackEnd + 'descuentosListaPrecios/byIdLp/' + id, this.auth.getHeaders());
  }

  getAsoById(id: number): Observable<LPdescuentosDTO> {
    return this.http.get<LPdescuentosDTO>(appPreferences.urlBackEnd + 'descuentosListaPrecios/' + id, this.auth.getHeaders());
  }

  createNewAso(data: LPdescuentosDTO): Observable<LPdescuentosDTO> {
    return this.http.post<LPdescuentosDTO>(appPreferences.urlBackEnd + 'descuentosListaPrecios', data, this.auth.getHeaders())
  }

  updateAso(id: number, data: ListaPreciosDTO): Observable<ListaPreciosDTO> {
    return this.http.put<ListaPreciosDTO>(appPreferences.urlBackEnd + 'descuentosListaPrecios/' + id, data, this.auth.getHeaders())
  }

  vencerVigenciaAso(id:number): Observable<LPdescuentosDTO> {
    return this.http.put<LPdescuentosDTO>(appPreferences.urlBackEnd + 'descuentosListaPrecios/cerrarVigencia/' + id, this.auth.getHeaders())
  }
}
