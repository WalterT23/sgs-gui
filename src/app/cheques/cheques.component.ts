import { Component, OnInit } from '@angular/core';
import { ChequesDTO } from './_dto/ChequesDTO';
import { BancosDTO } from '../bancos/_dto/BancosDTO';
import { CuentaBancoDTO } from '../cuenta-banco/_dto/CuentaBancoDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { ChequesService } from './_services/cheques.service';
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { appPreferences } from '../../environments/environment';
import { BancosService } from '../bancos/services/bancos.service';
import { CuentaBancoService } from '../cuenta-banco/_services/cuenta-banco.service';
import { NgForm } from '@angular/forms';


declare var $: any;
declare var M: any;


@Component({
  selector: 'app-cheques',
  templateUrl: './cheques.component.html',
  styleUrls: ['./cheques.component.css']
})
export class ChequesComponent implements OnInit {
  public datosCheque: ChequesDTO;
  public listaCheques: ChequesDTO[];
  public updCheques: boolean = false;
  public search: boolean = false;
  public listaBancos: BancosDTO[];
  public listaCuentaBanco: CuentaBancoDTO[];
  public listaEstados: InfoRefOpcDTO[];
  public listaTipoCheque : InfoRefOpcDTO[];


  constructor(
  private cheqSrv: ChequesService,
  private bancoSrv: BancosService,
  private ctaBcoSrv: CuentaBancoService,
  private infrefSrv: InfoRefService,
  public auth: AuthService,
  private util: UtilsService

  ) { 

  this.listaCuentaBanco = [] ;
  this.listaEstados = [];
  this.listaTipoCheque = [];
  this.listaBancos= [];
  
  this.datosCheque= {
  idCuentaBanco: null,
  serie: '',
  numeroInicial: '',
  numeroFinal: '',
  idEstado: null,
  idTipoCheque: null,
  ultNumero: '',
  idBanco: null,
  tipoCheque: '',
  banco: '',
  cuentaBanco: '',
  estado: ''
}
  }



  ngOnInit() {
    
    $('select').formSelect();

    setTimeout(() => {
      $('.tooltipped').tooltip();  
    }, 50);
    
    $('.modal').modal({
      dismissible : false,
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: function () {
        $('.modal-close').click();
      }
    });
 
    
    this.getLstCheques();
    
   
    
  }

  addNewChequera(){
    this.resetForm();
    this.getLstBancos();
    this.getLstEstados();
    this.getLstCtaBanco();
    this.getLstTipoCheque();
    $('select').formSelect();

  }

  getLstCheques() {
    this.cheqSrv.getCheques().subscribe(
      success => {
        this.listaCheques = success;
        console.log(this.listaCheques);
      },
      err => { }
    );
  }

  getInfoCheques(id: number) {
    $('select').formSelect();
    M.updateTextFields()

    this.updCheques = true;
    
    this.cheqSrv.getChequesById(id).subscribe(
      success => {
        this.datosCheque = success;
        setTimeout(() => {
          console.log(this.datosCheque)
          this.ctaBcoSrv.getCuentaBancoByIdBanco(this.datosCheque.idBanco).subscribe(
            success => {
              this.listaCuentaBanco= success;
            },
            err => { }
          );
              
        }, 150);
        
      },
      err => { }
    );
    this.bancoSrv.getBancos().subscribe(
      success => {
        this.listaBancos = success;
        console.log(this.listaBancos);
      },
      err => { }
    );

    
    this.infrefSrv.getInfoRefOpcByCodRef('TIPO_CQUE').subscribe(
      success => {
        this.listaTipoCheque = success;
      },
      err => { }
    );
    
        
    this.infrefSrv.getInfoRefOpcByCodRef('ESTCTA').subscribe(
      success => {
        this.listaEstados = success;
      },
      err => { }
    );

  }


  getLstBancos() {

    this.listaBancos = [];

    this.bancoSrv.getBancos().subscribe(
      success => {
        this.listaBancos = success;
        console.log(this.listaBancos);

        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      },
      err => {
        $('select').formSelect();
        M.toast("No se pudieron obtener los datos del Banco.", appPreferences.toastErrorDuration, 'toastErrorColor');
      }
    );
  }


  getLstEstados() {

    this.listaEstados = [];

    this.infrefSrv.getInfoRefOpcByCodRef('ESTCTA').subscribe(
      success => {
        this.listaEstados = success;
      },
      err => { }
    );
  }

  getLstTipoCheque() {

    this.listaTipoCheque = [];

    this.infrefSrv.getInfoRefOpcByCodRef('TIPO_CQUE').subscribe(
      success => {
        this.listaTipoCheque = success;
        
      },
      err => { }
    );
  }

  getLstCtaBanco() {

    this.listaCuentaBanco = [];
    console.log("hola",this.datosCheque.idBanco)

    if (this.datosCheque.idBanco != null) {
      this.ctaBcoSrv.getCuentaBancoByIdBanco(this.datosCheque.idBanco).subscribe (
        success => {
          this.listaCuentaBanco = success;
          console.log("id banco",this.datosCheque.idBanco,"cuentas por banco",this.listaCuentaBanco);
        },
        err => { }
      )
    }
  }


  guardar(forma: NgForm) {
    console.log("datos cheque",this.datosCheque)
    if ( forma.value.idCuentaBanco == null || forma.value.serie == ''|| forma.value.numeroInicial == null|| forma.value.numeroFinal == null|| forma.value.idTipoCheque == null 
      || forma.value.idEstado == null || forma.value.idBanco == null )  {
      console.log(forma.value);
      M.toast({html: 'Faltan completar datos del Cheque.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
    } else if (this.updCheques) {
      let  cheque= forma.value;
      this.cheqSrv.updateCheques(this.datosCheque.id, cheque).subscribe(
        success => {
          M.toast({html: 'Datos de la cuentaBanco actualizados correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalCheque').modal('close');
          this.getLstCheques();
        },
        err => { 
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         }
      );
    } else {
      let cheque = forma.value;
      this.cheqSrv.createCheques(cheque).subscribe(
        success => {
          M.toast({html: 'Cheque creado correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalCheque').modal('close');
          this.getLstCheques();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
         }
      );
    }
  }

  resetForm() {
    this.updCheques = false;
    this.datosCheque= { 
      idCuentaBanco: null,
      serie: '',
      numeroInicial: '',
      numeroFinal: '',
      idEstado: null,
      idTipoCheque: null,
      ultNumero: '',
      idBanco: null,
      tipoCheque: '',
      banco: '',
      cuentaBanco: '',
      estado: ''
    }
  }




}
