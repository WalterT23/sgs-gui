export interface ChequesDTO {
    id?: number;
    idBanco: number;
    idCuentaBanco: number;
    serie: string;
    numeroInicial: string;
    numeroFinal: string;
    ultNumero: string;
    idEstado: number;
    idTipoCheque: number;
    tipoCheque: string;
    banco: string;
    cuentaBanco: string;
    estado: string;
}