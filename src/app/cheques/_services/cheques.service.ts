import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../_services/auth.service';
import { ChequesDTO } from '../_dto/ChequesDTO';
import { appPreferences } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ChequesService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getCheques(): Observable<ChequesDTO[]> {
    return this.http.get<ChequesDTO[]>(appPreferences.urlBackEnd + 'cheques', this.auth.getHeaders());
  }

  getChequesByIdBanco(idBanco:number): Observable<ChequesDTO[]> {
    return this.http.get<ChequesDTO[]>(appPreferences.urlBackEnd + 'cheques/ByIdBanco/'+ idBanco, this.auth.getHeaders());
  }

  getChequesByIdCuenta(IdCuentaBanco:number): Observable<ChequesDTO[]> {
    return this.http.get<ChequesDTO[]>(appPreferences.urlBackEnd + 'cheques/ByIdCuenta/'+ IdCuentaBanco, this.auth.getHeaders());
  }


  getChequesById(id: number): Observable<ChequesDTO> {
    return this.http.get<ChequesDTO>(appPreferences.urlBackEnd + 'cheques/' + id, this.auth.getHeaders());
  }

  createCheques(datos: ChequesDTO): Observable<ChequesDTO> {
    return this.http.post<ChequesDTO>(appPreferences.urlBackEnd + 'cheques', datos, this.auth.getHeaders());
  }

  updateCheques(id: number, data: ChequesDTO): Observable<ChequesDTO> {
    return this.http.put<ChequesDTO>(appPreferences.urlBackEnd + 'cheques/' + id, data, this.auth.getHeaders())
  }

  deleteCheques(id: number): Observable<ChequesDTO> {
    return this.http.delete<ChequesDTO>(appPreferences.urlBackEnd + 'cheques/' + id, this.auth.getHeaders())
  }

}
