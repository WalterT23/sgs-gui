import { TestBed, inject } from '@angular/core/testing';

import { UnidadMedidaService } from './unidad-medida.service';

describe('UnidadMedidaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnidadMedidaService]
    });
  });

  it('should be created', inject([UnidadMedidaService], (service: UnidadMedidaService) => {
    expect(service).toBeTruthy();
  }));
});
