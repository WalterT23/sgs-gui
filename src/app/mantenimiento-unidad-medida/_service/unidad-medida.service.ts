import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UnidadMedDTO } from '../_dto/UnidadMedDTO';

@Injectable()
export class UnidadMedidaService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getUniMed(): Observable<UnidadMedDTO[]> {
    return this.http.get<UnidadMedDTO[]>(appPreferences.urlBackEnd + 'productosUniMed', this.auth.getHeaders());
  }

  getUniMedbyId(id:number): Observable<UnidadMedDTO> {
    return this.http.get<UnidadMedDTO>(appPreferences.urlBackEnd + 'productosUniMed/' + id, this.auth.getHeaders());
  }

  createUniMed(datos: UnidadMedDTO): Observable<UnidadMedDTO> {
    return this.http.post<UnidadMedDTO>(appPreferences.urlBackEnd + 'productosUniMed', datos, this.auth.getHeaders());
  }

  updateUniMed(id: number, data: UnidadMedDTO): Observable<UnidadMedDTO> {
    return this.http.put<UnidadMedDTO>(appPreferences.urlBackEnd + 'productosUniMed/' + id, data, this.auth.getHeaders())
  }

  deleteUniMed(id: number): Observable<UnidadMedDTO> {
    return this.http.delete<UnidadMedDTO>(appPreferences.urlBackEnd + 'productosUniMed/' + id, this.auth.getHeaders())
  }

}
