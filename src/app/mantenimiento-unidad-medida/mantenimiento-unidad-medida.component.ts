import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { UnidadMedidaService } from './_service/unidad-medida.service';

//DTO
import { UnidadMedDTO } from './_dto/UnidadMedDTO';
import { appPreferences } from '../../environments/environment';

declare var $: any;
declare var M: any;


@Component({
  selector: 'app-mantenimiento-unidad-medida',
  templateUrl: './mantenimiento-unidad-medida.component.html',
  styleUrls: ['./mantenimiento-unidad-medida.component.css']
})
export class MantenimientoUnidadMedidaComponent implements OnInit {

  @Input() mantenimiento: boolean = false;
  
  public uniMed: UnidadMedDTO;
  public listaUnimed: UnidadMedDTO[];

  constructor(
    private uniMedSrv: UnidadMedidaService,
    private util: UtilsService,
    public auth: AuthService
  ) {
    this.uniMed = {
      descripcion: ''
    }; 
  }

  ngOnInit() {
    $('select').formSelect();

    if (!this.mantenimiento) {

      setTimeout(() => {
        $('.modal').modal({
          dismissible : false,
          onOpenStart: function (modal, trigger) {
            M.updateTextFields();
            $('select').formSelect();

            console.log(modal, trigger);
          },
          complete: function () {
            $('#btnCancelModalUnimed').click();
          }
        });
      }, 60);
      this.getLstUniMed();
    } 
  }

  getLstUniMed () {
    this.uniMedSrv.getUniMed().subscribe(
      success => {
        this.listaUnimed = success;
      },
      err => { }
    )
  }

  guardar(forma: NgForm) {
    
    let newUniMed = forma.value;

    this.uniMedSrv.createUniMed(newUniMed).subscribe(
      success => {
        M.toast({html:'Unidad de medida agregada correctamente', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
        $('#modalUniMed').modal('close');
        this.getLstUniMed();
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  resetForm(){
    this.uniMed = {
      descripcion: ''
    };
  }

}
