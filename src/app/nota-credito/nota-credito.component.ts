import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';


// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { GestionVentasService } from '../gestion-ventas/_services/gestionVentas.service';
import { NotaCreditoVentaService } from './_services/nota-credito-venta.service';
import { ClientesService } from '../gestion-clientes/_service/clientes.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';
import { StockService } from '../gestion-stock/_service/stock.service';
import { saveAs } from 'file-saver';

// DTO
import { NCreditoVentaDTO, NCreditoVentaDetaleDTO } from './_dto/NCreditoVentaDTO';
import { VentasDTO, VentaDetallesDTO } from '../gestion-ventas/_dto/VentasDTO';
import { ClienteDTO } from '../gestion-clientes/_dto/ClienteDTO';
import { ProductoDTO, ProductoVentaDTO } from '../gestion-productos/_dto/ProductoDTO';
import { GestionClientesComponent } from '../gestion-clientes/gestion-clientes.component';
import { StockDTO } from '../gestion-stock/_dto/StockDTO';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { generarBlobForPDF } from '../_utils/millard-formatter';
import { TotalNCventa } from '../pipes/sumas.pipe';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-nota-credito',
  templateUrl: './nota-credito.component.html',
  styleUrls: ['./nota-credito.component.css']
})
export class NotaCreditoComponent implements OnInit {

  public datosNotaCre: NCreditoVentaDTO;
  public listaNCventa: NCreditoVentaDTO[];
  public datosNCdetalle: NCreditoVentaDetaleDTO;
  public listaNCVdetalles: NCreditoVentaDetaleDTO[];
  public datosFV: VentasDTO;
  public detalleFV: VentaDetallesDTO;
  public listaDetFV: VentaDetallesDTO[];
  nroFactura: string = '';
  fechaHoy: string;

  constructor(
    private ncvSrv: NotaCreditoVentaService,
    public auth: AuthService,
    private infoRef: InfoRefService,
    private ventaSrv: GestionVentasService,

  ) {
    let dp = new DatePipe('es-PY');
    let cur = new CurrencyPipe('es-PY'); //para transformar el monto
    this.fechaHoy = dp.transform(new Date(), 'yyyy-MM-dd');
    this.listaDetFV = [];
    this.listaNCventa = [];
    this.listaNCVdetalles = [];
    this.datosFV = {
      id: null,
      idTimbradoPtoExp: null,
      timbradoPtoExpedicion: null,
      nroFactura: null,
      fechaCreacion: null,
      cajero: '',
      gravada10: null,
      gravada5: null,
      exenta: null,
      iva10: null,
      iva5: null,
      descuento: null,
      neto: null,
      idCliente: null,
      cliente: null,
      idAperturaCierre: null,
      idSucursal: null,
      detalles: [],
      formasPago: [],
      empresa: null
    }
    this.detalleFV = {
      id: null,
      idCabVenta: null,
      idProducto: null,
      cantidad: null,
      precioTotal: null,
      tipoIdentificadorFiscal: '',
      porcentajeDescuento: null,
      montoDescuento: null,
      precioUnitario: null,
      producto: {
        cod: '',
        nombre: '',
        descripcion: '',
        idUnidadMed: null,
        unidadMed: '',
        stockMin: null,
        idGrupo: null,
        grupo: '',
        idMarca: null,
        marca: '',
        idTipoTributo: null,
        tipoTributo: '',
        abvTributo: '',
        ultCostoCompra: null,
        porcRecarga: null
      }
    }
    this.datosNotaCre = {
      id: null,
      idCabeceraVenta: null,
      idTimbradoPtoExp: null,
      idCliente: null,
      cliente: {
        ruc: '',
        razonSocial: '',
        direccion: '',
        telefono: '',
        correo: '',
        fechaRegistro: null,
        fechaModificacion: null,
        idEstado: null,
        estado: '',
        nombreFantasia: ''
      },
      nroNotaCredito: '',
      iva5: null,
      iva10: null,
      exentas: null,
      montoTotal: null,
      fechaCreacion: null,
      usuarioCreacion: '',
      idSucursal: null,
      fechaFactura: null,
      detalles: []
    }
    this.datosNCdetalle = {
      id: null,
      idNotaCreditoVenta: null,
      idProducto: null,
      cantidad: null,
      iva5: null,
      iva10: null,
      total: null,
      observaciones: '',
      producto: {
        cod: '',
        nombre: '',
        descripcion: '',
        idUnidadMed: null,
        unidadMed: '',
        stockMin: null,
        idGrupo: null,
        grupo: '',
        idMarca: null,
        marca: '',
        idTipoTributo: null,
        tipoTributo: '',
        abvTributo: '',
        ultCostoCompra: null,
        porcRecarga: null
      }
    }
  }

  ngOnInit() {

    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.tabs').tabs();
      M.updateTextFields();
    }, 100);

  }

  buscarFacturaXnroFactura() {
    let aux = new DatePipe('es-PY');
    this.ventaSrv.getCabeceraVentaByNroFt(this.nroFactura).subscribe(
      success => {
        this.datosFV = success;
        this.datosNotaCre.idCabeceraVenta = success.id;
        this.datosNotaCre.idCliente = success.idCliente;
        this.datosNotaCre.cliente = success.cliente;
        this.datosNotaCre.montoTotal = success.neto;
        this.datosNotaCre.fechaFactura = aux.transform(success.fechaCreacion, 'dd-MM-yyyy')
        this.datosNotaCre.detalles = [];

        success.detalles.forEach(det => {
          this.datosNCdetalle.idProducto = det.idProducto;
          this.datosNCdetalle.cantidad = det.cantidad;
          this.datosNCdetalle.total = det.precioTotal;
          this.datosNCdetalle.precioUni = (det.precioTotal / det.cantidad);
          this.datosNCdetalle.producto = det.producto;
          this.datosNotaCre.detalles.push(this.datosNCdetalle);
          this.resetNCVdet();
        });

        console.log(this.datosNotaCre);
      }
    );
  }

  registrarNC() {
    this.datosNotaCre.fechaFactura = null;
    let totalNC: number = 0;

    this.datosNotaCre.detalles.forEach(element => {
      element.total = element.cantidad * element.precioUni;
      totalNC = totalNC + element.total;
    });
    this.datosNotaCre.montoTotal = totalNC;

    console.log('datos antes de llamar al servicio', this.datosNotaCre);

    this.ncvSrv.createNCventa(this.datosNotaCre).subscribe(
      success => {

        let resul = confirm(`Desea imprimir la Nota de Credito ${success.nroNotaCredito}?`);

        if (resul) {
          this.imprimirNotaCredito(success.id);
        }
        this.resetForm(); this.resetNCVdet();
      }
    );
  }

  imprimirNotaCredito(id: number) {
    this.ncvSrv.imprimirNotaCredito(id).subscribe(
      success => {

        let pdf = generarBlobForPDF(success.bytes);
        saveAs(pdf, success.fileName + ".pdf");
        console.log(success.type);
      }
    );
  }

  removeRegistro(pos: number) {
    this.datosNotaCre.detalles.splice(pos, 1);
  }

  resetNCVdet(){
    this.datosNCdetalle = {
      id: null,
      idNotaCreditoVenta: null,
      idProducto: null,
      cantidad: null,
      iva5: null,
      iva10: null,
      total: null,
      observaciones: '',
      producto: {
        cod: '',
        nombre: '',
        descripcion: '',
        idUnidadMed: null,
        unidadMed: '',
        stockMin: null,
        idGrupo: null,
        grupo: '',
        idMarca: null,
        marca: '',
        idTipoTributo: null,
        tipoTributo: '',
        abvTributo: '',
        ultCostoCompra: null,
        porcRecarga: null
      }
    }
  }

  resetForm(){
    this.nroFactura = '';
    this.datosNotaCre = {
      id: null,
      idCabeceraVenta: null,
      idTimbradoPtoExp: null,
      idCliente: null,
      cliente: {
        ruc: '',
        razonSocial: '',
        direccion: '',
        telefono: '',
        correo: '',
        fechaRegistro: null,
        fechaModificacion: null,
        idEstado: null,
        estado: '',
        nombreFantasia: ''
      },
      nroNotaCredito: '',
      iva5: null,
      iva10: null,
      exentas: null,
      montoTotal: null,
      fechaCreacion: null,
      usuarioCreacion: '',
      idSucursal: null,
      fechaFactura: null,
      detalles: []
    }
  }
}
