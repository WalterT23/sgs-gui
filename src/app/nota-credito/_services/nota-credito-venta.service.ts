import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { NCreditoVentaDTO, NCreditoVentaDetaleDTO } from '../_dto/NCreditoVentaDTO';

@Injectable()
export class NotaCreditoVentaService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getListNCventas(): Observable<NCreditoVentaDTO[]> {
    return this.http.get<NCreditoVentaDTO[]>(appPreferences.urlBackEnd + 'notaCreditoVenta', this.auth.getHeaders());
  }

  getNCventaById(id:number): Observable<NCreditoVentaDTO> {
    return this.http.get<NCreditoVentaDTO>(appPreferences.urlBackEnd + 'notaCreditoVenta/' + id, this.auth.getHeaders());
  }

  createNCventa(datos: NCreditoVentaDTO): Observable<NCreditoVentaDTO> {
    return this.http.post<NCreditoVentaDTO>(appPreferences.urlBackEnd + 'notaCreditoVenta', datos, this.auth.getHeaders());
  }

  updateNCventa(id: number, data: NCreditoVentaDTO): Observable<NCreditoVentaDTO> {
    return this.http.put<NCreditoVentaDTO>(appPreferences.urlBackEnd + 'notaCreditoVenta/' + id, data, this.auth.getHeaders())
  }

  deleteNCventa(id: number): Observable<NCreditoVentaDTO> {
    return this.http.delete<NCreditoVentaDTO>(appPreferences.urlBackEnd + 'notaCreditoVenta/' + id, this.auth.getHeaders())
  }

  imprimirNotaCredito(id: number): Observable<any> {
    return this.http.get<any>(`${appPreferences.urlBackEnd}/notaCreditoVenta/imprimir/${id}`, this.auth.getHeaders());
  }

// Servicios de los detalles de nota de credito 

  getListNCventasDet(): Observable<NCreditoVentaDetaleDTO[]> {
    return this.http.get<NCreditoVentaDetaleDTO[]>(appPreferences.urlBackEnd + 'notaCreditoVentaDetalle', this.auth.getHeaders());
  }

  getNCventaDetById(id:number): Observable<NCreditoVentaDetaleDTO> {
    return this.http.get<NCreditoVentaDetaleDTO>(appPreferences.urlBackEnd + 'notaCreditoVentaDetalle/' + id, this.auth.getHeaders());
  }

  createNCventaDet(datos: NCreditoVentaDTO): Observable<NCreditoVentaDTO> {
    return this.http.post<NCreditoVentaDTO>(appPreferences.urlBackEnd + 'notaCreditoVentaDetalle', datos, this.auth.getHeaders());
  }

  updateNCventaDet(id: number, data: NCreditoVentaDTO): Observable<NCreditoVentaDTO> {
    return this.http.put<NCreditoVentaDTO>(appPreferences.urlBackEnd + 'notaCreditoVentaDetalle/' + id, data, this.auth.getHeaders())
  }

  deleteNCventaDet(id: number): Observable<NCreditoVentaDTO> {
    return this.http.delete<NCreditoVentaDTO>(appPreferences.urlBackEnd + 'notaCreditoVentaDetalle/' + id, this.auth.getHeaders())
  }

}
