import { ProductoDTO } from "../../gestion-productos/_dto/ProductoDTO";
import { ClienteDTO } from "../../gestion-clientes/_dto/ClienteDTO";
import { TimbradoDTO } from "src/app/gestion-timbrados/_dto/TimbradoDTO";
import { PuntoExpDTO } from "src/app/gestion-puntos-expedicion/_dto/PuntoExpDTO"

export interface NCreditoVentaDTO {
    id?: number;
    idCabeceraVenta: number;
    nroNotaCredito?: string;
    montoTotal: number;
    iva5?: number;
    iva10?: number;
    exentas?: number;
    fechaCreacion?: Date;
    usuarioCreacion?: string;
    idTimbradoPtoExp?: number;
    idCliente: number;
    cliente: ClienteDTO;
    idSucursal?: number;
    fechaFactura?: any;
    detalles?: NCreditoVentaDetaleDTO[];
    empresa?: EmpresaDTO;
    timbradoPtoExpedicion?: TimbradoPuntoExpedicionDTO
}

export interface NCreditoVentaDetaleDTO {
    id?: number;
    idNotaCreditoVenta: number;
    idProducto: number;
    iva5?: number;
    iva10?: number;
    precioUni?: number;
    total?: number;
    observaciones?:string;
    cantidad: number;
    producto?: ProductoDTO;
}

export interface EmpresaDTO {
    id: number;
	ruc: string;
	razonSocial: string;
	nombreFantasia: string;
	direccion: string;
	telefono: string;
}

export interface TimbradoPuntoExpedicionDTO {
    timbrado: TimbradoDTO;
    puntoExpedicion: PuntoExpDTO;
}

