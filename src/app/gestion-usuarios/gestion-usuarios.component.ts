import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';

// Servicios
import { UsuarioServiceService } from './_services/usuario-service.service';
import { RolService } from '../gestion-roles/_services/rol.service';
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { SucursalesService } from '../sucursales/_services/sucursales.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';

// DTO
import { UsuarioDTO, GstRolDTO, UsuSucursalesDTO } from './_dto/UsuariosDTO';
import { RolDTO } from '../gestion-roles/_dto/RolDTO';
import { SucursalesDTO } from '../sucursales/_dto/SucursalesDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';




declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-usuarios',
  templateUrl: './gestion-usuarios.component.html',
  styleUrls: ['./gestion-usuarios.component.css']
})
export class GestionUsuariosComponent implements OnInit {

  public datosUsuario: UsuarioDTO;//datos de un usuario
  public listaUsuarios: any[];//lista de todos los usuarios del sistema
  public updUsr: boolean = false; // bandera para indicar si se hace una modificacion de un usr o se crea un usr nuevo
  public listaRoles: RolDTO[]; //lista de todos los roles existentes en la BD
  public rolesUsuario: RolDTO[]; //lista de roles asignados al usuario
  public rolesDispUsuario: RolDTO[]; //lista de roles NO asignadas al usuario
  public datosRol: any[] = [];
  public rolAsigLocal: any[] = [];
  public repeatPass;
  public listaSucursales: SucursalesDTO[]; //lista de sucursales existentes
  public lstSucursales: any[] = [];
  public sucursalesDispUsuario: SucursalesDTO[]; //lista de sucursales NO asignadas al usuario
  public sucursalesUsuario: UsuSucursalesDTO[]; //lista de sucursales por usuario
  public newAso: UsuSucursalesDTO;
  public incVencidas: boolean = false;
  public listaEstados: InfoRefOpcDTO[];

  // totalReg: number = 0; //total de registros de usuarios existentes
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  sortProperty: string = '';
  sortOrder = 2;
  buscarTexto = "";

  constructor(
    private usuSrv: UsuarioServiceService,
    private rolSrv: RolService,
    private sucSrv: SucursalesService,
    private infrefSrv: InfoRefService,
    public auth: AuthService,
    private util: UtilsService
  ) {

    this.listaUsuarios = [];
    this.listaRoles = [];
    this.rolesUsuario = [];
    this.rolesDispUsuario = [];
    this.listaSucursales = [];
    this.sucursalesDispUsuario = [];
    this.sucursalesUsuario = [];
    this.listaEstados = [];

    this.datosUsuario = {
      nombre: '',
      apellido: '',
      usuario: '',
      pass: '',
      repeatpass: '',
      idEstado: null,
      estado: '',
      sucursales: null,
      funcionalidades: null,
      roles: null,
      accesstoken: ''
    }

    this.newAso = {
      id: null,
      idUsuario: null,
      idSucursal: null,
      codSucursal: null,
      nombreSucursal: '',
      inicioVigencia: null,
      finVigencia: null,
      fechaRegistro: null
    }

  }

  ngOnInit() {

    $('select').formSelect();

    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);

    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: function () {
        $('.modal-close').click();
      }
    });

    this.getLstUsuarios();

    this.datosUsuario.usuario = null;
    this.datosUsuario.pass = null;

  }

  getEstados() {
    this.infrefSrv.getInfoRefOpcByCodRef('Estado').subscribe(
      success => {
        this.listaEstados = success;
      },
      err => { }
    )
  }

  mostrar_cambios() {
    console.log(this.lstSucursales);
  }

  addNewUser() {
    this.resetForm();
    this.getLstSucursales();
    $('select').formSelect();
  }

  nuevaAso(idUsuario: number) {
    $('.modal').modal({
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      }
    });
    this.newAso.idUsuario = idUsuario;
  }

  getLstUsuarios(pagina?: number, cantidad?: number) {
    // this.usuSrv.getUsuarios(pagina ? pagina : 0, cantidad ? cantidad : 0).subscribe(
    this.usuSrv.getUsuarios().subscribe(
      success => {
        this.listaUsuarios = success

        this.listaUsuarios.forEach(element => {
          element.sucursales = element.sucursales.filter(row => (row.vigente == true));
        });
        this.getEstados();
        // if (!pagina && !cantidad) {
        //   this.totalReg = this.listaUsuarios.length;
        //   console.log('cantreg', this.totalReg);
        // }
      },
      err => { }
    );

  }
  nombreEstado(idEstado: number) {
    let auxLst = this.listaEstados.filter(row => (row.id == idEstado));
    return (auxLst.length === 1) ? auxLst[0].descripcion : 'Verificar Estado';
  }

  getDatosUsuario(id: number) {
    this.usuSrv.getUserById(id).subscribe(
      success => {
        this.datosUsuario = success;
      },
      err => {
      }
    );
  }

  getRoles() {

    this.rolSrv.getRoles().subscribe(
      success => {
        this.listaRoles = success;
      },
      err => { }
    );

  }

  getLstSucursales() {
    this.sucSrv.getSucursales().subscribe(
      success => {
        this.listaSucursales = success;
      },
      err => { }
    );
    console.log(this.listaSucursales);
  }

  resetForm() {
    this.updUsr = false;
    this.datosUsuario = {
      nombre: '',
      apellido: '',
      usuario: '',
      pass: '',
      repeatpass: '',
      idEstado: null,
      estado: '',
      sucursales: null,
      funcionalidades: null,
      roles: null,
      accesstoken: ''
    }
    this.newAso = {
      id: null,
      idUsuario: null,
      idSucursal: null,
      codSucursal: null,
      nombreSucursal: '',
      inicioVigencia: null,
      finVigencia: null,
      fechaRegistro: null
    }
    this.rolAsigLocal = [];
    this.datosRol = [];
    this.listaSucursales;
    this.lstSucursales;
  }

  returnError(atributo: string, forma: NgForm): string {
    let msgInfo: string = '';
    switch (atributo) {
      case 'nombre':
        break;
      default:
        break;
    }
    return msgInfo;
  }

  getInfoUser(id: number) {
    $('select').formSelect();
    // M.updateTextFields();
    this.usuSrv.getUserById(id).subscribe(
      success => {
        this.datosUsuario = success;
        if (this.updUsr) {
          this.datosUsuario.pass = null;
          this.datosUsuario.repeatpass = null;
        }
      },
      err => {
      }
    );
    this.usuSrv.getRolesByUser(id).subscribe(
      success => {
        this.rolesUsuario = success;
      },
      err => {
      }
    )
    this.usuSrv.getRolesDispByUser(id).subscribe(
      success => {
        this.rolesDispUsuario = success;
      },
      err => {
      }
    )

    console.log(this.lstSucursales, "sucdisp", this.sucursalesDispUsuario);
  }

  getInfoUserSucursal(idUsuario: number) {
    this.incVencidas = false;
    this.getLstSucursales();
    this.getDatosUsuario(idUsuario);
    console.log(this.datosUsuario);
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('select').formSelect();
    }, 100);

    this.usuSrv.getUserById(idUsuario).subscribe(
      success => {
        this.sucursalesUsuario = success.sucursales.filter(row => (row.vigente == true));

        console.log('sucursalesXusuario', this.sucursalesUsuario)
      },
      err => {
      }
    );

    this.sucSrv.sucDispByUsuario(idUsuario).subscribe(
      success => {
        this.sucursalesDispUsuario = success;
        console.log('sucDispByUsuario', this.sucursalesDispUsuario);
      },
      err => {
      }
    )
  }

  sucursalNombre(id: number) {
    let auxLst = this.listaSucursales.filter(row => (row.id == id));
    return (auxLst.length === 1) ? auxLst[0].descripcion : 'Sin Nombre';
  }

  buscarAsociaciones(idUsuario: number) {
    console.log('entroAca', this.incVencidas);
    this.getLstSucursales();
    if (this.incVencidas) {
      this.usuSrv.getUsuAllSucs(idUsuario).subscribe(
        success => {
          this.sucursalesUsuario = success;

        }
      )
    } else {
      this.usuSrv.getUsuSucs(idUsuario).subscribe(
        success => {
          this.sucursalesUsuario = success;
        }
      )
    }
    console.log(this.sucursalesUsuario);
  }

  vencerAsoSucursal(aso: UsuSucursalesDTO) {
    var seguro = confirm("Esta seguro que desea cerrar la vigencia?");
    if (seguro) {
      this.usuSrv.vencerAso(aso.id).subscribe(
        success => {
          M.toast({ html: 'Realizado con exito', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          this.getInfoUserSucursal(aso.idUsuario);
          this.getLstUsuarios();
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      )
    }
  }

  guardar(forma: NgForm) {
    let campoNull: boolean = false;
    if (forma.value.nombre == "") {
      M.toast({ html: 'El campo Nombre no puede estar vacio.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      campoNull = true;
    } else if (forma.value.apellido == "") {
      M.toast({ html: 'El campo Apellido no puede estar vacio.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      campoNull = true;
    } else if (forma.value.usuario == null) {
      M.toast({ html: 'El campo Usuario no puede estar vacio.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      campoNull = true;
    }

    if (!campoNull) {
      if (this.updUsr) {

        let usuario = forma.value;
        let passOK: boolean = false;
        console.log(usuario);

        if (usuario.pass != null && usuario.repeatPass != null) {
          console.log('entro aqui');
          if (usuario.pass === usuario.repeatPass) {
            delete usuario.repeatPass;
            passOK = true;
          } else {
            console.log('entro aqui2');
            M.toast({ html: 'Las contraseñas no son iguales.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          }
        } else {
          passOK = true;
        }
        if (passOK == true) {
          this.usuSrv.updateUsuario(this.datosUsuario.id, usuario).subscribe(
            success => {
              M.toast({ html: 'Datos del usuario actualizado correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
              $('#modalUsuario').modal('close');
              this.getLstUsuarios();
            },
            err => {
              let msg = err.error;
              M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
            }
          );
        }

      } else {
        let pass = forma.value.pass;
        let repeatPass = forma.value.repeatPass;

        if (pass === repeatPass) {
          let usuario = forma.value;
          delete usuario.repeatPass;

          this.usuSrv.createUsuario(usuario).subscribe(

            success => {
              M.toast({ html: 'Usuario creado correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
              $('#modalUsuario').modal('close');
              this.getLstUsuarios();
            },
            err => {
              let msg = err.error;
              M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
            }
          );
        } else {
          M.toast({ html: 'Las contraseñas no son iguales.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      }
    }
    this.resetForm();
  }

  guardarAsociacion() {
    console.log(this.newAso);
    let campoNull: boolean = false;
    if (this.newAso.idSucursal == null) {
      M.toast({ html: 'Debe seleccionar una sucursal.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      campoNull = true;
    }
    if (!campoNull) {
      this.usuSrv.asoUsuSuc(this.newAso).subscribe(
        success => {
          M.toast({ html: 'Sucursal asociada correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          $('#modalNuevaAsociacion').modal('close');
          this.getInfoUserSucursal(this.newAso.idUsuario);
          this.getLstUsuarios();
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      );
    }
  }

  agregarRol(idUsuario: number) {

    if (this.datosRol) {
      for (let elItem = 0; elItem < this.datosRol.length; elItem++) {
        const item = this.datosRol[elItem];

        let dato: GstRolDTO = {
          idUsuario: idUsuario,
          idRol: parseInt(item)
        };

        this.usuSrv.asoRol(dato).subscribe(
          success => { },
          err => { }
        );
      }
    }


    let newLst: RolDTO[] = [];

    if (this.rolesDispUsuario) {
      for (let index = 0; index < this.rolesDispUsuario.length; index++) {
        const allRolItem = this.rolesDispUsuario[index];

        let flagExiste: boolean = false;
        if (this.datosRol) {
          for (let i = 0; i < this.datosRol.length; i++) {
            const rolSelectedItem = this.datosRol[i];

            if (allRolItem.id.toString() == rolSelectedItem) {
              flagExiste = true;
              this.rolesUsuario.push(allRolItem);
              break;
            }
          }
        }
        if (!flagExiste) {
          newLst.push(allRolItem);

        }
      }
    }


    newLst.sort();
    this.rolesDispUsuario = newLst; //se carga la lista de roles disponible con los datos actualizados
    this.datosRol = [];

  }

  removerRol(idUsuario: number) {

    if (this.rolAsigLocal) {
      for (let elItem = 0; elItem < this.rolAsigLocal.length; elItem++) {
        const item = this.rolAsigLocal[elItem];

        let dato: GstRolDTO = {
          idUsuario: idUsuario,
          idRol: parseInt(item)
        };

        this.usuSrv.remRol(dato).subscribe(
          success => { },
          err => { }
        );
      }
    }
    let newLst2: RolDTO[] = [];
    if (this.rolesUsuario) {
      for (let index = 0; index < this.rolesUsuario.length; index++) {
        const allRolItem = this.rolesUsuario[index]; // vector del tipo RolDTO[]
        let flagExiste: boolean = false;

        for (let i = 0; i < this.rolAsigLocal.length; i++) {
          const rolSelectedItem = this.rolAsigLocal[i];

          if (allRolItem.id.toString() == rolSelectedItem) {
            flagExiste = true;
            this.rolesDispUsuario.push(allRolItem);
            break;
          }
        }
        if (!flagExiste) {
          newLst2.push(allRolItem);
        }
      }
    }
    newLst2.sort();
    this.rolesUsuario = newLst2; //se carga la lista de roles asignados con los datos actualizados
    this.rolAsigLocal = [];
  }


  inactivarUsuario(user: UsuarioDTO) {
    var seguro = confirm("Esta seguro que quiere inactivar el usuario ?");
    if (seguro) {
      this.usuSrv.inactivarUsuario(user).subscribe(
        success => {
          M.toast({ html: 'Usuario inactivado correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          this.getLstUsuarios();
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      );
    }
  }

  changePag(pagNumber: number) {
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }
  // changeRegXpag(){  // p/ paginacion serve site
  //   this.getLstUsuarios(0, this.regXpag);
  // }

  sortBy(property: string) {
    if (property === this.sortProperty) {
      switch (this.sortOrder) {
          case 0:
            //ASCENDENTE
            this.sortOrder = 1;

            break;
          case 1:
            //DESCENDENTE
            this.sortOrder = 2;
            break;
          case 2:
            //NO ORDEN
            this.sortOrder = 0;
            break;
      }
    } else {
      this.sortOrder = 1;
    }
    this.sortProperty = property;
    this.sortList(property);
}

sortList(property: string) {
  switch (this.sortOrder) {
    case 1:
      this.listaUsuarios.sort((a,b) =>{
        if(!a.property || a.property < b.property){
         return -1
        }else if (!b.property || a.property > b.property){
          return 1
        }
        return 0
      })
      break;
    case 2:
      this.listaUsuarios.sort((b,a) =>{
        if(!a.property || a.property < b.property){
         return -1
        }else if (!b.property || a.property > b.property){
          return 1
        }
        return 0
      })
      break;
  }

}

  sortIcon(property: string) {
      if (property === this.sortProperty) {
          return this.sortOrder === 0 ? '⇅' : this.sortOrder === 1? '⇈' : '⇊';
      }
      return '⇅';
  }
}
