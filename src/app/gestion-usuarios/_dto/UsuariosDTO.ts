export interface UsuarioDTO {
    id?: number;
    nombre: string;
    apellido: string;
    usuario: string;
    pass:string;
    repeatpass?: string;
    idEstado: number;
    estado?: string;
    sucursales?: UsuSucursalesDTO[];
    funcionalidades?: string[];
    roles?: any[];
    accesstoken?: string;

}

export interface UsuSucursalesDTO {
    id?: number;
    idUsuario?: number;
    idSucursal: number;
    codSucursal?: string;
    nombreSucursal?: string;
    inicioVigencia?: Date;
    finVigencia?: Date;
    fechaRegistro?: Date;
    vigente?: Boolean;
}

export interface GstRolDTO {
    idUsuario: number;
    idRol: number;
}
