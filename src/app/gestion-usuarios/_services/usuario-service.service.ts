import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { UsuarioDTO, GstRolDTO, UsuSucursalesDTO } from '../_dto/UsuariosDTO';
import { RolDTO } from '../../gestion-roles/_dto/RolDTO';

@Injectable()
export class UsuarioServiceService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  // getUsuarios( pag: number, cant: number): Observable<UsuarioDTO[]> {
  //   return this.http.get<UsuarioDTO[]>(appPreferences.urlBackEnd + 'usuarios/' + pag + '/' + cant, this.auth.getHeaders());
  // }
  getUsuarios(): Observable<UsuarioDTO[]> {
    return this.http.get<UsuarioDTO[]>(appPreferences.urlBackEnd + 'usuarios', this.auth.getHeaders());
  }

  getUserById(id: number): Observable<UsuarioDTO> {
    return this.http.get<UsuarioDTO>(appPreferences.urlBackEnd + 'usuarios/' + id, this.auth.getHeaders());
  }

  createUsuario(datos: UsuarioDTO): Observable<UsuarioDTO> {
    return this.http.post<UsuarioDTO>(appPreferences.urlBackEnd + 'usuarios', datos, this.auth.getHeaders());
  }

  updateUsuario(id: number, data: UsuarioDTO): Observable<UsuarioDTO> {
    return this.http.put<UsuarioDTO>(appPreferences.urlBackEnd + 'usuarios/' + id, data, this.auth.getHeaders())
  }

  inactivarUsuario(data: UsuarioDTO): Observable<UsuarioDTO> {
    return this.http.post<UsuarioDTO>(appPreferences.urlBackEnd + 'usuarios/inactivarUser', data, this.auth.getHeaders())
  }

  getRolesByUser(id: number): Observable<RolDTO[]> {
    return this.http.get<RolDTO[]>(appPreferences.urlBackEnd + 'roles/byUsuario/' + id, this.auth.getHeaders())
  }

  getRolesDispByUser(id: number): Observable<RolDTO[]> {
    return this.http.get<RolDTO[]>(appPreferences.urlBackEnd + 'roles/dispByUsuario/' + id, this.auth.getHeaders())
  }

  getUsuSucs(idusuario: number): Observable<UsuSucursalesDTO[]> {
    return this.http.get<UsuSucursalesDTO[]>(appPreferences.urlBackEnd + 'usuarioSucursal/sucsVigentesByUser/' + idusuario, this.auth.getHeaders())
  }

  getUsuAllSucs(idusuario: number): Observable<UsuSucursalesDTO[]> {
    return this.http.get<UsuSucursalesDTO[]>(appPreferences.urlBackEnd + 'usuarioSucursal/sucursalesByUser/' + idusuario, this.auth.getHeaders())
  }

  asoRol(data: GstRolDTO): Observable<any> {
    return this.http.post<any>(appPreferences.urlBackEnd + 'roles/usuarioRol/', data, this.auth.getHeaders());
  }

  asoUsuSuc(data: UsuSucursalesDTO) : Observable<any> {
    return this.http.post<any>(appPreferences.urlBackEnd + 'usuarioSucursal/', data, this.auth.getHeaders());
  }

  remRol(data: GstRolDTO): Observable<any> {
    return this.http.delete<any>(appPreferences.urlBackEnd + 'roles/usuarioRol/' + data.idUsuario + '/' + data.idRol, this.auth.getHeaders());
  }

  vencerAso(id: number): Observable<any> {
    return this.http.delete<any>(appPreferences.urlBackEnd + 'usuarioSucursal/' + id, this.auth.getHeaders()); 
  }

}
