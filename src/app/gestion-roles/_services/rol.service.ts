import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { appPreferences } from '../../../environments/environment';

import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { RolDTO, FuncionalidadesDTO, GstFuncionalidadDTO } from '../_dto/RolDTO';

const headers = new HttpHeaders().set('Content-Type', 'application/json');

@Injectable()
export class RolService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getRoles(): Observable<RolDTO[]> {
    return this.http.get<RolDTO[]>(appPreferences.urlBackEnd + 'roles', this.auth.getHeaders())
  }

  getRolById(id: number): Observable<RolDTO> {
    return this.http.get<RolDTO>(appPreferences.urlBackEnd + 'roles/' + id, this.auth.getHeaders())
  }

  createRol(datos: RolDTO): Observable<RolDTO> {
    return this.http.post<RolDTO>(appPreferences.urlBackEnd + 'roles', datos, this.auth.getHeaders())
  }

  updateRol(id: number, data: RolDTO): Observable<RolDTO> {
    return this.http.put<RolDTO>(appPreferences.urlBackEnd + 'roles/' + id, data, this.auth.getHeaders())
  }

  deleteRol(id: number): Observable<RolDTO> {
    return this.http.delete<RolDTO>(appPreferences.urlBackEnd + 'roles/' + id, this.auth.getHeaders())
  }

  getFuncionalidades(): Observable<FuncionalidadesDTO[]> {
    return this.http.get<FuncionalidadesDTO[]>(appPreferences.urlBackEnd + 'funcionalidades', this.auth.getHeaders())
  }

  asoFuncionalidad(data: GstFuncionalidadDTO): Observable<any> {
    return this.http.post<any>(appPreferences.urlBackEnd + 'funcionalidades/asignarFunc', data, this.auth.getHeaders());
  }

  remFuncionalidad(data: GstFuncionalidadDTO): Observable<any> {
    return this.http.delete<any>(appPreferences.urlBackEnd + 'funcionalidades/desasignarFunc/' + data.idRol + '/' + data.idFuncionalidad, this.auth.getHeaders());
  }

}
