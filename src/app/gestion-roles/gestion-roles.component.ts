import { Component, OnInit } from '@angular/core';

// Servicios
import { RolService } from './_services/rol.service';
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';

// DTO
import { RolDTO, FuncionalidadesDTO, GstFuncionalidadDTO } from './_dto/RolDTO';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';


declare var $: any;
declare var M: any;


@Component({
  selector: 'app-gestion-roles',
  templateUrl: './gestion-roles.component.html',
  styleUrls: ['./gestion-roles.component.css']
})
export class GestionRolesComponent implements OnInit {

  public datosRol: RolDTO;
  public listaRoles: RolDTO[];
  public updRol: boolean = false;
  public listaFuncRol: FuncionalidadesDTO[];
  public listaFunc: FuncionalidadesDTO[];
  public datosFuncAsignadas: FuncionalidadesDTO;

  public funcionalidadAsigLocal: any[] = [];
  public datosFuncionalidad: any[] = [];

  private listaRolesIntero: RolDTO[] = [];

  constructor(
    private rolSrv: RolService,
    public auth: AuthService,
    private util: UtilsService
  ) {

    this.listaRoles = [];

    this.datosRol = {
      rol: '',
      descripcion: '',
      funcionalidades: []
    }

    this.listaFuncRol = [];

    this.listaFunc = [];
  }

  ngOnInit() {
    $('select').formSelect();

    $('.fixed-action-btn').floatingActionButton();
    
    $('.modal').modal({
      dismissible : false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    $(document).ready(function(){
      $('.tooltipped').tooltip({delay: 50});
    });
        
    this.getRoles();
    console.log ('hola');
  }

  getRoles() {
    this.listaRoles = [];
    this.rolSrv.getRoles().subscribe(
      success => {
        this.listaRoles = success;
        this.listaRolesIntero = success;
      },
      err => { }
    );
    $(document).ready(function(){
      $('.tooltipped').tooltip({delay: 50});
    });
  }

  getFuncionalidades(funcionalidades?: FuncionalidadesDTO[]) {
    this.listaFunc = [];
    this.rolSrv.getFuncionalidades().subscribe(
      success => {

        //Recorre el listado de todas las funcionalidades 
        for (let index = 0; index < success.length; index++) {
          const funcAllItem = success[index];
          console.log(funcAllItem);
          let existe: boolean = false;

          //Recorre el listado de funcionalidades asignadas al rol
          for (let index = 0; index < funcionalidades.length; index++) {
            const funAsigItem = funcionalidades[index];
            
            // si la funcionalidad existe entre las asignadas activa la bandera
            if (funcAllItem.id == funAsigItem.id) {
              existe = true;
              break;
            }
          }
          //si existe = true entonces no hace nada (''=nada)
          //si existe = false entonces agrega la funcionalidad al array
          (existe) ? '' : this.listaFunc.push(funcAllItem);
          
        }

      },
      err => { }
    );
  }

  getInfoRol(infoRol: RolDTO) {

    this.getFuncionalidades(infoRol.funcionalidades);
    this.updRol = true;
    this.datosRol = infoRol;

  }

  showFuncRol(funcionalidades: FuncionalidadesDTO[], rol: RolDTO) {
    this.listaFuncRol = funcionalidades;
    this.datosRol = rol;
  }

  guardar(formaR: NgForm) {  
    console.log(this.updRol);
    if (this.updRol) {
      let rol = formaR.value;
      this.rolSrv.updateRol(this.datosRol.id, rol).subscribe(
        success => {
          M.toast({html: 'Datos del rol actualizados correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalRol').modal('close');
          this.getRoles();
        },
        err => { }
      );
    } else {
      let rol = formaR.value;
      console.log(rol);
      this.rolSrv.createRol(rol).subscribe(
        success => {
          M.toast({html: 'Rol creado correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalRol').modal('close');
          this.getRoles();
        },
        err => { }
      );
    } 


  }

  resetForm() {

    this.datosRol = {
      rol: '',
      descripcion: '',
      funcionalidades: []
    }

    this.datosFuncionalidad = [];
    this.funcionalidadAsigLocal = [];

    console.log("Reset");

  }

  nuevoRol() {

    this.rolSrv.createRol(this.datosRol).subscribe(
      success => {
        alert('sss');
      },
      err => {

      }
    );

  }

  getFuncionalidadRol(id: number) {
    console.log(id);
  }

  guardarCambios(forma: NgForm) {
    // console.log(forma);
  }

  borrarRol(id: number) {

    this.rolSrv.deleteRol(id).subscribe(
      success => {
        M.toast({html: 'Rol eliminado correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
        $('#modalAlertDelete').modal('close');
        this.getRoles();
      },
      err => {

      }
    );
    $(document).ready(function(){
      $('.tooltipped').tooltip({delay: 50});
    });
       
  }

  agregarFunc(idRol: number) {

    for (let elItem = 0; elItem < this.datosFuncionalidad.length; elItem++) {
      const item = this.datosFuncionalidad[elItem];
      
      let dato: GstFuncionalidadDTO = {
        idRol: idRol,
        idFuncionalidad: parseInt(item)
      };
      
      this.rolSrv.asoFuncionalidad(dato).subscribe(
        success => {},
        err => {}
      );

    }

    let newLst: FuncionalidadesDTO[] = [];

    for (let index = 0; index < this.listaFunc.length; index++) {
      const allFuncItem = this.listaFunc[index];

      let flagExiste: boolean = false;
      for (let i = 0; i < this.datosFuncionalidad.length; i++) {
        const funcSelectedItem = this.datosFuncionalidad[i];

        if (allFuncItem.id.toString() == funcSelectedItem) {
          flagExiste = true;
          this.datosRol.funcionalidades.push(allFuncItem);
          break;
        }

      }

      if (!flagExiste) {
        newLst.push(allFuncItem);
      }

    }

    newLst.sort();
    this.listaFunc = newLst;
    this.datosFuncionalidad = [];


    console.log(this.datosFuncionalidad);
  }

  removerFunc(idRol: number) {

    for (let elItem = 0; elItem < this.funcionalidadAsigLocal.length; elItem++) {
      const item = this.funcionalidadAsigLocal[elItem];
      
      let dato: GstFuncionalidadDTO = {
        idRol: idRol,
        idFuncionalidad: parseInt(item)
      };
      
      this.rolSrv.remFuncionalidad(dato).subscribe(
        success => {},
        err => {}
      );

    }

    let newLst: FuncionalidadesDTO[] = [];

    for (let index = 0; index < this.datosRol.funcionalidades.length; index++) {
      const allFuncItem = this.datosRol.funcionalidades[index];

      let flagExiste: boolean = false;
      for (let i = 0; i < this.funcionalidadAsigLocal.length; i++) {
        const funcSelectedItem = this.funcionalidadAsigLocal[i];

        if (allFuncItem.id.toString() == funcSelectedItem) {
          flagExiste = true;
          this.listaFunc.push(allFuncItem);
          break;
        }

      }

      if (!flagExiste) {
        newLst.push(allFuncItem);
      }

    }

    newLst.sort();
    this.datosRol.funcionalidades = newLst;
    this.funcionalidadAsigLocal = [];

    console.log(this.funcionalidadAsigLocal);
  }

}

