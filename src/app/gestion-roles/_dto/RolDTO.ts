export interface FuncionalidadesDTO {
	id?: number;
	funcionalidad: string;
	abreviatura?: string;
}
export interface RolDTO {
    id?: number;
	rol: string;
	descripcion: string;
	funcionalidades: FuncionalidadesDTO[]; 
}

export interface GstFuncionalidadDTO {
	idRol: number;
	idFuncionalidad: number;
}
