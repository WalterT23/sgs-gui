
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';
import { GestionProveedoresComponent } from '../gestion-proveedores/gestion-proveedores.component';
import { formatearMillar, generarBlobForPDF } from '../_utils/millard-formatter';
import { saveAs } from 'file-saver';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';

// DTO
import { ProveedoresDTO } from '../gestion-proveedores/_dto/ProveedoresDTO';
import { ProductoDTO } from '../gestion-productos/_dto/ProductoDTO';
import { NotaCreditoCompraDTO,  NCCompraDetalleDTO } from './_dto/NotaCreditoCompraDTO';
import { NotaCreditoCompraService } from './_services/nota-credito-compra.service';
import { FacturaCompraDTO, FCdetalleDTO } from '../factura-compra/_dto/FacturaCompraDTO';
import { FacturasProveedorService } from '../factura-compra/_services/facturas-proveedor.service';



declare var $: any;
declare var M: any;

@Component({
  selector: 'app-nota-credito-compra',
  templateUrl: './nota-credito-compra.component.html',
  styleUrls: ['./nota-credito-compra.component.css']
})

export class NotaCreditoCompraComponent implements OnInit {

  @ViewChild(GestionProveedoresComponent) proveedor: GestionProveedoresComponent;

  public datosNC: NotaCreditoCompraDTO;
  public listaNC: NotaCreditoCompraDTO[];
  public datosNCdetalle: NCCompraDetalleDTO;
  public listaDetNC: NCCompraDetalleDTO[];
  public updNC: boolean = false;
  public search: boolean = false;
  public restar: boolean = false;
  public listaProv: ProveedoresDTO[];
  public listaProd: ProductoDTO[];
  public datosProducto: ProductoDTO;
  public OrdenCompraFlag: boolean = true;
  public listaForAdd: NCCompraDetalleDTO[];
  public listaForDel: NCCompraDetalleDTO[];
  public totalNC: number = null;
  public datosFacturaCompra: FacturaCompraDTO;
  public dealleFact: FCdetalleDTO[];
  public listaFacturasCompra:FacturaCompraDTO[];
  updOC:boolean = false;

  regXpag: number = 5; //variable que utsiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  creg:number;
  facturasAux: any[] = [];

    constructor(

    private ncSrv: NotaCreditoCompraService,
    private fcSrv: FacturasProveedorService,
    private provSrv: ProveedoresService,
    private prodSrv: ProductosService,
    private util: UtilsService,
    public auth: AuthService
  ) {
  
    
    this.listaNC = [];
    this.listaProv = [];
    this.listaDetNC = [];
    this.listaProd = [];
    this.listaForDel = [];
    this.datosNC = {
      id: null,
      idProveedor:null,
      proveedor: '',
      idCabeceraCompra:null,
      idEstado:null,
      estado:  '',
      nroNotaCredito:  '',
      fecha:null,
      montoTotal:null,
      iva5:null,
      iva10:null,
      exentas:null,
      fechaCreacion:null,
      usuarioCreacion: '',
      idSucursal: null,
      detalles:[
        {
          id: null,
          idNotaCreditoCompra: null,
          idProducto: null,
          codProducto: '',
          producto: '',
          cantidad: null,
          costoUnitario: null,
          total: null,
          observacion: ''
        }
      ],
      detallesAEliminar: [],
      detallesAAgregar: []
    }
    this.datosNCdetalle = {
      id: null,
      idNotaCreditoCompra: null,
      idProducto: null,
      codProducto: '',
      producto: '',
      cantidad: null,
      costoUnitario: null,
      total: null,
      observacion: ''
    }
    this.datosFacturaCompra = {
      id:  null,
      nroFactura:'',
      timbrado: null,
      tipoFactura: null,
      ocAsociada:  null,
      idOrden: null,
      nroOrdenCompra: null,
      motivo: '',
      fecha: null,
      tipoDocumento: null,
      idProveedor: null,
      idEstado:  null,
      estado:'',
      fechaEstado:  null,
      saldo:  null,
      total: null,
      gravada10:  null,
      gravada5:  null,
      exenta:  null,
      iva5:  null,
      iva10:  null,
      fechaCreacion: null,
      usuarioCreacion: '',
      idSucursal: null,
      detalles:[],
      detallesAEliminar: [],
      detallesAAgregar: [],
      cuotas: null,
      pagos: null
    }
    this.dealleFact = [
      {
      id: null,
      idCabeceraCompra: null,
      idProducto: null,
      codProducto: '',
      producto: '',
      cantidad: null,
      precioUnitario: null,
      precioTotal: null,
      impuestoFiscal: null
      }]
   

  }

  ngOnInit() {
    $('select').formSelect();

    $('.tooltipped').tooltip();

    $('.modal').modal({
      dismissible : false,
      ready: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        this.getLstProductos() ;
        $('#btnCancelModal').click();
      }
    });

    $('#modalProveedor').modal({
      dismissible : false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: () => this.getLstProveedores()
    });
  
    this.getLstNotasCredito();
    this.getLstProductos() ;

  }

  formatNumber(nro: number): string {
    return formatearMillar(nro);
  }

  chargeLstNewNota() {
    this.getLstProductos();
    //this.addProducto();
    this.getLstProveedores();
    console.log("datos cargados al apretar nueva NC");
   }

  buscarFacturas(idProveedor:number){
    
    $('select').formSelect();

    this.fcSrv.getFacturasByIdProveedor(idProveedor).subscribe(
      success => {
        this.listaFacturasCompra = success;
        setTimeout(() => {
          $('select').formSelect();
          M.updateTextFields();
        }, 80);

      },
      err => { }
    );




  }

  chargeLst4Show(id: number) {
    //this.getInfoNC(id);
    this.getLstProveedores();
  }

  getLstNotasCredito() {
    this.ncSrv.getNotasCreditoCompra().subscribe(
      success => {
        this.listaNC = success;
        console.log("listaNC", this.listaNC);
        //this.creg=this.listaNC.length;
       },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  getLstProveedores() {

    this.listaProv = [];

    this.provSrv.getProveedores().subscribe(
      success => {
        this.listaProv = success;
        console.log(this.listaProv);

        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      },
      err => {
        $('select').formSelect();
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  getLstProductos() {
    this.prodSrv.getProductos().subscribe(
      success => {
        this.listaProd = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }


  //si es sin factura;
  getProductoByCod(codProducto: string, i: number) {
    if(i == null){
      i=0
    }
    console.log('pto control', i, this.datosNC);
    this.prodSrv.getProductoByCod(codProducto).subscribe(
      success => {
        this.datosProducto = success;

        this.datosNC.detalles[i].producto= success.nombre;
        this.datosNC.detalles[i].idProducto = success.id;;
        this.datosNC.detalles[i].costoUnitario= success.ultCostoCompra;
        console.log( this.datosProducto);
      },
      err => {

        let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  nombreProducto(idPro: number){
    let auxLst = this.listaProd.filter(row => (row.id == idPro));
    return (auxLst.length > 0) ? auxLst[0].nombre : '--';
  }

  codProducto(idPro: number){
    let auxLst = this.listaProd.filter(row => (row.id == idPro));
    return (auxLst.length > 0) ? auxLst[0].cod : '--';
  }

  getInfoNC(id: number) {
    $('select').formSelect();
    this.ncSrv.getNotaCreditoCompraById(id).subscribe(
      success => {
        this.datosNC = success;
        this.listaDetNC = this.datosNC.detalles;
        console.log("datosNC", this.datosNC, "listaDetNC", this.listaDetNC);
      },
      err => {
      }
    );
  }

  addProducto() {
    let newPro: NCCompraDetalleDTO = {
      id: null,
      idNotaCreditoCompra: null,
      idProducto: null,
      producto: '',
      codProducto:'',
      cantidad: null,
      costoUnitario: null,
      total: null,
      observacion: ''
    };
    this.datosNC.detalles.push(newPro)
  }

 


  calcMontoTotal(producto: NCCompraDetalleDTO, i?: number) {
     if (!this.restar) {
      console.log ("costo unitario",producto.costoUnitario);
      let total = producto.costoUnitario * producto.cantidad;
      this.listaDetNC[i].total = total;
      this.totalNC = this.totalNC + total;
    } else {
      this.totalNC = this.totalNC - producto.costoUnitario;
    }
    this.datosNC.montoTotal = this.totalNC;
    this.restar = false;
    
  }

  guardar() {
    console.log("datos al apretar el btn guardar", this.listaDetNC, this.datosNC);
    this.datosNC.idProveedor = $('#idProveedor').val();

    if (this.datosNC.idProveedor == null) {
      M.toast({html: 'Debe seleccionar un proveedor.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
    }

    if (this.datosNC.idProveedor != null) {
      if (this.updNC) {

        this.datosNC.detallesAEliminar = this.listaForDel;

        for (const iterator of this.listaDetNC) {
          if (iterator.id == null) {
            this.datosNC.detallesAAgregar.push(iterator);
            console.log(this.datosNC.detallesAAgregar);
          } else {
            this.datosNC.detalles.push(iterator);
          }
        }
        console.log(this.datosNC);

        this.ncSrv.updateNotaCreditoCompra(this.datosNC.id, this.datosNC).subscribe(
          success => {

            M.toast({html: 'Datos del producto actualizados correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
            $('#modalNCCompra').modal('close');
            this.getLstNotasCredito();
            this.resetForm();
          },
          err => {
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
        );
      } else {

        console.log("datosNC a enviar", this.datosNC);

        for (const iterator of this.listaDetNC) {
          if (iterator.idProducto != null) {
            this.datosNC.detalles.push(iterator);
            console.log(this.datosNC.detalles);
            setTimeout(() => {
            }, 40);
          }
        }
        console.log("datosNC a despues del push", this.datosNC);

        this.ncSrv.createNotaCreditoCompra(this.datosNC).subscribe(
          success => {

            M.toast({html:"Nota Credito creada correctamente.",displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
            $('#modalNCCompra').modal('close');
            setTimeout(() => {
              this.resetForm();
              this.getLstNotasCredito();
            }, 200);
            
          },
          err => {
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
           }
        );
      }
    }
  }

  borrarNota(id){
    this.ncSrv.deleteNotaCreditoCompra(id).subscribe(
      success => {
        M.toast({ html: 'La Nota de Crédito ha sido Eliminada', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        this.getLstNotasCredito();
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  removeRegistro(pos: number) {
    this.datosNC.detalles.splice(pos, 1);
  }

  detalleFact(idCabecera:number) {
   
    this.fcSrv.getFacturasById(idCabecera).subscribe(
      success => {
        this.datosNC.detalles = [];
        
        success.detalles.forEach(det => {

          det.total = det.precioTotal;
          det.costoUnitario = det.precioUnitario;
          this.datosNC.detalles.push(det);
        });
        console.log(this.datosNC);
      }
    );
  }

  resetForm() {
    this.updNC = false;
    this.search = false;
    this.restar = false;
    this.totalNC = null;
    this.datosNC = {
      id: null,
      nroNotaCredito: '',
      idProveedor: null,
      idCabeceraCompra:null,
      proveedor:'',
      fechaCreacion: null,
      fecha: null,
      usuarioCreacion: '',
      idEstado: null,
      estado: '',
      montoTotal: null,
      iva5: null,
      iva10: null,
      exentas: null,
      detalles: [],
      detallesAEliminar: [],
      detallesAAgregar: []
    }
    this.datosNCdetalle = {
      id: null,
      idNotaCreditoCompra: null,
      idProducto: null,
      producto: '',
      codProducto: '',
      cantidad: null,
      costoUnitario: null,
      total: null,
      observacion: ''
    }
    this.listaDetNC = [];
    this.listaForDel = [];

  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }


  
}
