import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { NotaCreditoCompraDTO, NCCompraDetalleDTO } from '../_dto/NotaCreditoCompraDTO';


@Injectable()
export class NotaCreditoCompraService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getNotasCreditoCompra(): Observable<NotaCreditoCompraDTO[]> {
    return this.http.get<NotaCreditoCompraDTO[]>(appPreferences.urlBackEnd + 'notaCreditoCompra', this.auth.getHeaders());
  }

  

  getNotaCreditoCompraById(id: number): Observable<NotaCreditoCompraDTO> {
    return this.http.get<NotaCreditoCompraDTO>(appPreferences.urlBackEnd + 'notaCreditoCompra/' + id, this.auth.getHeaders());
  }


  createNotaCreditoCompra(dataNC: NotaCreditoCompraDTO): Observable<any>  {
    return this.http.post<any>(appPreferences.urlBackEnd + 'notaCreditoCompra', dataNC, this.auth.getHeaders());
  }

  updateNotaCreditoCompra(id:number, dataNC: NotaCreditoCompraDTO): Observable<any> {
    
    return this.http.put<any>(appPreferences.urlBackEnd + 'notaCreditoCompra/'+ id, dataNC, this.auth.getHeaders())
  }

  updateDetalleNC(dataDetOC: NCCompraDetalleDTO[]): Observable<any> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'notaCreditoCompra/', dataDetOC, this.auth.getHeaders())
  }

  deleteNotaCreditoCompra(id: number): Observable<NotaCreditoCompraDTO> {
    return this.http.delete<NotaCreditoCompraDTO>(appPreferences.urlBackEnd + 'notaCreditoCompra/' + id, this.auth.getHeaders())
  }

  deletePorducto(id: number): Observable<NCCompraDetalleDTO> {
    return this.http.delete<NCCompraDetalleDTO>(appPreferences.urlBackEnd + 'notaCreditoCompraDetalle/' + id, this.auth.getHeaders())
  }

  anularNC (id:number): Observable<any> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'notaCreditoCompra/anularNC/'+ id, this.auth.getHeaders())
  }

  getPdfBoletaPago(id: number): Observable<any>{
    return this.http.get<any>(appPreferences.urlBackEnd + 'notaCreditoCompra/imprimir/'+ id, this.auth.getHeaders());
  }

  confirmarOC(id:number):Observable<any> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'notaCreditoCompra/confirmarOC/'+ id, this.auth.getHeaders())
  }

  
  getOrdenCompraByNroOc(nro: string): Observable<any> {
    return this.http.get<any>(`${appPreferences.urlBackEnd}notaCreditoCompra/byNroOc/${nro}`, this.auth.getHeaders());
  }

}
