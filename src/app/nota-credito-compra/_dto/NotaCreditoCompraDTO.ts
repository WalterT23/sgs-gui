export interface NotaCreditoCompraDTO {
    id?: number;
    idProveedor:number;
    proveedor?:string;
    idCabeceraCompra?:number;
    idEstado:number;
    estado: string;
    nroNotaCredito: string;
    fecha:Date;
    montoTotal:number;
    iva5:number;
    iva10:number;
    exentas:number;
    fechaCreacion?:Date;
    usuarioCreacion?:string;
    idSucursal?: number;
    detalles?: NCCompraDetalleDTO[];
    detallesAEliminar?:NCCompraDetalleDTO [];
    detallesAAgregar?:NCCompraDetalleDTO [];
}

export interface NCCompraDetalleDTO {
    id?: number;
    idNotaCreditoCompra?: number;
    idProducto: number;
    codProducto: string;
    producto:any;
    costoUnitario: number;
    cantidad?: number;
    total: number;
    observacion?:string;
}

