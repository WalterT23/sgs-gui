export interface FiltroFallo {
    fechaDesde: any;
    fechaHasta: any;
    total?: number;
    idSucursal?: number;
    idUsuario?: number;
    detalles?: FalloCajaGen[];
}

export interface FalloCajaGen {
    idUsuario:number;
	usuario:string;
	nombre:string;
	monto:number;
	idSucursal:number;
    detalles: FalloCaja[];
    
}
export interface FalloCaja {
    id:number;
    idAperturaCierre:number;
    puntoExp:string;
    idUsuario: number;
    monto: number;
    fecha: any;
    idSucursal: number;
    
}






