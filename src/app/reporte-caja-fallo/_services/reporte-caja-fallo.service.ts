import { Injectable } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { HttpClient } from '@angular/common/http';
import { FiltroFallo, FalloCaja } from '../_dto/FalloCajaDTO';
import { Observable } from 'rxjs';
import { appPreferences } from '../../../environments/environment';
import { FileDTO } from '../../reporte-ventas-realizadas/_dto/VentasRealizadasDTO';

@Injectable()
export class ReporteCajaFalloService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getFalloCaja(filtros: FiltroFallo): Observable<FiltroFallo> {
    return this.http.post<FiltroFallo>(appPreferences.urlBackEnd + 'falloCaja/reporteFallo', filtros, this.auth.getHeaders());
  }

  getDetalleFalloCaja(filtros: FiltroFallo): Observable<FalloCaja[]> {
    return this.http.post<FalloCaja[]>(appPreferences.urlBackEnd + 'falloCaja/listFallo', filtros, this.auth.getHeaders());
  }

  getReporte(filtros: FiltroFallo): Observable<FileDTO> {
    let tmp = Object.assign({},filtros)
    if(filtros.fechaDesde && filtros.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'falloCaja/reporteFallo/impresion', filtros, this.auth.getHeaders());
  }

}
