import { Component, OnInit } from '@angular/core';
import { FiltroFallo, FalloCajaGen, FalloCaja } from './_dto/FalloCajaDTO';
import { FiltrosChips } from '../_utils/show-chips-filter/plusFile/filtros-chips';
import { appPreferences } from '../../environments/environment';
import { ReporteCajaFalloService } from './_services/reporte-caja-fallo.service';
import { generarBlobForPDF } from '../_utils/millard-formatter';
import { saveAs } from 'file-saver';


declare var $: any;
declare var M: any;



@Component({
  selector: 'app-reporte-caja-fallo',
  templateUrl: './reporte-caja-fallo.component.html',
  styleUrls: ['./reporte-caja-fallo.component.css']
})
export class ReporteCajaFalloComponent implements OnInit {


  nameVarSession: any= "reporteFallo";

  public filtro: FiltroFallo;
  public fallos: FiltroFallo;
  public listaFC:FalloCajaGen[];
  public detalleFallo: FalloCaja[];
  public listaFiltros: FiltrosChips[];

  totalAux: number =null;
  cajero:string='';
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  regXpag2: number = 5; //variable que utiliza la paginación
  p2: number = 1; //variable que utiliza la paginación


  constructor(
    private repoSrv  : ReporteCajaFalloService,

  ) { 
    this.fallos=  {
      fechaDesde: null,
      fechaHasta: null,
      total: null,
      idSucursal: null,
      idUsuario: null,
      detalles: [],
    };
    let filtro = window.sessionStorage.getItem(btoa(this.nameVarSession));
    if (filtro) {
      this.filtro = JSON.parse(filtro);
      let tmpD = this.filtro.fechaDesde.split('-');
      let tmpH = this.filtro.fechaHasta.split('-');

      setTimeout(() => {
        let fechaD: any = document.getElementById('fechaDesde');
        let fechaH: any = document.getElementById('fechaHasta');
        fechaD.value = `${tmpD[2]}/${tmpD[1]}/${tmpD[0]}`;
        fechaH.value = `${tmpH[2]}/${tmpH[1]}/${tmpH[0]}`;
        this.loadRegistros();
      }, 100);
    } else {
      this.limpiarFiltro();
    }


  }

  ngOnInit() {
    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.tabs').tabs();
    }, 50);

    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });

    this.getLstFallo();
  }



mostrarFrmFiltro() {
  swShowFrmFiltro('filtroFallo');
  this.filtro.fechaDesde= null;
  this.filtro.fechaHasta= null;
  
}

limpiarFiltro() {
  console.log('entra aca')
  this.filtro = {
    fechaDesde: null,
    fechaHasta: null,
    
  }
}

mostrarChips() {
  let fechaD = changeFormatDate(this.filtro.fechaDesde);
  let fechaH = changeFormatDate(this.filtro.fechaHasta);
  console.log('format', fechaD, fechaH);
  this.listaFiltros.push({
    descripcion: 'Fechas',
    id: 'fdfh',
    value:  fechaD + ' al ' + fechaH,
    bloquearCierre: true
  });
}

loadRegistros() {

  swShowFrmFiltro('filtroFallo', 'none');

  if (!this.filtro.fechaDesde || !this.filtro.fechaHasta) {
    alert("Los campos fecha desde y fecha hasta son obligatorios.");
    return false
  }

  this.filtro.fechaDesde = ajusteZH(this.filtro.fechaDesde);
  this.filtro.fechaHasta = ajusteZH(this.filtro.fechaHasta);
  console.log('fecha para la consulta', this.filtro);
  this.listaFC = [];
  this.listaFiltros = [];
  this.mostrarChips();

  window.sessionStorage.setItem(btoa(this.nameVarSession), JSON.stringify(this.filtro));

  this.getLstFallo();

}

  getDetalle(item){
    console.log ("filtro:",this.filtro);
    this.filtro.idUsuario=item.idUsuario;
    this.cajero=item.nombre;
    this.totalAux=item.monto;
    

    this.repoSrv.getDetalleFalloCaja(this.filtro).subscribe(
    success => {

      this.detalleFallo = success;
      console.log ("trae:",success)
    },
    err => {
      let msg = err.error;
      M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
    }
  );
  }

  getReporte(){
    
    this.repoSrv.getReporte(this.fallos).subscribe(
      success => {
        let excel = generarBlobForPDF(success.archivo,"application/xls");
        saveAs(excel, success.nombre + success.tipo);
        console.log(success.archivo);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }
  
 

  volver(){

    $('#modalDetalleFallo').modal('close');

  }
  getLstFallo() {
 
  setTimeout(() => {
    $('.tooltipped').tooltip();
  }, 200);
  
  this.repoSrv.getFalloCaja(this.filtro).subscribe(
    success => {
      this.fallos = success;
      this.fallos.detalles= success.detalles;
      this.listaFC = success.detalles;
      console.log ("trae:",success)
    },
    err => {
      let msg = err.error;
      M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
    }
  );

  
}

changePag(pagNumber: number){
  this.p = pagNumber;
  // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
  setTimeout(() => {
    $('.tooltipped').tooltip();
  }, 100);
}

removerFiltro(event:any) {
  
}
}
