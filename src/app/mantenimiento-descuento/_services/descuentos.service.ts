import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { DescuentosDTO } from '../_dto/DescuentosDTO';

@Injectable()
export class DescuentosService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getDescuentos(): Observable<DescuentosDTO[]> {
    return this.http.get<DescuentosDTO[]>(appPreferences.urlBackEnd + 'productosDescuentos', this.auth.getHeaders());
  }

  getDescuentosById(id: number): Observable<DescuentosDTO> {
    return this.http.get<DescuentosDTO>(appPreferences.urlBackEnd + 'productosDescuentos/' + id, this.auth.getHeaders());
  }

  getDescuentosByIdProducto(idProducto: number): Observable<any> {
    return this.http.get<any>(appPreferences.urlBackEnd + 'productosDescuentos/porcentajeByProducto/' + idProducto, this.auth.getHeaders());
  }

  updateFamilia(id: number, data: DescuentosDTO): Observable<DescuentosDTO> {
    return this.http.put<DescuentosDTO>(appPreferences.urlBackEnd + 'productosDescuentos/' + id, data, this.auth.getHeaders())
  }

  createFamilia(datos: DescuentosDTO): Observable<DescuentosDTO> {
    return this.http.post<DescuentosDTO>(appPreferences.urlBackEnd + 'productosDescuentos', datos, this.auth.getHeaders());
  }
  
  deleteFamilia(id: number): Observable<DescuentosDTO> {
    return this.http.delete<DescuentosDTO>(appPreferences.urlBackEnd + 'productosDescuentos/' + id, this.auth.getHeaders())
  }

}
