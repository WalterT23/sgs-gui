export interface DescuentosDTO {
    id?: number;
	idProducto: number;
	porcentaje: number;
	inicioVigencia: Date;
	finVigencia: Date;
	usuarioCreacion: string;
	fechaCreacion: Date;
}