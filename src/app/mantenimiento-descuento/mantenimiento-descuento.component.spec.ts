import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoDescuentoComponent } from './mantenimiento-descuento.component';

describe('MantenimientoDescuentoComponent', () => {
  let component: MantenimientoDescuentoComponent;
  let fixture: ComponentFixture<MantenimientoDescuentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenimientoDescuentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenimientoDescuentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
