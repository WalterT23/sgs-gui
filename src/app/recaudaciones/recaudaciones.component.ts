import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';

// Servicios
import { RecaudacionesService } from './_services/recaudaciones.service';
import { BancosService } from '../bancos/services/bancos.service';
import { UtilsService } from '../_utils/utils.service';
import { AuthService } from '../_services/auth.service';

// DTO
import { RecaudacionesDTO, DepositoDineroDTO } from './_dto/RecaudacionesDTO';
import { BancosDTO } from '../bancos/_dto/BancosDTO';
import { DatePipe } from '@angular/common';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-recaudaciones',
  templateUrl: './recaudaciones.component.html',
  styleUrls: ['./recaudaciones.component.css']
})
export class RecaudacionesComponent implements OnInit {

  public datosRecau: RecaudacionesDTO;
  public listaRecau: RecaudacionesDTO[];
  public datosDepositos: DepositoDineroDTO;
  public listaDepositos: DepositoDineroDTO[];
  public lstBancos: BancosDTO[] = [];
  public updDep: Boolean = false;

  regXpag: number = 5; //variable que utiliza la paginación
  p2: number = 1; //variable que utiliza la paginación
  p1: number = 1;

  constructor(
    private recSrv: RecaudacionesService,
    private bancoSrv: BancosService,
    public auth: AuthService
  ) {
    this.listaRecau = [];
    this.listaDepositos= [];
    this.datosRecau= {
      id: null,
      fecha: null,
      saldoInicial: null,
      ingresos: null,
      egresos: null,
      saldoFinal: null,
      idSucursal: null
    }
    this.datosDepositos= {
      id: null,
      idBanco: null,
      monto: null,
      nroCuenta: '',
      fechaDeposito: null,
      fechaCreacion: null,
      usuarioCreacion: '',
      idSucursal: null
    }
   }

  ngOnInit() {

    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.tabs').tabs();
    }, 100);

    $('.modal').modal({
      dismissible : false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });

    this.getLstRecaudaciones();
    this.getLstDepositos();
    this.getLstBanco();

  }

  getLstRecaudaciones() {
    this.recSrv.getRecaudaciones().subscribe(
      success => {
        this.listaRecau = success;
      },
      err => { }
    );
  }

  getLstDepositos() {
    this.recSrv.getDepositos().subscribe(
      success => {
        this.listaDepositos = success;
      },
      err => { }
    );
  }

  getLstBanco() {
    this.bancoSrv.getBancos().subscribe(
      success => {
        this.lstBancos = success;
      }
    );
  }
  
  nombreBanco(idBanco: number){
    let auxLst = this.lstBancos.filter(row => (row.id == idBanco));
    return (auxLst.length > 0) ? auxLst[0].nombre : '--';
  }

  getInfoDepo(idDepo: number){
    let aux= new DatePipe('es-PY');
    this.updDep = true;
    this.recSrv.getDepositosById(idDepo).subscribe(
      success => {
        this.datosDepositos = success;
        this.datosDepositos.fechaDeposito = aux.transform(success.fechaDeposito, 'dd/MM/yyyy');
      }
    );
  }

  guardarDeposito(){
    console.log('datos al guardar',this.datosDepositos);
    if (this.datosDepositos.fechaDeposito != null){
      this.datosDepositos.fechaDeposito = ajusteZH(this.datosDepositos.fechaDeposito);
    }
    var control: Boolean = true;
    if (this.datosDepositos.monto == null || this.datosDepositos.fechaDeposito == null){
      M.toast({html:'Los campos monto y fecha deposito son obligatorios', displayLength: appPreferences.toastErrorDuration,  classes: 'toastErrorColor'});
      control = false;
    }

    if (control){
      if(this.updDep){
        this.recSrv.updateDepositoDinero(this.datosDepositos.id, this.datosDepositos).subscribe(
          success => {
            M.toast({html:'Datos del deposito actualizados correctamente.', displayLength: appPreferences.toastOkDuration,  classes: 'toastOkColor'});
            $('#modalDeposito').modal('close');
            this.getLstDepositos();
          },
          err => {
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
        );
      } else {
        this.recSrv.createDepositoDinero(this.datosDepositos).subscribe(
          success => {
            M.toast({html:'Deposito registrado correctamente.', displayLength: appPreferences.toastOkDuration,  classes: 'toastOkColor'});
            $('#modalDeposito').modal('close');
            this.getLstDepositos();
          },
          err => {
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
        );
      }
    }
  }

  updateLst(){
    this.getLstDepositos();
    this.getLstRecaudaciones();
  }

  resetForm(){
    this.datosDepositos= {
      id: null,
      idBanco: null,
      monto: null,
      nroCuenta: '',
      fechaDeposito: null,
      fechaCreacion: null,
      usuarioCreacion: '',
      idSucursal: null
    }
  }

  changePag(pagNumber: number){
    this.p2 = pagNumber;
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }
}
