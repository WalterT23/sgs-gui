export interface RecaudacionesDTO {
    id: number;
	fecha: Date;
	saldoInicial: number;
	ingresos: number;
	egresos:number;
	saldoFinal?:number;
	idSucursal?:number;
}

export interface DepositoDineroDTO {
	id?: number;
	monto: number;
	idBanco: number;
	nroCuenta?: string;
	fechaDeposito: any;
	fechaCreacion: Date;
	usuarioCreacion: string;
	idSucursal: number;
}