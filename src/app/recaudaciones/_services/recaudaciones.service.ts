import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { RecaudacionesDTO, DepositoDineroDTO } from '../_dto/RecaudacionesDTO';

@Injectable()
export class RecaudacionesService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getRecaudaciones(): Observable<RecaudacionesDTO[]> {
    return this.http.get<RecaudacionesDTO[]>(appPreferences.urlBackEnd + 'recaudacion', this.auth.getHeaders());
  }

  getRecaudacionesById(id: number): Observable<RecaudacionesDTO> {
    return this.http.get<RecaudacionesDTO>(appPreferences.urlBackEnd + 'recaudacion/' + id, this.auth.getHeaders());
  }

  createDepositoDinero(datos: DepositoDineroDTO): Observable<DepositoDineroDTO> {
    return this.http.post<DepositoDineroDTO>(appPreferences.urlBackEnd + 'depositoDinero', datos, this.auth.getHeaders());
  }

  updateDepositoDinero(id: number, data: DepositoDineroDTO): Observable<DepositoDineroDTO> {
    return this.http.put<DepositoDineroDTO>(appPreferences.urlBackEnd + 'depositoDinero/' + id, data, this.auth.getHeaders())
  }

  getDepositos(): Observable<DepositoDineroDTO[]> {
    return this.http.get<DepositoDineroDTO[]>(appPreferences.urlBackEnd + 'depositoDinero', this.auth.getHeaders());
  }

  getDepositosById(id: number): Observable<DepositoDineroDTO> {
    return this.http.get<DepositoDineroDTO>(appPreferences.urlBackEnd + 'depositoDinero/' + id, this.auth.getHeaders());
  }


}
