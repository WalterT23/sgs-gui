import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FamiliaDTO } from '../_dto/FamiliaDTO';

@Injectable()
export class FamiliaService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getFamilias(): Observable<FamiliaDTO[]> {
    return this.http.get<FamiliaDTO[]>(appPreferences.urlBackEnd + 'grupoProductos', this.auth.getHeaders());
  }

  getFamiliasById(id: number): Observable<FamiliaDTO> {
    return this.http.get<FamiliaDTO>(appPreferences.urlBackEnd + 'grupoProductos/' + id, this.auth.getHeaders());
  }

  createFamilia(datos: FamiliaDTO): Observable<FamiliaDTO> {
    return this.http.post<FamiliaDTO>(appPreferences.urlBackEnd + 'grupoProductos', datos, this.auth.getHeaders());
  }

  updateFamilia(id: number, data: FamiliaDTO): Observable<FamiliaDTO> {
    return this.http.put<FamiliaDTO>(appPreferences.urlBackEnd + 'grupoProductos/' + id, data, this.auth.getHeaders())
  }

  deleteFamilia(id: number): Observable<FamiliaDTO> {
    return this.http.delete<FamiliaDTO>(appPreferences.urlBackEnd + 'grupoProductos/' + id, this.auth.getHeaders())
  }
}
