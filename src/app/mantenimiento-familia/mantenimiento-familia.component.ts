import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { FamiliaService } from './_service/familia.service';

//DTO
import { FamiliaDTO } from './_dto/FamiliaDTO';
import { appPreferences } from '../../environments/environment';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-mantenimiento-familia',
  templateUrl: './mantenimiento-familia.component.html',
  styleUrls: ['./mantenimiento-familia.component.css']
})
export class MantenimientoFamiliaComponent implements OnInit {

  @Input() mantenimiento: boolean = false;
  
  public familia: FamiliaDTO;
  public listaFamilia: FamiliaDTO[];

  constructor(
    private fliaSrv: FamiliaService,
    private util: UtilsService,
    public auth: AuthService
  ) { 
    this.listaFamilia = [];
    this.familia = {
      id: null,
      abreviatura: '',
      nombre: '',
      idPadre: null
    };
  }

  ngOnInit() {
    $('select').formSelect();

    if (!this.mantenimiento) {

      setTimeout(() => {
        $('.modal').modal({
          dismissible : false,
          onOpenStart: function (modal, trigger) {
            M.updateTextFields();
            $('select').formSelect();

            console.log(modal, trigger);
          },
          complete: function () {
            $('#btnCancelModalFlia').click();
          }
        });
      }, 60);
      this.getLstFamilias();
    }
    this.getLstFamilias();
    console.log(this.listaFamilia) ;
  }

  getLstFamilias () {
    this.fliaSrv.getFamilias().subscribe(
      success => {
        this.listaFamilia = success;
      },
      err => { }
    )
  }

  guardar(forma: NgForm) {
    
    let familia = forma.value;

    this.fliaSrv.createFamilia(familia).subscribe(
      success => {
        M.toast({html:'Nueva Familia agregada correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor'});
        $('#modalFamilia').modal('close');
        this.resetForm();
        this.getLstFamilias();
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  resetForm(){
    this.familia = {
      id: null,
      abreviatura: '',
      nombre: '',
      idPadre: null
    };
  }

}
