
export interface FamiliaDTO {
    id?: number;
    abreviatura: string;
    nombre: string;
    idPadre?: number;
}