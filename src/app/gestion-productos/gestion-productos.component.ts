import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { ProductosService } from './_service/productos.service';
import { UnidadMedidaService } from '../mantenimiento-unidad-medida/_service/unidad-medida.service';
import { FamiliaService } from '../mantenimiento-familia/_service/familia.service';
import { MarcaService } from '../mantenimiento-marca/_service/marca.service';

//DTO
import { ProductoDTO, TributosDTO } from './_dto/ProductoDTO'
import { UnidadMedDTO } from '../mantenimiento-unidad-medida/_dto/UnidadMedDTO';
import { FamiliaDTO } from '../mantenimiento-familia/_dto/FamiliaDTO';  
import { MarcaDTO } from '../mantenimiento-marca/_dto/MarcaDTO';
import { appPreferences } from '../../environments/environment';

import { BusquedaGral } from '../pipes/busqueda.pipe';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-productos',
  templateUrl: './gestion-productos.component.html',
  styleUrls: ['./gestion-productos.component.css']
})
export class GestionProductosComponent implements OnInit {

  public datosProducto: ProductoDTO;
  public listaProductos: ProductoDTO[];
  public updPro: boolean = false;
  public search: boolean = false;
  public codGenerado: string;
  public listaUniMed: UnidadMedDTO[];
  public listaGrupo: FamiliaDTO[];
  public listaMarca: MarcaDTO[];
  public listaTributos: TributosDTO[];
  public idInfoRefTributos: number;
  public ultCostoCompra: number;

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  searchText = "";
  constructor(

    private proSrv: ProductosService,
    private uniMedSrv: UnidadMedidaService,
    private fliaSrv: FamiliaService,
    private marcaSrv: MarcaService,
    public auth: AuthService

  ) {
    this.listaProductos = [];
    this.listaUniMed = [];
    this.listaGrupo = [];
    this.listaMarca = [];
    this.listaTributos = [];
    this.idInfoRefTributos = 6;
    this.datosProducto = {
      cod: '',
      nombre: '',
      descripcion: '',
      idUnidadMed: null,
      unidadMed: '',
      stockMin: null,
      idGrupo: null,
      grupo: '',
      idMarca: null,
      marca: '',
      idTipoTributo: null,
      tipoTributo: '',
      abvTributo: '',
      ultCostoCompra: null,
      porcRecarga: null
    }
    
  }

  ngOnInit() {

    $('select').formSelect();


    $('.modal').modal({
      dismissible : false,
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    $('#modalFamilia').modal({
      dismissible : false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: () => this.getLstFamilias() 
    });

    $('#modalMarca').modal({
      dismissible : false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: () => this.getLstMarcas() 
    });

    $('#modalUniMed').modal({
      dismissible : false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: () => this.getLstUniMed() 
    });

    this.getLstProductos();
  }

  chargeLsts(){
    this.getLstUniMed();
    this.getLstFamilias();
    this.getLstMarcas();
    this.getLsTributos();
  }

  getLstProductos() {
    this.proSrv.getProductos().subscribe(
      success => {
        this.listaProductos = success;
      },
      err => { }
    );
  }

  getLstUniMed () {
    this.uniMedSrv.getUniMed().subscribe(
      success => {
        this.listaUniMed = success;
      },
      err => { }
    )
  }
  newUniMed(){
    $('#modalUniMed').modal({
      onCloseStart: () => this.getLstUniMed() 
    });
  }

  getLstFamilias () {
    this.fliaSrv.getFamilias().subscribe(
      success => {
        this.listaGrupo = success;
        console.log(this.listaGrupo);
      },
      err => { }
    )
  }
  newGrupo () {
    $('#modalFamilia').modal({
      onCloseStart: () => this.getLstFamilias() 
    });
  }

  getLstMarcas () {
    this.marcaSrv.getMarcas().subscribe(
      success => {
        this.listaMarca = success;
      },
      err => { }
    )
  }
  newMarca(){
    $('#modalMarca').modal({
      onCloseStart: () => this.getLstMarcas() 
    });
  }

  getLsTributos () {
    
    this.proSrv.getTributos(this.idInfoRefTributos).subscribe(
      success => {
        this.listaTributos = success;
      },
      err => { }
    )
  }

  getInfoProducto(id: number) {

    $('select').formSelect();
    this.updPro = true;
    this.chargeLsts();
    this.proSrv.getProductoById(id).subscribe(
      success => {
        this.datosProducto = success;
        console.log(id, this.datosProducto);
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
    M.updateTextFields();
  }

  genCodigoPro (){
    this.proSrv.getCodigo().subscribe (
      success => {
        this.datosProducto.cod = success.toString();  
      },
      err => { 
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
    setTimeout(() => {
      M.updateTextFields();
    }, 40);
  }

  resetForm() {

    this.updPro = false;
    this.datosProducto = {
      cod: '',
      nombre: '',
      descripcion: '',
      idUnidadMed: null,
      unidadMed: '',
      stockMin: null,
      idGrupo: null,
      grupo: '',
      idMarca: null,
      marca: '',
      idTipoTributo: null,
      tipoTributo: '',
      abvTributo: '',
      ultCostoCompra: null,
      porcRecarga: null
    }

  }

  guardar(forma: NgForm) {
    let campoNull : boolean = false;
    let producto = forma.value;
    console.log(producto);

    if (forma.value.cod == ""){
      M.toast({html:'El campo Codigo no puede estar vacio.', displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      campoNull = true;
    } else if (forma.value.nombre == ""){
      M.toast({html:'El campo Nombre no puede estar vacio.', displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      campoNull = true;
    } else if (forma.value.idUnidadMed == null){
      M.toast({html:'El campo Unidad de Medida no puede estar vacio.', displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      campoNull = true;
    }

    if (!campoNull) {
      if (this.updPro) {
        this.proSrv.updateProducto(this.datosProducto.id, producto).subscribe(
          success => {
            M.toast({html:'Datos del producto actualizados correctamente.', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
            $('#modalFichaProducto').modal('close');
            this.getLstProductos();
          },
          err => {
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
        );
      } else {
        this.proSrv.createProducto(producto).subscribe(
          success => {
            M.toast({html:'Producto creado correctamente.', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
            $('#modalFichaProducto').modal('close');
            this.getLstProductos();
          },
          err => { 
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
        );
      }
    }
  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

}
