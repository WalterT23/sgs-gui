import { StockDTO } from "../../gestion-stock/_dto/StockDTO";

export interface ProductoDTO {
    id?: number;
    cod: string;
    nombre: string;
    descripcion?: string;
    idUnidadMed?: number;
    unidadMed?: string;
    stockMin?: number;
    idGrupo?: number;
    grupo?: string;
    idMarca?: number;
    marca?: string;
    idTipoTributo?: number;
    tipoTributo?: string;
    abvTributo?: string;
    porcRecarga?: number;
    ultCostoCompra?: number;
}

export interface TributosDTO {
    id?: number;
    idInfoRef: number;
    descripcion: string;

}

export interface ProductoVentaDTO {
    costoBase: number;
    id: number;
    idListaPrecios: number;
    idProducto: number;
    idTipoRecarga: number;
    montoRecarga: number;
    precioVenta: number;
    stock: StockDTO;
}