import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ProductoDTO, TributosDTO, ProductoVentaDTO } from '../_dto/ProductoDTO';
import { ProductosByGroupDTO } from '../../gestion-descuentos/_dto/DescuentosDTO';


@Injectable()
export class ProductosService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getProductos(): Observable<ProductoDTO[]> {
    return this.http.get<ProductoDTO[]>(appPreferences.urlBackEnd + 'productos', this.auth.getHeaders());
  }

  getProductoById(id: number): Observable<ProductoDTO> {
    return this.http.get<ProductoDTO>(appPreferences.urlBackEnd + 'productos/' + id, this.auth.getHeaders());
  }

  getProductoByCod(cod: string): Observable<ProductoDTO> {
    return this.http.get<ProductoDTO>(appPreferences.urlBackEnd + 'productos/byCodigo/' + cod, this.auth.getHeaders());
  }

  getProductoByCod2(cod: string): Observable<ProductoVentaDTO> {
    return this.http.get<ProductoVentaDTO>(appPreferences.urlBackEnd + `listaPreciosDetalle/pVenta/${cod}`, this.auth.getHeaders());

  }

  likeProductos(cod: string): Observable<ProductoDTO[]> {
    return this.http.get<ProductoDTO[]>(appPreferences.urlBackEnd + 'productos/likeCod/' + cod, this.auth.getHeaders());
  }
  
  getProductosByGroup(data: ProductosByGroupDTO): Observable<ProductoDTO[]> {
    return this.http.post<ProductoDTO[]>(appPreferences.urlBackEnd + 'productos/byGroup', data, this.auth.getHeaders());
  }

  getCodigo (): Observable<number>{
    return this.http.get<number>(appPreferences.urlBackEnd + 'productos/genCodigo', this.auth.getHeaders())
  }

  getTributos (idInfoRef: number): Observable<TributosDTO[]>{
    return this.http.get<TributosDTO[]>(appPreferences.urlBackEnd + 'inforefopc/byIdRef/' + idInfoRef, this.auth.getHeaders());
  } 

  createProducto(datos: ProductoDTO): Observable<ProductoDTO> {
    return this.http.post<ProductoDTO>(appPreferences.urlBackEnd + 'productos', datos, this.auth.getHeaders());
  }

  updateProducto(id: number, data: ProductoDTO): Observable<ProductoDTO> {
    return this.http.put<ProductoDTO>(appPreferences.urlBackEnd + 'productos/' + id, data, this.auth.getHeaders())
  }

  deleteProducto(id: number): Observable<ProductoDTO> {
    return this.http.delete<ProductoDTO>(appPreferences.urlBackEnd + 'productos/' + id, this.auth.getHeaders())
  }

}
