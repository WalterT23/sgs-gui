import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthService } from '../../_services/auth.service';
import { VentaSendDto } from '../_dto/venta-send-dto';
import { VentaResponseDto } from '../_dto/venta-response-dto';


@Injectable()
export class VentasService {

  constructor(
    private authSrv: AuthService,
    private http: HttpClient
  ) { }

  registrarVenta(venta: VentaSendDto): Observable<VentaResponseDto> {

    return this.http.post<VentaResponseDto>(`${appPreferences.urlBackEnd}/cabeceraVenta/getResumenVenta`, venta, this.authSrv.getHeaders());

  }

  guardarVenta(dato: any): Observable<any>  {

    return this.http.post<any>(`${appPreferences.urlBackEnd}/cabeceraVenta/generarVenta`, dato, this.authSrv.getHeaders());

  }

  imprimirFactura(id: number): Observable<any> {
    return this.http.get<any>(`${appPreferences.urlBackEnd}/cabeceraVenta/imprimir/${id}`, this.authSrv.getHeaders());
  }

}
