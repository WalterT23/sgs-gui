
import { ProductoVentaResumen } from "./venta-send-dto";

export interface VentaResponseDto {
    montoTotal: number,
    iva5: number,
    iva10: number,
    exenta: number,
    gravada5: number,
    gravada10: number,
    descuentos: number,
    productos: ProductoVentaResumen[]
}
