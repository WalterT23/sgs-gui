import { VentaResponseDto } from "./venta-response-dto";
import { InfoRefOpcDTO } from "../../info-ref/_dto/InfoRefDTO";
import { DetallePagoDTO } from "./FacturaVentaDTO";

export interface VentaSendDto {
    idCliente: number;
    productos: ProductoVentaResumen[]
}

export interface ProductoVentaResumen { 
    id: number, 
    cantidad: number, 
    abvTributo: string, 
    precioProducto?: number, 
    total?: number, 
    totalDescuento?: number 
}

export interface CrearVentaDto {
    cliente: {
        id: number
    },
    infoVenta: VentaResponseDto,
    formasPago: MedioPagoDto[]
}

export interface MedioPagoDto {
    idMedioPago: number;
    montoPago: number;
    medioPago: InfoRefOpcDTO;
    detallesPagos: DetallePagoDTO[];
}