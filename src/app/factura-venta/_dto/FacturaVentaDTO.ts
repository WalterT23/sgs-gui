import { ProductoDTO } from "../../gestion-productos/_dto/ProductoDTO";
import { InfoRefOpcDTO } from "../../info-ref/_dto/InfoRefDTO";
import { ClienteDTO } from "../../gestion-clientes/_dto/ClienteDTO";

export interface FacturaVentaDTO {
    id?: number;
	idTimbradoPtoExp: number;
	nroFactura: string;
	fechaCreacion: Date;
	cajero: string;
	gravada10: number;
	gravada5: number;
	exenta: number;
	iva10: number;
	iva5: number;
	descuento: number;
	neto: number;
    idCliente: number;
    detalles: DetalleVentaDTO[];
    formasPago: FormasPagoDTO[];
    cliente: ClienteDTO;
    timbradoPtoExpedicion: TimbradoPuntoExpedicionDTO;

}

export interface DetalleVentaDTO {
	id: number;
	idCabVenta: number;
	idProducto: number;
	cantidad: number;
	precioTotal: number;
	tipoIdentificadorFiscal: string;
	porcentajeDescuento: number;
	montoDescuento: number;
	precioUnitario: number;
	producto: ProductoDTO;
}

export interface FormasPagoDTO {
    medioPago: InfoRefOpcDTO;
    detallesPagos: DetallePagoDTO[];
}

export interface TimbradoPuntoExpedicionDTO {
    timbrado: TimbradoDTO;
    puntoExpedicion: PuntoExpedicionDTO;
}

export interface EmpresaDTO {
    id: number;
	ruc: string;
	razonSocial: string;
	nombreFantasia: string;
	direccion: string;
	telefono: string;
}


export interface DetallePagoDTO {
    id?: number;
	idFormaPago: number;
	idBanco: number;
	numeroTarjeta: string;
	marca: string;
	nroCupon: string;
	codautorizacionNrocheque?: number;
}

export interface TimbradoDTO {
	id: number;
	numero: number;
	facturaInicial: number;
	facturaFinal: number;
	inicioVigencia: Date;
	finVigencia: Date;
	idEstado: number;
	usuarioCreacion: string;
    fechaCreacion: Date;  
}

export interface PuntoExpedicionDTO {
	id: number;
	puntoExp: number;
	idEstado: number;
	usuarioCreacion: string;
	fechaCreacion: Date; 
}