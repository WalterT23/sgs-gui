import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { VentasService } from './_services/ventas.service';
import { ClientesService } from '../gestion-clientes/_service/clientes.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';
import { StockService } from '../gestion-stock/_service/stock.service';
import { CajaUserService } from '../caja-user/_services/caja-user.service';
import { PtoExpedicionService } from '../gestion-puntos-expedicion/_services/pto-expedicion.service';
import { SucursalesService } from '../sucursales/_services/sucursales.service';
import { saveAs } from 'file-saver';

// DTO
import { FacturaVentaDTO, DetalleVentaDTO } from './_dto/FacturaVentaDTO';
import { ClienteDTO } from '../gestion-clientes/_dto/ClienteDTO';
import { ProductoDTO, ProductoVentaDTO } from '../gestion-productos/_dto/ProductoDTO';
import { GestionClientesComponent } from '../gestion-clientes/gestion-clientes.component';
import { StockDTO } from '../gestion-stock/_dto/StockDTO';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { DatePipe } from '@angular/common';
import { VentaSendDto, CrearVentaDto } from './_dto/venta-send-dto';
import { VentaResponseDto } from './_dto/venta-response-dto';
import { BancosService } from '../bancos/services/bancos.service';
import { BancosDTO } from '../bancos/_dto/BancosDTO';
import { CajaUserDTO } from '../caja-user/_dto/CajaUserDTO';
import { generarBlobForPDF } from '../_utils/millard-formatter';
import { PDetalleVentaPagos } from '../pipes/sumas.pipe';
import { PuntoExpDTO } from '../gestion-puntos-expedicion/_dto/PuntoExpDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-factura-venta',
  templateUrl: './factura-venta.component.html',
  styleUrls: ['./factura-venta.component.css']
})
export class FacturaVentaComponent implements OnInit {

  @ViewChild(GestionClientesComponent) cliente: GestionClientesComponent;
  public listaCajas: CajaUserDTO[];
  public datosFV: FacturaVentaDTO;
  public detalleFV: DetalleVentaDTO;
  public listaDetFV: DetalleVentaDTO[];
  public listaForDel: DetalleVentaDTO[];
  public restar: boolean = false;
  public datosCliente: ClienteDTO;
  public listaClientes: ClienteDTO[];
  public listaProd: ProductoDTO[];
  public lstProductos: ProductoDTO[];
  public listaStock: StockDTO[];
  public datosStock: StockDTO;
  public datosPtoExp: PuntoExpDTO;
  public totalFV: number = null;
  public i: number = null //index de la lista de productos en carga
  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  responseVenta: VentaResponseDto;
  listaFormaPago: InfoRefOpcDTO[] = [];
  listaTipoVenta: InfoRefOpcDTO[] = [];
  productoDet: ProductoDTO;
  fechaHoy: string;
  clienteDescripcion: string;
  rucCliente: string;
  telefonoCliente: string;
  cantidad: number;
  formaPagoSelected: string = '-1';
  lstClientes: ClienteDTO[] = [];
  lstMedioPago: InfoRefOpcDTO[] = [];
  lstBancos: BancosDTO[] = [];
  detalleVentaCompleta: CrearVentaDto;
  search:any;
  searchText: string = "";
  cajero: string = "";
  ptoExp: number;
  Sucursal: string = "";
  constructor(
    private cliSrv: ClientesService,
    private prodSrv: ProductosService,
    public auth: AuthService,
    private infoRef: InfoRefService,
    private ventaSrv: VentasService,
    private bancoSrv: BancosService,
    private cajaSrv: CajaUserService,
    private ptoExpSrv: PtoExpedicionService,
    private sucSrv: SucursalesService,
  ) {

    let dp = new DatePipe('es-PY');
    this.fechaHoy = dp.transform(new Date(), 'yyyy-MM-dd');
    this.listaCajas = [];
    this.listaClientes = [];
    this.listaProd = [];
    this.lstProductos = [];
    this.listaDetFV = [];
    this.listaForDel = [];
    this.datosFV = {
      id: null,
      idTimbradoPtoExp: null,
      nroFactura: '',
      fechaCreacion: null,
      cajero: '',
      gravada10: null,
      gravada5: null,
      exenta: null,
      iva10: null,
      iva5: null,
      descuento: null,
      neto: null,
      idCliente: null,
      detalles: [],
      formasPago: [],
      cliente: null,
      timbradoPtoExpedicion: null
    }

    this.detalleFV = {
      id: null,
      idCabVenta: null,
      idProducto: null,
      cantidad: null,
      precioTotal: null,
      tipoIdentificadorFiscal: '',
      porcentajeDescuento: null,
      montoDescuento: null,
      precioUnitario: null,
      producto: {
        cod: '',
        nombre: '',
        descripcion: '',
        idUnidadMed: null,
        unidadMed: '',
        stockMin: null,
        idGrupo: null,
        grupo: '',
        idMarca: null,
        marca: '',
        idTipoTributo: null,
        tipoTributo: '',
        abvTributo: '',
        ultCostoCompra: null,
        porcRecarga: null
      }
    }

  }

  ngOnInit() {

    $('select').formSelect();
    $('.modal').modal();
    $('#modalCliente').modal({
      dismissible: false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: () => this.getLstClientes()
    });

    console.log(this.auth._infoUser);
    this.getCajeros();
    this.getLstProductos();
    this.getLstMediosPagos();
    this.loadLstMedioPago();
    this.loadlstBanco();
  }

  getCajeros(){
    this.cajaSrv.getCajaUser().subscribe(
      success => {
        this.listaCajas = success.filter(x=> x.idUsuario == this.auth._infoUser.id);
        if(this.listaCajas.length == 1){
          this.cajero = this.auth._infoUser.nombre + ' ' + this.auth._infoUser.apellido; 
          this.ptoExp = this.listaCajas[0].puntoExp;
          this.ptoExpSrv.getPtosExpById(this.listaCajas[0].idPuntoExpedicion).subscribe(
            success2 =>{
              console.log(success2);
            }
          );
        }
        
      }
    );
    
  }

  buscarCliente(){
    this.clienteDescripcion = '';
    if (this.rucCliente) {
      this.cliSrv.likeCliente(this.rucCliente).subscribe(
        success => {
          if (success.length === 1) {
            let tmpCli = success[0];
            this.clienteDescripcion = `${tmpCli.razonSocial} - ${tmpCli.nombreFantasia}`;
            this.telefonoCliente = tmpCli.telefono;
            this.datosFV.idCliente = tmpCli.id;
          }
        }
      );
    }
  }

  searchCliente(idCliente: any, modal: boolean = false) {

    this.clienteDescripcion = '';

    if (idCliente) {
      this.cliSrv.likeCliente(idCliente).subscribe(
        success => {

          if (success.length === 1 && !modal) {
            let tmpCli = success[0];
            $('#txtCliente').val(tmpCli.ruc);
            this.clienteDescripcion = `${tmpCli.razonSocial} - ${tmpCli.nombreFantasia}`;
            this.datosFV.idCliente = tmpCli.id;

          } else if (success.length > 0 && !modal) {
            this.lstClientes = success;
            $('#txtCliente').val('')
            $('#busquedaCliente').modal('open')
          } else if (modal) {
            this.lstClientes = success;
          }
        }
      );
    }

  }

  seleccionarCliente(cliente: ClienteDTO) {

    $('#txtCliente').val(cliente.ruc);
    this.datosFV.idCliente = cliente.id;
    this.clienteDescripcion = `${cliente.razonSocial} - ${cliente.nombreFantasia}`;
    $('#busquedaCliente').modal('close');
    this.lstClientes = [];

    $('#textBuscarCliente').val('');

    setTimeout(() => {
      M.updateTextFields();
    }, 20);

  }

  searchProducto() {
    if ($('#txtProductoCod').val()) {
      this.prodSrv.getProductoByCod($('#txtProductoCod').val()).subscribe(
        success => {
          this.productoDet = success;
          $('#txtProductoCod').val(success.cod);
          $('#txtArticulo').val(success.nombre + '-' + success.descripcion + '-' + success.unidadMed);
          $('#txtPrecio').val(success.ultCostoCompra);
        }
      );
    }
  }

  seleccionarProducto(producto: ProductoDTO) {
    this.datosStock = null;
    this.productoDet = null;
    this.prodSrv.getProductoByCod(producto.cod).subscribe(
      success => {
        this.productoDet = success;
        this.prodSrv.getProductoByCod2(producto.cod).subscribe(
          success2 => {
            this.productoDet.ultCostoCompra = success2.precioVenta;
            this.datosStock= success2.stock;
            $('#txtStock').val(this.datosStock.stock);
          },
          err => {
            let msg = err.error;
            M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
            setTimeout(() => {
              $('#codProducto_txt').focus();
            }, 20);
          }
        );

        $('#txtProductoCod').val(success.cod);
        $('#txtArticulo').val(success.nombre + '-' + success.descripcion + '-' + success.unidadMed);
        $('#txtPrecio').val(this.productoDet.ultCostoCompra);
        
        
        $('#textBuscarProducto').val('');
        $('#busquedaProducto').modal('close');
        setTimeout(() => {
          M.updateTextFields();
        }, 20);
      },
      err => {
        let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  getLstMediosPagos() {

    this.infoRef.getInfoRefOpcByCodRef('med_pago').subscribe(
      success => {

        this.listaFormaPago = success;
        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      }
    );

  }

  loadlstBanco() {

    this.bancoSrv.getBancos().subscribe(
      success => {
        this.lstBancos = success;
      }
    );

  }

  loadLstMedioPago() {

    this.infoRef.getInfoRefOpcByCodRef('MED_PAGO').subscribe(
      success => {
        this.lstMedioPago = success;
      }
    );

  }

  // getLstTipoVenta() {
  //   this.infoRef.getInfoRefOpcByCodRef('tipos_lp').subscribe(
  //     success => {
  //       this.listaTipoVenta = success;
  //       setTimeout(() => {
  //         $('select').formSelect();
  //       }, 80);
  //     }
  //   );
  // }

  getLstClientes() {
    this.listaClientes = [];
    // $('select').formSelect('destroy');
    this.cliSrv.getClientes().subscribe(
      success => {
        this.listaClientes = success;
        console.log(this.listaClientes);
        setTimeout(() => {
          $('select').formSelect();
        }, 80);
      },
      err => {
        $('select').formSelect();
        M.toast({ html: 'No se pudieron obtener los datos de los clientes.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });

      }
    );
  }

  getInfoCliente(id: number) {

    $('select').formSelect();
    this.cliSrv.getClienteById(id).subscribe(
      success => {
        this.datosCliente = success;
      },
      err => {

      }
    );

  }

  getLstProductos() {
    this.prodSrv.getProductos().subscribe(
      success => {
        this.listaProd = success;
      },
      err => {
        M.toast({ html: 'No se pudieron obtener los productos.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });

      }
    )
  }

  getProductoByCod(cod: string, i: number) {

    if (cod) {

      let productoCargado = this.listaDetFV.filter(row => row.producto.cod == cod);


      if (productoCargado.length === 0) {


        console.log('pto control', i);
        this.prodSrv.getProductoByCod2(cod).subscribe(
          success => {
            this.listaDetFV[i].precioUnitario = success.precioVenta;
            this.listaDetFV[i].producto = success.stock.productoDTO;
          },
          err => {
            let msg = err.error;
            M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
            setTimeout(() => {
              $('#codProducto_txt').focus();
            }, 20);
          }
        );

      } else {

        $('#codProducto_txt').val('');
        let pos = $('#txtCantProducto_' + cod).data('pos');
        this.listaDetFV[pos].cantidad++;
        setTimeout(() => {
          $('#txtCantProducto_' + cod).select();
          $('#txtCantProducto_' + cod).focus();
        }, 22);

      }
    }
  }

  abrirModalBuscarCliente() {
    $('#busquedaCliente').modal('open');
    setTimeout(() => {
      $('#textBuscarCliente').focus();
    }, 20);
  }

  abrirModalBuscarProducto() {
    $('#busquedaProducto').modal({
      dismissible : false,
    });
    $('#busquedaProducto').modal('open');
    this.getLstProductos()
  }

  addProducto(form: NgForm) {
    if(this.datosStock.stock > 0 && (this.datosStock.stock >= this.cantidad )){
      let precioTotal = this.productoDet.ultCostoCompra * this.cantidad;
      if(precioTotal > 0){
        this.totalFV = this.totalFV + precioTotal;
        this.datosFV.neto = this.totalFV;

        let newPro: DetalleVentaDTO = {
          id: null,
          idCabVenta: null,
          idProducto: this.productoDet.id,
          cantidad: this.cantidad,
          precioTotal: precioTotal,
          tipoIdentificadorFiscal: '',
          porcentajeDescuento: null,
          montoDescuento: null,
          precioUnitario: this.productoDet.ultCostoCompra,
          producto: {
            id: this.productoDet.id,
            cod: this.productoDet.cod,
            nombre: this.productoDet.nombre,
            idUnidadMed: this.productoDet.idUnidadMed,
            unidadMed: this.productoDet.unidadMed,
            stockMin: this.productoDet.stockMin,
            idGrupo: this.productoDet.idGrupo,
            grupo: this.productoDet.grupo,
            idMarca: this.productoDet.idMarca,
            marca: this.productoDet.marca,
            idTipoTributo: this.productoDet.idTipoTributo,
            tipoTributo: this.productoDet.tipoTributo,
            abvTributo: this.productoDet.abvTributo,
            porcRecarga: this.productoDet.porcRecarga
          }
        };
        this.productoDet = null;
        console.log(newPro);
        $('#txtProductoCod').val('');
        $('#txtArticulo').val('');
        $('#txtPrecio').val('');
        $('#txtStock').val('');
        this.listaDetFV.push(newPro);
        form.reset();
        setTimeout(() => {
          $('#codProducto_txt').focus();
        }, 10);
      }
    }
  }

  lessProducto(i: number) {
    this.totalFV = this.totalFV - this.listaDetFV[i].precioTotal;
    this.datosFV.neto = this.totalFV;
    this.listaDetFV.splice(i, 1);
    //this.restar = true;
    //this.calcMontoTotal(this.listaDetFV[i])
  }

  calcMontoTotal(producto: DetalleVentaDTO, i?: number) {
    if (!this.restar) {
      let total = producto.precioUnitario * producto.cantidad;
      this.totalFV = this.totalFV + total;
    } else {
      this.totalFV = this.totalFV - producto.precioTotal;
    }
    this.datosFV.neto = this.totalFV;
    this.restar = false;
  }

  resetForm() {
    this.listaClientes = [];
    this.listaProd = [];
    this.listaDetFV = [];
    this.restar = false;
    this.totalFV = null;
    this.clienteDescripcion = '';
    $('#txtCliente').val('');
    this.formaPagoSelected = '';
    this.datosFV = {
      id: null,
      idTimbradoPtoExp: null,
      nroFactura: '',
      fechaCreacion: null,
      cajero: '',
      gravada10: null,
      gravada5: null,
      exenta: null,
      iva10: null,
      iva5: null,
      descuento: null,
      neto: null,
      idCliente: null,
      detalles: [],
      formasPago: [],
      cliente: null,
      timbradoPtoExpedicion: null
    }

    this.detalleFV = {
      id: null,
      idCabVenta: null,
      idProducto: null,
      cantidad: null,
      precioTotal: null,
      tipoIdentificadorFiscal: '',
      porcentajeDescuento: null,
      montoDescuento: null,
      precioUnitario: null,
      producto: null
    }
  }

  registrarVenta() {

    // Modal: modalPagos
    var permitir: Boolean = true;
    let venta: VentaSendDto = {
      idCliente: this.datosFV.idCliente,
      productos: []
    }
    if (this.datosFV.idCliente != null) {
      console.log('new',this.listaDetFV);
      for (let index = 0; index < this.listaDetFV.length; index++) {
        const detalle = this.listaDetFV[index];
        if (detalle.producto.id != null) {
          let detProd: { id: number, cantidad: number, abvTributo: string } = {
            id: detalle.producto.id,
            cantidad: detalle.cantidad,
            abvTributo: detalle.producto.abvTributo
          }
          venta.productos.push(detProd);
        }
      }
      if(venta.productos.length > 0){
        this.ventaSrv.registrarVenta(venta).subscribe(
          success => {
  
            this.responseVenta = success;
            $('#modalPagos').modal('open');
  
            this.detalleVentaCompleta = {
              cliente: {
                id: this.datosFV.idCliente
              },
              infoVenta: this.responseVenta,
              formasPago: [{
                idMedioPago: null,
                montoPago: 0,
                medioPago: {
                  id: null,
                  abreviatura: null,
                  descripcion: null,
                  id_info_ref: 0
                },
                detallesPagos: [{
                  idFormaPago: null,
                  idBanco: null,
                  marca: null,
                  numeroTarjeta: null,
                  nroCupon: null,
                  codautorizacionNrocheque: null
                }]
              }]
            }
          }
        );
      }
      
    } else {
      M.toast({ html: 'Debe seleccionar un cliente', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
    }
  }

  agregarFormaPago() {

    let aux = {
      idMedioPago: null,
      montoPago: 0,
      medioPago: {
        id: null,
        abreviatura: null,
        descripcion: null,
        id_info_ref: 0
      },
      detallesPagos: [{
        idFormaPago: null,
        idBanco: null,
        marca: null,
        numeroTarjeta: null,
        nroCupon: null,
        codautorizacionNrocheque: null
      }]
    }

    this.detalleVentaCompleta.formasPago.push(aux);

  }

  setearMedioDePagoId(pos: number, id: number) {

    this.detalleVentaCompleta.formasPago[pos].idMedioPago = id;

  }

  crearRegistroVenta() {

    let suma = new PDetalleVentaPagos();

    let totalPago = suma.transform(this.detalleVentaCompleta);

    if (this.responseVenta.montoTotal > totalPago) {
      alert("Registre los pagos correctamente para continuar")
      return false;
    }

    this.ventaSrv.guardarVenta(this.detalleVentaCompleta).subscribe(
      success => {

        let resul = confirm(`Desea imprimir la factura ${success.nroFactura}?`);

        if (resul) {
          this.imprimirFactura(success.id);
        }
        $('#modalPagos').modal('close')
        
        this.resetForm();
      }
    );
    this.rucCliente = "";
    this.clienteDescripcion = "";
    this.telefonoCliente = "";
  }

  imprimirFactura(id: number) {


    this.ventaSrv.imprimirFactura(id).subscribe(

      success => {

        let pdf = generarBlobForPDF(success.bytes);
        saveAs(pdf, success.fileName + ".pdf");
        console.log(success.type);

      }

    );

  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

}
