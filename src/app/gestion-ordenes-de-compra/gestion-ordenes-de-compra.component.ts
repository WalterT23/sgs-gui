
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';
import { GestionProveedoresComponent } from '../gestion-proveedores/gestion-proveedores.component';
import { formatearMillar, generarBlobForPDF } from '../_utils/millard-formatter';
import { saveAs } from 'file-saver';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { OrdenesDeCompraService } from './_service/ordenes-de-compra.service';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';

// DTO
import { OrdenCompraDTO, OCdetalleDTO } from './_dto/OrdenCompraDTO';
import { ProveedoresDTO } from '../gestion-proveedores/_dto/ProveedoresDTO';
import { ProductoDTO } from '../gestion-productos/_dto/ProductoDTO';

import { BusquedaGral } from '../pipes/busqueda.pipe';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-ordenes-de-compra',
  templateUrl: './gestion-ordenes-de-compra.component.html',
  styleUrls: ['./gestion-ordenes-de-compra.component.css']
})

export class GestionOrdenesDeCompraComponent implements OnInit {

  @ViewChild(GestionProveedoresComponent) proveedor: GestionProveedoresComponent;

  public datosOC: OrdenCompraDTO;
  public listaOC: OrdenCompraDTO[];
  public datosOCdetalle: OCdetalleDTO;
  public listaDetOC: OCdetalleDTO[];
  public updOC: boolean = false;
  public search: boolean = false;
  public restar: boolean = false;
  public fechaOC: Date;
  public listaProv: ProveedoresDTO[];
  public listaProd: ProductoDTO[];
  public datosProducto: ProductoDTO;
  public OrdenCompraFlag: boolean = true;
  public listaForAdd: OCdetalleDTO[];
  public listaForDel: OCdetalleDTO[];
  public totalOc: number = null;
  public indexDetOC: number = 0;

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  creg:number;
  searchText = "";
  constructor(

    private ocSrv: OrdenesDeCompraService,
    private provSrv: ProveedoresService,
    private prodSrv: ProductosService,
    private util: UtilsService,
    public auth: AuthService
  ) {
    this.listaOC = [];
    this.listaProv = [];
    this.listaDetOC = [];
    this.listaProd = [];
    this.listaForDel = [];
    this.datosOC = {
      id: null,
      nroOrdenCompra: '',
      idProveedor: null,
      proveedor: '',
      fechaRegistro: null,
      usuarioRegistro: '',
      fechaModificacion: null,
      usuarioModificacion: '',
      idEstado: null,
      estado: '',
      fechaEstado: null,
      montoTotal: null,
      totalIva5: null,
      totalIva10: null,
      totalExcenta: null,
      detalles: [],
      detallesAEliminar: [],
      detallesAAgregar: []
    }
    this.datosOCdetalle = {
      id: null,
      idOrdenCompra: null,
      idProducto: null,
      codProducto: '',
      producto: '',
      cantidad: null,
      ultCostoCompra: null,
      tributo: '',
      idTipoTributo: null,
      costoTotal: null
    }
  }

  ngOnInit() {
    $('select').formSelect();

    $('.tooltipped').tooltip();

    $('#modalOrdenCompra').modal({
      dismissible : false,
      ready: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    $('#modalProveedor').modal({
      dismissible : false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: () => this.getLstProveedores()
    });
    console.log("listaDetOC", this.listaDetOC);
    this.getLstOrdenesDeCompras();
    this.getLstProductos();
  }

  formatNumber(nro: number): string {
    return formatearMillar(nro);
  }

  chargeLstNewOrder() {
    this.fechaOC = new Date();
    this.getLstProductos();
    this.addProducto();
    this.getLstProveedores();
    console.log("datos cargados al apretar nueva OC");
  }

  chargeLst4Show(id: number) {
    this.getInfoOC(id);
    this.getLstProveedores();
  }

  getLstOrdenesDeCompras() {
    this.ocSrv.getOrdenesDeCompras().subscribe(
      success => {
        this.listaOC = success;
        console.log("listaOC", this.listaOC);
        //this.creg=this.listaOC.length;
       },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  getLstProveedores() {

    this.listaProv = [];

    this.provSrv.getProveedores().subscribe(
      success => {
        this.listaProv = success;
        console.log(this.listaProv);

        setTimeout(() => {
          $('select').formSelect();
        }, 80);

      },
      err => {
        $('select').formSelect();
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    );
  }

  getLstProductos() {
    this.prodSrv.getProductos().subscribe(
      success => {
        this.listaProd = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  getProductoByCod(cod: string, i: number) {
    console.log('llamo a getProductoByCod le paso cod: ' + cod + ' indice: ' + i);
    this.prodSrv.getProductoByCod(cod).subscribe(
      success => {
        console.log('El servicio retorna: ' + success);
        this.datosProducto = success;
        this.listaDetOC[i].producto = success.nombre;
        this.listaDetOC[i].idProducto = success.id;
        this.listaDetOC[i].tributo = success.abvTributo;
        this.listaDetOC[i].ultCostoCompra= success.ultCostoCompra;
        console.log( this.listaDetOC[i]);
      },
      err => {

        let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  getInfoOC(id: number) {
    this.getLstProveedores();
    $('select').formSelect();
    this.updOC = true;
    this.ocSrv.getOrdenDeCompraById(id).subscribe(
      success => {
        this.datosOC = success;
        this.listaDetOC = this.datosOC.detalles;
        console.log("datosOC", this.datosOC, "listaDetOC", this.listaDetOC);
        this.datosOC.detalles = [];
        this.datosOC.detallesAAgregar = [];
      },
      err => {
      }
    );
  }

  addProducto() {
    let newPro: OCdetalleDTO = {
      id: null,
      idOrdenCompra: null,
      idProducto: null,
      codProducto: '',
      producto: '',
      cantidad: null,
      ultCostoCompra: null,
      idTipoTributo: null,
      tributo: null,
      costoTotal: null
    };
    this.listaDetOC.push(newPro);
  }

  lessProducto(i: number) {
    if (this.listaDetOC[i].id) {
      this.listaForDel.push(this.listaDetOC[i]);
    }
    this.restar = true;
    this.listaDetOC.splice(i, 1)
    this.calcMontoTotal(this.listaDetOC[i])
  }

  get getMontoTotal(): number {
    let total = 0;
    this.listaDetOC.forEach(x=>{
      total = total + x.costoTotal
    })
    return total;
  }

  get getTotalIva5(): number {
    console.log('llamo get total iva5');
    let totalIva5 = 0;
    var arrayIva5 = this.listaDetOC.filter(x=> x.idTipoTributo == 15);
    arrayIva5.forEach(x=>{
      totalIva5 = totalIva5 + (x.costoTotal/22)
    })
    return totalIva5;
  }

  get getTotalIva10(): number {
    let totalIva10 = 0;
    var arrayIva10 = this.listaDetOC.filter(x=> x.idTipoTributo == 16);
    arrayIva10.forEach(x=>{
      totalIva10 = totalIva10 + (x.costoTotal/11)
    })
    return totalIva10;
  }

  calcMontoTotal(producto: OCdetalleDTO, i?: number) {
    console.log("calcuar monto total", producto, "lista detalles", this.listaDetOC);
    if (!this.restar) {
      let total = producto.ultCostoCompra * producto.cantidad;
      this.listaDetOC[i].costoTotal = total;
      this.totalOc = this.totalOc + total;
    } else {
      //this.totalOc = this.totalOc - producto.ultCostoCompra;
      this.totalOc = this.totalOc - producto.costoTotal;
    }
    this.datosOC.montoTotal = this.totalOc;
    this.restar = false;
    //this.addProducto();
  }

  guardar() {
    console.log("datos al apretar el btn guardar", this.listaDetOC, this.datosOC);
    if (this.datosOC.idProveedor == null) {
      M.toast({html: 'Debe seleccionar un proveedor.',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});

    }

    if (this.datosOC.idProveedor != null) {
      if (this.updOC) {

        this.datosOC.detallesAEliminar = this.listaForDel;

        for (const iterator of this.listaDetOC) {
          if (iterator.id == null) {
            this.datosOC.detallesAAgregar.push(iterator);
            console.log(this.datosOC.detallesAAgregar);
          } else {
            console.log(iterator);
            this.datosOC.detalles.push(iterator);
          }
        }
        console.log(this.datosOC);

        this.ocSrv.updateOrdenDeCompra(this.datosOC.id, this.datosOC).subscribe(
          success => {

            M.toast({html: 'Datos del producto actualizados correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
            $('#modalOrdenCompra').modal('close');
            this.getLstOrdenesDeCompras();
          },
          err => {
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }
        );
      } else {

        console.log("datosOC a enviar", this.datosOC);

        for (const iterator of this.listaDetOC) {
          if (iterator.idProducto != null) {
            this.datosOC.detalles.push(iterator);
            console.log(this.datosOC.detalles);
            setTimeout(() => {
            }, 40);
          }
        }
        console.log("datosOC a despues del push", this.datosOC);

        this.ocSrv.createOrdenDeCompra(this.datosOC).subscribe(
          success => {

            M.toast({html:"Orden de compra creada correctamente.",displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
             $('#modalOrdenCompra').modal('close');
            this.getLstOrdenesDeCompras();
          },
          err => {
            let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
           }
        );
      }
    }
  }

  anularOC(id: number) {
    this.ocSrv.anularOC(id).subscribe(
      success => {
        M.toast({html:"Orden de compra anulada.",displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
         this.getLstOrdenesDeCompras();
      },
      err => {

        let msg = err.error;
         M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  confirmar() {
    this.ocSrv.confirmarOC(this.datosOC.id).subscribe(
      success => {
        M.toast({html:"Orden de Compra confirmada.",displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
        $('#modalOrdenCompra').modal('close');
        this.getLstOrdenesDeCompras();
      },
      err => {

        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
    );
  }

  imprimirOrdenCompra(idOC: number) {
    console.log(idOC);
    this.ocSrv.getPdfBoletaPago(idOC).subscribe(
      success => {
        let pdf = generarBlobForPDF(success.bytes);
        saveAs(pdf, success.fileName + ".pdf");
        console.log(success.type);
      },
      error => {

      }
    );
  }

  resetForm() {
    this.updOC = false;
    this.search = false;
    this.restar = false;
    this.totalOc = null;
    this.datosOC = {
      id: null,
      nroOrdenCompra: null,
      idProveedor: null,
      proveedor: '',
      fechaRegistro: null,
      usuarioRegistro: '',
      fechaModificacion: null,
      usuarioModificacion: '',
      idEstado: null,
      estado: '',
      fechaEstado: null,
      montoTotal: null,
      totalIva5: null,
      totalIva10: null,
      totalExcenta: null,
      detalles: [],
      detallesAEliminar: [],
      detallesAAgregar: []
    }
    this.datosOCdetalle = {
      id: null,
      idOrdenCompra: null,
      idProducto: null,
      codProducto: '',
      producto: '',
      cantidad: null,
      ultCostoCompra: null,
      tributo: '',
      idTipoTributo: null,
      costoTotal: null
    }
    this.listaDetOC = [];
    this.listaForDel = [];

    this.getLstProveedores();

  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

  abrirModalBuscarProducto(index) {
    this.indexDetOC = index;
    $('#busquedaProducto').modal({
      dismissible : false,
    });
    $('#busquedaProducto').modal('open');
  }

  seleccionarProducto(producto: ProductoDTO) {
    this.prodSrv.getProductoByCod(producto.cod).subscribe(
      success => {
        this.datosProducto = success;
        this.listaDetOC[this.indexDetOC].codProducto = success.cod;
        this.listaDetOC[this.indexDetOC].producto = success.nombre;
        this.listaDetOC[this.indexDetOC].cantidad = 0;
        this.listaDetOC[this.indexDetOC].idProducto = success.id;
        this.listaDetOC[this.indexDetOC].tributo = success.abvTributo;
        this.listaDetOC[this.indexDetOC].idTipoTributo = success.idTipoTributo;
        this.listaDetOC[this.indexDetOC].ultCostoCompra= success.ultCostoCompra;
        this.listaDetOC[this.indexDetOC].costoTotal= 0;
      },
      err => {

        let msg = err.error;
            M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
    $('#busquedaProducto').modal('close');
  }


}
