import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { OrdenCompraDTO, OCdetalleDTO } from '../_dto/OrdenCompraDTO';


@Injectable()
export class OrdenesDeCompraService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getOrdenesDeCompras(): Observable<OrdenCompraDTO[]> {
    return this.http.get<OrdenCompraDTO[]>(appPreferences.urlBackEnd + 'ordenDeCompra', this.auth.getHeaders());
  }

  getOCconfirmadas(): Observable<OrdenCompraDTO[]> {
    return this.http.get<OrdenCompraDTO[]>(appPreferences.urlBackEnd + 'ordenDeCompra/confirmadas', this.auth.getHeaders());
  }

  getOrdenDeCompraById(id: number): Observable<OrdenCompraDTO> {
    return this.http.get<OrdenCompraDTO>(appPreferences.urlBackEnd + 'ordenDeCompra/' + id, this.auth.getHeaders());
  }

  // getDetalleByOrdenCompra(idOC: number): Observable<OCdetalleDTO[]> {
  //   return this.http.get<OCdetalleDTO[]>(appPreferences.urlBackEnd + 'ordenCompraDetalle/byIdOC/' + idOC, this.auth.getHeaders());
  // }

  createOrdenDeCompra(dataOC: OrdenCompraDTO): Observable<any>  {
    return this.http.post<any>(appPreferences.urlBackEnd + 'ordenDeCompra', dataOC, this.auth.getHeaders());
  }

  updateOrdenDeCompra(id:number, dataOC: OrdenCompraDTO): Observable<any> {
    
    return this.http.put<any>(appPreferences.urlBackEnd + 'ordenDeCompra/'+ id, dataOC, this.auth.getHeaders())
  }

  updateDetalleOC(dataDetOC: OCdetalleDTO[]): Observable<any> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'ordenDeCompra/', dataDetOC, this.auth.getHeaders())
  }

  deleteOrdenDeCompra(id: number): Observable<OrdenCompraDTO> {
    return this.http.delete<OrdenCompraDTO>(appPreferences.urlBackEnd + 'ordenDeCompra/' + id, this.auth.getHeaders())
  }

  deletePorducto(id: number): Observable<OCdetalleDTO> {
    return this.http.delete<OCdetalleDTO>(appPreferences.urlBackEnd + 'ordenCompraDetalle/' + id, this.auth.getHeaders())
  }

  anularOC (id:number): Observable<any> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'ordenDeCompra/anularOC/'+ id, this.auth.getHeaders())
  }

  getPdfBoletaPago(id: number): Observable<any>{
    return this.http.get<any>(appPreferences.urlBackEnd + 'ordenDeCompra/imprimir/'+ id, this.auth.getHeaders());
  }

  confirmarOC(id:number):Observable<any> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'ordenDeCompra/confirmarOC/'+ id, this.auth.getHeaders())
  }

  
  getOrdenCompraByNroOc(nro: string): Observable<any> {
    return this.http.get<any>(`${appPreferences.urlBackEnd}ordenDeCompra/byNroOc/${nro}`, this.auth.getHeaders());
  }

}
