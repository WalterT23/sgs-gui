export interface OrdenCompraDTO {
    id?: number;
    nroOrdenCompra: string;
    idProveedor?: number;
    proveedor: string;
    fechaRegistro?: Date;
    usuarioRegistro: string;
    fechaModificacion?: Date;
    usuarioModificacion?: string;
    idEstado?: number;
    estado: string;
    fechaEstado: Date;
    montoTotal: number;
    totalIva5: number;
    totalIva10: number;
    totalExcenta: number;
    detalles: OCdetalleDTO[];
    detallesAEliminar?: OCdetalleDTO[];
    detallesAAgregar: OCdetalleDTO[];
}

export interface OCdetalleDTO {
    id?: number;
    idOrdenCompra: number;
    idProducto: number;
    codProducto: string;
    producto: string;
    cantidad: number;
    ultCostoCompra: number;
    idTipoTributo: number;
    tributo: string;
    costoTotal?: number;
}