import { StockDTO } from "../../gestion-stock/_dto/StockDTO";

export interface MovimientoProductosDTO {
    fechaDesde: any;
    fechaHasta: any;
    idSucursal?: number;
    detalles: MovProductosDetDTO[];
    grupo?:any;
}

export interface MovProductosDetDTO {
    cod: string;
    nombre: string;
    stock: StockDTO;
    ingresos: number;
    egresos: number;
    
}