import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { appPreferences } from '../../environments/environment';
import { formatearMillar, generarBlobForPDF, newFormatFecha } from '../_utils/millard-formatter';

// Servicios
import { AuthService } from '../_services/auth.service';
import { ReporteMovimientoProductosService } from './_service/reporte-movimiento-productos.service';
import { saveAs } from 'file-saver';

//DTO
import { MovimientoProductosDTO, MovProductosDetDTO } from './_dto/MovimientoProductosDTO';
import { FiltrosChips } from '../_utils/show-chips-filter/plusFile/filtros-chips';

declare var $: any;
declare var M: any;


@Component({
  selector: 'app-reporte-movimiento-productos',
  templateUrl: './reporte-movimiento-productos.component.html',
  styleUrls: ['./reporte-movimiento-productos.component.css']
})
export class ReporteMovimientoProductosComponent implements OnInit {

  nameVarSession: any = "reporteMovimientoProductos";
  public listaMovPro: MovProductosDetDTO[];
  public filtro: MovimientoProductosDTO;
  public listaFiltros: FiltrosChips[];

  regXpag: number = 10; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación

  constructor(
    private reporSrv: ReporteMovimientoProductosService,
    public auth: AuthService
  ) {
    this.listaMovPro = [];
    this.listaFiltros = [];
    let hoy = new Date();
    let antes = new Date(new Date().setDate(new Date().getDate()-7));
    this.filtro = {
      fechaDesde: newFormatFecha(antes),
      fechaHasta: newFormatFecha(hoy),
      idSucursal: null,
      detalles: []
    }

    let filtro = window.sessionStorage.getItem(btoa(this.nameVarSession));
    if (filtro) {
      this.filtro = JSON.parse(filtro);
      let tmpD = this.filtro.fechaDesde.split('-');
      let tmpH = this.filtro.fechaHasta.split('-');

      setTimeout(() => {
        let fechaD: any = document.getElementById('fechaDesde');
        let fechaH: any = document.getElementById('fechaHasta');
        fechaD.value = `${tmpD[2]}/${tmpD[1]}/${tmpD[0]}`;
        fechaH.value = `${tmpH[2]}/${tmpH[1]}/${tmpH[0]}`;
        this.loadRegistros();
      }, 100);
    } else {
      this.limpiarFiltro();
    }
   }

  ngOnInit() {
    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.tabs').tabs();
    }, 50);

    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });
    this.getlstMovimientos();
    // this.mostrarChips();
  }

  getlstMovimientos() {
    console.log('dto enviado en la llamada',this.filtro);
    
    this.reporSrv.getMovimientosProductos(this.filtro).subscribe(
      success => {

        this.listaMovPro = success.detalles;
        
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  mostrarFrmFiltro() {
    swShowFrmFiltro('filtroReporte');
  }

  limpiarFiltro() {
    console.log('limpia el filtro')
    this.filtro = {
      fechaDesde: null,
      fechaHasta: null,
      detalles: []
    }
  }

  mostrarChips() {
    let fechaD = this.filtro.fechaDesde;
    let fechaH = this.filtro.fechaHasta;
    console.log('format', fechaD, fechaH);
    this.listaFiltros.push({
      descripcion: 'Fechas',
      id: 'fdfh',
      value:  fechaD + ' al ' + fechaH,
      bloquearCierre: true
    });
  }

  loadRegistros() {

    swShowFrmFiltro('filtroReporte', 'none');

    if (!this.filtro.fechaDesde || !this.filtro.fechaHasta) {
      alert("Los campos fecha desde y fecha hasta son obligatorios.");
      return false
    }
    console.log('fecha para la consulta', this.filtro);
    this.listaMovPro = [];
    this.listaFiltros = [];
    this.mostrarChips();

    window.sessionStorage.setItem(btoa(this.nameVarSession), JSON.stringify(this.filtro));

    this.getlstMovimientos();
  }

  getReporte(){
    this.reporSrv.getReporte(this.filtro).subscribe(
      success => {
        let excel = generarBlobForPDF(success.archivo,"application/xls");
        saveAs(excel, success.nombre + success.tipo);
        console.log(success.archivo);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  removerFiltro(event:any) {
    
  }
}
