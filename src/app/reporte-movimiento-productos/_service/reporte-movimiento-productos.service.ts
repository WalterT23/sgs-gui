import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MovimientoProductosDTO, MovProductosDetDTO } from '../_dto/MovimientoProductosDTO';
import { FileDTO } from '../../reporte-ventas-realizadas/_dto/VentasRealizadasDTO';

@Injectable()
export class ReporteMovimientoProductosService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getMovimientosProductos(fechas: MovimientoProductosDTO): Observable<MovimientoProductosDTO> {
    let tmp = Object.assign({},fechas)
    if(fechas.fechaDesde && fechas.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<MovimientoProductosDTO>(appPreferences.urlBackEnd + 'reportes/movimientoProductos', tmp, this.auth.getHeaders());
  }

  getReporte(datos: MovimientoProductosDTO): Observable<FileDTO> {
    let tmp = Object.assign({},datos)
    if(datos.fechaDesde && datos.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'reportes/movimientoProductos/impresion', tmp, this.auth.getHeaders());
  }

}
