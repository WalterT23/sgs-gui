import { Component, OnInit, Input } from '@angular/core';
import { appPreferences } from '../../environments/environment';
import { NgForm } from '@angular/forms';

// Servicios
import { TimbradoPtoExpService } from './_service/timbrado-pto-exp.service';
import { TimbradosService } from '../gestion-timbrados/_services/timbrados.service';
import { PtoExpedicionService } from '../gestion-puntos-expedicion/_services/pto-expedicion.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';

// DTO
import { TimbradoPtoExpDTO } from './_dto/TimbradoPtoExpDTO';
import { TimbradoDTO } from '../gestion-timbrados/_dto/TimbradoDTO';
import { PuntoExpDTO } from '../gestion-puntos-expedicion/_dto/PuntoExpDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-timbrado-punto-exp',
  templateUrl: './gestion-timbrado-punto-exp.component.html',
  styleUrls: ['./gestion-timbrado-punto-exp.component.css']
})
export class GestionTimbradoPuntoExpComponent implements OnInit {

  @Input() mantenimiento: boolean = false;
  @Input() idTPE: number;
  
  public datosTimbradoPtoExp: TimbradoPtoExpDTO;//datos de un timbrado
  public listaTimbradosPtoExp: TimbradoPtoExpDTO[];//lista de todos los timbrados del sistema para la sucursal conectada
  public listaTimbrados: TimbradoDTO[];
  public listaPtosExp: PuntoExpDTO[];
  public listaEstados: InfoRefOpcDTO[];

  constructor(
    private timbPtoExpSrv: TimbradoPtoExpService,
    private timbSrv: TimbradosService,
    private ptoExpSrv: PtoExpedicionService,
    private infrefSrv: InfoRefService,
    public auth: AuthService,
    private util: UtilsService
  ) { 
    this.listaTimbradosPtoExp = [];
    this.listaTimbrados = [];
    this.listaPtosExp = [];
    this.listaEstados = [];
    
    this.datosTimbradoPtoExp = {
      id: null,
      idTimbrado: null,
      idPtoExpedicion: null,
      numeroDesde: '',
      numeroHasta: '',
      ultimoNumero: '',
      idEstado: null,
      usuarioModificacion: '',
      fechaModificacion: null,
      tipoComprobante: '',
    }
  }

  ngOnInit() {

    if (!this.mantenimiento) {

      setTimeout(() => {
        $('.modal').modal({
          dismissible : false,
          onOpenEnd: function (modal, trigger) {
            M.updateTextFields();
            $('select').formSelect();

            console.log(modal, trigger);
          },
          onCloseEnd: function () {
            $('#cancelModalTimbPtoExp').click();
          }
        });
      }, 60);
    };
    this.getEstados();
    if (this.mantenimiento) {
      this.getLstTimbrado();
      this.getLstPtosExp();
      if (this.idTPE != null){
        this.getDatosTimbradoPtoExp(this.idTPE);
      }
    }
  }

  getEstados(){
    this.infrefSrv.getInfoRefOpcByCodRef('ESTADO').subscribe(
      success => {
        this.listaEstados = success;
        console.log('estados',this.listaEstados);
      },
      err => { }
    )
  }

  getLstTimbradoPtoExp(){
    this.timbPtoExpSrv.getTimbradosPtoExp().subscribe(
      success => {
        this.listaTimbradosPtoExp = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  getLstTimbrado() {
    this.timbSrv.getTimbrados().subscribe(
      success => {
        this.listaTimbrados = success.filter(row => (row.vigente == true));
        console.log('listimbrado',this.listaTimbrados);
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  getLstPtosExp() {
    this.ptoExpSrv.getPtosExp().subscribe(
      success => {
        this.listaPtosExp = success;
        console.log('listapto',this.listaPtosExp);
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  getDatosTimbradoPtoExp(id:number) {
    this.timbPtoExpSrv.getTimbradoPtoExpById(id).subscribe(
      success => {
        this.datosTimbradoPtoExp = success;
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
       }
    )
  }

  guardar(forma: NgForm){

    let timbradoPtoExp = forma.value;
    let faltaCompletar: boolean = false;
    console.log(timbradoPtoExp);
    if (!faltaCompletar) {
      this.timbPtoExpSrv.createTimbradoPtoExp(timbradoPtoExp).subscribe(
        success => {
          M.toast({html:'Asociación creada correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor'});
          $('#modalTimbradoPtoExp').modal('close');
          this.resetForm();
        },
        err => {
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
        }
      );
    } else {
      M.toast({html:'Porfavor complete todos los campos.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor'});
    } 
  }

  resetForm() {
    this.listaTimbradosPtoExp = [];
    this.datosTimbradoPtoExp = {
      id: null,
      idTimbrado: null,
      idPtoExpedicion: null,
      numeroDesde: '',
      numeroHasta: '',
      ultimoNumero: '',
      idEstado: null,
      usuarioModificacion: '',
      fechaModificacion: null,
      tipoComprobante: '',
    }
  }

}
