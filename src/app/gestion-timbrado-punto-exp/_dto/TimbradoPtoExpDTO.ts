export interface TimbradoPtoExpDTO {
    id?: number;
    idTimbrado: number;
    idPtoExpedicion: number;
    numeroDesde: string;
    numeroHasta: string;
    ultimoNumero?: string;
    idEstado?: number;
    usuarioModificacion?: string;
    fechaModificacion?: Date;
    tipoComprobante: string;
}