import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { TimbradoPtoExpDTO } from '../_dto/TimbradoPtoExpDTO';

@Injectable()
export class TimbradoPtoExpService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getTimbradosPtoExp(): Observable<TimbradoPtoExpDTO[]> {
    return this.http.get<TimbradoPtoExpDTO[]>(appPreferences.urlBackEnd + 'timbradoPtoExpedicion', this.auth.getHeaders());
  }

  getTimbradoPtoExpById(id: number): Observable<TimbradoPtoExpDTO> {
    return this.http.get<TimbradoPtoExpDTO>(appPreferences.urlBackEnd + 'timbradoPtoExpedicion/' + id, this.auth.getHeaders());
  }

  createTimbradoPtoExp(datos: TimbradoPtoExpDTO): Observable<TimbradoPtoExpDTO> {
    return this.http.post<TimbradoPtoExpDTO>(appPreferences.urlBackEnd + 'timbradoPtoExpedicion', datos, this.auth.getHeaders());
  }

  updateTimbradoPtoExp(id: number, data: TimbradoPtoExpDTO): Observable<TimbradoPtoExpDTO> {
    return this.http.put<TimbradoPtoExpDTO>(appPreferences.urlBackEnd + 'timbradoPtoExpedicion/' + id, data, this.auth.getHeaders())
  }

  deleteTimbradoPtoExp(id: number): Observable<TimbradoPtoExpDTO> {
    return this.http.delete<TimbradoPtoExpDTO>(appPreferences.urlBackEnd + 'timbradoPtoExpedicion/' + id, this.auth.getHeaders())
  }


}
