import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

// Servicio
import { AuthService } from '../_services/auth.service';

// DTO
import { UsuarioDTO } from './_dto/UsuarioDTO';
import { appPreferences } from '../../environments/environment';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public datosUser: UsuarioDTO;

  constructor(
    public auth: AuthService,
    public router: Router,
    private cookie: CookieService
  ) { 
    this.datosUser = {
      pass: '',
      usuario: ''
    }
  }

  ngOnInit() {
  }

  ingresar(form: NgForm) {
    console.log(form.value, this.auth._sucursal);
    if (this.datosUser.usuario != "" && this.datosUser.pass != "") {
      this.auth.getCredencial(form.value).subscribe(
        success => {

          this.cookie.set(appPreferences.cookName, btoa(form.value.pass + ':' + form.value.usuario));
          this.cookie.set(appPreferences.cookNamePer, JSON.stringify(success));
          
          this.auth._infoUser = success;
          let sucursalesVigentes = this.auth._infoUser.sucursales.filter(row => (row.vigente == true));
          this.auth._sucursal = (sucursalesVigentes.length == 1) ? this.auth._infoUser.sucursales[0].codSucursal : '-1';

          if (this.auth._sucursal) {    
            this.auth._auth = true;
            this.router.navigate(['/dashboard']);
            $('.dropdown-trigger').dropdown({
              coverTrigger: false
            });
          }else {
            M.toast({html:'Usuario no posee sucursales', displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          }

        },
        err => {
          // Ejemplo de uso del toast con la nueva version de materialize
          let msg = err.error;
          M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
          this.datosUser.pass = "";
        }
      );
    } else {
      M.toast({html:'Los campos usuario y contraseña son obligatorios.', displayLength:appPreferences.toastErrorDuration, classes:'toastErrorColor'});
    }
  }
  mostrarPass(e: string) {
    $('#' + e).prop('type', 'text');
    $('#' + e + "Icon").text('visibility');
  }

  ocultarPass(e: string) {
    $('#' + e).prop('type', 'password');
    $('#' + e + "Icon").text('visibility_off');
  }

}
