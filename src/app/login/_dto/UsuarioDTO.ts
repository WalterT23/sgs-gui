export interface UsuarioDTO {
    usuario: string;
    pass: string;
}

export interface ResponseUserDTO {
    id: number;
    nombre: string;
    apellido: string;
    usuario: string;
    pass:string;
    idEstado: number;
    sucursales?: UsuSucursalesDTO[];
    funcionalidades?: string[];
    roles?: any[];
    accesstoken: string;
}

export interface UsuSucursalesDTO {
    id?: number;
    idUsuario?: number;
    idSucursal: number;
    codSucursal?: string;
    nombreSucursal?: string;
    inicioVigencia?: Date;
    finVigencia?: Date;
    fechaRegistro?: Date;
    vigente?: Boolean;
}
