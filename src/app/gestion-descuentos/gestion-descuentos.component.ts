import { Component, OnInit } from '@angular/core';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { DescuentosServiceService } from './_services/descuentos-service.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { FamiliaService } from '../mantenimiento-familia/_service/familia.service';

//DTO
import { appPreferences, environment } from '../../environments/environment';
import { DescuentoDTO, DescuentoDetallesDTO, ProductosByGroupDTO } from './_dto/DescuentosDTO';
import { ProductoDTO } from '../gestion-productos/_dto/ProductoDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { FamiliaDTO } from '../mantenimiento-familia/_dto/FamiliaDTO';


declare var $: any;
declare var M: any;


@Component({
  selector: 'app-gestion-descuentos',
  templateUrl: './gestion-descuentos.component.html',
  styleUrls: ['./gestion-descuentos.component.css']
})
export class GestionDescuentosComponent implements OnInit {
  
  public datosCD: DescuentoDTO;
  public listaDescuentos: DescuentoDTO[];
  public datosDD: DescuentoDetallesDTO;
  public listaDsctoDetalle: DescuentoDetallesDTO[];
  public lstProductos: ProductoDTO[];
  public listaTiposDscto: InfoRefOpcDTO[];
  public listaGrupos: FamiliaDTO[];
  public updDscto: boolean = false;
  public onlyView: boolean = false;
  public datosParaCons: ProductosByGroupDTO;
  public monto: number = null;
  public porcentaje: number = null;
  public cantLlevar: number = null;
  public cantPagar: number = null;
  public incSubgroups: boolean = false;
  public grupos: any = [];
  public tipDscto: String = '';

  regXpag: number = 5; p: number = 1;  //variable que utiliza la paginación
  regXpag2: number = 6; p2: number = 1; 
  search:any;

  constructor(
    private descuentosSrv: DescuentosServiceService,
    private productosSrv: ProductosService,
    private infrefSrv: InfoRefService,
    private grupoSrv: FamiliaService,
    public auth: AuthService,
    private util: UtilsService
  ) {
    this.listaDescuentos = [];
    this.listaDsctoDetalle = [];
    this.lstProductos = [];
    this.listaTiposDscto = [];
    this.listaGrupos = [];
    this.datosCD = {
      id: null,
      idTipoDescuento: null,
      descripcion: '',
      usuarioCreacion: '',
      fechaCreacion: null,
      tipoDescuento: null,
      detalles: []
    }
    this.datosDD = {
      id: null,
      idDescuento: null,
      idProducto: null,
      cantACobrar: null,
      cantALlevar: null,
      porcentaje: null,
      monto: null,
      producto: null
    }
    this.datosParaCons = {
      incSubgroups: false,
      grupos: [],
      productos: []
    }
   }

  ngOnInit() {
    
    $('#modalDescuento').addClass('hide');
    $('select').formSelect();
    $('.modal').modal({
      dismissible : false,
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    this.getLstDescuentos();
    this.getLstProductos();
    // this.getTiposDescuentos();
    // 
  }

  getTiposDescuentos(){
    this.infrefSrv.getInfoRefOpcByCodRef('TIP_DESC').subscribe(
      success => {
        this.listaTiposDscto = success;
      },
      err => { }
    )
  }

  tipoDescuento(abv?: string){
    let aux: any = null;
    if(this.datosCD.idTipoDescuento != null && this.listaDescuentos != null){
      aux = this.listaTiposDscto.filter(row => (row.abreviatura == abv));
      if(aux.length == 1){
        return aux[0].id;
      }
    } else {
      return 0;
    }
  }

  getLstFamilias () {
    this.grupoSrv.getFamilias().subscribe(
      success => {
        this.listaGrupos = success;
        setTimeout(() => {
          $('select').formSelect();
        }, 20);
      },
      err => { }
    )
  }

  getLstDescuentos() {
    this.descuentosSrv.getDescuentos().subscribe(
      success => {
        this.listaDescuentos = success;
        console.log('descuentos',this.listaDescuentos);
      },
      err => { }
    );
  }

  getInfoDescuento(id: number){
    this.descuentosSrv.getDescuentoById(id).subscribe(
      success => {
        this.datosCD = success;
        console.log ('datos del descuento selecionado',this.datosCD);
      },
      err => { }
    );
    this.getTiposDescuentos();
  }

  getInfoDetallesDscto(data: DescuentoDTO){
    
    setTimeout(() => {
      M.updateTextFields();
    }, 100);

    this.getLstProductos();
    this.tipDscto = data.tipoDescuento.abreviatura;
    if (this.listaDescuentos != null) {
      this.descuentosSrv.getDsctoDetalleByIdDescuento(data.id).subscribe(
        success => {
          this.listaDsctoDetalle = success;
          console.log('datos del descuento selecionado', this.listaDsctoDetalle);
        },
        err => { }
      );
    }
  }

  modalDsctoClose(){
    this.tipDscto = '';
    this.resetFormDD();
  }

  getLstProductos(){
    this.productosSrv.getProductos().subscribe(
      success => {
        this.lstProductos = success;
        console.log ('lista productos',this.lstProductos);
      },
      err => { }
    );
  }

  addNewDscto() {
    $('#listaDescuentos').addClass('hide');
    $('#modalDescuento').removeClass('hide');
    this.resetFormDC();
    setTimeout(() => {
      this.getLstProductos();
      this.getTiposDescuentos();
      this.getLstFamilias();
    }, 100);
  }

  editDescuento(id:number){
    $('#tableDetDscto').removeClass('hide');
    this.addNewDscto();
    this.getInfoDescuento(id);
    this.updDscto = true;
    setTimeout(() => {
      M.updateTextFields();
      $('select').formSelect();
      $('.tooltipped').tooltip();
    }, 50);
  }

  bandera(){
    if (this.incSubgroups) {
      this.incSubgroups = false;
    }else {
      this.incSubgroups= true;
    }
    setTimeout(() => {
      console.log(this.incSubgroups);
    }, 50);
  }

  goBack(): void {
    $('#modalDescuento').addClass('hide');
    $('#tableDetDscto').addClass('hide');
    $('#listaDescuentos').removeClass('hide');
    this.updDscto = false;
  }

  codProducto(idProducto: number){
    let aux = this.lstProductos.filter(row => (row.id == idProducto));
    if(aux.length > 0) {
      return aux[0].cod;
    }else {
      return ''
    }
  }
  nombreProducto(idProducto: number){
    let aux = this.lstProductos.filter(row => (row.id == idProducto));
    if(aux.length > 0){
      return aux[0].nombre;
    }else {
      return ''
    }
  }

  cargarGrupos(array: any[]){
    console.log(this.grupos, array);
  }

  mostrarInfo(d: any) {
    console.log(d);
  }

  inicializarV(){
    this.porcentaje = null;
    this.monto= null;
    this.cantLlevar= null;
    this.cantPagar= null;
  }

  crearDscto(){
    $('#tableDetDscto').removeClass('hide');
    this.datosCD.detalles = [];
    this.productosSrv.getProductosByGroup(this.datosParaCons).subscribe(
      success => {
        this.lstProductos = success;
        for (let index = 0; index < success.length; index++) {
          let aux: DescuentoDetallesDTO = {
            idProducto: success[index].id,
            producto: {
              cod: success[index].cod,
              nombre: success[index].nombre
            }
          }
          this.datosCD.detalles.push(aux);
          if (this.porcentaje != null) {
            this.datosCD.detalles[index].porcentaje = this.porcentaje;
          } else if (this.monto != null){
            this.datosCD.detalles[index].monto = this.monto;
          } else if (this.cantLlevar != null || this.cantPagar != null) {
            if (this.cantLlevar != null) {
            this.datosCD.detalles[index].cantALlevar = this.cantLlevar;
            }
            if (this.cantPagar != null) {
              this.datosCD.detalles[index].cantACobrar = this.cantPagar;
            }
          }
          
        }
        console.log ('lista detalles descuento',this.datosCD.detalles);
      },
      err => { }
    );
  }

  guardar(){
    console.log("datos al apretar el btn guardar",this.datosCD);
    let aux: boolean = false;
    if(this.datosCD.idTipoDescuento == null || this.datosCD.descripcion.trim()==''){
      M.toast({ html:"Debe cargar los datos del tipo y descripcion del descuento", displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      aux = true;
    }
    if (this.datosCD.idTipoDescuento == this.tipoDescuento('PORC')) {
      for (let index = 0; index < this.datosCD.detalles.length; index++) {
        const element = this.datosCD.detalles[index];
        if (element.idProducto == null || element.porcentaje == null) {
          M.toast({ html:"Debe cargar el porcentaje para todos los productos", displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          aux = true;
          break;
        }
      }
    } else if (this.datosCD.idTipoDescuento == this.tipoDescuento('MFIJO')) {
      for (let index = 0; index < this.datosCD.detalles.length; index++) {
        const element = this.datosCD.detalles[index];
        if (element.idProducto == null || element.monto == null) {
          M.toast({ html:"Debe cargar el monto para todos los productos", displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          aux = true;
          break;
        }
      }
    } else if (this.datosCD.idTipoDescuento == this.tipoDescuento('PXY')) {
      for (let index = 0; index < this.datosCD.detalles.length; index++) {
        const element = this.datosCD.detalles[index];
        if (element.idProducto == null || element.cantACobrar == null || element.cantALlevar == null) {
          M.toast({ html:"Debe cargar la cantidad a llevar y cobrar para todos los productos", displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          aux = true;
          break;
        }
      }
    }
    if (!aux) {
      if (this.updDscto) {
        this.descuentosSrv.updateDescuento(this.datosCD.id, this.datosCD).subscribe(
          () => {
            M.toast({ html:"Descuento actualizado correctamente.", displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
            this.goBack();
            this.getLstDescuentos();
          },
          err => {
            let msg = err.error;
            M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          }
        );
      }else {
        this.descuentosSrv.createDescuento(this.datosCD).subscribe(
          () => {
            M.toast({ html:"Descuento creado correctamente.", displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
            this.goBack();
            this.getLstDescuentos();
          },
          err => {
            let msg = err.error;
            M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          }
        );
      }
    }
    
  }

  limpiar(){
    this.resetFormDC();
    this.inicializarV();
  }

  resetFormDC(){
    this.datosCD = {
      id: null,
      idTipoDescuento: null,
      descripcion: '',
      usuarioCreacion: '',
      fechaCreacion: null,
      tipoDescuento: null,
      detalles: []
    }
  }
  resetFormDD(){
    this.datosDD = {
      id: null,
      idDescuento: null,
      idProducto: null,
      cantACobrar: null,
      cantALlevar: null,
      porcentaje: null,
      monto: null,
      producto: null
    }
  }

  getProductoByCod(cod: string, i: number) {
    console.log('que tiene cargado inicialmente',this.datosCD.detalles, i)
    if (cod){
      let productoCargado: any[] = null;
      productoCargado = this.datosCD.detalles.filter(row => row.producto != null && row.producto.cod == cod);
      console.log('pro cargado',productoCargado)
      if (productoCargado.length === 0) {

        this.productosSrv.getProductoByCod2(cod).subscribe(
          success => {
            this.datosCD.detalles[i].idProducto = success.stock.idProducto;
            this.datosCD.detalles[i].producto = success.stock.productoDTO;
            if(this.updDscto){
              this.datosCD.detalles[i].idDescuento =this.datosCD.id;
            }
          },
          err => {
            let msg = err.error;
            M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
          }
        );
        
      }else if (productoCargado.length > 0) {
        M.toast({ html: "el producto ya esta agregado", displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        $('#codProducto_txt').val('');
        $('#codProducto_txt').focus();
      }

    }
  }

  addProducto(){
    let newPro: DescuentoDetallesDTO = {
      id: null,
      idDescuento: null,
      idProducto: null,
      cantACobrar: null,
      cantALlevar: null,
      porcentaje: null,
      monto: null,
      producto: null
    }
    this.datosCD.detalles.push(newPro);
    $('#codProducto_txt').focus();
    console.log('despues de hacer el push',this.datosCD.detalles)
  }

  lessProducto(i: number) {
    console.log('llama a esto?',i)
    this.datosCD.detalles.splice(i, 1);
  }


  changePag(pagNumber: number){
    this.p = pagNumber;
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

}
