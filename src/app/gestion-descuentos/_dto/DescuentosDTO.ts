import { InfoRefOpcDTO } from "../../info-ref/_dto/InfoRefDTO";
import { FamiliaDTO } from "../../mantenimiento-familia/_dto/FamiliaDTO";
import { ProductoDTO } from "../../gestion-productos/_dto/ProductoDTO";

export interface DescuentoDTO {
    id?: number;
    idTipoDescuento: number; //id inforefopc
    descripcion: string;
    usuarioCreacion: string;
    fechaCreacion: Date;
    DsctoDetalle?: Boolean;
    tipoDescuento: InfoRefOpcDTO;
    detalles: DescuentoDetallesDTO[];
}

export interface DescuentoDetallesDTO {
    id?: number;
    idDescuento?: number;
    idProducto: number;
    cantACobrar?: number;
    cantALlevar?: number;
    porcentaje?: number;
    monto?: number;
    producto?: ProductoDTO;
}

export interface ProductosByGroupDTO {
    incSubgroups: boolean;
    grupos: FamiliaDTO[];
    productos: ProductoDTO[];
}

