import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DescuentoDTO, DescuentoDetallesDTO } from '../_dto/DescuentosDTO';

@Injectable()
export class DescuentosServiceService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getDescuentos(): Observable<DescuentoDTO[]> {
    return this.http.get<DescuentoDTO[]>(appPreferences.urlBackEnd + 'descuentos', this.auth.getHeaders());
  }

  getDescuentoById(id: number): Observable<DescuentoDTO> {
    return this.http.get<DescuentoDTO>(appPreferences.urlBackEnd + 'descuentos/' + id, this.auth.getHeaders());
  }

  createDescuento(data: DescuentoDTO): Observable<any> {
    return this.http.post<any>(appPreferences.urlBackEnd + 'descuentos', data, this.auth.getHeaders());
  }

  updateDescuento(id: number, data: DescuentoDTO): Observable<any> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'descuentos/' + id, data, this.auth.getHeaders())
  }

  deleteDescuento(id: number): Observable<DescuentoDTO> {
    return this.http.delete<DescuentoDTO>(appPreferences.urlBackEnd + 'descuentos/' + id, this.auth.getHeaders())
  }

  getDsctoDetalle(): Observable<DescuentoDetallesDTO[]> {
    return this.http.get<DescuentoDetallesDTO[]>(appPreferences.urlBackEnd + 'descuentosDetalles', this.auth.getHeaders());
  }

  getDsctoDetalleByIdDescuento(id: number): Observable<DescuentoDetallesDTO[]> {
    return this.http.get<DescuentoDetallesDTO[]>(appPreferences.urlBackEnd + 'descuentosDetalles/byIdDescuento/' + id, this.auth.getHeaders());
  }

  getDsctoDetalleById(id: number): Observable<DescuentoDetallesDTO> {
    return this.http.get<DescuentoDetallesDTO>(appPreferences.urlBackEnd + 'descuentosDetalles/' + id, this.auth.getHeaders());
  }

  createDsctoDetalle(data: DescuentoDetallesDTO): Observable<any> {
    return this.http.post<any>(appPreferences.urlBackEnd + 'descuentosDetalles', data, this.auth.getHeaders());
  }

  updateDsctoDetalle(id: number, data: DescuentoDetallesDTO): Observable<any> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'descuentosDetalles/' + id, data, this.auth.getHeaders())
  }

  deleteDsctoDetalle(id: number): Observable<DescuentoDetallesDTO> {
    return this.http.delete<DescuentoDetallesDTO>(appPreferences.urlBackEnd + 'descuentosDetalles/' + id, this.auth.getHeaders())
  }

}
