import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProMasVendido } from '../_dto/ReportesVentasDTO';
import { FileDTO } from '../../reporte-ventas-realizadas/_dto/VentasRealizadasDTO';

@Injectable()
export class ReporteVentaProductosService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getVentasRealizadas(fechas: ProMasVendido): Observable<ProMasVendido> {
    let tmp = Object.assign({},fechas)
    if(fechas.fechaDesde && fechas.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<ProMasVendido>(appPreferences.urlBackEnd + 'reportes/productosVendidos', tmp, this.auth.getHeaders());
  }

  getReporte(datos: ProMasVendido): Observable<FileDTO> {
    let tmp = Object.assign({},datos)
    if(datos.fechaDesde && datos.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'reportes/productosVendidos/impresion', tmp, this.auth.getHeaders());
  }

}
