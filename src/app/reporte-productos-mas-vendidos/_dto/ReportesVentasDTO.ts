import { ProductoDTO } from "../../gestion-productos/_dto/ProductoDTO";

export interface ProMasVendido {
    fechaDesde: any;
    fechaHasta: any;
    grupo: Boolean;
    subGrupo: Boolean;
    idSucursal?: number;
    detalles?: ProMasVendidoDet[];
    datosGraphics?: DatosForGraphic[];
}

export interface ProMasVendidoDet {
    idProducto:number;
    grupo: string;
    cantVendida: number;
    montoRecaudado: number;
    producto: ProductoDTO;
}

export interface DatosForGraphic {
    nombre: string;
    cantidad: number;
    monto: number;
    totCant: number;
    totMonto: number
}
