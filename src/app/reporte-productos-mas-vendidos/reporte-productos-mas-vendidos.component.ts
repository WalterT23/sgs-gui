import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { appPreferences } from '../../environments/environment';
import { formatearMillar, generarBlobForPDF } from '../_utils/millard-formatter';
import { pie_graphic } from '../_utils/chartjs-options';
import { Chart, registerables } from 'chart.js';
import 'chartjs-plugin-labels';

// Servicios
import { AuthService } from '../_services/auth.service';
import { ReporteVentaProductosService } from './_services/reporte-venta-productos.service';
import { saveAs } from 'file-saver';

//DTO
import { ProMasVendido, ProMasVendidoDet, DatosForGraphic } from './_dto/ReportesVentasDTO';
import { FiltrosChips } from '../_utils/show-chips-filter/plusFile/filtros-chips';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-reporte-productos-mas-vendidos',
  templateUrl: './reporte-productos-mas-vendidos.component.html',
  styleUrls: ['./reporte-productos-mas-vendidos.component.css']
})
export class ReporteProductosMasVendidosComponent implements OnInit {

  nameVarSession: any = "reporteProMasVendidos";
  public listaProductosVen: ProMasVendidoDet[];
  public filtro: ProMasVendido;
  public listaFiltros: FiltrosChips[];

  chartCant: Chart; 
  chartMonto: Chart;
  @ViewChild('grafCant') grafCant: ElementRef;
  @ViewChild('grafMonto') grafMonto: ElementRef;
  public colorOperacion = "#43a047";
  public datosGrap: DatosForGraphic[] ;

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  regXpag2: number = 5; //variable que utiliza la paginación
  p2: number = 1; //variable que utiliza la paginación

  constructor(
    private reporSrv: ReporteVentaProductosService,
    public auth: AuthService

  ) {
    Chart.register(...registerables);
    this.listaProductosVen = [];
    this.datosGrap = [];
    this.filtro = {
      fechaDesde: null,
      fechaHasta: null,
      grupo: false,
      subGrupo: false,
      detalles: [],
      datosGraphics: []
    }

    let filtro = window.sessionStorage.getItem(btoa(this.nameVarSession));
    if (filtro) {
      this.filtro = JSON.parse(filtro);
      this.filtro.grupo = false;
      let tmpD = this.filtro.fechaDesde.split('-');
      let tmpH = this.filtro.fechaHasta.split('-');

      setTimeout(() => {
        let fechaD: any = document.getElementById('fechaDesde');
        let fechaH: any = document.getElementById('fechaHasta');
        fechaD.value = `${tmpD[2]}/${tmpD[1]}/${tmpD[0]}`;
        fechaH.value = `${tmpH[2]}/${tmpH[1]}/${tmpH[0]}`;
        
      }, 100);
    } else {
   
    }

  }

  ngOnInit() {
    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.tabs').tabs();
    }, 50);

    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });

    this.getlstProductosVendidos();
  }

  pressCheck(){
    if(this.filtro.grupo == true){
      this.filtro.grupo = false;
    }else {
      this.filtro.grupo = true;
    }
    console.log ('estado check despues de cambiar',this.filtro)
  }

  getlstProductosVendidos() {
    console.log('dto enviado en la llamada',this.filtro);
    if(this.filtro.grupo == null) {this.filtro.grupo= false}
    
    this.reporSrv.getVentasRealizadas(this.filtro).subscribe(
      success => {
        this.listaProductosVen = success.detalles;
        this.datosGrap = success.datosGraphics;
        setTimeout(() => {
          this.graphicCreate();
        }, 50);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  graphicCreate(){
    if (this.chartCant != null) { this.chartCant.destroy() }
    if (this.chartMonto != null) { this.chartMonto.destroy() }

    let nombres: string[]= []; 
    let montos: number[]= []; 
    let cantidades: number[]= []; 
    let colores: string[]= [];
  
    this.datosGrap.forEach(element => {
      nombres.push(element.nombre);
      cantidades.push(element.cantidad);
      montos.push(element.monto);
    });

    for (let index = 0; index < nombres.length; index++) {
      colores.push(randomColor(0.8));
    };

    let ctx01 = this.grafCant.nativeElement.getContext('2d')
    this.chartCant = new Chart(ctx01,
      {
        type: 'pie',
        data: { 
                labels: nombres,
                datasets: [
                  { backgroundColor: colores,
                    data: cantidades
                  }
                ] 
              },
        //options: pie_graphic
      }
    );
    let ctx02 = this.grafMonto.nativeElement.getContext('2d')
    this.chartMonto = new Chart(ctx02,
      {
        type: 'pie',
        data: { 
                labels: nombres,
                datasets: [
                  { backgroundColor: colores,
                    data: montos
                  }
                ] 
              },
        //options: pie_graphic
      }
    );
  }

  mostrarFrmFiltro() {
    swShowFrmFiltro('filtroReporte');
    // this.filtro.fechaDesde= null;
    // this.filtro.fechaHasta= null;
  }

  limpiarFiltro() {
    console.log('limpia el filtro')
    this.filtro = {
      fechaDesde: null,
      fechaHasta: null,
      grupo: false,
      subGrupo: false
    }
  }

  mostrarChips() {
    let fechaD = this.filtro.fechaDesde;
    let fechaH = this.filtro.fechaHasta;
    console.log('format', fechaD, fechaH);
    this.listaFiltros.push({
      descripcion: 'Fechas',
      id: 'fdfh',
      value:  fechaD + ' al ' + fechaH,
      bloquearCierre: true
    });
    if(this.filtro.grupo == true){
      this.listaFiltros.push({
        descripcion: 'Resultados por Grupo de productos',
        id: 'gp',
        bloquearCierre: true
      });
    }
  }

  loadRegistros() {

    swShowFrmFiltro('filtroReporte', 'none');

    if (!this.filtro.fechaDesde || !this.filtro.fechaHasta) {
      alert("Los campos fecha desde y fecha hasta son obligatorios.");
      return false
    }
    console.log('fecha para la consulta', this.filtro);
    this.listaProductosVen = [];
    this.datosGrap = [];
    this.listaFiltros = [];
    this.mostrarChips();

    window.sessionStorage.setItem(btoa(this.nameVarSession), JSON.stringify(this.filtro));

    this.getlstProductosVendidos();

  }

  getReporte(){
    this.reporSrv.getReporte(this.filtro).subscribe(
      success => {
        let excel = generarBlobForPDF(success.archivo,"application/xls");
        saveAs(excel, success.nombre + success.tipo);
        console.log(success.archivo);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  removerFiltro(event:any) {
    
  }

}
