import { StockDTO } from "../../gestion-stock/_dto/StockDTO";

export interface ProductosAagotarseDTO {
    stockMin: Boolean;
    stock20: Boolean;
    stock40: Boolean;
    idSucursal?: number;
    detalles: StockDTO[];
    grupo?:any;
}