import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductosAagotarseDTO } from '../_dto/ProductosAagotarseDTO';
import { FileDTO } from '../../reporte-ventas-realizadas/_dto/VentasRealizadasDTO';

@Injectable()
export class ReporteProductosAgotarseService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getProductosStock(fechas: ProductosAagotarseDTO): Observable<ProductosAagotarseDTO> {
    return this.http.post<ProductosAagotarseDTO>(appPreferences.urlBackEnd + 'reportes/productosAagotarse', fechas, this.auth.getHeaders());
  }

  getReporte(datos: ProductosAagotarseDTO): Observable<FileDTO> {
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'reportes/productosAagotarse/impresion', datos, this.auth.getHeaders());
  }

}
