import { Component, OnInit, ElementRef } from '@angular/core';
import { appPreferences } from '../../environments/environment';
import { formatearMillar, generarBlobForPDF } from '../_utils/millard-formatter';
import { pie_graphic } from '../_utils/chartjs-options';
import { Chart } from 'chart.js';
import 'chartjs-plugin-labels';

// Servicios
import { AuthService } from '../_services/auth.service';
import { ReporteProductosAgotarseService } from './_services/reporte-productos-agotarse.service';
import { saveAs } from 'file-saver';

//DTO
import { ProductosAagotarseDTO } from './_dto/ProductosAagotarseDTO';
import { FiltrosChips } from '../_utils/show-chips-filter/plusFile/filtros-chips';
import { StockDTO } from '../gestion-stock/_dto/StockDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-reporte-productos-agotarse',
  templateUrl: './reporte-productos-agotarse.component.html',
  styleUrls: ['./reporte-productos-agotarse.component.css']
})
export class ReporteProductosAgotarseComponent implements OnInit {

  nameVarSession: any = "reporteProducto";
  public listaProductosAgoDet: StockDTO[];
  public filtro: ProductosAagotarseDTO;
  public listaFiltros: FiltrosChips[];

  regXpag: number = 10; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación



  constructor(
    private reporSrv: ReporteProductosAgotarseService,
    public auth: AuthService
  ) {
    this.listaProductosAgoDet = [];
    this.filtro= {
      stockMin: true,
      stock20: false,
      stock40: false,
      idSucursal: null,
      detalles: []
    }
    this.listaFiltros = [];
  }


  ngOnInit() {
    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
      M.updateTextFields();
    }, 50);

    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });
    this.getlstProductosStock();
    this.mostrarChips();
  }

  selectedRadioButton(dato: any){
    console.log('dato',dato);
    this.resetFiltro();
    if(dato == 'stock40'){
      this.filtro.stock40 = true;
    }else if (dato == 'stock20') {
      this.filtro.stock20 = true;
    }else {
      this.filtro.stockMin = true;
    }
    console.log('filtro', this.filtro);
  }

  getlstProductosStock() {
    console.log('dto enviado en la llamada',this.filtro);

    this.reporSrv.getProductosStock(this.filtro).subscribe(
      success => {

        this.listaProductosAgoDet = success.detalles;
  
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }
  
  mostrarFrmFiltro() {
    swShowFrmFiltro('filtroReporte');
  }

  mostrarChips() {    
    if(this.filtro.stock40 == true){
      this.listaFiltros.push({
        descripcion: 'Disponibilidad',
        id: '40s',
        value: 'Al 40% del stock de Seguridad',
        bloquearCierre: true
      });
    } else if (this.filtro.stock20 == true) {
      this.listaFiltros.push({
        descripcion: 'Disponibilidad',
        id: '20s',
        value: 'Al 20% del stock de Seguridad',
        bloquearCierre: true
      });
    } else {
      this.listaFiltros.push({
        descripcion: 'Disponibilidad',
        id: 'iss',
        value: 'Inferior al stock de Seguridad',
        bloquearCierre: true
      });
    }
  }

  loadRegistros() {

    swShowFrmFiltro('filtroReporte', 'none');

    console.log('opc seleccionada', this.filtro);
    this.listaProductosAgoDet = [];
    this.listaFiltros = [];
    this.mostrarChips();

    window.sessionStorage.setItem(btoa(this.nameVarSession), JSON.stringify(this.filtro));

    this.getlstProductosStock();

  }

  getReporte(){
    this.reporSrv.getReporte(this.filtro).subscribe(
      success => {
        let excel = generarBlobForPDF(success.archivo,"application/xls");
        saveAs(excel, success.nombre + success.tipo);
        console.log(success.archivo);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  resetFiltro(){
    this.filtro= {
      stockMin: false,
      stock20: false,
      stock40: false,
      idSucursal: null,
      detalles: []
    }
  }
  removerFiltro(event:any) {
    
  }
}
