import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';


// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { MarcaService } from './_service/marca.service';

//DTO
import { MarcaDTO } from './_dto/MarcaDTO';
import { appPreferences } from '../../environments/environment';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-mantenimiento-marca',
  templateUrl: './mantenimiento-marca.component.html',
  styleUrls: ['./mantenimiento-marca.component.css']
})
export class MantenimientoMarcaComponent implements OnInit {
  
  @Input() mantenimiento: boolean = false;
  
  public marca: MarcaDTO;
  public listaMarcas: MarcaDTO[];

  constructor(
    private marcaSrv: MarcaService,
    private util: UtilsService,
    public auth: AuthService
  ) {
    this.marca = {
      descripcion: ''
    };
   }

  ngOnInit() {
    $('select').formSelect();

    if (!this.mantenimiento) {

      setTimeout(() => {
        $('.modal').modal({
          dismissible : false,
          onOpenStart: function (modal, trigger) {
            M.updateTextFields();
            $('select').formSelect();

            console.log(modal, trigger);
          },
          complete: function () {
            $('#btnCancelModalMarca').click();
          }
        });
      }, 60);
      this.getLstMarcas();
    } 
  }

  getLstMarcas () {
    this.marcaSrv.getMarcas().subscribe(
      success => {
        this.listaMarcas = success;
        console.log("marcas", this.listaMarcas);
      },
      err => { }
    )
  }

  guardar(forma: NgForm) {
    
    let newMarca = forma.value;

    this.marcaSrv.createMarca(newMarca).subscribe(
      success => {
        M.toast({html:'Nueva Marca agregada correctamente.', displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
        this.resetForm();
        $('#modalMarca').modal('close');
      },
      err => {
        let msg = err.error;
        M.toast({html:msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
      }
    );
  }

  resetForm(){
    this.marca = {
      descripcion: ''
    };
  }

}
