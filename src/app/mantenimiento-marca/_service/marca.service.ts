import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {  MarcaDTO } from '../_dto/MarcaDTO';


@Injectable()
export class MarcaService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getMarcas(): Observable<MarcaDTO[]> {
    return this.http.get<MarcaDTO[]>(appPreferences.urlBackEnd + 'productosMarca', this.auth.getHeaders());
  }

  getMarcasById(id:number): Observable<MarcaDTO> {
    return this.http.get<MarcaDTO>(appPreferences.urlBackEnd + 'productosMarca/' + id, this.auth.getHeaders());
  }
  
  createMarca(datos: MarcaDTO): Observable<MarcaDTO> {
    return this.http.post<MarcaDTO>(appPreferences.urlBackEnd + 'productosMarca', datos, this.auth.getHeaders());
  }

  updateMarca(id: number, data: MarcaDTO): Observable<MarcaDTO> {
    return this.http.put<MarcaDTO>(appPreferences.urlBackEnd + 'productosMarca/' + id, data, this.auth.getHeaders())
  }

  deleteMarca(id: number): Observable<MarcaDTO> {
    return this.http.delete<MarcaDTO>(appPreferences.urlBackEnd + 'productosMarca/' + id, this.auth.getHeaders())
  }
}
