import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { VentasRealizadas, FileDTO } from '../_dto/VentasRealizadasDTO';

@Injectable()
export class VentasRealizdasService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getVentasRealizadas(fechas: VentasRealizadas): Observable<VentasRealizadas> {
    let tmp = Object.assign({},fechas)
    if(fechas.fechaDesde && fechas.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<VentasRealizadas>(appPreferences.urlBackEnd + 'reportes/ventasRealizadas', fechas, this.auth.getHeaders());
  }

  getReporte(fechas: VentasRealizadas): Observable<FileDTO> {
    let tmp = Object.assign({},fechas)
    if(fechas.fechaDesde && fechas.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'reportes/ventasRealizadas/impresion', fechas, this.auth.getHeaders());
  }
}
