import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { appPreferences } from '../../environments/environment';

// Servicios
import { AuthService } from '../_services/auth.service';
import { VentasRealizdasService } from './_services/ventas-realizadas.service';
import { GestionVentasService } from '../gestion-ventas/_services/gestionVentas.service';
import { Chart } from 'chart.js'
import { saveAs } from 'file-saver';

//DTO
import { VentasDTO, VentaDetallesDTO } from '../gestion-ventas/_dto/VentasDTO';
import { VentasRealizadas, VentasForGraphic } from './_dto/VentasRealizadasDTO';
import { FiltrosChips } from '../_utils/show-chips-filter/plusFile/filtros-chips';
import { formatearMillar, generarBlobForPDF } from '../_utils/millard-formatter';
import { DatePipe } from '@angular/common';
import { bar_graphic } from '../_utils/chartjs-options';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-reporte-ventas-realizadas',
  templateUrl: './reporte-ventas-realizadas.component.html',
  styleUrls: ['./reporte-ventas-realizadas.component.css']
})
export class ReporteVentasRealizadasComponent implements OnInit {

  nameVarSession: any = "reporteVentasRealizadas";
  public listaCV: VentasDTO[];
  public filtro: VentasRealizadas;
  public ventasRealizadas: VentasRealizadas;
  public listaFiltros: FiltrosChips[];

  chart01: Chart; 
  @ViewChild('grafVtas') grafVtas: ElementRef;
  public colorOperacion = "#43a047";
  public datosGrap: VentasForGraphic[] ;

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  regXpag2: number = 5; //variable que utiliza la paginación
  p2: number = 1; //variable que utiliza la paginación

  constructor(

    private reporSrv: VentasRealizdasService,
    private ventaSrv: GestionVentasService,
    public auth: AuthService

  ) {
    this.listaCV = [];
    this.ventasRealizadas=  {
      fechaDesde: null,
      fechaHasta: null,
      totalVendido: null,
      detalles: [],
      totalXfecha: []
    };
    this.filtro = {
      fechaDesde: null,
      fechaHasta: null,
      totalVendido: null,
      detalles: [],
      totalXfecha: []
    }

    let filtro = window.sessionStorage.getItem(btoa(this.nameVarSession));
    if (filtro) {
      this.filtro = JSON.parse(filtro);
      let tmpD = this.filtro.fechaDesde.split('-');
      let tmpH = this.filtro.fechaHasta.split('-');

      setTimeout(() => {
        let fechaD: any = document.getElementById('fechaDesde');
        let fechaH: any = document.getElementById('fechaHasta');
        fechaD.value = `${tmpD[2]}/${tmpD[1]}/${tmpD[0]}`;
        fechaH.value = `${tmpH[2]}/${tmpH[1]}/${tmpH[0]}`;
        this.loadRegistros();
      }, 100);
    } else {
      this.limpiarFiltro();
    }

  }

  ngOnInit() {
    $('select').formSelect();
    setTimeout(() => {
      $('.tooltipped').tooltip();
      $('.tabs').tabs();
    }, 50);

    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function (modal, trigger) {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseEnd: function () {
        $('#cancelModal').click();
      }
    });

    this.getlstVentas();
  }

  getlstVentas() {
    this.reporSrv.getVentasRealizadas(this.filtro).subscribe(
      success => {
        this.ventasRealizadas = success;
        this.listaCV = success.detalles;
        setTimeout(() => {
          this.graphicCreate();
        }, 50);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  graphicCreate(){
    if (this.chart01 != null) { this.chart01.destroy() }

    this.datosGrap = this.ventasRealizadas.totalXfecha;

    let dias: any[] = []; let montos: number[]=[];
    let aux = new DatePipe("es_PY");

    this.datosGrap.forEach(element => {
      dias.push(aux.transform(element.dia, "dd/MM/yyyy"));
      montos.push(element.totalXdia);
    });

    let ctx01 = this.grafVtas.nativeElement.getContext('2d')
    this.chart01 = new Chart(ctx01,
      {
        type: 'line',
        data: { 
                labels: dias,
                datasets: [
                  { label:"Monto Recaudado",
                    backgroundColor: "#43a04778",
                    hoverBackgroundColor: "#f2ba1a",
                    data: montos
                  }
                ] 
              },
        //options: bar_graphic
      }
    )
  }

  mostrarFrmFiltro() {
    swShowFrmFiltro('filtroConsultaVenta');
    // this.filtro.fechaDesde= null;
    // this.filtro.fechaHasta= null;
  }

  limpiarFiltro() {
    console.log('entra aca')
    this.filtro = {
      fechaDesde: null,
      fechaHasta: null
    }
  }

  mostrarChips() {
    let fechaD = this.filtro.fechaDesde;
    let fechaH = this.filtro.fechaHasta;
    console.log('format', fechaD, fechaH);
    this.listaFiltros.push({
      descripcion: 'Fechas',
      id: 'fdfh',
      value:  fechaD + ' al ' + fechaH,
      bloquearCierre: true
    });
  }

  loadRegistros() {

    swShowFrmFiltro('filtroConsultaVenta', 'none');

    if (!this.filtro.fechaDesde || !this.filtro.fechaHasta) {
      alert("Los campos fecha desde y fecha hasta son obligatorios.");
      return false
    }
    console.log('fecha para la consulta', this.filtro);
    this.listaCV = [];
    this.listaFiltros = [];
    this.mostrarChips();

    window.sessionStorage.setItem(btoa(this.nameVarSession), JSON.stringify(this.filtro));

    this.getlstVentas();

  }

  getReporte(){
    this.reporSrv.getReporte(this.filtro).subscribe(
      success => {
        let excel = generarBlobForPDF(success.archivo,"application/xls");
        saveAs(excel, success.nombre + success.tipo);
        console.log(success.archivo);
      },
      err => {
        let msg = err.error;
        M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      }
    );
  }

  removerFiltro(event:any) {

  }

}
