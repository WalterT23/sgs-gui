import { VentasDTO } from "../../gestion-ventas/_dto/VentasDTO";

export interface VentasRealizadas {
    fechaDesde: any;
    fechaHasta: any;
    totalVendido?: number;
    idSucursal?: number;
    detalles?: VentasDTO[];
    totalXfecha?: VentasForGraphic[];
}

export interface VentasForGraphic {
    dia: Date;
    totalXdia: number;
}

export interface FileDTO {
    nombre: string;
    tipo: string;
    archivo: any;
}