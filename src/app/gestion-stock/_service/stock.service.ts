import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { StockDTO, MovimientoMercaderiaDTO } from '../_dto/StockDTO';

@Injectable()
export class StockService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getstock(): Observable<StockDTO[]> {
    return this.http.get<StockDTO[]>(appPreferences.urlBackEnd + 'stock', this.auth.getHeaders());
  }

  getStockById(id: number): Observable<StockDTO> {
    return this.http.get<StockDTO>(appPreferences.urlBackEnd + 'stock/' + id, this.auth.getHeaders());
  }

  getStockByCod(cod: string): Observable<StockDTO> {
    return this.http.get<StockDTO>(appPreferences.urlBackEnd + 'stock/getByCod/' + cod, this.auth.getHeaders());
  }

  updateStock(id: number, data: StockDTO): Observable<StockDTO> {
    return this.http.put<StockDTO>(appPreferences.urlBackEnd + 'stock/' + id, data, this.auth.getHeaders())
  }

  deleteStock(id: number): Observable<StockDTO> {
    return this.http.delete<StockDTO>(appPreferences.urlBackEnd + 'stock/' + id, this.auth.getHeaders())
  }

}
