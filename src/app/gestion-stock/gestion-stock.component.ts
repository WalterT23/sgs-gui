import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';
import { StockService } from './_service/stock.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { MovMercService } from '../movimiento-mercaderias/_services/mov-merc.service';

//DTO
import { appPreferences } from '../../environments/environment';
import { StockDTO } from './_dto/StockDTO';
import { ProductoDTO } from '../gestion-productos/_dto/ProductoDTO';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { MovimientoMercaderiaDTO } from '../movimiento-mercaderias/_dto/MovimientoMercaderiaDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-stock',
  templateUrl: './gestion-stock.component.html',
  styleUrls: ['./gestion-stock.component.css']
})
export class GestionStockComponent implements OnInit {

  public datosStock: StockDTO;
  public listaStock: any[];
  public datosAjuste: MovimientoMercaderiaDTO;
  public datosProducto: ProductoDTO;
  public listaEstados: InfoRefOpcDTO[];
  public onlyView: boolean = false;
  public producto: string = '';

  regXpag: number = 5; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  buscarTexto = "";
  sortProperty: string = '';
  sortOrder = 2;

  constructor(

    private stockSrv: StockService,
    private proSrv: ProductosService,
    private infrefSrv: InfoRefService,
    private movSrv: MovMercService,
    private util: UtilsService,
    public auth: AuthService

  ) {
    this.listaStock =[];
    this.listaEstados = [];
    this.datosStock = {
      id: null,
      idProducto: null,
      codProducto: '',
      ultCostoCompra: null,
      promPonderado: null,
      stock: null,
      fechaUltModif: null,
      usuarioUltModif: '',
      idSucursal: null,
      productoDTO: null
    };
    this.datosAjuste = {
      id: null,
      idTipoMovimiento: null,
      fechaMovimiento: null,
      tipoOperacion: null,
      usuarioRegistro: '',
      idSucursal: null,
      idOperacion: null,
      detalles: [{
        id: null,
        idMovimiento: null,
        idProducto: null,
        producto: '',
        codProducto: '',
        cantidad: null
      }]
    }
  }

  ngOnInit() {
    $('select').formSelect();
    $('.tooltipped').tooltip();
    $('.modal').modal({
      dismissible : false,
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    this.getLstStock();
  
  }

  getEstados(tipoEstado: string){
    this.infrefSrv.getInfoRefOpcByCodRef(tipoEstado).subscribe(
      success => {
        this.listaEstados = success;
        console.log(this.listaEstados);
      },
      err => { }
    )
  }

  getTipoOperacion(){
    this.infrefSrv.getInfoRefOpcByAbv('AJT').subscribe(
      success => {
        this.datosAjuste.tipoOperacion = success.id;
        console.log('tipo operacion',success);
      },
      err => { }
    )
  }

  getLstStock() {
    this.stockSrv.getstock().subscribe(
      success => {
        this.listaStock = success;
        console.log ('lista stock',this.listaStock);
        
      },
      err => { }
    );
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 50);
    console.log("listado",this.listaStock);
  }

  getInfoStock(id: number) {

    $('select').formSelect();
    this.stockSrv.getStockById(id).subscribe(
      success => {
        this.datosStock = success;
        setTimeout(() => {
          M.updateTextFields();
        }, 100);
        console.log('detalles del producto seleccionado',this.datosStock);
      },
      err => {

      }
    );
  }

  getLstProductos(idProducto: number) {
    this.proSrv.getProductoById(idProducto).subscribe(
      success => {
        this.datosProducto = success;
        console.log('datos del producto', this.datosProducto);
      },
      err => { }
    );
  }

  ajustarStock(item: StockDTO){
    this.getInfoStock(item.id);
    this.getEstados('TIPO_MOV_M');
    setTimeout(() => {
      if (this.datosStock != null) {
        this.producto = this.datosStock.codProducto + ' - ' + this.datosStock.productoDTO.nombre;
        console.log('producto',this.producto);
      }
      $('select').formSelect();
    }, 100);
   
  }


  guardar() {
    this.getTipoOperacion();
    this.datosAjuste.detalles[0].idProducto = this.datosStock.idProducto;
    console.log("datosAjuste",this.datosAjuste);

    if(this.datosAjuste.idTipoMovimiento == null) {
      M.toast({html: 'Debe seleccionar un tipo de ajuste',displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
    }
    setTimeout(() => {
      this.movSrv.createAjuste(this.datosAjuste).subscribe(
        success => {
          M.toast({html: 'Stock ajustado correctamente.',displayLength: appPreferences.toastOkDuration, classes:'toastOkColor'});
          $('#modalAjusteStock').modal('close');
          this.getLstStock();
          this.resetForm();
        },
        err => {
          let msg = err.error;
          M.toast({html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes:'toastErrorColor'});
           }
      );  
    }, 200);
    
  }

  resetForm() {
    this.datosStock = {
      id: null,
      idProducto: null,
      codProducto: '',
      ultCostoCompra: null,
      promPonderado: null,
      stock: null,
      fechaUltModif: null,
      usuarioUltModif: '',
      idSucursal: null,
      productoDTO: null
    }
    this.datosAjuste = {
      id: null,
      idTipoMovimiento: null,
      fechaMovimiento: null,
      tipoOperacion: null,
      usuarioRegistro: '',
      idSucursal: null,
      idOperacion: null,
      detalles: [{
        id: null,
        idMovimiento: null,
        idProducto: null,
        producto: '',
        codProducto: '',
        cantidad: null
      }]
    }
  }

  changePag(pagNumber: number){
    this.p = pagNumber;
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }

  sortBy(property: string) {
    if (property === this.sortProperty) {
      switch (this.sortOrder) {
          case 0:
            //ASCENDENTE
            this.sortOrder = 1;

            break;
          case 1:
            //DESCENDENTE
            this.sortOrder = 2;
            break;
          case 2:
            //NO ORDEN
            this.sortOrder = 0;
            break;
      }
    } else {
      this.sortOrder = 1;
    }
    this.sortProperty = property;
    this.sortList(property);
}


  sortList(property: string) {
    switch (this.sortOrder) {
      case 1:
        this.listaStock.sort((a,b) =>{
          if(!a.property || a.property < b.property){
           return -1
          }else if (!b.property || a.property > b.property){
            return 1
          }
          return 0
        })
        break;
      case 2:
        this.listaStock.sort((b,a) =>{
          if(!a.property || a.property < b.property){
           return -1
          }else if (!b.property || a.property > b.property){
            return 1
          }
          return 0
        })
        break;
    }
  
  }
  
    sortIcon(property: string) {
        if (property === this.sortProperty) {
            return this.sortOrder === 0 ? '⇅' : this.sortOrder === 1? '⇈' : '⇊';
        }
        return '⇅';
    }
}
