import { ProductoDTO } from "../../gestion-productos/_dto/ProductoDTO";

export interface StockDTO {
    id?: number;
    idProducto: number;
    codProducto: string;
    ultCostoCompra: number;
    promPonderado: number;
    stock: number;
    fechaUltModif: Date;
    usuarioUltModif: string;
    idSucursal: number;
    productoDTO: ProductoDTO;
}

export interface MovimientoMercaderiaDTO {
    id?: number;
    idTipoMovimiento: number;
    fechaMovimiento?: Date;
    tipoOperacion?: number;
    usuarioRegistro?: string;
    idSucursal?: number;
    idOperacion?: number;
    detalles: MovMercDetDTO[];
}

export interface MovMercDetDTO {
    id?: number;
    idMovimiento?: number;
    idProducto?: number;
    producto: string;
    codProducto: string;
    cantidad: number;
}