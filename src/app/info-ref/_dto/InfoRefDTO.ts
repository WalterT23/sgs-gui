export interface InfoRefDTO {
    id?: number;
    codigo: string;
    descripcion: string;
    tabref: number;
}

export interface InfoRefOpcDTO {
    id?: number;
    id_info_ref: number;
    descripcion: string;
    abreviatura: string;
}