import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { InfoRefOpcDTO } from '../_dto/InfoRefDTO';

@Injectable()
export class InfoRefService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getInfoRefOpc(): Observable<InfoRefOpcDTO[]> {
    return this.http.get<InfoRefOpcDTO[]>(appPreferences.urlBackEnd + 'inforefopc', this.auth.getHeaders());
  }

  getInfoRefOpcByCodRef(codRef: string): Observable<InfoRefOpcDTO[]> {
    return this.http.get<InfoRefOpcDTO[]>(appPreferences.urlBackEnd + 'inforefopc/byCodRef/' + codRef, this.auth.getHeaders());
  }

  getInfoRefOpcById(id: number): Observable<InfoRefOpcDTO> {
    return this.http.get<InfoRefOpcDTO>(appPreferences.urlBackEnd + 'inforefopc/' + id, this.auth.getHeaders());
  }

  getInfoRefOpcByAbv(abv: string): Observable<InfoRefOpcDTO> {
    return this.http.get<InfoRefOpcDTO>(appPreferences.urlBackEnd + 'inforefopc/byAbv/' + abv, this.auth.getHeaders());
  }

  createInfoRefOpc(datos: InfoRefOpcDTO): Observable<InfoRefOpcDTO> {
    return this.http.post<InfoRefOpcDTO>(appPreferences.urlBackEnd + 'inforefopc', datos, this.auth.getHeaders());
  }

  updateInfoRefOpc(id: number, data: InfoRefOpcDTO): Observable<InfoRefOpcDTO> {
    return this.http.put<InfoRefOpcDTO>(appPreferences.urlBackEnd + 'inforefopc/' + id, data, this.auth.getHeaders())
  }

  deleteInfoRefOpc(id: number): Observable<InfoRefOpcDTO> {
    return this.http.delete<InfoRefOpcDTO>(appPreferences.urlBackEnd + 'inforefopc/' + id, this.auth.getHeaders())
  }

}
