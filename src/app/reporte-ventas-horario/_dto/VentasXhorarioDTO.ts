export interface VentasXhorarioDTO {
    fechaDesde?: any;
    fechaHasta?: any;
    idSucursal?: number;
    detalles: any[];
    grupo?:any;
}

export interface ventasXhoraDetDTO {
    id: number;
    descripcion: string;
    cantidad: number;
    monto: number;
}