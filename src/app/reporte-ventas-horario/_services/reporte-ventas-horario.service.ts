import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ventasXhoraDetDTO, VentasXhorarioDTO } from '../_dto/VentasXhorarioDTO';
import { FileDTO } from '../../reporte-ventas-realizadas/_dto/VentasRealizadasDTO';

@Injectable()
export class ReporteVentasHorarioService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getVentasXhorario(fechas: VentasXhorarioDTO): Observable<VentasXhorarioDTO> {
    let tmp = Object.assign({},fechas)
    if(fechas.fechaDesde && fechas.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<VentasXhorarioDTO>(appPreferences.urlBackEnd + 'cabeceraVenta/ventasPorHorarios', tmp, this.auth.getHeaders());
  }

  getReporte(datos: VentasXhorarioDTO): Observable<FileDTO> {
    let tmp = Object.assign({},datos)
    if(datos.fechaDesde && datos.fechaHasta){
      tmp.fechaDesde =  ajusteZH(tmp.fechaDesde);
      tmp.fechaHasta =  ajusteZH(tmp.fechaHasta);
    }
    return this.http.post<FileDTO>(appPreferences.urlBackEnd + 'cabeceraVenta/ventasPorHorarios/impresion', tmp, this.auth.getHeaders());
  }

}
