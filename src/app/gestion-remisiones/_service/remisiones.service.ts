import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RemisionDTO, RemisionDetalleDTO } from '../_dto/RemisionDTO';


@Injectable()
export class RemisionesService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getRemisiones(): Observable<RemisionDTO[]> {
    return this.http.get<RemisionDTO[]>(appPreferences.urlBackEnd + 'remision', this.auth.getHeaders());
  }

  getRemisionById(id: number): Observable<RemisionDTO> {
    return this.http.get<RemisionDTO>(appPreferences.urlBackEnd + 'remision/' + id, this.auth.getHeaders());
  }

  getRemisionDetalleByIdRemision(idrem: number): Observable<RemisionDetalleDTO[]> {
    return this.http.get<RemisionDetalleDTO[]>(appPreferences.urlBackEnd + 'remisionDetalle/byIdRem/' + idrem, this.auth.getHeaders());
  }

  createRemision(data: RemisionDTO): Observable<RemisionDTO>  {
    return this.http.post<any>(appPreferences.urlBackEnd + 'remision', data, this.auth.getHeaders());
  }

  updateRemision(id:number, data: RemisionDTO): Observable<RemisionDTO> {
    return this.http.put<any>(appPreferences.urlBackEnd + 'remision/'+ id, data, this.auth.getHeaders())
  }

  deleteRemision(id: number): Observable<RemisionDTO> {
    return this.http.delete<RemisionDTO>(appPreferences.urlBackEnd + 'remision/' + id, this.auth.getHeaders())
  }

  confirmarRemision(id: number): Observable<RemisionDTO> {
    return this.http.get<RemisionDTO>(appPreferences.urlBackEnd + 'remision/confirmar/' + id, this.auth.getHeaders());
  }

}
