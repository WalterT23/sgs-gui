import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionRemisionesComponent } from './gestion-remisiones.component';

describe('GestionRemisionesComponent', () => {
  let component: GestionRemisionesComponent;
  let fixture: ComponentFixture<GestionRemisionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionRemisionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionRemisionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
