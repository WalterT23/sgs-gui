export interface RemisionDTO {
    id: number;
    idOrdenCompra: number;
    nroOC:string;
    nroRemision: string;
    proveedor: string;
    fechaRegistro:  Date;
    usuarioRegistro: string;
    idEstado: number;
    estado: string;
    detalles: RemisionDetalleDTO[];
    detallesAAgregar: RemisionDetalleDTO[];
    detallesAEliminar: RemisionDetalleDTO[];
}

export interface RemisionDetalleDTO {
    id: number;
    idRemision: number;
    idProducto: number;
    codProducto: string;
    producto: string;
    cantidad: number;
}
