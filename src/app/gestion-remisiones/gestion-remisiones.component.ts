import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, NgForm } from '@angular/forms';
import { appPreferences } from '../../environments/environment';
import { GestionProveedoresComponent} from '../gestion-proveedores/gestion-proveedores.component';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { RemisionesService } from './_service/remisiones.service';
import { OrdenesDeCompraService } from '../gestion-ordenes-de-compra/_service/ordenes-de-compra.service';
import { ProductosService } from '../gestion-productos/_service/productos.service';
import { ProveedoresService } from '../gestion-proveedores/_service/proveedores.service';

// DTO
import { RemisionDTO, RemisionDetalleDTO} from './_dto/RemisionDTO';
import { OrdenCompraDTO, OCdetalleDTO } from '../gestion-ordenes-de-compra/_dto/OrdenCompraDTO';
import { ProductoDTO } from '../gestion-productos/_dto/ProductoDTO';
import { ProveedoresDTO } from '../gestion-proveedores/_dto/ProveedoresDTO';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-remisiones',
  templateUrl: './gestion-remisiones.component.html',
  styleUrls: ['./gestion-remisiones.component.css']
})
export class GestionRemisionesComponent implements OnInit {

  public datosRemi: RemisionDTO;
  public datosOC: OrdenCompraDTO;
  public listaRemi: RemisionDTO[];
  public datosRemiDetalle: RemisionDetalleDTO;
  public listaDetRemi: RemisionDetalleDTO[];
  public updRemi: boolean = false;
  public search: boolean = false;
  public fechaRemi: Date;
  public listaOC: OrdenCompraDTO[];
  public listaDetOC: OCdetalleDTO[];
  public listaProd: ProductoDTO[];
  public datosProveedor: ProveedoresDTO;
  public datosProducto: ProductoDTO;
  public listaForAdd: RemisionDetalleDTO[];
  public listaForDel: RemisionDetalleDTO [];
  ocSelected: number = null;

  constructor(
    private remSrv: RemisionesService,
    private ocSrv: OrdenesDeCompraService,
    private prodSrv: ProductosService,
    private provSrv: ProveedoresService,
    private util: UtilsService,
    public auth: AuthService
  ) { 
    this.listaRemi = [];
    this.listaDetRemi = [];
    this.listaOC = [];
    this.listaProd = [];
    this.listaForAdd = [];
    this.listaForDel = [];
    this.listaDetOC = [];
    this.datosRemi = {
      id: null,
      idOrdenCompra: null,
      nroOC: '',
      nroRemision: '',
      proveedor: '',
      idEstado: null,
      estado: '',
      fechaRegistro: null,
      usuarioRegistro: null,
      detalles: [],
      detallesAAgregar: [],
      detallesAEliminar: []
    }
    this.datosRemiDetalle= {
      id: null,
      idRemision: null,
      idProducto: null,
      codProducto: '',
      producto: '',
      cantidad: null
    }
    
  }

  ngOnInit() {
    $('select').formSelect();
    $('.tooltipped').tooltip({delay: 50});
    $('.modal').modal({
      onOpenStart: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      complete: function () {
        $('#btnCancelModal').click();
      }
    });

    this.getLstRemisiones();
    this.fechaRemi = new Date ();
    this. getLstOC();
  }

  chargeLstForNew (){
    
  }

  chargeLstForEdit (id:number){
    $('select').formSelect();
    this.updRemi=true
    console.log("updremi",this.updRemi,"flag search", this.search);
    this.getInfoRemi(id);
  }

  chargeLstForView (id:number){
    this.search=true
    this.getInfoRemi(id); 
    this.getDetalleByRemi(id);
  }

  getLstRemisiones() {
    this.remSrv.getRemisiones().subscribe (
      success => {
        this.listaRemi = success;
        console.log("listaRemi", this.listaRemi)
      },
      err => {
        M.toast("No se pudieron obtener las Remisiones.", appPreferences.toastErrorDuration, 'toastErrorColor');
      }
    );
  }

  getDetalleByRemi(idRemi:number){
    this.remSrv.getRemisionDetalleByIdRemision(idRemi).subscribe (
      succes => {
        this.listaDetRemi = succes;
        console.log ("lista DetRemi",this.listaDetRemi);
      },
      err => {
        M.toast("No se pudieron obtener los detalles de la Remision.", appPreferences.toastErrorDuration, 'toastErrorColor');
      }
    )
  }
  
  getLstOC(){
    //Se obtienen unicamente las ordenes de compra que aun no tienen una remisión creada y que esten confirmadas

    var listaTmp: any[];
    var aux: number; var cont: number = 0;
    this.listaOC = [];
    

    this.ocSrv.getOCconfirmadas().subscribe (
      succes => {
        listaTmp = succes;
        console.log("Lista Temp",listaTmp, "Lista Remision",this.listaRemi);

        for (let index = 0; index < listaTmp.length; index++) {
          aux = 0; 
          for (let i = 0; i < this.listaRemi.length; i++) {
            if (listaTmp[index].id == this.listaRemi[i].idOrdenCompra) {
              aux = aux + 1;
            }
          }
          if (aux == 0) {
            this.listaOC[cont]= listaTmp[index];
            cont = cont + 1;
          }
        }
        console.log ("lista OC",this.listaOC);
      },
      err => {
        M.toast("No se pudieron obtener las Ordenes de Compras.", appPreferences.toastErrorDuration, 'toastErrorColor');
      }
    )
  }

  getLstProductos (){
    this.prodSrv.getProductos().subscribe (
      success => {
        this.listaProd = success;
        console.log("listaProd",this.listaProd);
      },
      err => {
        M.toast("No se pudieron obtener los productos.", appPreferences.toastErrorDuration, 'toastErrorColor');
       }
    )
  }

  getProductoByCod (cod:string, i:number){
    this.prodSrv.getProductoByCod(cod).subscribe (
      success => {
        this.datosProducto = success;
        this.listaDetRemi[i].producto = success.nombre;
        this.listaDetRemi[i].idProducto = success.id;
        console.log("datosProducto",this.datosProducto);
      },
      err => {
        M.toast("No se pudieron obtener los productos.", appPreferences.toastErrorDuration, 'toastErrorColor');
      }
    );
  }
  // chargeDatos (){
    
  //   if (this.ocSelected != -1) {
  //     let datos: OrdenCompraDTO = this.listaOC[this.ocSelected];
  //     console.log (datos);
  //     this.getDatosProveedor(datos.idProveedor);
  //     this.getLstDetalleByOC(datos.id);
  //     //this.datosRemi.idOrdenCompra = datos.id;
  //   }
  // }
  chargeDatos1(){
    if (this.ocSelected != -1 && this.ocSelected != null) {
      console.log ("id para obtener datos de la OC",this.listaOC[this.ocSelected].id);
      this.ocSrv.getOrdenDeCompraById (this.listaOC[this.ocSelected].id).subscribe (
        success => {
          this.datosOC = success;
          console.log("oc obtenida de la consulta", this.datosOC);
          this.listaDetOC = this.datosOC.detalles;
          this.datosRemi.idOrdenCompra = this.listaOC[this.ocSelected].id;
          this.datosRemi.nroOC = this.datosOC.nroOrdenCompra;
          this.datosRemi.proveedor = this.datosOC.proveedor;
          let lstDetRemision: any[]= [];
          for (let index = 0; index < this.listaDetOC.length; index++) {
            const element = this.listaDetOC[index];
            let detRemision: any = {
              idProducto: element.idProducto,
              codProducto: element.codProducto,
              producto : element.producto,
              cantidad : element.cantidad
            }
            lstDetRemision.push(detRemision);
          }
          this.listaDetRemi = lstDetRemision;
        },
        err => { 
          M.toast("No se pudieron obtener los detalles de la Orden de Compra.", appPreferences.toastErrorDuration, 'toastErrorColor');
        }
      )
    }
  }

  getDatosProveedor(id:number){
    this.provSrv.getProveedorById(id).subscribe (
      success => {
        this.datosProveedor = success;
        this.datosRemi.proveedor = success.razonSocial;
      },
      err => { }
    )
    
  }

  getInfoRemi(id:number){
    $('select').formSelect();
    this.remSrv.getRemisionById(id).subscribe(
      success => {
        this.datosRemi = success;
        this.listaDetRemi = this.datosRemi.detalles;
        console.log("datosRemi", this.datosRemi, "detRemi", this.listaDetRemi);
        //this.getDetalleByRemi(id);
        //this.datosRemi.detalles = [];
        //this.datosRemi.detallesAAgregar = [];
      },
      err => {
      }
    );
  }

  addProducto(){
    let newPro: RemisionDetalleDTO= {
      id: null,
      idRemision: null,
      idProducto: null,
      codProducto: '',
      producto: '',
      cantidad: null
    };
    this.listaDetRemi.push(newPro);
  }

  lessProducto (id:number){
    
    if (this.listaDetRemi[id].id) {
      this.listaForDel.push(this.listaDetRemi[id]);
    }  
    this.listaDetRemi.splice(id,1)
  }

  guardar() {
    
    console.log("listaDetOC",this.listaDetOC, "update?",this.updRemi);

    if (this.updRemi) {
      
      this.datosRemi.detallesAEliminar= this.listaForDel;

      for (const iterator of this.listaDetRemi) {
        if (iterator.id == null){
          this.datosRemi.detallesAAgregar.push(iterator)  ;
          console.log("detallesAAgregar",this.datosRemi.detallesAAgregar);
        }else {
          this.datosRemi.detalles.push(iterator);
        }
      }
      console.log("lista completa para update",this.datosRemi);

      this.remSrv.updateRemision(this.datosRemi.id, this.datosRemi).subscribe(
        success => {
          M.toast("Datos de la Remision actualizados correctamente.", appPreferences.toastOkDuration, 'toastOkColor');
          $('#modalOrdenCompra').modal('close');
        },
        err => {
          M.toast("No se pudieron actualizar los datos de la Remision.", appPreferences.toastErrorDuration, 'toastErrorColor');
        }
      );
    } else {
      console.log(this.datosRemi);
      this.datosRemi.detalles = this.listaDetRemi;
      this.remSrv.createRemision(this.datosRemi).subscribe(
        success => {
          M.toast("Remision creada correctamente.", appPreferences.toastOkDuration, 'toastOkColor');
          $('#modalRemision').modal('close');
        },
        err => { 
          M.toast("No se pudo guardar la Remision.", appPreferences.toastErrorDuration, 'toastErrorColor');
        }
      );
    }
    setTimeout(() => {
      this.getLstRemisiones();
      this. getLstOC();
    }, 1000);
  }

  confirmar(id:number){
    this.remSrv.confirmarRemision(id).subscribe(
      success => {
        M.toast("Remision confirmada correctamente.", appPreferences.toastOkDuration, 'toastOkColor');
        $('#modalRemision').modal('close');
        this.getLstRemisiones();
      },
      err => { 
        M.toast("No se pudo confirmar la Remision.", appPreferences.toastErrorDuration, 'toastErrorColor');
      }
    )
  }

  resetForm(){
    this.updRemi= false;
    this.search= false;
    this.ocSelected = null;
    this.listaDetRemi = [];
    this.listaForDel= [];
    this.datosRemi = {
      id: null,
      idOrdenCompra: null,
      nroOC: '',
      nroRemision: '',
      proveedor: '',
      idEstado: null,
      estado: '',
      fechaRegistro: null,
      usuarioRegistro: null,
      detalles: [],
      detallesAAgregar: [],
      detallesAEliminar: []
    }
    this.datosRemiDetalle= {
      id: null,
      idRemision: null,
      idProducto: null,
      codProducto: '',
      producto: '',
      cantidad: null
    }
  }
}
