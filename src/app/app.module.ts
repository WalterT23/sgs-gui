import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, LOCALE_ID } from '@angular/core';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { NgxPaginationModule } from 'ngx-pagination';
import { ChartModule } from 'angular2-chartjs';

// Componentes
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { MenuComponent } from './menu/menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GestionUsuariosComponent } from './gestion-usuarios/gestion-usuarios.component';
import { GestionRolesComponent } from './gestion-roles/gestion-roles.component';
import { GestionClientesComponent } from './gestion-clientes/gestion-clientes.component';
import { GestionProductosComponent } from './gestion-productos/gestion-productos.component';
import { GestionProveedoresComponent } from './gestion-proveedores/gestion-proveedores.component';
import { MantenimientoComponent } from './mantenimiento/mantenimiento.component';
import { MantenimientoUnidadMedidaComponent } from './mantenimiento-unidad-medida/mantenimiento-unidad-medida.component';
import { MantenimientoFamiliaComponent } from './mantenimiento-familia/mantenimiento-familia.component';
import { MantenimientoMarcaComponent } from './mantenimiento-marca/mantenimiento-marca.component';
import { GestionOrdenesDeCompraComponent } from './gestion-ordenes-de-compra/gestion-ordenes-de-compra.component';
import { GestionRemisionesComponent } from './gestion-remisiones/gestion-remisiones.component';
import { GestionStockComponent } from './gestion-stock/gestion-stock.component';
import { FacturaCompraComponent } from './factura-compra/factura-compra.component';
import { FacturaVentaComponent } from './factura-venta/factura-venta.component';
import { GestionCajasComponent } from './gestion-cajas/gestion-cajas.component';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { BancosComponent } from './bancos/bancos.component';
import { InfoRefComponent } from './info-ref/info-ref.component';
import { GestionTimbradosComponent } from './gestion-timbrados/gestion-timbrados.component';
import { CuentaBancoComponent } from './cuenta-banco/cuenta-banco.component';
import { ChequesComponent } from './cheques/cheques.component';
import { GestionPuntosExpedicionComponent } from './gestion-puntos-expedicion/gestion-puntos-expedicion.component';
import { GestionListaPreciosComponent } from './gestion-lista-precios/gestion-lista-precios.component';
import { GestionLpDetallesComponent } from './gestion-lp-detalles/gestion-lp-detalles.component';
import { CajaUserComponent } from './caja-user/caja-user.component';
import {NotaCreditoCompraComponent } from './nota-credito-compra/nota-credito-compra.component';
import { GestionDescuentosComponent } from './gestion-descuentos/gestion-descuentos.component';
import { NotaCreditoComponent } from './nota-credito/nota-credito.component';
import { PagoProveedoresComponent } from './pago-proveedores/pago-proveedores.component';
import { GestionTimbradoPuntoExpComponent } from './gestion-timbrado-punto-exp/gestion-timbrado-punto-exp.component';
import { GestionComprasComponent } from './gestion-compras/gestion-compras.component';
import { MovimientoMercaderiasComponent } from './movimiento-mercaderias/movimiento-mercaderias.component';
import { GestionVentasComponent } from './gestion-ventas/gestion-ventas.component';
import { RecaudacionesComponent } from './recaudaciones/recaudaciones.component';
import { ReporteProductosMasVendidosComponent } from './reporte-productos-mas-vendidos/reporte-productos-mas-vendidos.component';
import { ShowChipsFilterComponent } from './_utils/show-chips-filter/show-chips-filter.component';
import { ReporteVentasRealizadasComponent } from './reporte-ventas-realizadas/reporte-ventas-realizadas.component';
import { GestionPagosComponent } from './gestion-pagos/gestion-pagos.component';
import { ReporteEstadoCuentaComponent } from './reporte-estado-cuenta/reporte-estado-cuenta.component';
import { ReporteCuotasProximasComponent } from './reporte-cuotas-proximas/reporte-cuotas-proximas.component';
import { ReporteComprasRealizadasComponent } from './reporte-compras-realizadas/reporte-compras-realizadas.component';
import { ReporteCajaFalloComponent } from './reporte-caja-fallo/reporte-caja-fallo.component';
import { ReporteProveedoresComponent } from './reporte-proveedores/reporte-proveedores.component';
import { BalanceIngresoEgresoComponent } from './balance-ingreso-egreso/balance-ingreso-egreso.component';
// End Componentes


// Sevicios
import { AuthService } from './_services/auth.service';
import { AuthGuardService } from './_services/auth-guard.service';
import { UtilsService } from './_utils/utils.service';
import { LoginComponent } from './login/login.component';
import { UsuarioServiceService } from './gestion-usuarios/_services/usuario-service.service'
import { RolService } from './gestion-roles/_services/rol.service';
import { ClientesService } from './gestion-clientes/_service/clientes.service';
import { ProveedoresService } from './gestion-proveedores/_service/proveedores.service';
import { ProductosService } from './gestion-productos/_service/productos.service';
import { UnidadMedidaService } from './mantenimiento-unidad-medida/_service/unidad-medida.service';
import { FamiliaService } from './mantenimiento-familia/_service/familia.service';
import { MarcaService } from './mantenimiento-marca/_service/marca.service';
import { OrdenesDeCompraService } from './gestion-ordenes-de-compra/_service/ordenes-de-compra.service';
import { RemisionesService} from './gestion-remisiones/_service/remisiones.service';
import { StockService } from './gestion-stock/_service/stock.service';
import { FacturasProveedorService } from './factura-compra/_services/facturas-proveedor.service';
import { MantenimientoService } from './mantenimiento/_services/mantenimiento.service';
import { VentasService } from './factura-venta/_services/ventas.service';
import { CajasService } from './gestion-cajas/_services/cajas.service';
import { SucursalesService } from './sucursales/_services/sucursales.service';
import { BancosService } from './bancos/services/bancos.service';
import { InfoRefService } from './info-ref/_services/info-ref.service';
import { TimbradosService } from './gestion-timbrados/_services/timbrados.service';
import { CuentaBancoService } from './cuenta-banco/_services/cuenta-banco.service';
import { ChequesService } from './cheques/_services/cheques.service';
import { PtoExpedicionService } from './gestion-puntos-expedicion/_services/pto-expedicion.service';
import { ListaPreciosService } from './gestion-lista-precios/_service/lista-precios.service';
import { LpdetallesService } from './gestion-lp-detalles/_service/lpdetalles.service';
import { CajaUserService } from './caja-user/_services/caja-user.service';
import { DescuentosServiceService } from './gestion-descuentos/_services/descuentos-service.service';
import { PagoProveedoresService } from './pago-proveedores/_services/pago-proveedores.service';
import { NotaCreditoCompraService } from './nota-credito-compra/_services/nota-credito-compra.service';
import { TimbradoPtoExpService } from './gestion-timbrado-punto-exp/_service/timbrado-pto-exp.service';
import { GestionComprasService } from './gestion-compras/_services/gestion-compras.service';
import { MovMercService } from './movimiento-mercaderias/_services/mov-merc.service';
import { GestionVentasService } from './gestion-ventas/_services/gestionVentas.service';
import { RecaudacionesService } from './recaudaciones/_services/recaudaciones.service';
import { NotaCreditoVentaService } from './nota-credito/_services/nota-credito-venta.service';
import { ReporteVentaProductosService } from './reporte-productos-mas-vendidos/_services/reporte-venta-productos.service'; 
import { VentasRealizdasService } from './reporte-ventas-realizadas/_services/ventas-realizadas.service';
import { GestionPagosService } from './gestion-pagos/_services/gestion-pagos.service';
import { ReporteEstadoCuentaService } from './reporte-estado-cuenta/_services/reporte-estado-cuenta.service';
import { ReporteCuotasProximasService } from './reporte-cuotas-proximas/_services/reporte-cuotas-proximas.service';
import { ReporteComprasRealizadasService } from './reporte-compras-realizadas/_services/reporte-compras-realizadas.service';
import { ReporteCajaFalloService } from './reporte-caja-fallo/_services/reporte-caja-fallo.service';
import { ReporteProveedoresService } from './reporte-proveedores/_services/reporte-proveedores.service';
import { ReporteProductosAgotarseService } from './reporte-productos-agotarse/_services/reporte-productos-agotarse.service';
import { ReporteMovimientoProductosService } from './reporte-movimiento-productos/_service/reporte-movimiento-productos.service';
import { ReporteVentasHorarioService } from './reporte-ventas-horario/_services/reporte-ventas-horario.service';
import { BalanceService } from './balance-ingreso-egreso/_services/balance.service';
// End Servicios


// Pipes

// End Pipes
 

// Para definir la localidad
import {SumasFactCompra ,TotalFactCompra, iFormatNumber, TotalFactVenta, SumasPipe, PDetalleVentaPagos, TotalNCventa, TotalNCcompra} from './pipes/sumas.pipe';
import {BusquedaGral} from './pipes/busqueda.pipe';
import localePy from '@angular/common/locales/es-PY';
import { registerLocaleData } from '@angular/common';

import { ReporteProductosAgotarseComponent } from './reporte-productos-agotarse/reporte-productos-agotarse.component';
import { ReporteMovimientoProductosComponent } from './reporte-movimiento-productos/reporte-movimiento-productos.component';
import { ReporteVentasHorarioComponent } from './reporte-ventas-horario/reporte-ventas-horario.component';
import { MessageService } from './_utils/message.service';



registerLocaleData(localePy, 'es-PY');

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    MenuComponent,
    GestionUsuariosComponent,
    GestionRolesComponent,
    LoginComponent,
    GestionClientesComponent,
    GestionProductosComponent,
    GestionProveedoresComponent,
    MantenimientoComponent,
    MantenimientoUnidadMedidaComponent,
    MantenimientoFamiliaComponent,
    MantenimientoMarcaComponent,
    GestionOrdenesDeCompraComponent,
    GestionRemisionesComponent,
    SumasPipe,
    iFormatNumber,
    TotalFactVenta,
    GestionStockComponent,
    FacturaCompraComponent,
    InfoRefComponent,
    FacturaVentaComponent,
    GestionCajasComponent,
    SucursalesComponent,
    GestionTimbradosComponent,
    BancosComponent,
    CuentaBancoComponent,
    ChequesComponent,
    GestionPuntosExpedicionComponent,
    GestionListaPreciosComponent,
    GestionLpDetallesComponent,
    NotaCreditoComponent,
    GestionDescuentosComponent,
    SumasFactCompra,
    TotalFactCompra,
    CajaUserComponent,
    PagoProveedoresComponent,
    GestionDescuentosComponent,
    NotaCreditoCompraComponent,
    GestionTimbradoPuntoExpComponent,
    MovimientoMercaderiasComponent,
    PDetalleVentaPagos,
    GestionVentasComponent,
    GestionComprasComponent,
    PDetalleVentaPagos,
    RecaudacionesComponent,
    GestionPagosComponent,
    TotalNCventa,
    TotalNCcompra,
    ReporteProductosMasVendidosComponent,
    ShowChipsFilterComponent,
    ReporteVentasRealizadasComponent,
    ReporteEstadoCuentaComponent,
    ReporteCuotasProximasComponent,
    ReporteComprasRealizadasComponent,
    ReporteProveedoresComponent,
    ReporteCajaFalloComponent,
    ReporteProductosAgotarseComponent,
    ReporteMovimientoProductosComponent,
    ReporteVentasHorarioComponent,
    BalanceIngresoEgresoComponent,
    BusquedaGral
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    ChartModule
  ],
  providers: [
    UsuarioServiceService,
    RolService,
    ClientesService,
    ProductosService,
    ProveedoresService,
    UnidadMedidaService,
    FamiliaService,
    MarcaService,
    OrdenesDeCompraService,
    RemisionesService,
    StockService,
    AuthService,
    FacturasProveedorService,
    MantenimientoService,
    VentasService,
    DescuentosServiceService,
    CajasService,
    SucursalesService,
    BancosService,
    CuentaBancoService,
    InfoRefService,
    TimbradosService,
    AuthGuardService,
    CookieService,
    UtilsService,
    ChequesService,
    PtoExpedicionService,
    ListaPreciosService,
    LpdetallesService,
    CajaUserService,
    PagoProveedoresService,
    NotaCreditoCompraService,
    TimbradoPtoExpService,
    MovMercService,
    GestionVentasService,
    GestionComprasService,
    RecaudacionesService,
    NotaCreditoVentaService,
    GestionPagosService,
    ReporteVentaProductosService,
    VentasRealizdasService,
    ReporteEstadoCuentaService,
    ReporteCuotasProximasService,
    ReporteComprasRealizadasService,
    ReporteProveedoresService,
    ReporteCajaFalloService,
    ReporteProductosAgotarseService,
    ReporteMovimientoProductosService,
    ReporteVentasHorarioService,
    BalanceService,
    { provide: LOCALE_ID, useValue: 'es-PY' },
    MessageService
  ],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }  