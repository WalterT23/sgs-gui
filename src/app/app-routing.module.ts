import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

// Componente
import { GestionUsuariosComponent } from './gestion-usuarios/gestion-usuarios.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GestionRolesComponent } from './gestion-roles/gestion-roles.component';
import { LoginComponent } from './login/login.component';
import { GestionClientesComponent } from './gestion-clientes/gestion-clientes.component';
import { GestionProductosComponent } from './gestion-productos/gestion-productos.component';
import { GestionProveedoresComponent } from './gestion-proveedores/gestion-proveedores.component';
import { MantenimientoComponent } from './mantenimiento/mantenimiento.component';
import { MantenimientoUnidadMedidaComponent } from './mantenimiento-unidad-medida/mantenimiento-unidad-medida.component';
import { MantenimientoFamiliaComponent } from './mantenimiento-familia/mantenimiento-familia.component';
import { MantenimientoMarcaComponent } from './mantenimiento-marca/mantenimiento-marca.component';
import { GestionOrdenesDeCompraComponent } from './gestion-ordenes-de-compra/gestion-ordenes-de-compra.component';
import { GestionRemisionesComponent } from './gestion-remisiones/gestion-remisiones.component';
import { GestionStockComponent } from './gestion-stock/gestion-stock.component';
import { FacturaCompraComponent } from './factura-compra/factura-compra.component';
import { FacturaVentaComponent } from './factura-venta/factura-venta.component';
import { GestionCajasComponent } from './gestion-cajas/gestion-cajas.component';
import { BancosComponent } from './bancos/bancos.component';
//import { BancosComponent } from './bancos/bancos.component';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { GestionTimbradosComponent } from './gestion-timbrados/gestion-timbrados.component';
import { CuentaBancoComponent } from './cuenta-banco/cuenta-banco.component';
import { ChequesComponent } from './cheques/cheques.component';
import { GestionListaPreciosComponent } from './gestion-lista-precios/gestion-lista-precios.component';
import { GestionLpDetallesComponent } from './gestion-lp-detalles/gestion-lp-detalles.component';
import { CajaUserComponent } from './caja-user/caja-user.component';
import { GestionDescuentosComponent } from './gestion-descuentos/gestion-descuentos.component';
import { MovimientoMercaderiasComponent } from './movimiento-mercaderias/movimiento-mercaderias.component';
import { NotaCreditoComponent } from './nota-credito/nota-credito.component';
import { GestionVentasComponent } from './gestion-ventas/gestion-ventas.component';
import { RecaudacionesComponent } from './recaudaciones/recaudaciones.component';
import { ReporteProductosMasVendidosComponent } from './reporte-productos-mas-vendidos/reporte-productos-mas-vendidos.component';
import { PagoProveedoresComponent } from './pago-proveedores/pago-proveedores.component';
import { NotaCreditoCompraComponent } from './nota-credito-compra/nota-credito-compra.component';
import { GestionComprasComponent } from './gestion-compras/gestion-compras.component';
import { GestionPagosComponent } from './gestion-pagos/gestion-pagos.component';
import { ReporteVentasRealizadasComponent } from './reporte-ventas-realizadas/reporte-ventas-realizadas.component';
import { ReporteEstadoCuentaComponent } from './reporte-estado-cuenta/reporte-estado-cuenta.component';
import { ReporteCuotasProximasComponent } from './reporte-cuotas-proximas/reporte-cuotas-proximas.component';
import { ReporteProductosAgotarseComponent } from './reporte-productos-agotarse/reporte-productos-agotarse.component';
import { ReporteMovimientoProductosComponent } from './reporte-movimiento-productos/reporte-movimiento-productos.component';
import { ReporteVentasHorarioComponent } from './reporte-ventas-horario/reporte-ventas-horario.component';


// Servicios
import { AuthGuardService } from './_services/auth-guard.service';
import { ReporteComprasRealizadasComponent } from './reporte-compras-realizadas/reporte-compras-realizadas.component';
import { ReporteProveedoresComponent } from './reporte-proveedores/reporte-proveedores.component';
import { ReporteCajaFalloComponent } from './reporte-caja-fallo/reporte-caja-fallo.component';
import { BalanceIngresoEgresoComponent } from './balance-ingreso-egreso/balance-ingreso-egreso.component';


const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService]},
  { path: 'usuarios', component: GestionUsuariosComponent, canActivate: [AuthGuardService], data: { id: 'conusr' } },
  { path: 'perfiles', component: GestionRolesComponent, canActivate: [AuthGuardService], data: { id: 'conroles' }},
  { path: 'clientes', component: GestionClientesComponent, canActivate: [AuthGuardService], data: { id: 'concli' } },

  { 
    path: 'productos', 
    component: GestionProductosComponent, 
    canActivate: [AuthGuardService], 
    data: { id: 'conprod' } 
  },

  { path: 'proveedores', component: GestionProveedoresComponent, canActivate: [AuthGuardService], data: { id: 'conprov' } },
  { path: 'sucursales', component: SucursalesComponent, canActivate: [AuthGuardService], data: { id: 'consuc' } },
  { path: 'mantenimiento', component: MantenimientoComponent, canActivate: [AuthGuardService], data: { id: 'accmant' }},
  { path: 'mantenimientoUniMed', component: MantenimientoUnidadMedidaComponent, canActivate: [AuthGuardService], data: {id: 'conmanunimed'}},
  { path: 'mantenimientoFamilia', component: MantenimientoFamiliaComponent, canActivate: [AuthGuardService], data: { id: 'conmanflia' } },
  { path: 'mantenimientoMarca', component: MantenimientoMarcaComponent, canActivate: [AuthGuardService], data: {id: 'conbrand'}},
  { path: 'ordenesDeCompra', component: GestionOrdenesDeCompraComponent, canActivate: [AuthGuardService], data: {id: 'conoc'}},
  { path: 'remisiones', component: GestionRemisionesComponent, canActivate: [AuthGuardService], data: {id: 'conrem'}},
  { path: 'stock', component: GestionStockComponent, canActivate: [AuthGuardService], data: {id: 'constock'}},
  { path: 'facturaCompra', component: FacturaCompraComponent, canActivate: [AuthGuardService], data: {id: 'addftprov'}},
  { path: 'facturaVenta', component: FacturaVentaComponent, canActivate: [AuthGuardService], data: {id: 'regvtas'}},
  { path: 'cajas', component: GestionCajasComponent, canActivate: [AuthGuardService], data: {id: 'caja'}},
  { path: 'timbrados', component: GestionTimbradosComponent, canActivate: [AuthGuardService], data: {id: 'conTimb'}},
  { path: 'bancos', component: BancosComponent, canActivate: [AuthGuardService], data: { id: 'conbancos' }},
  { path: 'cuentaBanco', component: CuentaBancoComponent, canActivate: [AuthGuardService], data: { id: 'conctaBco' }},
  { path: 'cheques', component: ChequesComponent, canActivate: [AuthGuardService], data: { id: 'lstcheques' }},
  { path: 'listaPrecios', component: GestionListaPreciosComponent, canActivate: [AuthGuardService], data: { id: 'conLstP' }},
  { path: 'lpDetalles/:id', component: GestionLpDetallesComponent, canActivate: [AuthGuardService], data: { id: 'conLstP' }},
  { path: 'notaCredito', component: NotaCreditoComponent, canActivate: [AuthGuardService], data: { id: 'addncv' }},
  { path: 'cheques', component: ChequesComponent, canActivate: [AuthGuardService], data: { id: 'lstcheques' }},
  { path: 'cajaUser', component: CajaUserComponent, canActivate: [AuthGuardService], data: { id: 'lstcajas' }},
  { path: 'pagoProveedores', component: PagoProveedoresComponent, canActivate: [AuthGuardService], data: { id: 'lstPagos'}},  
  { path: 'descuentos', component: GestionDescuentosComponent, canActivate: [AuthGuardService], data: { id: 'condscto' }},
  { path: 'dsctoDetalle/:id', component: GestionDescuentosComponent, canActivate: [AuthGuardService], data: { id: 'condscto' }},
  { path: 'notaCreditoCompra', component: NotaCreditoCompraComponent, canActivate: [AuthGuardService], data: { id: 'conncc' }},
  { path: 'gestionCompras', component: GestionComprasComponent, canActivate: [AuthGuardService], data: { id: 'concra' }},
  { path: 'movimientoMercaderias', component: MovimientoMercaderiasComponent, canActivate: [AuthGuardService], data: { id: 'conMovmerc' }},
  { path: 'gestionVentas', component: GestionVentasComponent, canActivate: [AuthGuardService], data: { id: 'conVentas' }},
  { path: 'recaudaciones', component: RecaudacionesComponent, canActivate: [AuthGuardService], data: { id: 'conRecau' }},
  { path: 'gestionPagos/:idProveedor', component: GestionPagosComponent, canActivate: [AuthGuardService], data: { id: 'gespag' }},
  { path: 'reportesVenta/ventas', component: ReporteVentasRealizadasComponent, canActivate: [AuthGuardService], data: { id: 'conRecau' }},
  { path: 'reporteEstadoCuenta', component: ReporteEstadoCuentaComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'reporteEstadoCuenta/:idProveedor', component: ReporteEstadoCuentaComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'reporteCuotasProximas', component: ReporteCuotasProximasComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'reportesVenta/masVendidos', component: ReporteProductosMasVendidosComponent, canActivate: [AuthGuardService], data: { id: 'conRecau' }},
  { path: 'reporteComprasRealizadas', component: ReporteComprasRealizadasComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'reporteProveedores', component: ReporteProveedoresComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'reportesVenta/falloCaja', component: ReporteCajaFalloComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'reportesVenta/masVendidos', component: ReporteProductosMasVendidosComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'reportesVenta/volumenXhorario', component: ReporteVentasHorarioComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'reportesStock/productosAagotarse', component: ReporteProductosAgotarseComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'reportesStock/movimientoProductos', component: ReporteMovimientoProductosComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }},
  { path: 'balance', component: BalanceIngresoEgresoComponent, canActivate: [AuthGuardService], data: { id: 'conrepo' }}
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes, { useHash: true })]
})
export class AppRoutingModule { }
