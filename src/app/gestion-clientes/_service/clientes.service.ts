import { Injectable } from '@angular/core';
import { appPreferences } from '../../../environments/environment';
import { AuthService } from '../../_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ClienteDTO, CliListaPrecioDTO } from '../_dto/ClienteDTO';


@Injectable()
export class ClientesService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  getClientes(): Observable<ClienteDTO[]> {
    return this.http.get<ClienteDTO[]>(appPreferences.urlBackEnd + 'clientes', this.auth.getHeaders());
  }

  getClienteById(id: number): Observable<ClienteDTO> {
    return this.http.get<ClienteDTO>(appPreferences.urlBackEnd + 'clientes/' + id, this.auth.getHeaders());
  }

  createCliente(datos: ClienteDTO): Observable<ClienteDTO> {
    return this.http.post<ClienteDTO>(appPreferences.urlBackEnd + 'clientes', datos, this.auth.getHeaders() );
  }

  updateCliente(id: number, data: ClienteDTO): Observable<ClienteDTO> {
    return this.http.put<ClienteDTO>(appPreferences.urlBackEnd + 'clientes/' + id, data, this.auth.getHeaders())
  }

  deleteCliente(id: number): Observable<ClienteDTO> {
    return this.http.delete<ClienteDTO>(appPreferences.urlBackEnd + 'clientes/' + id, this.auth.getHeaders())
  }

  likeCliente(cod: string): Observable<ClienteDTO[]> {
    return this.http.get<ClienteDTO[]>( `${appPreferences.urlBackEnd}clientes/byRuc/${cod}`, this.auth.getHeaders())
  }
  // ---------------------------------------
 
  getClientesLP(): Observable<CliListaPrecioDTO[]> {
    return this.http.get<CliListaPrecioDTO[]>(appPreferences.urlBackEnd + 'clientesListaPrecio', this.auth.getHeaders());
  }

  getClienteLPById(id: number): Observable<CliListaPrecioDTO> {
    return this.http.get<CliListaPrecioDTO>(appPreferences.urlBackEnd + 'clientesListaPrecio/' + id, this.auth.getHeaders());
  }

  getClientesLPbyIdCliente(idCliente: number): Observable<CliListaPrecioDTO[]> {
    return this.http.get<CliListaPrecioDTO[]>(appPreferences.urlBackEnd + 'clientesListaPrecio/byIdCliente/' + idCliente, this.auth.getHeaders());
  }

  createClienteLP(datos: CliListaPrecioDTO): Observable<CliListaPrecioDTO> {
    return this.http.post<CliListaPrecioDTO>(appPreferences.urlBackEnd + 'clientesListaPrecio', datos, this.auth.getHeaders());
  }

  updateClienteLP(id: number, data: CliListaPrecioDTO): Observable<CliListaPrecioDTO> {
    return this.http.put<CliListaPrecioDTO>(appPreferences.urlBackEnd + 'clientesListaPrecio/' + id, data, this.auth.getHeaders())
  }

  deleteClienteLP(id: number): Observable<CliListaPrecioDTO> {
    return this.http.delete<CliListaPrecioDTO>(appPreferences.urlBackEnd + 'clientesListaPrecio/' + id, this.auth.getHeaders())
  }

}


