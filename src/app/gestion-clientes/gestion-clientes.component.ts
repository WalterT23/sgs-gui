import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

// Servicios
import { AuthService } from '../_services/auth.service';
import { UtilsService } from '../_utils/utils.service';
import { ClientesService } from './_service/clientes.service';
import { InfoRefService } from '../info-ref/_services/info-ref.service';
import { ListaPreciosService } from '../gestion-lista-precios/_service/lista-precios.service';
import { SucursalesService } from '../sucursales/_services/sucursales.service';

// DTO
import { ClienteDTO, CliListaPrecioDTO } from './_dto/ClienteDTO';
import { appPreferences } from '../../environments/environment';
import { InfoRefOpcDTO } from '../info-ref/_dto/InfoRefDTO';
import { ListaPreciosDTO } from '../gestion-lista-precios/_dto/ListaPreciosDTO';
import { SucursalesDTO } from '../sucursales/_dto/SucursalesDTO';

import { BusquedaGral } from '../pipes/busqueda.pipe';

declare var $: any;
declare var M: any;

@Component({
  selector: 'app-gestion-clientes',
  templateUrl: './gestion-clientes.component.html',
  styleUrls: ['./gestion-clientes.component.css']
})
export class GestionClientesComponent implements OnInit {

  @Input() facturaVenta: boolean = false;

  public datosCliente: ClienteDTO;
  public listaClientes: ClienteDTO[];
  public clientesLP: CliListaPrecioDTO[];
  public listaEstados: InfoRefOpcDTO[];
  public listasPrecio: ListaPreciosDTO[];
  public listaSucursales: SucursalesDTO[]; //lista de sucursales existentes
  public newAso: CliListaPrecioDTO;
  public updCli: boolean = false;
  public incVencidas: boolean = false;

  regXpag: number = 10; //variable que utiliza la paginación
  p: number = 1; //variable que utiliza la paginación
  search:any;
  searchText = "";
  constructor(

    private cliSrv: ClientesService,
    private infrefSrv: InfoRefService,
    private listaPrecioSrv: ListaPreciosService,
    private sucSrv: SucursalesService,
    public auth: AuthService

  ) {

    this.listaClientes = [];
    this.listaEstados = [];
    this.listaSucursales = [];
    this.clientesLP = [];
    this.listasPrecio = [];
    this.datosCliente = {
      ruc: '',
      razonSocial: '',
      direccion: '',
      telefono: '',
      correo: '',
      fechaRegistro: null,
      fechaModificacion: null,
      idEstado: null,
      estado: '',
      nombreFantasia: ''
    }
    this.newAso = {
      id: null,
      idCliente: null,
      idListaPrecio: null,
      observacion: '',
      fechaInicio: null,
      fechaFin: null,
      idSucursalListaP: null
    }

  }

  ngOnInit() {

    $('select').formSelect();

    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 50);
    $('.modal').modal({
      dismissible: false,
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      },
      onCloseStart: function () {
        $('#btnCancelModal').click();
      }
    });

    this.getLstClientes();

  }

  getLstClientes() {
    this.cliSrv.getClientes().subscribe(
      success => {
        this.listaClientes = success;
      },
      () => { }
    );
  }

  getInfoCliente(id: number) {

    $('select').formSelect();
    this.updCli = true;
    this.cliSrv.getClienteById(id).subscribe(
      success => {
        this.datosCliente = success;
      },
      () => {

      }
    );
    this.infrefSrv.getInfoRefOpcByCodRef('estado').subscribe(
      success => {
        this.listaEstados = success;
      },
      () => { }
    );

  }

  getLstEstados() {
    this.listaEstados = [];
    this.infrefSrv.getInfoRefOpcByCodRef('estado').subscribe(
      success => {
        this.listaEstados = success;
      },
      () => { }
    );
  }

  getLstSucursales() {
    this.sucSrv.getSucursales().subscribe(
      success => {
        this.listaSucursales = success;
      },
      err => { }
    );
  }

  codSucursal(id: number) {
    let auxLst = this.listaSucursales.filter(row => (row.id == id));
    return (auxLst.length === 1) ? auxLst[0].codigoSucursal : '000';
  }
  vigente(fecha: Date){
    let hoy = new Date();
    if (fecha > hoy) {
      return true;
    }else {
      return false;
    }
  }

  getClienteListasPrecios(idCliente: number) {
    this.incVencidas = false;
    this.getListasPrecios();
    this.getLstSucursales();
    this.getInfoCliente(idCliente);
    setTimeout(() => {
      $('.tooltipped').tooltip();
      this.clientesLP.forEach(element => {
        element.idCliente = idCliente;
        console.log('despues de asignarle el id cliente', this.clientesLP)
      });
    }, 50);

    this.cliSrv.getClienteById(idCliente).subscribe(
      success => {
        let hoy = new Date();
        this.clientesLP = success.cliListaPrecio.filter(row => (row.fechaFin > hoy));
        console.log('listasXusuario', this.clientesLP)
      },
      err => {
      }
    );
    
    this.listaPrecioSrv.getListaPrecioVigente().subscribe(
      success => {
        this.listasPrecio = success;
      },
      err => {
      }
    )
  }

  getListasPrecios() {
    this.listaPrecioSrv.getListaPrecio().subscribe(
      success => {
        this.listasPrecio = success;
        this.listasPrecio.forEach(element => {
          if (element.descripcion == null) {
            element.descripcion = '';
          }
        });
        console.log(this.listasPrecio,'abc')
      },
      err => { }
    );
  }

  buscarAsociaciones(idCliente: number) {
    console.log('entroAca', this.incVencidas);
    this.getListasPrecios(); // tiene que llamar al nuevo servicio de asociciones de listas por idcliente
    if (this.incVencidas) {
      this.cliSrv.getClientesLPbyIdCliente(idCliente).subscribe(
        success => {
          this.clientesLP = success;
        }
      )
    } else {
      this.cliSrv.getClientesLPbyIdCliente(idCliente).subscribe(
        success => {
          let hoy = new Date();
          this.clientesLP = success.filter(row => (row.fechaFin > hoy)); 
        }
      )
    }
    console.log(this.clientesLP);
  }

  vencerAsoLP(aso: CliListaPrecioDTO) {
    var seguro = confirm("Esta seguro que desea cerrar la vigencia de la asociación?");
    console.log(aso);
    if (seguro) {
      this.cliSrv.deleteClienteLP(aso.id).subscribe(
        success => {
          M.toast({ html: 'Realizado con exito', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          this.getClienteListasPrecios(aso.idCliente);
          this.getLstClientes();
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      ) 
    }
  }

  nuevaAso(idCliente: number) {
    $('.modal').modal({
      onOpenEnd: function () {
        M.updateTextFields();
        $('select').formSelect();
      }
    });
    this.getListasPrecios();
    this.newAso.idCliente = idCliente;
  }

  guardarAsociacion() {
    console.log(this.newAso);
    let campoNull: boolean = false;
    if (this.newAso.idListaPrecio == null || this.newAso.idCliente == null ) {
      M.toast({ html: 'Debe seleccionar una lista de precios.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
      campoNull = true;
    }
    if (!campoNull) {
      this.cliSrv.createClienteLP(this.newAso).subscribe(
        success => {
          M.toast({ html: 'Lista de Precios asociada correctamente.', displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          $('#modalNuevaAsociacion').modal('close');
          this.getClienteListasPrecios(this.newAso.idCliente);
          this.getLstClientes();
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      );
    }
  }

  resetForm() {
    this.updCli = false;
    this.datosCliente = {
      ruc: '',
      razonSocial: '',
      direccion: '',
      telefono: '',
      correo: '',
      fechaRegistro: null,
      fechaModificacion: null,
      idEstado: null,
      estado: '',
      nombreFantasia: ''
    }
  }

  guardar(forma: NgForm) {

    if (forma.value.ruc == null || forma.value.razonSocial == '' || forma.value.direccion == '') {
      console.log(forma.value);
      M.toast({ html: 'Faltan completar datos del Cliente.', displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
    } else if (this.updCli) {
      let cliente = forma.value;
      this.cliSrv.updateCliente(this.datosCliente.id, cliente).subscribe(
        () => {
          M.toast({ html: "Datos del usuario actualizados correctamente.", displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          $('#modalCliente').modal('close');
          this.getLstClientes();
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      );
    } else {
      let cliente = forma.value;
      this.cliSrv.createCliente(cliente).subscribe(
        () => {
          M.toast({ html: "Cliente creado correctamente.", displayLength: appPreferences.toastOkDuration, classes: 'toastOkColor' });
          $('#modalCliente').modal('close');
          this.getLstClientes();
        },
        err => {
          let msg = err.error;
          M.toast({ html: msg.errorMessage, displayLength: appPreferences.toastErrorDuration, classes: 'toastErrorColor' });
        }
      );
    }
  }

  changePag(pagNumber: number) {
    this.p = pagNumber;
    // this.getLstUsuarios(pagNumber, this.regXpag); p/ paginacion serve site
    setTimeout(() => {
      $('.tooltipped').tooltip();
    }, 100);
  }
}
