export interface ClienteDTO {
    id?: number;
    ruc: string;
    razonSocial: string;
    nombreFantasia:string;
    direccion: string;
    telefono: string;
    correo: string;
    idEstado: number;
    fechaRegistro?: Date;
    fechaModificacion?: Date;
    estado: string;
    cliListaPrecio?: CliListaPrecioDTO[];
}

export interface CliListaPrecioDTO {
    id?: number;
    idCliente?: number;
    idListaPrecio: number;
    observacion?: string;
    fechaInicio: Date;
    fechaFin: Date;
    idSucursalListaP?: number;
}

