$(document).ready(function() {
  
  var animating = false,
      submitPhase1 = 1100,
      submitPhase2 = 400,
      logoutPhase1 = 800,
      $login = $(".login"),
      $app = $(".app");
  
  function ripple(elem, e) {
    $(".ripple").remove();
    var elTop = elem.offset().top,
        elLeft = elem.offset().left,
        x = e.pageX - elLeft,
        y = e.pageY - elTop;
    var $ripple = $("<div class='ripple'></div>");
    $ripple.css({top: y, left: x});
    elem.append($ripple);
  };
  
  $(document).on("click", ".login__submit", function(e) {
    if (animating) return;
    animating = true;
    var that = this;
    ripple($(that), e);
    $(that).addClass("processing");
    setTimeout(function() {
      setTimeout(function() {
        animating = false;
        $(that).removeClass("success processing");
      }, submitPhase2);
    }, submitPhase1);
  });
  
  //M.AutoInit();
  
  $('.fixed-action-btn').floatingActionButton();

  $('.datepicker').datepicker({     // $('.datepicker').datepicker();
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });

});

function ajusteZH(fecha){
  let newFecha = new Date(fecha); 
  let ajuste = new Date().getTimezoneOffset() / 60;
  newFecha.setHours(newFecha.getHours() + ajuste);
  return (newFecha);
}

function changeFormatDate(date) {
  var monthNames = [
    "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
    "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' de ' + monthNames[monthIndex] + ' del ' + year;
}

function swShowFrmFiltro(idFrm, state = null) {
  let frmFiltro = document.getElementById(idFrm);
  if (frmFiltro) {
      let bloqueoPantalla = document.getElementsByClassName('filtro-overlay')[0];

      let estado = 'block';

      if (state == null) {
          if (frmFiltro.getAttribute('style') && (frmFiltro.getAttribute('style').indexOf('block') != -1)) {
              estado = 'none';
          }
      } else {
          estado = state;
          M.updateTextFields();
      }
      frmFiltro.setAttribute('style', 'display: ' + estado);
      bloqueoPantalla.setAttribute('style', 'display: ' + estado);
  } else {
      iconsole.warn("No se encuentra el formulario para mostrar/ocultar");
  }
}

function randomColorFactor() {
  return Math.round(Math.random() * 255);
}

function randomColor(opacity) {
  return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
}


