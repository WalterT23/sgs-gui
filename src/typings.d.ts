/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare var iconsole: any;
declare function swShowFrmFiltro(idFrm: string, state?: string): void
declare var M: any;

declare function ajusteZH(fecha:string): Date ;
declare function changeFormatDate(fecha: Date): string;
declare function randomColor(opacity: any): string;